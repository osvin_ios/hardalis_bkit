//
//  ViewControllerExtensions.swift
//  App411
//
//  Created by osvinuser on 6/16/17.
//  Copyright © 2017 osvinuser. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import Photos
import MobileCoreServices
import Cloudinary
import ObjectMapper

extension UIViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func openImagePickerViewController(sourceType: UIImagePickerControllerSourceType, mediaTypes: [String]) {
        
        if sourceType == .camera {
            
            self.checkPermissionsCamera(sourceType: sourceType, mediaTypes: mediaTypes)
        } else {
            
            self.checkPhotoStatusPhotos(sourceType: sourceType, mediaTypes: mediaTypes)
        }
    }
    
    func checkPermissionsCamera(sourceType: UIImagePickerControllerSourceType, mediaTypes: [String]) {
        
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        
        switch cameraAuthorizationStatus {
            
        case .denied:
            self.alertPromptToAllowCameraAccessViaSetting(accessType: "Camera")
        case .authorized:
            self.openImagePicker(sourceType: sourceType, mediaTypes: mediaTypes)
        case .restricted:
            self.alertPromptToAllowCameraAccessViaSetting(accessType: "Camera")
        case .notDetermined:
            self.openAccessCameraPop(mediaTypes: mediaTypes)
        }
    }
    
    func alertPromptToAllowCameraAccessViaSetting(accessType: String) {
        
        let alert = UIAlertController(title: "Access to \(accessType) is restricted", message: "You need to enable access to \(accessType). Apple Settings > Privacy > \(accessType).", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel) { (alert) -> Void in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
        })
        present(alert, animated: true)
    }
    
    func openImagePicker(sourceType: UIImagePickerControllerSourceType, mediaTypes: [String]) {
        let picker = UIImagePickerController()
        picker.sourceType = sourceType
        picker.delegate = self
        picker.mediaTypes = mediaTypes
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func openAccessCameraPop(mediaTypes: [String]) {
        
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { (granted) in
            
            if(granted){
                self.openImagePicker(sourceType: .camera, mediaTypes: mediaTypes)
            } else {
                self.alertPromptToAllowCameraAccessViaSetting(accessType: "Camera")
            }
        }
    }
    
    func checkPhotoStatusPhotos(sourceType: UIImagePickerControllerSourceType, mediaTypes: [String]) {
        
        let photsAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        switch photsAuthorizationStatus {
            
        case .denied:
            self.alertPromptToAllowCameraAccessViaSetting(accessType: "Library")
        case .authorized:
            self.openImagePicker(sourceType: sourceType, mediaTypes: mediaTypes)
        case .restricted:
            self.alertPromptToAllowCameraAccessViaSetting(accessType: "Library")
        case .notDetermined:
            self.openAccessPhotoLibraryPop(mediaTypes: mediaTypes)
        }
    }
    
    func openAccessPhotoLibraryPop(mediaTypes: [String]) {
        
        PHPhotoLibrary.requestAuthorization({(status:PHAuthorizationStatus)in
            
            switch status {
                
            case .denied:
                self.alertPromptToAllowCameraAccessViaSetting(accessType: "Library")
                break
                
            case .authorized:
                self.openImagePicker(sourceType: .photoLibrary, mediaTypes: mediaTypes)
                break
                
            case .restricted:
                self.alertPromptToAllowCameraAccessViaSetting(accessType: "Library")
                break
                
            case .notDetermined:
                self.alertPromptToAllowCameraAccessViaSetting(accessType: "Library")
                break
            }
        })
    }
    
    func generateThumnail(url : URL) -> UIImage? {
        
        let asset: AVAsset = AVAsset(url: url)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time        : CMTime = CMTimeMake(1, 30)
        let img         : CGImage
        
        do {
            try img = assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let frameImg: UIImage = UIImage(cgImage: img)
            return frameImg
        } catch {
            
        }
        return nil
    }
}

