//
//  ChatMsgVCWebService.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 05/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import Cloudinary
import MediaPlayer
import AVFoundation
import MobileCoreServices
import AVKit
import Alamofire


extension ChatMessageViewController {
   
    // Chat Messsages for Chating
     func chatMessagesAPI() {
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let ChatMessageUrl = Constants.APIs.chatBaseURL+"chatMessages"
        // Group Chat Params
        let user_id = userInfoModel.id as? String
        let to_id = groupChatArray1?.to_id! ?? ""
        let type = groupChatArray1?.type! ?? ""
        let page = post_Count as AnyObject
        // Direct or Single Chat Params
        let post_idS = singleChatArray1?.post_id
        let user_idS = userInfoModel.id as? String
        let to_idS = singleChatArray1?.to_id! ?? ""
        let typeS = singleChatArray1?.type! ?? ""
        var block = String()
        
        let chatDetailUserId = userInfoModel.id as? String
        let chatDetailtoId  = otherUserDetail?.id ?? ""
        
        let notificationToId = chatNotificationArray?.to_id ?? ""
        let notificationType = chatNotificationArray?.type ?? ""
      
        if notificationCheck == true {
            if chatMemberToChat == false {
                self.paramsStr = "user_id=\(chatDetailUserId ?? "")&type=\("Personal")&to_id=\(chatDetailtoId)&page=\(page)"
            }else{
                if comingBtnStatus == true {
                    self.paramsStr = "user_id=\(user_id ?? "")&type=\(type)&to_id=\(to_id)&page=\(page)"
                } else {
                    self.paramsStr = "user_id=\(user_idS ?? "")&type=\(typeS)&to_id=\(to_idS)&page=\(page)"
                }
            }
        } else {
             self.paramsStr = "user_id=\(user_id ?? "")&type=\(notificationType)&to_id=\(notificationToId)&page=\(page)"
        }
        print(paramsStr)
//        Methods.sharedInstance.showLoader(object: self.view)
        WebServiceClass.dataTask(urlName: ChatMessageUrl, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            self.refreshControlTop.endRefreshing()
            if self.post_Count == 1 {
                self.chatGetArray.removeAll()
            }
            if success == true {
                //Methods.sharedInstance.hideLoader(object: self.view)
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    self.refreshControlTop.endRefreshing()
                    if let chatgetVar = Mapper<ChatMessageModel>().map(JSONObject: jsonResult){
                          self.chatGetVariable = chatgetVar
                          print(self.chatGetVariable)
                    }
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool{
                        if responeCode == false {
                            if (jsonResult["message"] as? String) != nil{
                                if let ChatData = jsonResult["result"] as? [Dictionary<String,AnyObject>] {
                                    if self.post_Count == 1{
                                        self.chatGetArray.removeAll()
                                    }
                                    if let getUserChatInfo = Mapper<ChatMessageModel>().mapArray(JSONObject: ChatData) {
                                        if self.post_Count == 1{
                                            self.chatGetArray = getUserChatInfo
                                            print(self.chatGetArray)
                                            //self.chatGetArray.removeAll()
                                            self.chatGetArray = self.chatGetArray.reversed()
                                        }else{
                                            
                                            var tempChatGetArray = [ChatMessageModel]()
                                            tempChatGetArray = getUserChatInfo
                                            tempChatGetArray = tempChatGetArray.reversed()
                                            tempChatGetArray += self.chatGetArray
                                            
                                            self.chatGetArray = tempChatGetArray
                                            print(self.chatGetArray)
                                        }
                                    }
                                }
                                if self.chatGetArray.count == 0 {
                                    self.showToast(message: "No Conversation Found")
                                }
                                
                                if let blockStatus = jsonResult["block"] as? [Dictionary<String,AnyObject>] {
                                    self.blockUnblockStatus = blockStatus
                                    
                                    self.blockStatus = (blockStatus[0]["blocked_status"] as? String)!
                                    self.blockStatus1 = self.blockStatus
                                    if self.blockStatus1 == "1"{
                                        self.bottomView.isHidden = true
                                        self.bottomViewHeight.constant = 0
                                        let label = UILabel(frame: CGRect(x: 0, y: Constants.ScreenSize.SCREEN_HEIGHT - 30, width: self.view.frame.width, height: 40))
                                       // label.center = CGPoint(x: 160, y: 285)
                                        label.textAlignment = .center
                                        label.font =  UIFont(name: "Gotham-Book", size: 14.0)
                                        label.textColor = UIColor.white
                                        label.backgroundColor = UIColor.lightGray
                                        label.text = "Anyone of you has blocked the other one."
                                        self.view.addSubview(label)
                                    }else{
                                        self.bottomView.isHidden = false
                                        self.bottomViewHeight.constant = 60
                                    }
                                }
                            }else{
                                print("something went wrong 1")
                            }
                            //self.tableViewScrollToBottoms(animated: true, milliseconds: 300)
                        }else{
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                                self.showAlert(jsonResult["message"] as? String ?? "")
                            })
                        }
                    } else {
                        print("Worng data found.")
                    }
                }
                DispatchQueue.main.async {
                  self.chatMessageTableView.reloadData()
                    if self.chatGetArray.count > 0 {
                        if self.post_Count == 1{
                            self.tableViewScrollToBottom()
                        }
                    }
                }
            } else {
                Methods.sharedInstance.hideLoader(object: self.view)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                    self.showAlert(errorMsg)
                })
            }
        }
    }
    
    func readMsgAPI() {
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let ChatMessageUrl = Constants.APIs.chatBaseURL+"readMessage"
        let user_id = userInfoModel.id as? String
        let to_id = groupChatArray1?.to_id! ?? ""
        let type = groupChatArray1?.type! ?? ""
        // Direct or Single Chat Params
        let user_idS = userInfoModel.id as? String
        let to_idS = singleChatArray1?.to_id! ?? ""
        let typeS = singleChatArray1?.type! ?? ""
        
        let chatDetailUserId = userInfoModel.id ?? ""
        let chatDetailtoId  = otherUserDetail?.id ?? ""
        
        let notificationToId = chatNotificationArray?.to_id ?? ""
        let notificationType = chatNotificationArray?.type ?? ""
        
        if notificationCheck == true
        {
            if chatMemberToChat == false
            {
                self.paramsStr = "user_id=\(chatDetailUserId ?? "")&type=\("Personal")&to_id=\(chatDetailtoId)"
            }else{
                if comingBtnStatus == true {
                    self.paramsStr = "user_id=\(user_id ?? "")&type=\(type)&to_id=\(to_id)"
                }else{
                    self.paramsStr = "user_id=\(user_idS ?? "")&type=\(typeS)&to_id=\(to_idS)"
                }
            }
        }
        else
        {
            self.paramsStr = "user_id=\(user_id ?? "")&type=\(notificationType)&to_id=\(notificationToId)"
        }
        print(paramsStr)
        
        WebServiceClass.dataTask(urlName: ChatMessageUrl, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        if responeCode == false {
                            if (jsonResult["message"] as? String) != nil {
                                if let readData = jsonResult["result"] as? [Dictionary<String,AnyObject>] {
                                   // self.readMsgDict = readData
                                }
                            } else{
                                print("something went wrong 1")
                            }
                        }
                    }else{
                        print("Something went wrong 2")
                    }
                } else {
                    print("Something went wrong 3")
                }
                DispatchQueue.main.async {
                  
                    //                    self.blockUserButtonOutlet.setNeedsLayout()
                    //                    self.blockUserButtonOutlet.layoutIfNeeded()
                }
            } else {
                print("Somthing went wrong 4")
            }
        }
        
    }
    
    //MARK:- TableView Set To bottom
    
    func tableViewScrollToBottom(){
        let numberOfSections = self.chatMessageTableView.numberOfSections
        let numberOfRows = self.chatMessageTableView.numberOfRows(inSection: numberOfSections-1)
        let indexPath = IndexPath(row: numberOfRows-1 , section: numberOfSections-1)
        self.chatMessageTableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.middle, animated: true)
    }
 
    // MARK:- send Profile Details on Server.
    
    func sendtextMsgDetailsOnServer() {
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let uploadUrl = Constants.APIs.chatBaseURL+"addPost"
        
        // Group Chat Params
        let user_id     = userInfoModel.id as? String
        let post_title = self.typeMessageTextField.text ?? ""
        let to_id = groupChatArray1?.to_id! ?? "'"
        let type = groupChatArray1?.type! ?? ""
        
        // Direct or Single Chat Params
        let to_idS = singleChatArray1?.to_id! ?? ""
        let typeS = singleChatArray1?.type! ?? ""
        let user_idS = userInfoModel.id as? String
        let media_type = self.mediaType = "text"
      
        let chatDetailUserId = userInfoModel.id as? String
        let chatDetailtoId  = otherUserDetail?.id ?? ""
    
        let notificationToId = chatNotificationArray?.to_id ?? ""
        let notificationType = chatNotificationArray?.type ?? ""
        
        if notificationCheck == true {
          
            if chatMemberToChat == false{
                self.paramsStr = "to_id=\(chatDetailtoId)&post_title=\(post_title)&post_media=\("")&user_id=\(chatDetailUserId ?? "")&media_duration=\("0")&media_type=\("text")&type=\("Personal")"
            }else{
                if comingBtnStatus == true {
                    self.paramsStr = "to_id=\(to_id)&post_title=\(post_title)&post_media=\("")&user_id=\(user_id ?? "")&media_duration=\("0")&media_type=\("text")&type=\(type)"
                }else{
                    self.paramsStr = "to_id=\(to_idS)&post_title=\(post_title)&post_media=\("")&user_id=\(user_idS ?? "")&media_duration=\("0")&media_type=\("text")&type=\(typeS)"
                }
            }
            
        }else{
             self.paramsStr = "to_id=\(notificationToId)&post_title=\(post_title)&post_media=\("")&user_id=\(user_idS ?? "")&media_duration=\("0")&media_type=\("text")&type=\(notificationType)"
        }
        
        
        print(paramsStr)
        // Methods.sharedInstance.showLoader(object: self.view)
        WebServiceClass.dataTask(urlName: uploadUrl, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            if success == true {
                //    Methods.sharedInstance.hideLoader(object: self.view)
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        if responeCode == false {
                            if let chatMessage = jsonResult["message"] as? String {
                                if let ChatData = jsonResult["result"] as? Dictionary<String,AnyObject> {
                                    if let userChatInfo = Mapper<AUserInfoModel>().map(JSONObject: ChatData) {
                                        self.userInfoChat = [userChatInfo]
                                        print(self.userInfoChat)
                                    }
                                }
                            } else {
                                print("something went wrong 1")
                            }
                        } else {
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                                self.showAlert(jsonResult["message"] as? String ?? "")
                            })
                        }
                    } else {
                        print("Worng data found.")
                    }
                }
                DispatchQueue.main.async {
                    self.typeMessageTextField.text = ""
                    self.chatMessagesAPI()
                    self.chatMessageTableView.reloadData()
                }
            } else {
                Methods.sharedInstance.hideLoader(object: self.view)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                    self.showAlert(errorMsg)
                })
            }
        }
    }
    
    //MARK:- send Profile Details on Server.
    
    func sendProfileDetailsOnServer(fileURL: URL , mediaduration: Int = 0, textMessage : String) {
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else{
            return
        }
        let uploadUrl = Constants.APIs.chatBaseURL+"addPost"
        // Group Chat Params
        let user_id     = userInfoModel.id as? String
        let post_title = self.typeMessageTextField.text ?? ""
        let to_id = groupChatArray1?.to_id! ?? "'"
        let type = groupChatArray1?.type! ?? ""
        
        // Direct or Single Chat Params
        let to_idS = singleChatArray1?.to_id! ?? ""
        let typeS = singleChatArray1?.type! ?? ""
        let user_idS = userInfoModel.id as? String
        let mediaDurationAfterChage =  self.secondsToHoursMinutesSeconds(seconds: Int(mediaduration))
        let chatDetailUserId = userInfoModel.id ?? ""
        let chatDetailtoId  = otherUserDetail?.id ?? ""
        
        let notificationToId = chatNotificationArray?.to_id ?? ""
        let notificationType = chatNotificationArray?.type ?? ""
        
        if notificationCheck == true {
            if chatMemberToChat == false{
                self.paramsStr = "to_id=\(chatDetailtoId)&post_title=\(post_title)&post_media=\(fileURL)&user_id=\(chatDetailUserId ?? "")&media_duration=\(mediaDurationAfterChage)&media_type=\(mediaType)&type=\("Personal")"
            }else{
                if comingBtnStatus == true {
                    self.paramsStr = "to_id=\(to_id)&post_title=\(post_title)&post_media=\(fileURL)&user_id=\(user_id ?? "")&media_duration=\(mediaDurationAfterChage)&media_type=\(mediaType)&type=\(type)"
                }else{
                    self.paramsStr = "to_id=\(to_idS)&post_title=\(post_title)&post_media=\(fileURL)&user_id=\(user_idS ?? "")&media_duration=\(mediaDurationAfterChage)&media_type=\(mediaType)&type=\(typeS)"
                }
            }
        }else{
             self.paramsStr = "to_id=\(notificationToId)&post_title=\(post_title)&post_media=\(fileURL)&user_id=\(user_idS ?? "")&media_duration=\(mediaDurationAfterChage)&media_type=\(mediaType)&type=\(notificationType)"
        }
     
        print(paramsStr)
       // Methods.sharedInstance.showLoader(object: self.view)
        WebServiceClass.dataTask(urlName: uploadUrl, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            if success == true {
            //    Methods.sharedInstance.hideLoader(object: self.view)
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        if responeCode == false {
                            if let chatMessage = jsonResult["message"] as? String {
                                if let ChatData = jsonResult["result"] as? Dictionary<String,AnyObject> {
                                    if let userChatInfo = Mapper<AUserInfoModel>().map(JSONObject: ChatData) {
                                    self.userInfoChat = [userChatInfo]
                                        print(self.userInfoChat)
                                    }
                                }
                            } else {
                                print("something went wrong 1")
                            }
                        }else{
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                                self.showAlert(jsonResult["message"] as? String ?? "")
                            })
                        }
                    }else{
                        print("Worng data found.")
                    }
                }
                self.previewImage.image = nil
                DispatchQueue.main.async {
                    self.typeMessageTextField.text = ""
                    self.chatMessagesAPI()
                    self.chatMessageTableView.reloadData()
                    self.paramsStr = ""
                    self.mediaType = ""
                }
            } else {
                Methods.sharedInstance.hideLoader(object: self.view)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                    //self.showAlert(errorMsg)
                })
            }
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int){
        return ((seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
//    //MARK: Audio and Video Uploading function
//    internal func audioPostViewController() {
//
//        if let userInfoModel = Methods.sharedInstance.getUserInfoData() {
//
//            if urlAudioGetCloudinary != nil {
//
//                self.uploadAudioFileURL(fileURL: urlAudioGetCloudinary!)
//
//            } else {
//                print("URL is not match")
//            }
//        } else {
//
//            print("user auth token not found")
//        }
//    }
 
    // Video Uploading on Cloudinary Functions
    
    internal func postViewController(){
        if let userInfoModel = Methods.sharedInstance.getUserInfoData() {
            
            if urlGetCloudinary != nil{
                self.uploadVideoOnCloudinaryByURL(fileURL: urlGetCloudinary!)
            }else{
                print("URL is not match")
            }
        }else {
            print("user auth token not found")
        }
    }
    
    //MARK: Compress Video
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = kUTTypeQuickTimeMovie as AVFileType
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    // MARK: - Upload Video After thumbnail
    
    internal func uploadVideoOnCloudinaryByURL(fileURL: URL)
    {
        // Configuration.
        let configuration = CloudinarySingletonClass.sharedInstance.configurationOfCloudinary( resourceType: .video)
        let cloudinary: CLDCloudinary = configuration.cloudinary
        let params: CLDUploadRequestParams = configuration.params
        let progressHud = Methods.sharedInstance.showPercentageLoader(object: self.view)
        progressHud.label.text = "Uploading Video ..."
        cloudinary.createUploader().signedUpload(url: (urlGetCloudinary ?? nil)!, params: params, progress: { (progress) in
            progressHud.progress = Float(progress.fractionCompleted)
        }) { (respone, error) in
            print(respone ?? "Not Found")
            progressHud.hide(animated: true)
            if let cldUploadResult: CLDUploadResult = respone {
                let fileUrls = NSURL(string: cldUploadResult.url ?? "")
                if cldUploadResult.url != nil{
                    self.compressVideoFromCamera(videoURL: fileUrls! as URL)
                    self.sendProfileDetailsOnServer(fileURL: (fileUrls! as URL) as URL,  mediaduration: Int(cldUploadResult.duration ?? 0), textMessage: self.typeMessageTextField.text! )
                }
            }
        }
    }
    
    // Upload Video on Cloudinary
    
    func compressVideoFromCamera(videoURL: URL) {
      //  Methods.sharedInstance.showLoader(object: self.view)
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString )
        compressVideo(inputURL: videoURL, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                Methods.sharedInstance.hideLoader(object: self.view)
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    //MARK: Audio Upload on Cloudinary
//    internal func uploadAudioFileURL(fileURL: URL){
//
//        let configuration = CloudinarySingletonClass.sharedInstance.configurationOfCloudinary( resourceType: .video )
//        let cloudinary: CLDCloudinary = configuration.cloudinary
//        let params: CLDUploadRequestParams = configuration.params
//
//        let progressHud = Methods.sharedInstance.showPercentageLoader(object: self.view)
//        progressHud.label.text = "Uploading Audio ..."
//
//        cloudinary.createUploader().signedUpload(url: (urlAudioGetCloudinary ?? nil)!, params: params, progress: { (progress) in
//            //            print(progress)
//            progressHud.progress = Float(progress.fractionCompleted)
//
//        }) { (respone, error) in
//
//            print(respone ?? "Not Found")
//            progressHud.hide(animated: true)
//           self.addSubView.removeFromSuperview()
//
//            if let cldUploadResult: CLDUploadResult = respone {
//                let fileUrls = NSURL(string: cldUploadResult.url ?? "")
//                if cldUploadResult.url != nil {
//                    self.sendProfileDetailsOnServer(fileURL: fileUrls as! URL, mediaduration: Int(cldUploadResult.duration ?? 0))
//                }
//            }
//        }
//    }
    
    //MARK: Images Upload on Cloudinary
    
    internal func uploadProfilePicName()
    {
        let config = CLDConfiguration(cloudinaryUrl: CLOUDINARY_URL)
        let cloudinary = CLDCloudinary(configuration: config!)
        let params = CLDUploadRequestParams()
        print(params)
        let progressHud = Methods.sharedInstance.showPercentageLoader(object: self.view)
        print(progressHud)
        cloudinary.createUploader().signedUpload(data: UIImageJPEGRepresentation(uploadImageThumnailSize, 1.0 )! , params: params, progress: { (progress) in
            progressHud.progress = Float(progress.fractionCompleted)
        }, completionHandler: { (respone, error) in
            progressHud.hide(animated: true)
            if error != nil {
                self.showAlert(error?.localizedDescription ?? "No Error Found")
            } else {
                print(respone ?? "Not Found")
                if let cldUploadResult: CLDUploadResult = respone {
                    let fileUrls = NSURL(string: cldUploadResult.url ?? "")
                    self.sendProfileDetailsOnServer(fileURL: fileUrls as! URL,  mediaduration: Int(cldUploadResult.duration ?? 0), textMessage: self.typeMessageTextField.text!)
                }
            }
        })
    }
    
    @objc func noOfLikes(_ sender : UIButton) {
        
        if chatGetArray[sender.tag].media_type == "text"{
            let indexPath = IndexPath(row: sender.tag, section: sender.tag)
            let cell = self.chatMessageTableView.cellForRow(at: indexPath) as? ChatAudioTableViewCell
            self.temp = chatGetArray[indexPath.section].total_like_count ?? "0"
            if Int(userInfoModel)! == Int(chatGetArray[indexPath.section].user_id ?? "0")!{
                // Sender View
                if temp == "0" {
                    cell?.likePersonListViewHeightS.constant = 0
                }else{
                    cell?.likePersonListViewHeightS.constant = 60
                }
            }else{
                // Receiver View
                if temp == "0" {
                    cell?.likePersonListViewHeightR.constant = 0
                }else{
                    cell?.likePersonListViewHeightR.constant = 60
                }
            }
        } else {
            let indexPath = IndexPath(row: sender.tag, section: sender.tag)
            let cell = self.chatMessageTableView.cellForRow(at: indexPath) as? VideoTableViewCell
            self.temp = chatGetArray[indexPath.section].total_like_count ?? "0"
            if Int(userInfoModel)! == Int(chatGetArray[indexPath.section].user_id ?? "0")!{
                // Sender View
                if temp == "0" {
                    cell?.likePersonLIstHeight.constant = 0
                }else{
                    cell?.likePersonLIstHeight.constant = 60
                }
            } else {
                // Receiver View
                if temp == "0" {
                    cell?.likePersonListHeightR.constant = 0
                }else{
                    cell?.likePersonListHeightR.constant = 60
                }
            }
        }
    }
    
    // Heart Button Action Like Unlike Button API
    
    @objc func favoriteOrUnFavoriteEventByUser(_ sender: UIButton){
        
        //        let modelObject = self.chatGetArray[selectedIndex]
        guard let userInfo = Methods.sharedInstance.getloginData() else {
            return
        }
        var post_id = String()
        post_id = (chatGetArray[sender.tag].id ?? "0")!
        let url = Constants.APIs.chatBaseURL+"likeChatMessage"
        let paramsStr = "user_id=\(userInfo.id!)&post_id=\(post_id)"
        print(paramsStr)
        WebServiceClass.dataTask(urlName: url, method: "POST", params: paramsStr) { (success, response, errorMsg) in
        if success == true {
            if let jsonResult = response as? Dictionary<String, AnyObject>{
                print(jsonResult)
                // do whatever with jsonResult
                if let responeCode = jsonResult["error"] as? Bool {
                    if responeCode == false {
                        if (jsonResult["message"] as? String) != nil {
                            if let likeData = jsonResult["result"] as? Dictionary<String,AnyObject> {
                                if let likePersonDataInfo = Mapper<ChatMessageModel>().map(JSONObject: likeData) {
                                    self.totalPersonLikeCount = likePersonDataInfo
                                    
                                    if let totalLikeChat = likeData["total_like_count"] as? String{
                                        self.likeCount1 = totalLikeChat
                                       // UserDefaults.setValue(self.likeCount, forKeyPath: "total_like_count")
                                        self.sum  = self.likeCount
                                        self.sum = Int(self.chatGetArray[sender.tag].total_like_count ?? "0")!
                                     
                                     //   UserDefaults.setValue(likeCount, forKey: "total_like_count")
                                        
                                    }
                                if let totalLikeChat = likeData["users"] as? [Dictionary<String, AnyObject>]{
                                    if let likeDataInfo = Mapper<ChatMessageModel>().mapArray(JSONObject: totalLikeChat){
                                        self.likeUnlikePersonDetailArray = likeDataInfo
                                        print(self.likeUnlikePersonDetailArray)
                                        
                                        if self.chatGetArray[sender.tag].is_liked_you == "1"{
                                            self.chatGetArray[sender.tag].is_liked_you = "0"
                                            let a = self.chatGetArray[sender.tag].total_like_count ?? "0"
                                            let b = 1
                                            self.sum = Int(a)! - Int(b)
                                            self.chatGetArray[sender.tag].total_like_count = "\(self.sum)"
                                            UserDefaults.standard.setValue(self.sum, forKeyPath: "total_like_count")
                                            UserDefaults.standard.setValue(0, forKey: "is_liked_you")
                                            
                                        }else{
                                            self.chatGetArray[sender.tag].is_liked_you = "1"
                                            let a = self.chatGetArray[sender.tag].total_like_count ?? "0"
                                            let b = 1
                                            self.sum = Int(a)! + Int(b)
                                            self.chatGetArray[sender.tag].total_like_count = "\(self.sum)"
                                            
                                            UserDefaults.standard.setValue(self.sum, forKeyPath: "total_like_count")
                                            UserDefaults.standard.setValue(1, forKey: "is_liked_you")
                                        }
                                    }
                                }
                            }
                        } else {
                            print("something went wrong 1")
                        }
                    }
                }else{
                    print("Something went wrong 2")
                }
            }else{
                print("Something went wrong 3")
            }
          }else{
             print("Somthing went wrong 4")
          }
        }
         DispatchQueue.main.async {
//            self.chatMessageTableView.reloadData()
         //   self.chatMessagesAPI()
            let indexPath = IndexPath(item: 0, section: sender.tag)
            self.chatMessageTableView.reloadRows(at: [indexPath], with: .fade)
         }
      }
   }
}

extension UITableViewDataSource {
    func lastIndexPath(_ tableView: UITableView) -> IndexPath? {
        guard let sections = self.numberOfSections?(in: tableView) else { return nil }
        for section in stride(from: sections-1, to: 0, by: -1) {
            let rows = self.tableView(tableView, numberOfRowsInSection: section)
            if rows > 0 {
                return IndexPath(row: rows - 1, section: section)
            }
        }
        return nil
    }
}
