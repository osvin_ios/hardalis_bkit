//
//  Methods.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 07/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics
import ObjectMapper
import MBProgressHUD

public class Methods : NSObject{
    
    static let sharedInstance = Methods()
    
    func getloginData() -> MyAccountDetailsModel?{
        let decoded                      = UserDefaults.standard.object(forKey: "userinfo") as! Data
        let userDataStr: String          = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! String
        
        guard let loginModel =  Mapper<MyAccountDetailsModel>().map(JSONString: userDataStr) else{
            return nil
        }
        return loginModel
    }
    
    func hbcuDetailData()  -> HBCUListModel?{
        let decoded                      = UserDefaults.standard.object(forKey: "hbcuInfo") as! Data
        let userDataStr: String          = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! String
        
        guard let hbcuModel =  Mapper<HBCUListModel>().map(JSONString: userDataStr) else {
            return nil
        }
        return hbcuModel
    }

    func showPercentageLoader(object:UIView) -> MBProgressHUD{
        let progressHud = MBProgressHUD.showAdded(to: object, animated: true)
        progressHud.mode = .annularDeterminate
        progressHud.label.text = "Uploading"
      
        return progressHud
    }
    
    func showLoader(object:UIView){
        // Go back to the main thread to update the UI
        DispatchQueue.main.async(execute: {
            MBProgressHUD.showAdded(to: object, animated: true)
        })
    }
    
    func hideLoader(object:UIView){
        // Go back to the main thread to update the UI
        DispatchQueue.main.async(execute: {
            MBProgressHUD.hide(for: object, animated: true)
        })
    }
    
    func getUserInfoData() -> AUserInfoModel?{
        
        let decoded                         = UserDefaults.standard.object(forKey: "userinfo") as! Data
        let userDataStr: String             = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! String
        
        guard let userInfoModel = Mapper<AUserInfoModel>().map(JSONString: userDataStr) else {
            return nil
        }
        return userInfoModel
    }
}
