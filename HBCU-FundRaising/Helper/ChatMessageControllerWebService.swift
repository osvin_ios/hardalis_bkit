//
//  ChatMessageControllerWebService.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 05/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import Cloudinary
import MediaPlayer
import AVFoundation
import MobileCoreServices
import AVKit

extension ChatMessageViewController {
    
    // Chat Messsages for Chating
    internal func chatMessagesAPI() {
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let ChatMessageUrl = "http://phphosting.osvin.net/HBCU/api/chat/chatMessages"

        // Group Chat Params
        let user_id = userInfoModel.id ?? ""
        let to_id = groupChatArray1?.to_id! ?? ""
        let type = groupChatArray1?.type! ?? ""
        let page = "1"
        
        // Direct or Single Chat Params
        let post_idS = singleChatArray1?.post_id
        let user_idS = userInfoModel.id ?? ""
        let to_idS = singleChatArray1?.to_id! ?? ""
        let typeS = singleChatArray1?.type! ?? ""
        
        if comingBtnStatus == true {
            self.paramsStr = "user_id=\(user_id)&type=\(type)&to_id=\(to_id)&pages=\(page)"
        }else{
            self.paramsStr = "user_id=\(user_idS)&type=\(typeS)&to_id=\(to_idS)&page=\(page)"
        }
        
        print(paramsStr)
        
        Methods.sharedInstance.showLoader(object: self.view)
        
        WebServiceClass.dataTask(urlName: ChatMessageUrl, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            
            if success == true {
                
                Methods.sharedInstance.hideLoader(object: self.view)
                
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        
                        if responeCode == false {
                            
                            if let chatMessage = jsonResult["message"] as? String {
                                
                                if let ChatData = jsonResult["result"] as? [Dictionary<String,AnyObject>] {
                                    
                                    if let getUserChatInfo = Mapper<ChatMessageModel>().mapArray(JSONObject: ChatData) {
                                        
                                        self.chatGetArray = getUserChatInfo
                                        print(self.chatGetArray)
                                    }
                                    
                                    self.chatMessageTableView.delegate = self
                                    self.chatMessageTableView.dataSource = self
                                    
                                    self.chatMessageTableView.reloadData()
                                }
                                
                            } else {
                                print("something went wrong 1")
                            }
                        } else {
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                                self.showAlert(jsonResult["message"] as? String ?? "")
                            })
                        }
                    } else {
                        
                        print("Worng data found.")
                    }
                }
            } else {
                
                Methods.sharedInstance.hideLoader(object: self.view)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                    self.showAlert(errorMsg)
                })
            }
        }
    }
    
    
    //MARK:- send Profile Details on Server.
    
    internal func sendProfileDetailsOnServer(fileURL: String, mediaduration: Int = 0) {
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let uploadUrl = "http://phphosting.osvin.net/HBCU/api/chat/addPost"
        
        // Group Chat Params
        let user_id      = userInfoModel.id  ?? ""
        let post_title = groupChatArray1?.post_title
        let to_id = groupChatArray1?.to_id
        let type = groupChatArray1?.type_org
        
        
        // Direct or Single Chat Params
        let post_titleS = singleChatArray1?.post_title
        let to_idS = singleChatArray1?.to_id
        let typeS = singleChatArray1?.type_org
        let user_idS = userInfoModel.id ?? ""
        
        let mediaDurationAfterChage =  self.secondsToHoursMinutesSeconds(seconds: Int(mediaduration))
        
        if comingBtnStatus == true {
            
            self.paramsStr = "to_id=\(to_id)&post_title=\(post_title)&post_media=\(fileURL)&user_id=\(user_id)&media_duration=\(mediaDurationAfterChage)&media_type=\(mediaType)&type=\(type)"
        }else{
            
            self.paramsStr = "to_id=\(to_idS)&post_title=\(post_titleS)&post_media=\(fileURL)&user_id=\(user_idS)&media_duration=\(mediaDurationAfterChage)&media_type=\(mediaType)&type=\(typeS)"
        }
        
        print(paramsStr)
        
        Methods.sharedInstance.showLoader(object: self.view)
        
        WebServiceClass.dataTask(urlName: uploadUrl, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            
            if success == true {
                
                Methods.sharedInstance.hideLoader(object: self.view)
                
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        
                        if responeCode == false {
                            
                            if let chatMessage = jsonResult["message"] as? String {
                                
                                if let ChatData = jsonResult["result"] as? Dictionary<String,AnyObject> {
                                    
                                    if let userChatInfo = Mapper<AUserInfoModel>().map(JSONObject: ChatData) {
                                        
                                        self.userInfoChat = [userChatInfo]
                                        print(self.userInfoChat)
                                    }
                                    self.uploadProfilePicClick()
                                    self.chatMessageTableView.reloadData()
                                    
                                }
                                
                            } else {
                                print("something went wrong 1")
                            }
                        } else {
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                                self.showAlert(jsonResult["message"] as? String ?? "")
                            })
                        }
                    } else {
                        
                        print("Worng data found.")
                    }
                }
                
            } else {
                
                Methods.sharedInstance.hideLoader(object: self.view)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                    self.showAlert(errorMsg)
                })
            }
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int) {
        
        return ((seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    //MARK: Audio and Video Uploading function
    internal func audioPostViewController() {
        
        if let userInfoModel = Methods.sharedInstance.getUserInfoData() {
            
            if urlAudioGetCloudinary != nil {
                
                self.uploadAudioFileURL(fileURL: urlAudioGetCloudinary!)
            } else {
                print("URL is not match")
            }
        } else {
            
            print("user auth token not found")
        }
    }
    
    internal func postViewController() {
        
        if let userInfoModel = Methods.sharedInstance.getUserInfoData() {
            
            if urlGetCloudinary != nil {
                
                self.uploadVideoOnCloudinaryByURL(fileURL: urlGetCloudinary!)
            } else {
                
                print("URL is not match")
            }
        } else {
            
            print("user auth token not found")
        }
    }
    
    //MARK: Compress Video
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = kUTTypeQuickTimeMovie as AVFileType
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
            
        }
    }
    
    // MARK: - Upload Video After thumbnail
    
    internal func uploadVideoOnCloudinaryByURL(fileURL: URL) {
        
        // Configuration.
        
        let configuration = CloudinarySingletonClass.sharedInstance.configurationOfCloudinary( resourceType: .video)
        let cloudinary: CLDCloudinary = configuration.cloudinary
        let params: CLDUploadRequestParams = configuration.params
        
        let progressHud = Methods.sharedInstance.showPercentageLoader(object: self.view)
        progressHud.label.text = "Uploading Video ..."
        
        cloudinary.createUploader().signedUpload(url: (urlGetCloudinary ?? nil)!, params: params, progress: { (progress) in
            
            progressHud.progress = Float(progress.fractionCompleted)
            
        }) { (respone, error) in
            
            print(respone ?? "Not Found")
            progressHud.hide(animated: true)
            
            if let cldUploadResult: CLDUploadResult = respone {
                
                if let url = cldUploadResult.url {
                    
                    self.compressVideoFromCamera(videoURL: self.urlGetCloudinary!)
                    
                    self.sendProfileDetailsOnServer(fileURL: cldUploadResult.url!,  mediaduration: Int(cldUploadResult.duration ?? 0))
                    //                    self.sendProfileDetailsOnServer(authToken: "bbbbbbbb-22721236-2727272", imageName: cldUploadResult.publicId!, imageURL: cldUploadResult.url!)
                }
            }
        }
    }
    
    // Upload Video on Clooudinary
    
    func compressVideoFromCamera(videoURL: URL) {
        
        Methods.sharedInstance.showLoader(object: self.view)
        
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mov")
        compressVideo(inputURL: videoURL, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                Methods.sharedInstance.hideLoader(object: self.view)
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    //MARK: Audio Upload on Cloudinary
    internal func uploadAudioFileURL(fileURL: URL) {
        
        let configuration = CloudinarySingletonClass.sharedInstance.configurationOfCloudinary( resourceType: .auto)
        let cloudinary: CLDCloudinary = configuration.cloudinary
        let params: CLDUploadRequestParams = configuration.params
        
        let progressHud = Methods.sharedInstance.showPercentageLoader(object: self.view)
        progressHud.label.text = "Uploading Audio ..."
        cloudinary.createUploader().signedUpload(url: (urlAudioGetCloudinary ?? nil)!, params: params, progress: { (progress) in
            
            //            print(progress)
            progressHud.progress = Float(progress.fractionCompleted)
            
        }) { (respone, error) in
            
            print(respone ?? "Not Found")
            progressHud.hide(animated: true)
            
            if let cldUploadResult: CLDUploadResult = respone {
                
                if let url = cldUploadResult.url {
                    
                    //                     self.sendProfileDetailsOnServer(authToken: "bbbbbbbb-22721236-2727272", imageName: cldUploadResult.publicId!, imageURL: cldUploadResult.url!)
                    
                    self.sendProfileDetailsOnServer(fileURL: cldUploadResult.url!, mediaduration: Int(cldUploadResult.duration ?? 0))
                }
            }
        }
    }
    
    //MARK: Images Upload on Cloudinary
    
    internal func uploadProfilePicName() {
        
        let config = CLDConfiguration(cloudinaryUrl: CLOUDINARY_URL)
        let cloudinary = CLDCloudinary(configuration: config!)
        
        let params = CLDUploadRequestParams()
        // params.setTransformation(CLDTransformation().setGravity(.northWest))
        // params.setPublicId(imageName)
        
        let progressHud = Methods.sharedInstance.showPercentageLoader(object: self.view)
        
        cloudinary.createUploader().signedUpload(data: UIImageJPEGRepresentation(imageThumbnailSize, 1.0)! , params: params, progress: { (progress) in
            progressHud.progress = Float(progress.fractionCompleted)
            
        }, completionHandler: { (respone, error) in
            progressHud.hide(animated: true)
            
            if error != nil {
                
                self.showAlert(error?.localizedDescription ?? "No Error Found")
            } else {
                
                print(respone ?? "Not Found")
                if let cldUploadResult: CLDUploadResult = respone {
                    
                    let decoded                         = UserDefaults.standard.object(forKey: "userinfo") as! Data
                    let userDataStr: String             = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! String
                    
                    if let userInfoObj = Mapper<AUserInfoModel>().map(JSONString: userDataStr) {
                        
                        //                                                 self.sendProfileDetailsOnServer(authToken: "bbbbbbbb-22721236-2727272", imageName: cldUploadResult.publicId!, imageURL: cldUploadResult.url!)
                        
                        //                            self.sendProfileDetailsOnServer(fileURL: cldUploadResult.url!)
                    }
                }
            }
        })
    }
}
