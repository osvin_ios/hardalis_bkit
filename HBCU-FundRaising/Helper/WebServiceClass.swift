//
//  WebServiceClass.swift
//  ConstruPlaza
//
//  Created by osvinuser on 10/28/16.
//  Copyright © 2016 osvinuser. All rights reserved.

import Foundation
import UIKit
import MBProgressHUD
import Alamofire
import Cloudinary

var googlePlaceID: String?
//var imageFullSize : UIImage?
//var imageThumbnailSize : UIImage?
var videoURL : URL?
var videoData: Data?

class WebServiceClass {
    
    static let sharedInstance = Methods()
    
    func showPercentageLoader(object:UIView) -> MBProgressHUD {
        
        let progressHud = MBProgressHUD.showAdded(to: object, animated: true)
        progressHud.mode = .annularDeterminate
        progressHud.label.text = "Uploading"
        
        return progressHud
    }
    
    //MARK:- Data task
//    static func dataTask(urlName: String, method: String, params: String, completion: @escaping ( _ success: Bool,  _ object: Any, _ errorMsg: String) -> ()) {
//
//        let urlString: URL = URL(string: urlName)!
//
//        //let body = self.createMultiPartBody(parameters: params as [[String : AnyObject]])
//        print("API Name:- \(urlString) Get body Data: \(params)")
//
//        let request = NSMutableURLRequest(url: urlString,
//                                          cachePolicy: .useProtocolCachePolicy,
//                                          timeoutInterval: 60.0)
//        request.httpMethod = method
//
//        request.addValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
//
//        request.setValue("application/json", forHTTPHeaderField: "Accept")
//
//        // request.allHTTPHeaderFields = self.requestHeaders()
//
//        request.httpBody = params.data(using: String.Encoding.utf8)
//
//        let session = URLSession.shared
//
//        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//
//            DispatchQueue.main.async(execute: {
//
//                print(data ?? "No Data found")
//                print(response ?? "No Data found")
//                print(error?.localizedDescription ?? "No Data found")
//
//                if (error != nil) {
//
//                    //print(error?.localizedDescription ?? "error details not found")
//                    //print(error ?? "error not found")
//
//                    completion(false, "", AKErrorHandler.CommonErrorMessages.UNKNOWN_ERROR_FROM_SERVER)
//
//                } else {
//
//                    if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode  {
//
//                        if response.statusCode == 201 || response.statusCode == 200 {
//
//                            // Check Data
//                            if let data = data {
//
//                                let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
//                                print(jsonString)
//
//                                // Json Response
//                                if let jsonResponse = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) {
//
//                                    completion(true, jsonResponse, "")
//
//                                } else {
//
//                                    completion(false, "", AKErrorHandler.CommonErrorMessages.INTERNAL_SERVER_ERROR)
//
//                                }
//
//                            } else {
//                                completion(false, "", AKErrorHandler.CommonErrorMessages.INTERNAL_SERVER_ERROR)
//                            }
//
//                        } else {
//                            completion(false, "", AKErrorHandler.CommonErrorMessages.INTERNAL_SERVER_ERROR)
//                        }
//
//                    } else {
//
//                        completion(false, "", AKErrorHandler.CommonErrorMessages.UNKNOWN_ERROR_FROM_SERVER)
//                    }
//                }
//            })
//        })
//        dataTask.resume()
//    }

    //MARK:- Data task
    static func dataTask(urlName: String, method: String, params: String, completion: @escaping (_ success: Bool, _ object: Any, _ errorMsg: String) -> ()) {

        let urlString: URL = URL(string: urlName)!

        //let body = self.createMultiPartBody(parameters: params as [[String : AnyObject]])
        print("API Name:- \(urlString) Get body Data: \(params)")

        let request = NSMutableURLRequest(url: urlString,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 60.0)
        request.httpMethod = method

        request.addValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")

        request.setValue("application/json", forHTTPHeaderField: "Accept")

        // request.allHTTPHeaderFields = self.requestHeaders()

        request.httpBody = params.data(using: String.Encoding.utf8)

        let session = URLSession.shared

        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in

            DispatchQueue.main.async(execute: {

                print(data ?? "No Data found")
                print(response ?? "No Data found")
                print(error?.localizedDescription ?? "No Data found")

                if (error != nil) {
                    //print(error?.localizedDescription ?? "error details not found")
                    //print(error ?? "error not found")

                    completion(false, "", AKErrorHandler.CommonErrorMessages.UNKNOWN_ERROR_FROM_SERVER)
                } else {

                    if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode  {

                        if response.statusCode == 201 || response.statusCode == 200 {

                            // Check Data
                            if let data = data {

                                let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
                                print(jsonString)

                                // Json Response
                                if let jsonResponse = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) {

                                    completion(true, jsonResponse, "")

                                } else {

                                    completion(false, "", AKErrorHandler.CommonErrorMessages.INTERNAL_SERVER_ERROR)

                                }

                            } else {
                                completion(false, "", AKErrorHandler.CommonErrorMessages.INTERNAL_SERVER_ERROR)
                            }
                        } else {
                            completion(false, "", AKErrorHandler.CommonErrorMessages.INTERNAL_SERVER_ERROR)
                        }
                    } else {
                        completion(false, "", AKErrorHandler.CommonErrorMessages.UNKNOWN_ERROR_FROM_SERVER)
                    }
                }
            })
        })
        dataTask.resume()
    }
    
//    static func requestHeaders() -> [String: String] {
//
//        let headers = [
//            "content-type": "Content-Type: application/json",
//            "cache-control": "no-cache"
//        ]
//        print(headers)
//
//        return headers
//
//    }

    static func hideLoader(view: UIView) {
        DispatchQueue.main.async(execute: {
            MBProgressHUD.hide(for: view, animated: true)
        })
    }

    static func showLoader(view: UIView) {
        DispatchQueue.main.async(execute: {
            MBProgressHUD.showAdded(to: view, animated: true)
        })
    }
    
    static func uploadeImageToServer(urlName: String, params: Dictionary<String, AnyObject>, uploadedImage: [UIImage]?, imageName: String?, isMultiple: Bool = false, completion: @escaping (_ success: Bool, _ object: Any, _ errorMsg: String) -> ()) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            var i = 0
            
            if let imgeArray = uploadedImage {
                for userImage in imgeArray {
                    i += 1
                    if let imageData = UIImageJPEGRepresentation(userImage , 0.8) {
                        let r = arc4random()
                        let str = "profile_pic"+String(r)+".jpg"
                        
                        var parameterName = String()
                        
                        if let imageNameValue = imageName {
                            if isMultiple {
                                parameterName = imageNameValue + "\(i)"
                            } else {
                                parameterName = imageNameValue
                            }
                        }
                        
                        multipartFormData.append(imageData, withName:parameterName, fileName:str, mimeType: "image/jpg")
                    }
                }
            }
            
            
            for (key, value) in params {
                let stringValue = String(describing: value)
                multipartFormData.append(stringValue.data(using: String.Encoding.utf8)!, withName: key )
            }
        }
            , to: urlName,encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        let Json = (response.result.value as AnyObject?)
                        print(Json ?? "Not Found")
                        if let httpStatus = response.response , httpStatus.statusCode == 200 {
                            if response.result.isSuccess {
                                print(response)
                                
                                completion(true, Json ?? [:], "")
                                
                            }  else {
                                
                                completion(false, Json ?? [:], "")
                                
                            }
                        }}
                case .failure(let encodingError):
                    print(encodingError)
                    completion(false, [:], encodingError.localizedDescription)
                    
                }})
    }
    
    //upload json object
    static func uploadArrayToServer(urlName: String, array:Dictionary<String, AnyObject>, completion: @escaping ( _ success: Bool,  _ object: Any, _ errorMsg: String) -> ()) {
        
        var request = URLRequest(url: try! urlName.asURL())
        request.httpMethod = "POST"
        request.httpBody = try! JSONSerialization.data(withJSONObject: array)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        //now just use the request with Alamofire
        
        Alamofire.request(request).responseJSON { response in
            
            switch (response.result) {
            case .success:
                
                let Json = (response.result.value as AnyObject?)
                print(Json ?? "Not Found")
                if let httpStatus = response.response , httpStatus.statusCode == 201 || httpStatus.statusCode == 200 {
                    if response.result.isSuccess {
                        
                        completion(true, Json ?? [:], "")
                        
                    }  else {
                        
                        completion(false, Json ?? [:], "")
                        
                    }
                }
            case .failure(let error):
                
                print(error.localizedDescription)
                completion(false, [:], error.localizedDescription)
            }
        }
    }
    
    
   
}



