//
//  Constants.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 05/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import UIKit

enum GradientDirection {
    
    case leftToRight
    case rightToLeft
    case topToBottom
    case bottomToTop
}

extension UIView {
    
    func gradientBackground(from color1: UIColor, mid color2: UIColor, to color3: UIColor, direction: GradientDirection) {
        
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [color1.cgColor, color2.cgColor, color3.cgColor]
        gradient.locations = [0.5, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1.0)

        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func borderLayer(){
        
    }
}

extension UIColor {
    
    convenience init(rgb: UInt, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: a
        )
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red)
            / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
}

struct Constants {
    
    struct APIs {
        
        static let baseURL         = "http://admin.iheartmyhbcu.org/api/User/"  //Live
        //static let baseURL         = "http://phphosting.osvin.net/HBCU/api/User/"
                
        static let chatBaseURL     = "http://admin.iheartmyhbcu.org/api/chat/"  //Live
        //static let chatBaseURL     = "http://phphosting.osvin.net/HBCU/api/chat/"
        
        static let imageBaseURL    = "http://admin.iheartmyhbcu.org/"   //Live
        //static let imageBaseURL    = "http://phphosting.osvin.net/HBCU/"
        
        static let challengeBaseURL = "http://admin.iheartmyhbcu.org/api/challenges/"   //Live
        //static let challengeBaseURL = "http://phphosting.osvin.net/HBCU/api/challenges/"
        
        static let plaidKeyWithAnd  = "c74afb41cee71cd888636cc90426fb&"
        
        static let plaidKey         = "c74afb41cee71cd888636cc90426fb"
        
        static let env             = "env=sandbox&"
        //static let env             = "env=development&"

        static let openBankListURL = "https://cdn.plaid.com/link/v2/stable/link.html?key="
        
        //static let clientName      =  "clientName=HBCU"
        static let clientName      =  "clientName=HBCU Fundraising"
        
        static let product         = "product=auth&"
        
        static let apiVersion      = "apiVersion=v2&"
        
        static let forgotPassword      = "forgotpassword"
        
        static let forgotPinByUser     = "forgotPin"
        
        static let listusers           = "listusers"
        
        static let listorgs            = "listorgs"
        
        static let imageUpload          = "imageUpload"
        
        static let blocked_users      = "blocked_users"
        
        static let block_unBlock_User = "block_unBlock_User"

        static let likeChatMessage = "likeChatMessage"
        
        static let likePersonList = "likePersonList"
        
        static let mutenotification = "mutenotification"
        
        static let linkedcards  =  "linkedcards"
        
        static let otherUserProfile = "otherUserProfile"
        
        static let allMembers = "allMembers"
        
        //Challenges
        static let getChallenges  = "getChallenges"
        
        static let getBonusChallenges  = "getBonusChallenges"
        
        static let getCollegeChallenges  = "getCollegeChallenges"
        
        static let challengeDetails = "challengeDetails"
        
        static let awardedhbcu = "awardedhbcu"
        
        static let allchallengeDetails = "allchallengeDetails"
        
        static let challengesProof = "challengesProof"
        
        static let editProof = "editProof"
        
        static let challengeDonation = "challengeDonation"
        
        //ForApplePay
        static let paymentsDoneWithAppleI = "paymentsDoneWithAppleI"
        
    }
    
//    struct AppConstants {
//
//        static let appDelegete = UIApplication.shared.delegate as! AppDelegate
//
//    }
    
    struct commomInternetmsz {
        
        static let checkInternet = "No Internet Available"
    }
    
    struct appTitle {
        
        static let alertTitle        = "I ❤️  My HBCU"
    }
    
    struct screen_type {
        static let MILLION_DOLLAR_CHALLENGE = "0"
        static let BONUS_CHALLENGE = "1"
        static let ACTION_CHALLENGE = "2"
    }
    
    struct ScreenSize {
        
        static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE =  UIDevice.current.userInterfaceIdiom == .phone
        static let IS_IPAD =  UIDevice.current.userInterfaceIdiom == .pad
    }
    
    static let ApplePayMerchantID = "merchant.HBCU"
    
}

struct AppConstants {
    
    static let appDelegete = UIApplication.shared.delegate as! AppDelegate
    
}

struct colorsForApplication {
    
    static let grayHeaderColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
    static let appRedColor = UInt(0x40B648)
    static let appBlackColor = UInt(0x050608)
    static let appGreenColor = UInt(0x050608)

    static let greenColor = UInt(0x2ECC71)
}

enum LoginCase : Int {
    case Facebook = 1
    case Email
}

enum SelectProfileFrom : Int {
    case SignUp = 1
    case EditProfile
}

enum SelectHBCUEnum : Int {
    case oneTime = 1
    case recuuring
    case SelectHBCU
    case SelectFromReocuuring
    case SelectFormDonation
    case ComingFromHBCUDonation
    case ChallengeDonation
}

enum PinController : Int {
    case generatePin = 1
    case changePin
}

enum Comment : Int {
    case viewComments = 1
    case viewFav
    case viewDonation
}

struct CallBackMethods {
    
     static let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    typealias SourceCompletionHandler = (_ success:AnyObject) -> () // for success case
}
