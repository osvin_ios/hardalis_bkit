//
//  ExploreServices.swift
//  App411
//
//  Created by osvinuser on 8/18/17.
//  Copyright © 2017 osvinuser. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import Cloudinary


extension ChatMessageViewController {
    
    
    // MARK: - Get Store Data List
    
    internal func getStoresDataList() {
    
        // Get current user id.
        guard let userInfoModel = Methods.sharedInstance.getUserInfoData() else {
            return
        }
        
    
    // MARK: - Upload Video By URL on Cloudinary
    
    func uploadVideoOnCloudinaryByURL(APIUrl: String, authToken: String, publicID: String, fileURL: URL, isVideo: String, view: UIView) {
        
        // Configuration.
        let configuration = CloudinarySingletonClass.sharedInstance.configurationOfCloudinary(resourceType: .auto)
        let cloudinary: CLDCloudinary = configuration.cloudinary
        let params: CLDUploadRequestParams = configuration.params
        
        cloudinary.createUploader().signedUpload(url: fileURL, params: params, progress: { (progress) in
            
            print(progress)
            
        }) { (respone, error) in
            
            print(respone ?? "Not Found")
            
            if error != nil {
                
                Methods.sharedInstance.hideLoader(object: self.view)
                self.showAlert(error?.localizedDescription ?? "No Error Found")
                
            } else {
                
                if let cldUploadResult: CLDUploadResult = respone {
                    
                    if let url = cldUploadResult.url {
                        
                        // Upload Data on our server.
//                            self.createStoreOnExplore(postDataPublicID: publicID, postDataURL: url, view: view)
                        
                        }
                    
                    }
                
                }
            }
        
        }
    }
}
