//
//  Singleton.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 27/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import CoreLocation

// MARK: - Singleton
final class Singleton {
    
    // Can't init is singleton
    private init() { }
    
    // MARK: Shared Instance
    static let sharedInstance = Singleton()
    
    var filterDistance : String!
    
    // MARK: UserCurrent Location
    var userCurrentLocation: CLLocationCoordinate2D!
    
    
    func getDateFormatterFromServer(stringDate: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date = dateFormatter.date(from: stringDate)
        return date
    }
}
