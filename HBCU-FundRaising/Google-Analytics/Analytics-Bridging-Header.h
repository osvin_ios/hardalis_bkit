//
//  Analytics-Bridging-Header.h
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 06/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

#ifndef Analytics_Bridging_Header_h
#define Analytics_Bridging_Header_h

#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <GoogleAnalytics/GAIFields.h>


#endif /* Analytics_Bridging_Header_h */
