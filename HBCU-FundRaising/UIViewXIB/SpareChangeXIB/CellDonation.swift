//
//  CellDonation.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 27/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class CellDonation: UITableViewCell {

    @IBOutlet weak var cellBorder: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
