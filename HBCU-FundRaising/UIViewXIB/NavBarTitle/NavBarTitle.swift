//
//  NavBarTitle.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 06/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class NavBarTitle: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib ()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let view = UINib(nibName: "View1", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }
    
}
