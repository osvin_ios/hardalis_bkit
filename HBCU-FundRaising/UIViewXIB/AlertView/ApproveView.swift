//
//  ApproveView.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 29/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import UIKit

class ApproveView: UIView {
    
 
    
    @IBAction func ApproveAction(_ sender: UIButton) {
        
    }
    
    @IBAction func unApproveAction(_ sender: UIButton) {
        
    }
    
    //MARK:- View Start
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let viewheader = UINib(nibName: "ApproveView" , bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        viewheader.frame = bounds
        viewheader.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(viewheader)
        
    }
}

