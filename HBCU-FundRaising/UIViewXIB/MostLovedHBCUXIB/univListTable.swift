//
//  univListTable.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 22/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class univListTable: UITableViewCell {

    @IBOutlet weak var goToCommentsAction: UIButton!
    @IBOutlet weak var univDonationAmount: UILabel!
    @IBOutlet weak var univImages: UIImageView!
    @IBOutlet weak var univName: UILabel!
    @IBOutlet weak var cellPlusButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var donateButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
