//
//  MostLovedHBCUHeader.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 22/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class MostLovedHBCUHeader: UIView {

    @IBOutlet weak var labelDef: UILabel!
    @IBOutlet weak var totalAmountDonation: UILabel!
    @IBOutlet weak var selectYear: ButtonDesign!
    @IBOutlet weak var selectMonth: ButtonDesign!
    @IBOutlet weak var showMonthYearLabel: UILabel!
    
    //    MARK:- view life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib ()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let view = UINib(nibName: "MostLovedHBCUHeader", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
    }

}
