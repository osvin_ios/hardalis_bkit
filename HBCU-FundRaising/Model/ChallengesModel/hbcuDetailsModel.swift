//
//  hbcuDetailsModel.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 21/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class hbcuDetailsModel: Mappable {
    
    var sum: String?
    var hbcu: String?
    
    var id: String?
    var logo: String?
    var image: String?
    var title: String?
    var created: String?
    var amount: String?
    var level: Int?
    
    var reward: String?
    var type: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    open func mapping(map: Map) {
        
        sum              <- map["sum"]
        hbcu             <- map["hbcu"]
        
        id               <- map["id"]
        logo             <- map["logo"]
        image            <- map["image"]
        title            <- map["title"]
        created          <- map["created"]
        amount           <- map["amount"]
        level            <- map["level"]
        
        reward           <- map["reward"]
        type             <- map["type"]
        
    }
}
