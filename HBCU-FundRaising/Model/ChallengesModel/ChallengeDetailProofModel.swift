//
//  ChallengeDetailProofModel.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 25/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class ChallengeDetailProofModel: Mappable {
    
    var id: String?
    var challenge_id: String?
    var user_id: String?
    var photo: String?
    var phone: String?
    var caption: String?
    var description: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    open func mapping(map: Map) {
     
        id               <- map["id"]
        challenge_id     <- map["challenge_id"]
        user_id          <- map["user_id"]
        photo            <- map["photo"]
        phone            <- map["phone"]
        caption          <- map["caption"]
        description      <- map["description"]
       
    }
}
