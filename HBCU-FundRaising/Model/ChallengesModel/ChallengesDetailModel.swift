//
//  ChallengesDetailModel.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 12/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class ChallengesDetailModel: Mappable {
    
    var id: String?
    var name: String?
    var type: String?
    var termsConditions: String?
    var image: String?
    var about: String?
    var description: String?
    var top_banner: String?
    var startDate: String?
    var endDate: String?
    var dateCreated: String?
    var amount: String?
    var bonusCount: Int?
    var collegeCount: Int?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    open func mapping(map: Map) {
        
        id               <- map["id"]
        name             <- map["name"]
        type             <- map["type"]
        termsConditions  <- map["terms_and_conditions"]
        image            <- map["image"]
        about            <- map["about"]
        description      <- map["description"]
        top_banner       <- map["top_banner"]
        startDate        <- map["start_date"]
        endDate          <- map["end_date"]
        dateCreated      <- map["date_created"]
        amount           <- map["amount"]
        bonusCount       <- map["bonus"]
        collegeCount     <- map["college"]
    }
}
