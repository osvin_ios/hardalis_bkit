//
//  BlockedUserListModel.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 21/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class BlockedUserListModel : Mappable {
    
    // Block User List
    var to_id: String?
    var first_name: String?
    var last_name : String?
    var profile : String?
    var blocked_status : Int?
    
    // Block Unblock User
    var error: String?
    var message: String?
 
    required init?(map: Map) {
        
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        to_id           <-  map["to_id"]
        first_name      <- map["first_name"]
        last_name       <- map["last_name"]
        profile         <- map["profile"]
        
        error           <-  map["error"]
        message         <- map["message"]
        blocked_status  <- map["blocked_status"]
    }
}


