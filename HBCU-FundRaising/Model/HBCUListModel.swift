//
//  HBCUListModel.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 07/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class HBCUListModel: Mappable {
    
    var id              : String?
    var logo            : String?
    var image           : String?
    var title           : String?
    var created         : String?
    var selectedHBCU    : String?
    var donation_percent : String?
    var hbcu             : String?
    var hbcuType         : String?
    var donation_amount  : Any?
    
    required init?(map: Map) {
        
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        
        id               <- map["id"]
        logo             <- map["logo"]
        image            <- map["image"]
        title            <- map["title"]
        created          <- map["created"]
        selectedHBCU     <- map["selectedHBCU"]
        donation_percent <- map["donation_percent"]
        hbcu             <- map["hbcu"]
        hbcuType         <- map["hbcuType"]
        donation_amount  <- map["donation_amount"]
    }
}
