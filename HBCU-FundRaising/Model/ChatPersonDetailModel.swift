//
//  ChatPersonDetailModel.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 25/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class ChatPersonDetailModel : Mappable {
    
    // Mute Notification

    var total_donation: String?
    var total_post_count: String?
    var total_favourite : String?
    var notification_status : String?
    var total_donatedby: String?
    var logo: String?
    var title : String?
    var image: String?

    required init?(map: Map) {
        
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        
        // Mute Notification
        total_donation           <-  map["total_donation"]
        total_post_count         <- map["total_post_count"]
        total_favourite          <- map["total_favourite"]
        notification_status      <- map["notification_status"]
        total_donatedby          <-  map["total_donatedby"]
        logo                     <- map["logo"]
        title                    <- map["title"]
        image                    <-  map["image"]
   
    }
}


