//
//  otherUserProfileModel.swift.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 26/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class otherUserProfileModel : Mappable {
    
    // other user profile
    var id: String?
    var first_name: String?
    var last_name : String?
    var profile : String?
    var blocked_status: String?
    var logo: String?
    var image : String?
    var title : String?
    var created: String?
    var hbcu_id: String?
    var org : String?
    var sum: String?
    var hbcu: String?
    var medal: String?
    var type : String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        // other user profile
        id                     <-  map["id"]
        first_name             <- map["first_name"]
        last_name              <- map["last_name"]
        profile                <- map["profile"]
        blocked_status         <-  map["blocked_status"]
        hbcu                   <- map["hbcu"]
        medal                  <- map["medal"]
        org                    <- map["org"]
        logo                   <- map["logo"]
        image                  <- map["image"]
        title                  <- map["title"]
        created                <-  map["created"]
        hbcu_id                <-  map["hbcu_id"]
        sum                    <-  map["sum"]
        type                   <- map["type"]
    }
}

