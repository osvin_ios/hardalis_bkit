//
//  TrophiesAndMedalModel.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 03/10/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class TrophiesAndMedalModel: Mappable {

    var id: String?
    var sum: String?
    var hbcu: String?
    var logo: String?
    var image: String?
    var title: String?
    var created: String?
    var medal: String?
    var type: String?

    required init?(map: Map) {
        mapping(map: map)
    }
    
    open func mapping(map: Map) {
        id               <- map["id"]
        sum             <- map["sum"]
        hbcu             <- map["hbcu"]
        logo             <- map["logo"]
        image            <- map["image"]
        title            <- map["title"]
        created      <- map["created"]
        medal       <- map["medal"]
        type        <- map["type"]
  
    }
}
