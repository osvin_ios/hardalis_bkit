//
//  RecurringModel.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 27/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class RecurringModel : Mappable {
    
    var id                 : String?
    var isActive           : String?
    var amount             : String?
    var hbcu_id            : String?
    var card_id            : String?
    var cycle              : String?
    var image              : String?
    var reoccuringid       : String?
    var title              : String?
    var logo               : String?
    
    required init?(map: Map) {
        
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        
        id               <- map["id"]
        isActive         <- map["isActive"]
        amount           <- map["amount"]
        hbcu_id          <- map["hbcu_id"]
        card_id          <- map["card_id"]
        cycle            <- map["spare_change"]
        image            <- map["image"]
        reoccuringid     <- map["reoccuringid"]
        title            <- map["title"]
        logo             <- map["logo"]
    }
}

