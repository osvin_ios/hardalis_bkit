//
//  HBCUOrganizationListModel.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 07/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class HBCUOrganizationListModel: Mappable {
    
    var id              : String?
    var logo            : String?
    var title           : String?
    var created         : String?
    var selectedOrganization    : String?
    var donation_amount  : Any?
    
    required init?(map: Map) {
        
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        
        id                   <- map["id"]
        logo                 <- map["logo"]
        title                <- map["title"]
        created              <- map["created"]
        selectedOrganization <- map["selectedOrganization"]
        donation_amount      <- map["donation_amount"]
    }
}
