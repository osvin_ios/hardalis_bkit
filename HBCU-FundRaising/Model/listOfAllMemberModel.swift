//
//  listOfAllMemberModel.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 26/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class  listOfAllMemberModel : Mappable {
    
    // other user Organization Detail
    var user_id: String?
    var name: String?
    var image : String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        // other user Organization Detail
        user_id                     <-  map["user_id"]
        name                        <- map["name"]
        image                       <- map["image"]
    }
}
