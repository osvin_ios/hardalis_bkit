//
//  AUserInfoModel.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 27/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class AUserInfoModel: Mappable {

    var error: String?
    var message: String?
    var result : String?
    var to_id : String?
    var post_title: String?
    var post_media: String?
    var user_id: Any?
    var type: String?
    var media_type: String?
    var media_duration: String?
    var date_created: String?
    
    required init?(map: Map) {
        
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        
        error               <- map["error"]
        message             <- map["message"]
        result                <- map["result"]
        to_id               <- map["to_id"]
        post_title          <- map["post_title"]
        post_media          <- map["post_media"]
        user_id             <- map["user_id"]
        type                <- map["type"]
        media_type          <- map["media_type"]
        media_duration      <- map["media_duration"]
        date_created        <- map["date_created"]
    }
}
