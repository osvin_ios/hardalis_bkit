//
//  ChatMessageModel.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 04/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class ChatMessageModel: Mappable {

    
    var error: String?
    var message: String?
    var result : String?
    var id     : String?
    var to_id : String?
    var post_title: String?
    var post_media: String?
    var user_id: String?
    var type: String?
    var media_type: String?
    var media_duration: String?
    var date_created: String?
    var name : String?
    var is_liked_you : String?
    var total_like_count : String?
    var image : String?
    var action : String?
    var body  : String?
    var badge : Int?
    var groupImage : String?
    var groupName : String?
    
//     var user_id : Any?
//    var liked_person_list = [likedPersonDetail]()
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        
        error               <- map["error"]
        message             <- map["message"]
        result              <- map["result"]
        id                  <- map["id"]
        name                <- map["name"]
        is_liked_you        <- map["is_liked_you"]
        total_like_count    <- map["total_like_count"]
        to_id               <- map["to_id"]
        post_title          <- map["post_title"]
        post_media          <- map["post_media"]
        user_id             <- map["user_id"]
        type                <- map["type"]
        media_type          <- map["media_type"]
        media_duration      <- map["media_duration"]
        date_created        <- map["date_created"]
        image               <- map["image"]
        action              <- map["action"]
        body                <- map["body"]
        badge               <- map["badge"]
        groupImage          <- map["groupImage"]
        groupName           <- map["groupName"]
//        liked_person_list   <- map["liked_person_list"]
//         user_id  <-  map["user_id"]
    }
}

//class likedPersonDetail : Mappable {
//
//    var user_id : Any?
//    var name: String?
//    var image : String?
//    var total_like_count : Int?
//    var message : String?
//
//    required init?(map: Map) {
//        mapping(map: map)
//    }
//
//    //Mapple:-
//    open func mapping(map: Map) {
//        user_id  <-  map["user_id"]
//        name     <- map["name"]
//        image    <- map["image"]
//        total_like_count <- map["total_like_count"]
//        message <- map["message"]
//    }
//}

