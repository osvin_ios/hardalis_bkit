//
//  CommentDataModel.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 03/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class CommentDataModel : Mappable {
    
    var image                    : String?
    var logo                     : String?
    var title                    : String?
    var total_comment            : String?
    var total_donatedby          : String?
    var total_donation           : String?
    var total_favourite          : String?
    var first_name               : String?
    var last_name                : String?
    var profile                  : String?
    var created                  : String?
    var comment                  : String?
    var total_amount             : String?
    var id                       : String?

    
    required init?(map: Map) {
        
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        
        image                   <- map["id"]
        logo                    <- map["isActive"]
        title                   <- map["amount"]
        total_comment           <- map["hbcu_id"]
        total_donatedby         <- map["card_id"]
        total_donation          <- map["spare_change"]
        total_favourite         <- map["image"]
        first_name              <- map["first_name"]
        last_name               <- map["last_name"]
        profile                 <- map["profile"]
        created                 <- map["created"]
        comment                 <- map["comment"]
        total_amount            <- map["total_amount"]
        id                      <- map["id"]
    }
}
