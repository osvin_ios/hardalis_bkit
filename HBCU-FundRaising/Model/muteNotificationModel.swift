//
//  MuteNotificationModel.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 25/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class MuteNotificationModel : Mappable {
    
    // Mute Notification
    var id: String?
    var user_id: String?
    var group_id : String?
    var notification_status : String?
    var type: String?
    var date_created: String?
 
    required init?(map: Map) {
        
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        // Mute Notification
        id               <-  map["id"]
        user_id          <- map["user_id"]
        group_id                 <- map["group_id"]
        notification_status      <- map["notification_status"]
        type              <-  map["type"]
        date_created      <- map["date_created"]
        
    }
}
