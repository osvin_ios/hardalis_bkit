//
//  SignInModel.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 06/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class SignInModel: Mappable {
    
    var email              : String?
    var fb_id              : String?
    var password           : String?
    var signup_level       : String?
    var signup_type        : String?
    var id                 : Any?
    
    required init?(map: Map) {
        
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        
        email            <- map["email"]
        fb_id            <- map["fb_id"]
        password         <- map["password"]
        signup_level     <- map["signup_level"]
        signup_type      <- map["signup_type"]
        id               <- map["id"]
    }
}
