//
//  ChatGroupModel.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 24/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class ChatGroupModel: Mappable {
    
    var to_id: String?
    var name: String?
    var type: String?
    var post_title: String?
    var post_media: String?
    var date_created: String?
    var counting: String?
    var from_id: String?
    var post_id: String?
    var type_org: String?
    var image: String?
    var media_type : String?
    
    required init?(map: Map) {
        
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        
        to_id                 <- map["to_id"]
        name                  <- map["name"]
        type                  <- map["type"]
        post_title            <- map["post_title"]
        post_media            <- map["post_media"]
        counting              <- map["counting"]
        from_id               <- map["from_id"]
        post_id               <- map["post_id"]
        type_org              <- map["type_org"]
        image                 <- map["image"]
        date_created          <- map["date_created"]
        media_type            <- map["media_type"]
    }
}
