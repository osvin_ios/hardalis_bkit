//
//  MyAccountDetailsModel.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 11/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class MyAccountDetailsModel : Mappable {
    
    var hbcu           = [HBCUListModel]()
    var signup_level   : String?
    var last_name      : String?
    var organization   : HBCUOrganizationListModel?
    var anonymous      : String?
    var first_name     : String?
    var referral_link  : String?
    var greek_org      : String?
    var profile        : String?
    var percent        : String?
    var password       : String?
    var login_type     : String?
    var active_status  : String?
    var email          : String?
    var id             : Any?
    var one_time       : String?
    var recent_donations : String?
    var reoccurring    :String?
    var spare_change   : String?
    var ins_id         : String?
    var unique_url     : String?
    var signup_type    : String?
    var account_id : String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        
        hbcu                  <- map["hbcu"]
        signup_level          <- map["signup_level"]
        last_name             <- map["last_name"]
        organization          <- map["organization"]
        first_name            <- map["first_name"]
        referral_link         <- map["referral_link"]
        greek_org             <- map["greek_org"]
        profile               <- map["profile"]
        percent               <- map["percent"]
        password              <- map["password"]
        login_type            <- map["login_type"]
        active_status         <- map["active_status"]
        email                 <- map["email"]
        id                    <- map["id"]
        one_time              <- map["one_time"]
        recent_donations      <- map["recent_donations"]
        reoccurring           <- map["reoccurring"]
        spare_change          <- map["spare_change"]
        ins_id                <- map["ins_id"]
        unique_url            <- map["unique_url"]
        signup_type           <- map["signup_type"]
        account_id            <- map["account_id"]
    }
}
