//
//  SpareChangeModel.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 28/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import ObjectMapper

class SpareChangeModel : Mappable {
    
    var id                 : String?
    var isActive           : String?
    var amount             : String?
    var hbcu_id            : String?
    var card_id            : String?
    var cycle              : String?
    var image              : String?
    var reoccuringid       : String?
    var title              : String?
    var logo               : String?
    var date               : String?
    var name               : String?
    var pending            : Int?
    var account_id         : String?
    var transaction_id     : String?
    var status             : String?
    
    
    required init?(map: Map) {
        
        mapping(map: map)
    }
    
    //Mapple:-
    open func mapping(map: Map) {
        
        id               <- map["id"]
        isActive         <- map["isActive"]
        amount           <- map["amount"]
        hbcu_id          <- map["hbcu_id"]
        card_id          <- map["card_id"]
        cycle            <- map["spare_change"]
        image            <- map["image"]
        reoccuringid     <- map["reoccuringid"]
        title            <- map["title"]
        logo             <- map["logo"]
        date             <- map["date"]
        name             <- map["name"]
        pending          <- map["pending"]
        account_id       <- map["account_id"]
        transaction_id   <- map["transaction_id"]
        status           <- map["status"]
    }
}

