 
//  AppDelegate.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 04/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import FBSDKLoginKit
import IQKeyboardManager
import LinkKit
import UserNotifications
import MFSideMenu
import CloudKit
import Cloudinary
import Branch
import ObjectMapper
import Stripe
 
 var comingFromPush = Bool()
 var BackForNoti = Bool()
 var isActivereoccuring = String()
 var isActivesparechange = String()
 
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var sideContainer : MFSideMenuContainerViewController?
    var notificationPush : ChatMessageModel?
    var tempDict : NSDictionary!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //Stripe.setDefaultPublishableKey("pk_test_ho0D4zAZULYWZcvNHEHFasyE")
        Stripe.setDefaultPublishableKey("pk_live_egNBOvzPd1DewC3sOGQe4H0c") //live
        
        #if USE_CUSTOM_CONFIG
        setupPlaidWithCustomConfiguration()
        #else 
        setupPlaidLinkWithSharedConfiguration()
        
        #endif
        //keyboard
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().previousNextDisplayMode = .alwaysShow
        
        
        UNUserNotificationCenter.current().delegate = self
        //facebook integration
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        //google analytis
        if let gai = GAI.sharedInstance() {
            
            gai.tracker(withTrackingId: "UA-120452762-1")
            // Optional: automatically report uncaught exceptions.
            gai.trackUncaughtExceptions = true
            // Optional: set Logger to VERBOSE for debug information.
            // Remove before app release.
            gai.logger.logLevel = .verbose;
        }
        //Requesting Authorization for User Interactions
        self.registerForPushNotifications(application: application)
        if let isUserSession = UserDefaults.standard.value(forKey: "loginsession") as? Bool, isUserSession {
            self.changeRootViewController()
        }
        UINavigationBar.appearance().barTintColor = UIColor(red: 234.0/255.0, green: 46.0/255.0, blue: 73.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        //Branch
        let branch: Branch = Branch.getInstance()
        Branch.setUseTestBranchKey(true)
        branch.setDebug()
        branch.initSession(launchOptions: launchOptions, isReferrable: true) { params, error in
            if error == nil {
                // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                // params will be empty if no data found
                // ... insert custom logic here ...
                print("params: %@", params as? [String: AnyObject] ?? {})
                
                self.moveToparticularController(dict: params as? [String: AnyObject] ?? [:])
            }
        }
        return true
    }
    internal func moveToparticularController(dict : [String: AnyObject]){
        if UserDefaults.standard.bool(forKey: "loginsession") == true {
            self.getCurrentNavigationController(dict: dict)
        }else{
            if let isUserClick = dict["+clicked_branch_link"] as? Int, isUserClick == 1 {
                self.getCurrentNavigationControllerNew(dict: dict)
            }
        }
    }
    
    func getCurrentNavigationController(dict : [String: AnyObject]){
        //Navigate to view controller
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let isUserClick = dict["+clicked_branch_link"] as? Int, isUserClick == 1 {
               
                guard let options = dict["screen_type"] as? String else { return }
                switch options {
                case "0" :
                    let initialViewControlleripad =  storyboard.instantiateViewController(withIdentifier: "JoinNowViewController") as! JoinNowViewController
                    initialViewControlleripad.fromAppDelegateEventID = dict["challenge_id"] as? String ?? ""
                    initialViewControlleripad.fromAppDelegate = true
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    self.window?.rootViewController = initialViewControlleripad
                    self.window?.makeKeyAndVisible()
                
                case "1" :
                    let initialViewControlleripad =  storyboard.instantiateViewController(withIdentifier: "BonusChallengeDetailViewController") as! BonusChallengeDetailViewController
                    initialViewControlleripad.fromAppDelegateEventID = dict["challenge_id"] as? String ?? ""
                    initialViewControlleripad.fromAppDelegate = true
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    self.window?.rootViewController = initialViewControlleripad
                    self.window?.makeKeyAndVisible()
                    
                case "2" :
                    
                    let initialViewControlleripad =  storyboard.instantiateViewController(withIdentifier: "ChallengeDetailViewController") as! ChallengeDetailViewController
                    initialViewControlleripad.fromAppDelegateEventID = dict["challenge_id"] as? String ?? ""
                    initialViewControlleripad.fromAppDelegate = true
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    self.window?.rootViewController = initialViewControlleripad
                    self.window?.makeKeyAndVisible()
                    
                default:
                    break
                }
            }
        }
    
    // for without login session
    func getCurrentNavigationControllerNew(dict : [String: AnyObject]) {
           
    //Navigate to view controller
            
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let isUserClick = dict["+clicked_branch_link"] as? Int, isUserClick == 1 {
            //if let rootViewObject = self.window?.rootViewController {
            
                guard let options = dict["screen_type"] as? String else { return }
                switch options {
                case "0" :
                    
                    let initialViewControlleripad =  storyboard.instantiateViewController(withIdentifier: "JoinNowViewController") as! JoinNowViewController
                    initialViewControlleripad.fromAppDelegateEventID = dict["challenge_id"] as? String ?? ""
                    initialViewControlleripad.fromAppDelegate = true
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    self.window?.rootViewController = initialViewControlleripad
                    self.window?.makeKeyAndVisible()
                    
                case "1" :
                    
                    let initialViewControlleripad =  storyboard.instantiateViewController(withIdentifier: "BonusChallengeDetailViewController") as! BonusChallengeDetailViewController
                    initialViewControlleripad.fromAppDelegateEventID = dict["challenge_id"] as? String ?? ""
                    initialViewControlleripad.fromAppDelegate = true
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    self.window?.rootViewController = initialViewControlleripad
                    self.window?.makeKeyAndVisible()
                    
                case "2" :
                    
                    let initialViewControlleripad =  storyboard.instantiateViewController(withIdentifier: "ChallengeDetailViewController") as! ChallengeDetailViewController
                    initialViewControlleripad.fromAppDelegateEventID = dict["challenge_id"] as? String ?? ""
                    initialViewControlleripad.fromAppDelegate = true
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    self.window?.rootViewController = initialViewControlleripad
                    self.window?.makeKeyAndVisible()
                    
                default:
                    break
                }
            //}
        }
    }
    
    // Respond to URI scheme links
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        // pass the url to the handle deep link call
        let branchHandled = Branch.getInstance().application(application,
                                                             open: url,
                                                             sourceApplication: sourceApplication,
                                                             annotation: annotation
        )
        //        if (!branchHandled) {
        //            // If not handled by Branch, do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        //            return GIDSignIn.sharedInstance().handle(url,
        //                                                     sourceApplication: sourceApplication,
        //                                                     annotation: annotation)
        //        }
        
        // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        return true
    }
    
    // Respond to Universal Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // pass the url to the handle deep link call
        Branch.getInstance().continue(userActivity)
        return true
    }
    
    func application(_ application: UIApplication, didFailToContinueUserActivityWithType userActivityType: String, error: Error) {
        Branch.getInstance().continue(userActivity)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        //        let stringUrl : String = url.absoluteString
        // pass the url to the handle deep link call
        Branch.getInstance().application(app,
                                         open: url,
                                         options:options
        )
        //        if stringUrl.contains("com.googleusercontent.apps") {
        //
        //            return GIDSignIn.sharedInstance().handle(url as URL?,
        //                                                     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String?,
        //                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        //
        //        } else {
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String?, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        //        }
    }
    
    // MARK: Plaid Link setup with shared configuration from Info.plist
    func setupPlaidLinkWithSharedConfiguration(){
        
        // <!-- SMARTDOWN_SETUP_SHARED -->
        // With shared configuration from Info.plist
        PLKPlaidLink.setup { (success, error) in
            if (success) {
                // Handle success here, e.g. by posting a notification
                NSLog("Plaid Link setup was successful")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PLDPlaidLinkSetupFinished"), object: self)
            }
            else if let error = error {
                NSLog("Unable to setup Plaid Link due to: \(error.localizedDescription)")
            }
            else {
                NSLog("Unable to setup Plaid Link")
            }
        }
        // <!-- SMARTDOWN_SETUP_SHARED -->
    }
    
    // MARK: Plaid Link setup with custom configuration
    func setupPlaidWithCustomConfiguration(){
        // <!-- SMARTDOWN_SETUP_CUSTOM -->
        // With custom configuration

        //development
        //let linkConfiguration = PLKConfiguration(key: "c74afb41cee71cd888636cc90426fb", env: .sandbox, product: .auth)
        
        //live
          //let linkConfiguration = PLKConfiguration(key: "pk_live_egNBOvzPd1DewC3sOGQe4H0c", env: .development, product: .auth)
        let linkConfiguration = PLKConfiguration(key: "c74afb41cee71cd888636cc90426fb", env: .development, product: .auth)
        
        linkConfiguration.clientName = "HBCU Fundraising"
        PLKPlaidLink.setup(with: linkConfiguration) { (success, error) in
            if (success) {
                // Handle success here, e.g. by posting a notification
                NSLog("Plaid Link setup was successful")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PLDPlaidLinkSetupFinished"), object: self)
            }
            else if let error = error {
                NSLog("Unable to setup Plaid Link due to: \(error.localizedDescription)")
            }
            else {
                NSLog("Unable to setup Plaid Link")
            }
        }
        // <!-- SMARTDOWN_SETUP_CUSTOM -->
    }
    
//    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
//        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
//    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        print("will resign Active")
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("Did enter background")
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("applicationWillEnterForeground")
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("applicationDidBecomeActive")
        if let isUserSession = UserDefaults.standard.value(forKey: "loginsession") as? Bool, isUserSession {
            if let topVc = self.getCurrentTopVC(topVCObject: 2) as? UIViewController {
                // if confirm pin already loaded to screen then nothings happen
//                if topVc is ConfirmPinViewController {
//                } else {
//                    let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                    if let confirmView = storyboard.instantiateViewController(withIdentifier: "confirmNavVC") as? UINavigationController {
//                        self.window?.rootViewController = confirmView
//                    }
//                }
                if topVc is ConfirmPinViewController{
                }else if topVc is ChatMessageViewController{
                }else if topVc is SelectHBCUToDonateVC{
                }else if topVc is DonationViewController{
                }else{
                    let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    if let confirmView = storyboard.instantiateViewController(withIdentifier: "confirmNavVC") as? UINavigationController{
                        self.window?.rootViewController = confirmView
                    }
                }
                
            }
        }
    }
    func getCurrentTopVC(topVCObject: Int) -> AnyObject?{
        if let windowRoot = self.window?.rootViewController as? MFSideMenuContainerViewController{
            if let navVC = windowRoot.centerViewController as? UINavigationController{
                if topVCObject == 1 {
                    return navVC as AnyObject
                } else {
                    if let topVC = navVC.topViewController {
                        return topVC as AnyObject
                    }
                }
            }
        }
        return nil
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
        comingFromPush = false
//        UserDefaults.standard.set(comingFromPush, forKey: "comingFromPush")
    }
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "HBCU_FundRaising")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    // MARK: - Core Data Saving support
    func saveContext (){
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            }catch{
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    //Ios 10 delegates for Push Notifications
    func userNotificationCenter( _ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping ( _ options:   UNNotificationPresentationOptions) -> Void) {
        print("Handle push from foreground")

        if let pushDict = notification.request.content.userInfo as? Dictionary<String, AnyObject> {
//            let aps = pushDict["aps"]
//            let to_id = aps?["to_id"]
//           // let to_id:String = (aps?["to_id"] as! NSString) as String
//            let kUserDefault = UserDefaults.standard
//            kUserDefault.set(to_id, forKey: "to_id")
//            kUserDefault.synchronize()
//            NotificationCenter.default.post(name: NSNotification.Name("reloadTheTable"), object: nil)
//           // self.fetchPushNotificationObject(dict: pushDict, isClicked:false)
//            let status:Int = (pushDict["aps"]!["notification_status"] as! NSInteger) as Int
//            if status == 1{
//                 completionHandler([.alert, .badge])
//            } else {
//                 completionHandler([.sound, .alert, .badge])
//            }

                if let windowRoot = self.window?.rootViewController as? MFSideMenuContainerViewController{
                    if let navVC = windowRoot.centerViewController as? UINavigationController{
                        let currentVC = navVC.bnc_current()
                        if currentVC is ChatMessageViewController{
                            completionHandler([])
                        }else{
                            completionHandler([.sound, .alert, .badge])
                        }
                    }else{
                        completionHandler([.sound, .alert, .badge])
                    }
                }else{
                    completionHandler([.sound, .alert, .badge])
                }
          
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void){
        print("Handle push from background or closed")
        comingFromPush = true
        // UserDefaults.standard.set(comingFromPush, forKey: "comingFromPush")
        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
        print("\(response.notification.request.content.userInfo)")
        if let pushDict = response.notification.request.content.userInfo as? Dictionary<String, AnyObject>{
            print(pushDict)
            self.fetchPushNotificationObject(dict: pushDict, isClicked:true)
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications()
        completionHandler()
    }
    func registerForPushNotifications(application: UIApplication) {
        let center  = UNUserNotificationCenter.current()
        center.delegate = self
        // set the type as sound or badge
        center.requestAuthorization(options: [.sound,.alert,.badge]){ (granted, error) in
            // Enable or disable features based on authorization
        }
        application.registerForRemoteNotifications()
    }
    func fetchPushNotificationObject(dict: Dictionary<String, AnyObject>, isClicked: Bool = false) {
        if let pushDict = dict["aps"] as? Dictionary<String, AnyObject> {
            if let userChatInfo = Mapper<ChatMessageModel>().map(JSONObject: pushDict) {
                notificationPush = userChatInfo
                tempDict = pushDict as NSDictionary
                self.navigateToChat()
            }
        }
    }
    func navigateToChat(){
            print(tempDict)
        //NotificationCenter.default.post(name: Notification.Name("handleback"), object: nil, userInfo: tempDict as? [AnyHashable : Any])
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let leftSideMenu = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as? SideMenuViewController
        let centerViewController = storyboard.instantiateViewController(withIdentifier: "ConfirmPinViewController") as? ConfirmPinViewController
        centerViewController?.temppinDict = tempDict
        let navVC = UINavigationController(rootViewController: centerViewController!)
        let sideContainer = MFSideMenuContainerViewController.container(withCenter: navVC, leftMenuViewController: leftSideMenu, rightMenuViewController: nil)
        self.window?.rootViewController = sideContainer
    }
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        UserDefaults.standard.set(token, forKey: "token")
        print("The device token\(token)")
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error){
        // Print the error to console (you should alert the user that registration failed)
        UserDefaults.standard.set("", forKey: "token")
    }
    func changeRootViewController(){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let leftSideMenu = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as? SideMenuViewController
        let centerViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        
        let navVC = UINavigationController(rootViewController: centerViewController!)
        navVC.navigationBar.tintColor = UIColor.white
        let sideContainer = MFSideMenuContainerViewController.container(withCenter: navVC, leftMenuViewController: leftSideMenu, rightMenuViewController: nil)
        self.window?.rootViewController = sideContainer
    }
    func changeRootViewControllerMakeFav(){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let leftSideMenu = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as? SideMenuViewController
        let centerViewController = storyboard.instantiateViewController(withIdentifier: "FavHBCUViewController") as? FavHBCUViewController
        let navVC = UINavigationController(rootViewController: centerViewController!)
        navVC.navigationBar.barTintColor = UIColor.white
        //navVC.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
        let sideContainer = MFSideMenuContainerViewController.container(withCenter: navVC, leftMenuViewController: leftSideMenu, rightMenuViewController: nil)
        self.window?.rootViewController = sideContainer
    }
    
}
