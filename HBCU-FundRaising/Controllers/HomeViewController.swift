//
//  HomeViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 13/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class HomeViewController: UIViewController {
    
    var accountModel : MyAccountDetailsModel?
    
    @IBOutlet weak var thankYouLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.hidesBackButton = true
        self.title = "HOME"
        barButton()
        hitApiOfHome()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        guard let userDetil = Methods.sharedInstance.getUserData() else {
            return
        }
        
        let name: String = userDetil.first_name ?? ""
        thankYouLabel.text = "Thank you, " + name  + " for showing our HBCU's some love!"
    }
    
        func barButton() {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: nil)
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
        }
        
    @objc func leftBar(sender: UIButton) {
        
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            print("drawer opened")
        }
    }
    
    //get data of home screen
    func hitApiOfHome(){
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let apiName        = "getDonationDetails"
        let user_id        = userInfoModel.id
        
        
        let paramsStr = "user_id=\(user_id ?? "")"
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + apiName + "?" + paramsStr, method: "GET", params: "") { (success, response, errorMsg) in
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    //self.showAlert((jsonResult["message"] as? String)!)
                    if let resultValue = jsonResult["result"] as? Dictionary<String,AnyObject>{
                        if let homeData = Mapper<HomeModal>().map(JSONObject: resultValue){
//                            self.homeDataArray = homeData
                        }
                    }
                }} else {
                print("Data is not in proper format")
            }}
    }
    
}
