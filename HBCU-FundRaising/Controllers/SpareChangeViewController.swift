//
//  SpareChangeViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 27/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import Stripe
import PassKit
import Alamofire

class SpareChangeViewController: UIViewController {
    
    @IBOutlet weak var dollarUntilLabel: UILabel!
    @IBOutlet weak var toDonateAmount: UILabel!
    @IBOutlet var spareChangeHeaderView: UIView!
    @IBOutlet weak var spareChangeTableView: UITableView!
    @IBOutlet weak var allUnderline: UIView!
    @IBOutlet weak var approvedUnderline: UIView!
    @IBOutlet weak var unapprovedUnderline: UIView!
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet var successAlertView: UIView!
    @IBOutlet weak var approveButton: UIButton!
    @IBOutlet weak var unapproveButton: UIButton!
    @IBOutlet weak var donateFromSpareChangeLabel: UILabel!
    @IBOutlet weak var donateNowBtnOutlet: UIButton!
    
    @IBOutlet var donateWithSubView: UIView!
    
    var donateAmountCheck : Bool = true
    var dataArray = [SpareChangeModel]()
    var type : String? = "1"
    var pendingStatus : String?
    var toDonate = String()
    var total_amount = String()
    var approved_idArray = [String]()
    var buttonTag  : Int?
    var resultArray = [Dictionary<String, AnyObject>]()
    
    var valueLabelTopString = String()
    var valueAmountArray = [String]()
    var doubleStr : String?
    var spareID : String?
    var approveUnapp : Int?
    
    //Apple Pay
    var subscriptionId : String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        spareChangeTableView.register(UINib(nibName: "CellDonation", bundle: nil), forCellReuseIdentifier: "CellDonation")
        self.title = "Spare Change"
        self.barButton()
        self.spareChangeHeaderView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "ic_navbar_bg"))
        self.allUnderline.backgroundColor = UIColor.white
        self.approvedUnderline.backgroundColor = UIColor.clear
        self.unapprovedUnderline.backgroundColor = UIColor.clear
        if donateNowBtnOutlet.isSelected == true {
            self.donateAmountCheck = false
          //  self.toDonate = "0"
        }else{
            self.donateAmountCheck = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool){
        
        allButton.isSelected = true
        if allButton.isSelected == true {
            if Reachability.isConnectedToNetwork() == true {
                self.hitAllSpareChangeAPI(offset: 0)
            }else{
                self.showAlert(Constants.commomInternetmsz.checkInternet)
            }
        }
        if donateNowBtnOutlet.isSelected == true{
            self.donateAmountCheck = false
          //  self.toDonate = "0"
        }else{
            self.donateAmountCheck = true
        }
        self.allUnderline.backgroundColor = UIColor.white
        self.approvedUnderline.backgroundColor = UIColor.clear
        self.unapprovedUnderline.backgroundColor = UIColor.clear
    }
    
    @IBAction func allButton(_ sender: UIButton){
        type = "1"
        allUnderline.backgroundColor = UIColor.white
        approvedUnderline.backgroundColor = UIColor.clear
        unapprovedUnderline.backgroundColor = UIColor.clear
        self.hitAllSpareChangeAPI(offset: 0)
    }
    
    @IBAction func approvedButton(_ sender: UIButton){
        type = "3"
        allUnderline.backgroundColor = UIColor.clear
        approvedUnderline.backgroundColor = UIColor.white
        unapprovedUnderline.backgroundColor = UIColor.clear
        self.hitAllSpareChangeAPI(offset: 0)
    }
    
    @IBAction func crossAction(_ sender: UIButton){
        DispatchQueue.main.async {
            self.successAlertView.removeFromSuperview()
        }
    }
    
    @IBAction func unapprovedAction(_ sender: UIButton){
        type = "2"
        allUnderline.backgroundColor = UIColor.clear
        approvedUnderline.backgroundColor = UIColor.clear
        unapprovedUnderline.backgroundColor = UIColor.white
        self.hitAllSpareChangeAPI(offset: 0)
    }
    
    @IBAction func donateWithSubViewCrossButtonPressed(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.donateWithSubView.removeFromSuperview()
        }
    }
    
    @IBAction func subViewDonateWithApplePayButtonPressed(_ sender: ButtonDesign) {
        if donateNowBtnOutlet.isSelected == true {
            self.donateAmountCheck = false
        }else{
            self.donateAmountCheck = true
        }
        var toDonateAmount = Double()
        if toDonate.isEmpty == false || toDonate != "" {
            toDonateAmount = Double(toDonate)!
            if toDonateAmount >= 1.50 && toDonateAmount < 5 {
                
//                self.alertFunctionsWithCallBack(title: "Do you really want to donate this amount?") { (isObject) in
//                    if isObject.boolValue {
//                        self.setupApplePay()
//                    }else{}
//                }
                self.setupApplePay()
                
            } else if toDonateAmount >= 5 {
                self.showAlert("Your Spare Change will be donated automatically.")
            } else {
                self.showAlert("Please approve some of the transactions to donate your Spare Change.")
            }
        }
    }
    
    @IBAction func subViewDonateButtonPressed(_ sender: ButtonDesign) {
        if donateNowBtnOutlet.isSelected == true {
            self.donateAmountCheck = false
        }else{
            self.donateAmountCheck = true
        }
        var toDonateAmount = Double()
        if toDonate.isEmpty == false || toDonate != "" {
            toDonateAmount = Double(toDonate)!
            if toDonateAmount >= 1.50 && toDonateAmount < 5 {
                
//                self.alertFunctionsWithCallBack(title: "Do you really want to donate this amount?") { (isObject) in
//                    if isObject.boolValue {
//                        self.hitDonateSpareChangeAPI()
//                    } else {print("rejected")}
//                }
                self.hitDonateSpareChangeAPI()
                
            } else if toDonateAmount >= 5 {
                self.showAlert("Your Spare Change will be donated automatically.")
            } else {
                self.showAlert("Please approve some of the transactions to donate your Spare Change.")
            }
        }
    }

    @IBAction func donateNowButtonAction(_ sender: UIButton){
//        let view = self.donateWithSubView
//        view?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
//        //view?.layer.cornerRadius = 0.2
//        self.view.addSubview(view!)
        
        if donateNowBtnOutlet.isSelected == true {
            self.donateAmountCheck = false
        }else{
            self.donateAmountCheck = true
        }
        var toDonateAmount = Double()
        if toDonate.isEmpty == false || toDonate != "" {
            toDonateAmount = Double(toDonate)!
            if toDonateAmount >= 1.50 && toDonateAmount < 5 {
                
                //                self.alertFunctionsWithCallBack(title: "Do you really want to donate this amount?") { (isObject) in
                //                    if isObject.boolValue {
                //                        self.hitDonateSpareChangeAPI()
                //                    } else {print("rejected")}
                //                }
                self.hitDonateSpareChangeAPI()
                
            } else if toDonateAmount >= 5 {
                self.showAlert("Your Spare Change will be donated automatically.")
            } else {
                self.showAlert("Please approve some of the transactions to donate your Spare Change.")
            }
        }
        
    }
    
    func barButton(){
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
    }
    
    @objc func leftBar(sender: UIButton){
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            print("drawer opened")
        }
    }
    
    @IBAction func unapproveActionSuccesView(_ sender: UIButton){
        self.hitAPIOnSuccAlert(type: "1")
    }
    
    @IBAction func approveSuccessAction(_ sender: UIButton){
        self.hitAPIOnSuccAlert(type: "2")
    }

    func spareChangeBalance(value : String) -> String{
        let valueInt = Double(value)!
        let result = 5.0 - valueInt
        //return String(result)
        return String(format: "%.2f", result)
    }
    
    func hitAllSpareChangeAPI(offset: Int){
        guard let userInfoModel = Methods.sharedInstance.getloginData() else{
            return
        }
        let user_id    = userInfoModel.id as? String
        let page       = offset as AnyObject
        let paramsStr  = "user_id=\(user_id ?? "")&page=\(page)&type=\(type!)"
        print(paramsStr)
        WebServiceClass.showLoader(view: self.view)
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + "plaidtransaction", method: "POST", params: paramsStr) { [weak self] (success, response, errorMsg) in
          
            guard let strongSelf = self else {return}
            
            WebServiceClass.hideLoader(view: strongSelf.view)
            //    strongSelf.refreshControlTop.endRefreshing()
            //    strongSelf.refreshControlBottom.endRefreshing()
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if strongSelf.type == "1"{
                        if let result = jsonResult["result"]!["all"] as? [Dictionary<String, AnyObject>]{
    //              if strongSelf.skipCount == 0 {
    //                strongSelf.dataArray.removeAll()     //.....
    //              }
                            strongSelf.resultArray = result
                            if let toDonateAmount =  jsonResult["result"]!["tilldonate"] as? [Dictionary<String, AnyObject>]{
                               
                                    if toDonateAmount.isEmpty == false {
                                            if self?.donateAmountCheck == true {
                                                for i in 0..<toDonateAmount.count {
                                                    strongSelf.toDonate = (toDonateAmount[0]["todonateamount"] as? String)!
                                                    self?.approved_idArray.append((toDonateAmount[i]["id"] as? String)!)
//                                                    print(i)
//                                                    print(toDonateAmount[i]["id"])
                                                    print(self?.approved_idArray.append((toDonateAmount[i]["id"] as? String)!))
                                                }
                                            }else{
                                                strongSelf.toDonate = "0"
                                                self?.doubleStr = "0"
                                                // self?.valueAmountArray = []
                                            }
                                    }else{
                                        strongSelf.toDonate = "0"
                                    }
                                
                            }else{
                                print("nilArray")
                            }
                            if let dataArray = Mapper<SpareChangeModel>().mapArray(JSONObject: result) {
                                strongSelf.dataArray = dataArray
                            }
                            //                            strongSelf.postsCountOFArray = strongSelf.dataArray.count     //.....
                        }} else if strongSelf.type == "2"{
                            if let result = jsonResult["result"]!["unapproved"] as? [Dictionary<String, AnyObject>]{
//                    if strongSelf.skipCount == 0 {
//                        strongSelf.dataArray.removeAll()     //.....
//                    }
                            strongSelf.resultArray = result
                            if let toDonateData =  jsonResult["result"]!["tilldonate"] as? [Dictionary<String, AnyObject>]{
                               
                                  if toDonateData.isEmpty == false {
                                        if self?.donateAmountCheck == true{
                                            strongSelf.toDonate = (toDonateData[0]["todonateamount"] as? String)!
                                           // print(strongSelf.toDonate)
                                        } else {
                                        strongSelf.toDonate = "0"
                                        self?.doubleStr = "0"
                                        //  self?.valueAmountArray = []
                                        }
                                    } else {
                                        strongSelf.toDonate = "0"
                                    }
                                
                            }
                            if let dataArray = Mapper<SpareChangeModel>().mapArray(JSONObject: result){
                                strongSelf.dataArray = dataArray
                            }
//                        strongSelf.postsCountOFArray = strongSelf.dataArray.count     //.....
                            
                        }} else if strongSelf.type == "3"{
                       
                         if let result = jsonResult["result"]!["approved"] as? [Dictionary<String, AnyObject>]{
//                                if strongSelf.skipCount == 0 {
//                                    strongSelf.dataArray.removeAll()     //.....
//                                }
                            strongSelf.resultArray = result
                            if let toDonateData =  jsonResult["result"]!["tilldonate"] as? [Dictionary<String, AnyObject>] {
                                if toDonateData.isEmpty == false {
                                    if self?.donateAmountCheck == true{
                                        strongSelf.toDonate = (toDonateData[0]["todonateamount"] as? String)!
                                    }else{
                                        strongSelf.toDonate = "0"
                                        self?.doubleStr = "0"
                                        //self?.valueAmountArray = []
                                    }
                                }else{
                                    strongSelf.toDonate = "0"
                                }
                            }
                            if let dataArray = Mapper<SpareChangeModel>().mapArray(JSONObject: result){
                                strongSelf.dataArray = dataArray
                            }
//                        strongSelf.postsCountOFArray = strongSelf.dataArray.count     //.....
                        }
                    }
                    
                    if let totaldonated = jsonResult["result"]!["totaldonated"] as? String {
                        let myDouble = Double(totaldonated)
                        let doubleStr = String(format: "%.2f", myDouble!)
                        self?.donateFromSpareChangeLabel.text =  "$" + doubleStr
                    }
                }
                DispatchQueue.main.async {
                    if self?.donateAmountCheck == true {
                        if strongSelf.toDonate.isEmpty == true || strongSelf.toDonate == "" {
                            strongSelf.toDonateAmount.text = "$0"
                        } else {
                            strongSelf.toDonateAmount.text = "$" + strongSelf.toDonate
                        }
                    }else{
                         strongSelf.toDonateAmount.text = "$0"
                    }
                    var floatamount = Double()
                    if strongSelf.toDonate.isEmpty == false || strongSelf.toDonate != "" {
                        floatamount = Double(strongSelf.toDonate)!
                        if floatamount < 5.00 {
                            if self?.donateAmountCheck == true{
                                strongSelf.dollarUntilLabel.isHidden = false
                                let damountLeft =  strongSelf.spareChangeBalance(value: strongSelf.toDonate)
                                strongSelf.dollarUntilLabel.text = "$" + damountLeft + " until $5"
                            } else {
                                strongSelf.dollarUntilLabel.isHidden = false
                                strongSelf.dollarUntilLabel.text = "$5 until $5"
                            }
                        }
//                        else if floatamount == 0{
//                            strongSelf.dollarUntilLabel.isHidden = false
//                            self?.dollarUntilLabel.text = "$5 until $5"
//                        }
                    }else{
                        strongSelf.dollarUntilLabel.isHidden = false
                        self?.dollarUntilLabel.text = "$5 until $5"
                    }
                    if strongSelf.dataArray.count == 0 {
                        strongSelf.showAlert("No Records Found!!!")
                    }}
                    strongSelf.spareChangeTableView.reloadData()
             }else{
                    print("Data is not in proper format")
                }
            }
    }
    func hitAPIOnSuccAlert(type : String){
        guard let userInfoModel = Methods.sharedInstance.getloginData() else{
            return
        }
        let user_id         = userInfoModel.id as? String
        self.total_amount  = self.toDonate
        let place_name      = dataArray[buttonTag!].name ?? ""
        let account_id      = dataArray[buttonTag!].account_id ?? ""
//        let amount          = dataArray[buttonTag!].amount ?? ""
        let amount          = valueLabelTopString
        let txnId           = dataArray[buttonTag!].transaction_id ?? ""

        let paramsStr = "user_id=\(user_id ?? "")&total_amount=\(total_amount)&place_name=\(place_name)&account_id=\(account_id)&amount=\(amount)&type=\(type)&txnId=\(txnId)&spareid=\(self.spareID ?? "")"
        print(paramsStr)
        WebServiceClass.showLoader(view: self.view)
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + "insertunapproved", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject>{
                    print(jsonResult)
                }
                self.hitAllSpareChangeAPI(offset: 0)
                DispatchQueue.main.async {
                    self.donateAmountCheck = true
                    self.successAlertView.removeFromSuperview()
                    self.spareChangeTableView.reloadData()
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    //hit donate spare change api
    func hitDonateSpareChangeAPI(){
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let user_id      = userInfoModel.id as? String
        let amount       = toDonate
        let card_id      = ""
        let approved_id  = approved_idArray.joined(separator: ",")
        
        let paramsStr = "user_id=\(user_id ?? "")&amount=\(amount)&card_id=\(card_id)&approved_id=\(approved_id)"
        WebServiceClass.showLoader(view: self.view)
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + "donateSpare", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)

            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                        self.donateAmountCheck = false
                        self.showAlert((jsonResult["message"] as? String)!)
                        self.toDonate = ""
                        self.toDonate  = "0"
                        self.toDonateAmount.text = "$" + self.toDonate
                        self.dollarUntilLabel.text = "$5 until $5"
                        self.hitAllSpareChangeAPI(offset: 0)
                    //self.spareChangeTableView.reloadData()
                }
            } else {
                 //self.showAlertWithoutCallBack(messageTitle: "Something went wrong!")
                self.showAlertWithoutCallBack(messageTitle: "Something is wrong. Please try again later.")
                
                 print("Data is not in proper format")
            }
        }
    }
}

extension SpareChangeViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell : CellDonation = tableView.dequeueReusableCell(withIdentifier: "CellDonation", for: indexPath) as! CellDonation
        let value = dataArray[indexPath.row]
        var amount = ""
        //let amount = resultArray[indexPath.row]["amount"] as? String
        if let amount1 = resultArray[indexPath.row]["amount"] as? String{
            amount = amount1
        }else if let amount2 = resultArray[indexPath.row]["amount"] as? Double{
            amount = "\(amount2)"
        }else if let amount3 = resultArray[indexPath.row]["amount"] as? Int{
            amount = "\(amount3)"
        }else{}
        
        approveUnapp = indexPath.row
        let date = convertDateFormater(value.date!)
        cell.cellBorder.dropShadow()
        cell.dateLabel.text = date
        //Convert: amount string into double and formatted
        
        //if let amountStr = amount {
        if amount != "" {
            let amountStr = amount
            
            let result = amountStr.components(separatedBy: ".")
//            let valueInDouble : Double = Double(result[1] as String ?? "0") ?? 0.0
            if result.count > 1{
                if result[1] != "" {
                    let formattedResult = "0" + "." + result[1]
                    let valueInDouble = Double(formattedResult as? String ?? "0") ?? 0.0
                    let calculatedResult = (1.0 - valueInDouble)
                   // print(calculatedResult)
                    self.doubleStr = "\(String(format: "%.2f", calculatedResult))"
                  //  print("\(doubleStr ?? "")")
                    if doubleStr == "0.00"{
                        cell.valueLabelTop.text = "+" + "0.10"
                        cell.valueLabelTop.tag = indexPath.row
                        approveUnapp = cell.valueLabelTop.tag
                        valueLabelTopString = "0.10"
                    }else{
                        cell.valueLabelTop.text = "+" + "\(doubleStr ?? "")"
                        cell.valueLabelTop.tag = indexPath.row
                        approveUnapp = cell.valueLabelTop.tag
                     //   print(approveUnapp as Any)
                        valueLabelTopString = "\(doubleStr ?? "")"
                        valueAmountArray = [valueLabelTopString]
                        print(valueAmountArray)
                        //valueAmountArray = [valueLabelTopString]
                    }
                }
            }else{}
        }
        cell.nameLabel.text = "+" + "\(amount ?? "")" + " " + value.name!
        cell.selectionStyle = .none
//        cell.statusButton.addTarget(self, action: #selector(statusButtonImage), for: .touchUpInside)
//        cell.statusButton.tag = indexPath.row
//        if value.pending == 0{
//            cell.statusButton.setImage(#imageLiteral(resourceName: "ic_round_tick_green_outline"), for: .normal)
//        }
//        if type == "2" {
//            cell.statusButton.setImage(#imageLiteral(resourceName: "ic_caution"), for: .normal)
//        } else if type == "3" {
//            cell.statusButton.setImage(#imageLiteral(resourceName: "ic_round_tick_green_outline"), for: .normal)
//        } else {
//            cell.statusButton.setImage(#imageLiteral(resourceName: "ic_round_tick_green_outline"), for: .normal)
//        }
        if value.status != "" {
            if value.status == "0"{
                cell.statusButton.setImage(#imageLiteral(resourceName: "ic_caution"), for: .normal)
                cell.statusButton.isEnabled = true
                cell.statusButton.addTarget(self, action: #selector(statusButtonImage), for: .touchUpInside)
                cell.statusButton.tag = indexPath.row
                unapproveButton.isEnabled = false
            }else if value.status == "1"{
                cell.statusButton.setImage(#imageLiteral(resourceName: "ic_round_tick_green_outline"), for: .normal)
                cell.statusButton.isEnabled = false
                unapproveButton.isEnabled = true
            }else{
                cell.statusButton.setImage(#imageLiteral(resourceName: "ic_plus_round"), for: .normal)
                cell.statusButton.isEnabled = true
                cell.statusButton.addTarget(self, action: #selector(statusButtonImage), for: .touchUpInside)
                cell.statusButton.tag = indexPath.row
                unapproveButton.isEnabled = true
            }
        }
//        if value.status  == "2"{
//            self.spareID = ""
//        }else if value.status == "0"{
//            self.spareID = "\(value.id ?? "")"
//        }else{}
        return cell
    }
    
    @objc func statusButtonImage(_ sender : UIButton){
        self.spareID = "\(dataArray[sender.tag].id ?? "")"
        print(self.spareID ?? "")
        self.approved_idArray.append((dataArray[sender.tag].id ?? "")!)
        print(self.approved_idArray.append((dataArray[sender.tag].id ?? "")!))
        
        var amount = ""
        //let amount = resultArray[sender.tag]["amount"] as? String
        if let amount1 = resultArray[sender.tag]["amount"] as? String{
            amount = amount1
        }else if let amount2 = resultArray[sender.tag]["amount"] as? Double{
            amount = "\(amount2)"
        }else if let amount3 = resultArray[sender.tag]["amount"] as? Int{
            amount = "\(amount3)"
        }else{}
        
        //Convert: amount string into double and formatted
        //if let amountStr = amount {
        if amount != "" {
            let amountStr = amount
        
            let result = amountStr.components(separatedBy: ".")
            //            let valueInDouble : Double = Double(result[1] as String ?? "0") ?? 0.0
            if result.count > 1 {
                if result[1] != "" {
                    let formattedResult = "0" + "." + result[1]
                    let valueInDouble = Double(formattedResult as? String ?? "0") ?? 0.0
                    let calculatedResult = (1.0 - valueInDouble)
                    // print(calculatedResult)
                    self.doubleStr = "\(String(format: "%.2f", calculatedResult))"
                    //  print("\(doubleStr ?? "")")
                    if doubleStr == "0.00"{
                        valueLabelTopString = "0.10"
                    }else{
                        valueLabelTopString = "\(doubleStr ?? "")"
                    }
                }
            }else{
            }
        }
        buttonTag = sender.tag
        let view = successAlertView
        view?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
        view?.layer.cornerRadius = 0.2
        self.view.addSubview(view!)
       // valueAmountArray = [valueLabelTopString]
    }
    
    func apprpve(_ sender : UIButton){
        approveUnapp = sender.tag
        for _ in 0..<valueLabelTopString.count{
            valueAmountArray.append(valueLabelTopString)
        }
    }
    
    func convertDateFormater(_ date: String) -> String{
        if date.isEmpty == false{
            let myDateString = date
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let myDate = dateFormatter.date(from: myDateString)!
            
            dateFormatter.dateFormat = "MMM dd, YYYY"
            let somedateString = dateFormatter.string(from: myDate)
            return somedateString
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        let view = spareChangeHeaderView
        view?.frame = CGRect(x: 0, y: 0, width: self.spareChangeTableView.frame.size.width, height: 400 )
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 280
    }
}

extension SpareChangeViewController : PKPaymentAuthorizationViewControllerDelegate{
    
    func setupApplePay(){
        
        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: Constants.ApplePayMerchantID)
        paymentRequest?.currencyCode = "USD"
        paymentRequest?.countryCode = "US"
        
        if let amount = Int(toDonate){
            paymentRequest?.paymentSummaryItems = [
                PKPaymentSummaryItem(label: "", amount: NSDecimalNumber(value: amount))
            ]
            
            //Payment networks array
            let paymentNetworks = [PKPaymentNetwork.amex, .visa, .masterCard, .discover]
            paymentRequest?.supportedNetworks = paymentNetworks
            paymentRequest?.merchantCapabilities = .capability3DS
        }
        
        if Stripe.canSubmitPaymentRequest(paymentRequest) {
            var paymentController: PKPaymentAuthorizationViewController?
            paymentController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest!)
            paymentController?.delegate = self
            paymentController?.modalPresentationStyle = .formSheet
            
            if let paymentController = paymentController {
                present(paymentController, animated: true, completion: nil)
            }
        } else {
            //self.showAlert("Apple Pay is not available on this device or May needs to set up the Apple Pay on the device")
            self.showAlert("You need to set up the Apple Pay on the device")
        }
        
    }
    
    //This function is called when Apple Pay payment will be authorized.
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void){
        // Payment token can be found like this
        print(payment.token)
        // Get response from the server and set the PKPaymentAuthorizationStatus
        let status = PKPaymentAuthorizationStatus(rawValue: 0)!
        switch status.rawValue {
        case 0:
            
            //Stripe.setDefaultPublishableKey("pk_test_ho0D4zAZULYWZcvNHEHFasyE")
            Stripe.setDefaultPublishableKey("pk_live_egNBOvzPd1DewC3sOGQe4H0c") //live
            
            STPAPIClient.shared().createToken(with: payment) {
                (token, error) -> Void in
                
                if (error != nil) {
                    print("%@", error!)
                    completion(PKPaymentAuthorizationStatus.failure)
                    return
                }
                
                if let token = token{
                    self.hitOneTimeAPI(token: token, VC: controller, completion: completion)
                }
                
            }
            
        default:
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
    //This function is called when Apple Pay payment authorization is finished.
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController){
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension SpareChangeViewController{
    
    //MARK:- one time charges
    func hitOneTimeAPI(token: STPToken, VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)) {
        
        let url            = "https://api.stripe.com/v1/charges"
        let description    = "One-off"
        let source         = token
        var price = 0
        let firstPrice = toDonate
        let newFirstPrice = Int(firstPrice) ?? 0
        price = newFirstPrice * 100
        
        var dict = Dictionary<String, Any>()
        
        dict = ["amount"        : price ,
                "currency"  : "usd" ,
                "description" : description  , "source" : source ]
        
        //"sk_test_0HEIX5gE8pRO5X5xuSPrPczU"
        //"sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        
        let header = [
            "Content-Type"   : "application/x-www-form-urlencoded",
            "Authorization"  : "Bearer sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        ]
        
        WebServiceClass.showLoader(view: self.view)
        Alamofire.request(url , method: .post, parameters: dict, encoding:  URLEncoding.httpBody, headers: header ).responseJSON { (response:DataResponse<Any>) in
            WebServiceClass.hideLoader(view: self.view)
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                if let json = response.result.value as? [String: Any] {
                    print("JSON: \(json)") // serialized json response
                    if let id = json["id"] as? String{
                        self.subscriptionId = id
                        
                        self.donateAmountCheck = false
                        self.toDonate = ""
                        self.toDonate  = "0"
                        self.toDonateAmount.text = "$" + self.toDonate
                        self.dollarUntilLabel.text = "$5 until $5"
                        
                        //self.hitAllSpareChangeAPI(offset: 0)      ^^^
                        
                        if let txnId = json["balance_transaction"] as? String{
                            if let source = json["source"] as? [String: Any]{
                                if let cardNum = source["last4"] as? String{
                                    if let cardId = source["id"] as? String{
                                        //self.saveSpareChangeDonationWithApplePayDetail(card_num: cardNum, card_id: cardId, txnId: txnId, VC: VC, completion: completion)
                                    }
                                }
                            }
                        }
                        
                    }else{
                        resultFailure()
                    }
                }else{
                    resultFailure()
                }
                
            case.failure(let error):
                print("Not Success",error)
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
//    func saveSpareChangeDonationWithApplePayDetail(card_num: String, card_id: String, txnId: String, VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)){
//
//        guard let userInfoModel = Methods.sharedInstance.getloginData()else{return}
//        let user_id      = userInfoModel.id as? String ?? ""
//        let amount       = toDonate
//        //let approved_id  = approved_idArray.joined(separator: ",")
//
//        let paramsStr = "user_id=\(user_id)&amount=\(amount)&donation_type=\(0)&hbcu=\()"
//
//        print(paramsStr)
//
//        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL +  Constants.APIs.paymentsDoneWithAppleI, method: "POST", params: paramsStr) { (success, response, errorMsg) in
//            WebServiceClass.hideLoader(view: self.view)
//            if success == true{
//                if let jsonResult = response as? Dictionary<String, AnyObject>{
//                    print(jsonResult)
//                    if let responseCode = jsonResult["error"] as? Bool ,responseCode{
//                        //self.showAlert(jsonResult["message"] as! String)
//                        resultFailure()
//                    }else{
//                        //                        let view = AlertView()
//                        //                        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
//                        //                        view.layer.cornerRadius = 0.2
//                        //                        view.alertView.layer.cornerRadius = 10
//                        //                        view.alertView.layer.borderColor = UIColor.black.cgColor
//                        //                        self.view.addSubview(view)
//
//                        completion(PKPaymentAuthorizationStatus.success)
//                        return
//                    }
//                }else{
//                    print("Data is not in proper format")
//                    resultFailure()
//                }
//
//            }else{
//                //self.showAlert("Something is wrong. Please try again later.")
//                //print("Rejected - 500")
//                resultFailure()
//            }
//        }
//
//        func resultFailure(){
//            WebServiceClass.hideLoader(view: self.view)
//            completion(PKPaymentAuthorizationStatus.failure)
//            return
//        }
//
//    }
    
}
