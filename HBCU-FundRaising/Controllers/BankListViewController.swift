//
//  BankListViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 11/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class BankListViewController: UIViewController, UIWebViewDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.title = "Select Your Bank"
        self.loadWebView()
    }
    override func viewWillAppear(_ animated: Bool){
        self.loadWebView()
    }
    func loadWebView(){
        let url = Constants.APIs.openBankListURL + Constants.APIs.plaidKeyWithAnd + Constants.APIs.product + Constants.APIs.apiVersion + Constants.APIs.env + Constants.APIs.clientName
        _ = URL(string: url + "Fundraising&selectAccount=false&webhook=http://requestb.in")
    }
    func webViewDidFinishLoad(_ webView: UIWebView){
        if let text = webView.request?.url?.absoluteString{
            print(text)
        }
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let text = request.url?.absoluteString.components(separatedBy: "?") {
            print(text)
        }
        return true
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        if let text = webView.request?.url?.absoluteString{
            print(text)
        }
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
         print(error)
    }
}

