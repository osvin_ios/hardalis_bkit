//
//  ForgotPasswordEmailViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 01/10/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ForgotPasswordEmailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func okBtnActn(_ sender: Any) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is LoginViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }

    }
}
