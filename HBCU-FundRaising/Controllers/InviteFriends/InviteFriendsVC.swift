//
//  InviteFriendsVC.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 28/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class InviteFriendsVC: UIViewController {

    @IBOutlet weak var invitationLink: UITextField!
    
    var invitationURL : URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let invitationLink = userInfoModel.unique_url
        invitationURL = URL(fileURLWithPath: invitationLink!)
        self.invitationLink.text = invitationLink
        self.title = "Invite Friends"
        self.barButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func barButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
    }
    
    @objc func leftBar(sender: UIButton) {
        
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
        }
    }
    
    @IBAction func viewCompleteRulesButton(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: "http://iheartmyhbcu.org/pages/refer.php")!)
    }
    
    @IBAction func inviteFriendsAction(_ sender: UIButton) {
        
        let vc = UIActivityViewController(activityItems: [invitationURL], applicationActivities: [])
        present(vc, animated: true)
    }
    
}
