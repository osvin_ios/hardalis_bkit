//
//  SelectHBCUOrgViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 07/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage

extension CAGradientLayer {
    class func gradientLayerForBounds(bounds: CGRect) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [UIColor.red, UIColor.black, UIColor.green]
        layer.startPoint = CGPoint(x: 0.0, y: 0.5)
        layer.endPoint = CGPoint(x: 1.0, y: 0.5)
        return layer
    }
}

class SelectHBCUOrgViewController: UIViewController {
    
    @IBOutlet weak var hbcuOrgListCollectionView: UICollectionView!
    var hcbuPostArray         = [HBCUOrganizationListModel]()
    var selectedHBCUPostArray = [HBCUOrganizationListModel]()
    var completionBlock: CallBackMethods.SourceCompletionHandler?
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customiseNavigationBar()
        
        if Reachability.isConnectedToNetwork() == true {
            self.getHBCUOrgListAPI()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    
    //MARK:- set navigation bar background color and buttons
    func customiseNavigationBar(){
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Select Organization"
        let saveItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(saveAction))
        navigationItem.rightBarButtonItem = saveItem
    }
    
    @objc func saveAction(){
        
        if (selectedHBCUPostArray.count < 1){
            self.showAlert("Please select 1 HBCU")
        }
        
        guard let cB = self.completionBlock else { return }
        cB(self.selectedHBCUPostArray as AnyObject)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    //MARK:- hit api
    func getHBCUOrgListAPI(){
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        if let userId = userInfoModel.id as? Int{
            let paramsStr = "user_id=\(userId)"
                let urlName = Constants.APIs.baseURL + "getOrganization?" + paramsStr
            
            WebServiceClass.dataTask(urlName:  urlName, method: "GET", params: "") { (success, response, errorMsg) in
                WebServiceClass.hideLoader(view: self.view)
                if success == true {
                    if let jsonResult = response as? Dictionary<String, AnyObject> {
                        if let responeCode = jsonResult["error"] as? Bool, responeCode {
                            print("Error status - true, signUp unsuccessful")
                        } else {
                            if let result = jsonResult["result"] as? [Dictionary<String,AnyObject>]{
                                if let hcbuPost = Mapper<HBCUOrganizationListModel>().mapArray(JSONObject: result){
                                    self.hcbuPostArray = hcbuPost
                                }
                            }
                        }
                    } else {
                        print("Data is not in proper format")
                    }
                    DispatchQueue.main.async {
                        self.hbcuOrgListCollectionView.reloadData()
                    }
                } else {
                    print("Data is not in proper format")
                }
            }
        }
       
    }
    
}

extension SelectHBCUOrgViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return hcbuPostArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectHBCUCollectionViewCell", for: indexPath) as! hbcuOrganizationCollectionViewCell
        let value = hcbuPostArray[indexPath.item]
        let logoImage = Constants.APIs.imageBaseURL + value.logo!
        cell.hbcuLogoImage.sd_setImage(with: URL(string: logoImage ), placeholderImage: nil)
        cell.hbcuTitle.text = value.title ?? ""
        
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        
        if self.selectedHBCUPostArray.contains(where: { $0.id == self.hcbuPostArray[indexPath.row].id }) {
            cell.selectedCellImage.isHidden = false
        } else {
            cell.selectedCellImage.isHidden = true
        }
        
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.selectedHBCUPostArray.contains(where: { $0.id == self.hcbuPostArray[indexPath.row].id }) {
            
            if let selectedIndex = self.selectedHBCUPostArray.index(where: { $0.id ==  self.hcbuPostArray[indexPath.row].id}) {
                 self.selectedHBCUPostArray.remove(at: selectedIndex)
            }
        } else {
            if selectedHBCUPostArray.count == 1 {
                self.showAlert("Oops!You can select only 1 organization.")
            } else {
                self.selectedHBCUPostArray.append(self.hcbuPostArray[indexPath.row])
            }
        }
        collectionView.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0 - 2
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 1, 1, 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
}

