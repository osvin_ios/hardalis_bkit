//
//  SelectHBCUCollectionViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 07/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SDWebImage

class SelectHBCUCollectionViewController: UIViewController {
    
    @IBOutlet weak var hbcuCollectionView: UICollectionView!
    var hcbuPostArray = [HBCUListModel]()
    var selectedHBCUPostArray = [HBCUListModel]()
    var searchbarState : Bool = false
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    //search
    var searchResultArray : [String]?
    var filterArray = [HBCUListModel]()

    var completionBlock: CallBackMethods.SourceCompletionHandler?
    
    @IBOutlet weak var topView: MKGradientView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            self.getHBCUListAPI()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        customiseNavigationBar()
        
        self.navigationController?.isNavigationBarHidden = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false

    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)  {
        searchBar.resignFirstResponder()
        
    }
    
    //MARK:- set navigation bar background color and buttons
    func customiseNavigationBar() {

        topView.type = .linear
        topView.colors = [UIColor.red, UIColor.black, UIColor.green]
        topView.startPoint = CGPoint(x: 0.0, y: 0.5)
        topView.endPoint = CGPoint(x: 1.0, y: 0.5)
        
    }
    
    @IBAction func saveAction(_ sender: UIButton){
        
        if (selectedHBCUPostArray.count < 5){
            self.showAlert("Please select 5 HBCUs")
        }
        if selectedHBCUPostArray.count == 5 {
            guard let cB = self.completionBlock else { return }
            cB(self.selectedHBCUPostArray as AnyObject)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- api function
    func getHBCUListAPI(){
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let userId = userInfoModel.id as? Int
        let paramsStr = "user_id=\(userId ?? 0)"
        let urlName = Constants.APIs.baseURL + "getHbcu?" + paramsStr
        
        WebServiceClass.dataTask(urlName:  urlName, method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        print("Error status - true, signUp unsuccessful")
                    } else {
                        
                        if let result = jsonResult["result"] as? [Dictionary<String,AnyObject>]{
                            if let hcbuPost = Mapper<HBCUListModel>().mapArray(JSONObject: result){
                                self.hcbuPostArray = hcbuPost
                            }
                        }
                    }
                } else {
                    print("Data is not in proper format")
                }
                DispatchQueue.main.async {
                    self.hbcuCollectionView.reloadData()
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
}
extension SelectHBCUCollectionViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if searchbarState == false {
            return hcbuPostArray.count
        } else {
            return filterArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hbcuCell", for: indexPath) as! hbcuCell
        
        if searchbarState == false {
            let value = hcbuPostArray[indexPath.item]
            let logoImage = Constants.APIs.imageBaseURL + value.logo!
            cell.hbcuLogoImage.sd_setImage(with: URL(string: logoImage ), placeholderImage: nil)
            
            if self.selectedHBCUPostArray.contains(where: { $0.id == self.hcbuPostArray[indexPath.row].id }) {
                cell.selectedCellImage.isHidden = false
            } else {
                cell.selectedCellImage.isHidden = true
            }
            
            cell.hbcuTitle.text = value.title ?? ""
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 1
            
            return cell
        } else {
            let value = filterArray[indexPath.item]
            let logoImage = Constants.APIs.imageBaseURL + value.logo!
            cell.hbcuLogoImage.sd_setImage(with: URL(string: logoImage ), placeholderImage: nil)
            
            if self.selectedHBCUPostArray.contains(where: { $0.id == self.filterArray[indexPath.row].id }) {
                cell.selectedCellImage.isHidden = false
            } else {
                cell.selectedCellImage.isHidden = true
            }
            
            cell.hbcuTitle.text = value.title ?? ""
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 1
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if searchbarState == true {
            if self.selectedHBCUPostArray.contains(where: { $0.id == self.filterArray[indexPath.row].id }) {
                if let selectedIndex = self.selectedHBCUPostArray.index(where: { $0.id ==  self.filterArray[indexPath.row].id}) {
                    self.selectedHBCUPostArray.remove(at: selectedIndex)
                }
            } else {
                if selectedHBCUPostArray.count == 5 {
                    self.showAlert("Oops! You can only select 5 schools.")
                } else {
                    self.selectedHBCUPostArray.append(self.filterArray[indexPath.row])
                }
            }
            collectionView.reloadData()
        } else {
            if self.selectedHBCUPostArray.contains(where: { $0.id == self.hcbuPostArray[indexPath.row].id }) {
                if let selectedIndex = self.selectedHBCUPostArray.index(where: { $0.id ==  self.hcbuPostArray[indexPath.row].id}) {
                    self.selectedHBCUPostArray.remove(at: selectedIndex)
                }
            } else {
                if selectedHBCUPostArray.count == 5 {
                    self.showAlert("Oops! You can only select 5 schools.")
                } else {
                    self.selectedHBCUPostArray.append(self.hcbuPostArray[indexPath.row])
                }
            }
            collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0 - 2
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 1, 1, 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}
extension SelectHBCUCollectionViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchbarState = true
        filterArray = hcbuPostArray.filter({ ($0.title?.contains(searchText))!})
        print(filterArray)
        self.hbcuCollectionView.reloadData()
        if searchText == "" {
            searchbarState = false
            self.dismissKeyboard()
            self.hbcuCollectionView.reloadData()
        }
    }
    
}

