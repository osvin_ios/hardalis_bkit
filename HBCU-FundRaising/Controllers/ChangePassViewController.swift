//
//  ChangePassViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 28/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ChangePassViewController: UIViewController {

    @IBOutlet weak var currentPsssText: UITextField!
    @IBOutlet weak var newPassText: UITextField!
    @IBOutlet weak var confirmPassText: UITextField!
    @IBOutlet weak var saveChanges: ButtonDesign!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Change Password"
    }
    
    @IBAction func saveChanges(_ sender: UIButton) {
        
        if currentPsssText.text?.count == 0{
            self.showAlert("Please Enter Current Password")
        } else if newPassText.text?.count == 0 {
            self.showAlert("Please Enter New Password")
        } else if confirmPassText.text?.count == 0 {
            self.showAlert("Please Enter Confirm Password")
        } else {
            if newPassText.text == confirmPassText.text {
                if Reachability.isConnectedToNetwork() == true {
                    self.changePasswordAPI()
                } else {
                    self.showAlert(Constants.commomInternetmsz.checkInternet)
                }
            }  else {
                self.showAlert("Password donot match")
            }
        }
    }
    
    func changePasswordAPI() {
        WebServiceClass.showLoader(view: self.view)
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        if let userId = userInfoModel.id as? String{
            let new_password = newPassText.text ?? ""
            let old_password = currentPsssText.text ?? ""
            let paramsStr = "user_id=\(userId)&old_password=\(old_password)&new_password=\(new_password)"
            
            let urlName = Constants.APIs.baseURL + "changePassword"
            
            WebServiceClass.dataTask(urlName:  urlName, method: "POST", params: paramsStr) { (success, response, errorMsg) in
                WebServiceClass.hideLoader(view: self.view)
                if success == true {
                    if let jsonResult = response as? Dictionary<String, AnyObject> {
                        print(jsonResult)
                        self.showAlert((jsonResult["message"] as? String)!)
                    }
                } else {
                    print("Data is not in proper format")
                }
            }
        }
    }
}
