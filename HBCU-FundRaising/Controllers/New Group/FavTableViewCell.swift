//
//  FavTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 19/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class FavTableViewCell: UITableViewCell {

    @IBOutlet weak var hbcuImageView: UIImageView!
    @IBOutlet weak var hbcuTitle: UILabel!
    @IBOutlet weak var hbcuDonationPercentage: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    
    
}
