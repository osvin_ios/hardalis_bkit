//
//  LinkedCardViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 24/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class LinkedCardViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
        self.title = "Linked  Card"
      
    }
    
    @objc func backAction() {
        
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension LinkedCardViewController {
    
   
}

