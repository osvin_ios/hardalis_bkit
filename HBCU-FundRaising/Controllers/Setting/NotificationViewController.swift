//
//  NotificationViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 10/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {

    @IBOutlet weak var toggleBtnOutlet: UIButton!
    @IBOutlet weak var notificationBorder: UIView!
    var status = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if toggleBtnOutlet.imageView?.image == #imageLiteral(resourceName: "ic_toggle_on"){
             self.status = "1"
        }else{
            self.status = "0"
        }
        self.notificationBorder.dropShadow()
        if Reachability.isConnectedToNetwork() == true{
            self.hitNotificationAPI()
        }else{
             self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }

    @IBAction func toggleButon(_ sender: UIButton){
        
        sender.isSelected = !sender.isSelected
        
        if toggleBtnOutlet.imageView?.image == #imageLiteral(resourceName: "ic_toggle_on") {
            self.status = "0"
            self.hitNotificationAPI()
            toggleBtnOutlet.setImage(#imageLiteral(resourceName: "ic_toggle_off"), for: .normal)
            toggleBtnOutlet.isSelected = false
        } else {
            self.status = "1"
            self.hitNotificationAPI()
            toggleBtnOutlet.setImage(#imageLiteral(resourceName: "ic_toggle_on"), for: .normal)
            toggleBtnOutlet.isSelected = true
        }
        self.showAlert("Notification status updated successfully.")
    }
    
    func hitNotificationAPI(){

        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id        = userInfoModel.id as? String 
        let status         = "0"

        let paramsStr = "user_id=\(user_id ?? "0")&status=\(status)"
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + "pushNotifications", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject>{
                    
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
}
