//
//  BlockedUserTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 14/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class BlockedUserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var dummyProfileImage: ImageViewDesign!
    @IBOutlet weak var blockedPersonName: UILabel!
    
    @IBOutlet var blockUnblockBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func unblockBtnActn(_ sender: UIButton) {
        
       // self.blockUnblockAPI()
    }
}
