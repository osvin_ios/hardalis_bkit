//
//  PauseDonationViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 05/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class PauseDonationViewController: UIViewController {

    @IBOutlet weak var spareChangeToggle: UIButton!
    @IBOutlet weak var reoccuringToggle: UIButton!
    @IBOutlet weak var reocuuringView: UIView!
    @IBOutlet weak var spareChangeView: UIView!
    var spare_change : String?
    var reoccurring  : String?
    var isSelected : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Pause Donations"
        self.spareChangeView.layer.cornerRadius = 5
        self.spareChangeView.layer.borderColor = UIColor.lightGray.cgColor
        self.spareChangeView.layer.borderWidth = 0.5
        self.reocuuringView.layer.cornerRadius = 5
        self.reocuuringView.layer.borderColor = UIColor.lightGray.cgColor
        self.reocuuringView.layer.borderWidth = 0.5
        
        if Reachability.isConnectedToNetwork() == true {
            self.getUserDetail()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }

    @IBAction func reocuuringToggleAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if reoccuringToggle.isSelected == true {
            self.isSelected = true
            self.reoccurring = "0"
            self.pauseDonationAPI()
        } else {
            self.isSelected = false
            self.reoccurring = "1"
            self.pauseDonationAPI()
        }
    }
    @IBAction func spareChangeAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if spareChangeToggle.isSelected == true {
            self.isSelected = true
            self.spare_change = "0"
            self.pauseDonationAPI()
        } else {
            self.isSelected = false
            self.spare_change = "1"
            self.pauseDonationAPI()
        }
    }
    
    func getUserDetail(){
        
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        if let user_id        = userInfoModel.id as? String {
            let urlName        = Constants.APIs.baseURL + "getUserDetails?user_id=" + user_id
            WebServiceClass.dataTask(urlName: urlName , method: "GET", params: "") { (success, response, errorMsg) in
                WebServiceClass.hideLoader(view: self.view)
                if success == true {
                    if let jsonResult    = response as? Dictionary<String, AnyObject> {
                        if let result = jsonResult["result"] as? Dictionary<String, AnyObject>{
                            self.reoccurring = result["isActivereoccuring"] as? String
                            isActivereoccuring = (result["isActivereoccuring"] as? String)!
                            if self.reoccurring == "1"{
                                self.reoccuringToggle.isSelected = false
                            } else {
                                self.reoccuringToggle.isSelected = true
                            }
                            self.spare_change = result["isActivesparechange"] as? String
                            isActivesparechange = (result["isActivesparechange"] as? String)!
                            if self.spare_change == "1"{
                                self.spareChangeToggle.isSelected = false
                            } else {
                                self.spareChangeToggle.isSelected = true
                            }
                        }
                    }
                }else {
                    print("Data is not in proper format")
                }
            }
        }
    }
    
    func pauseDonationAPI()
    {
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }

        let user_id        = userInfoModel.id as? String
        let spare_change   = self.spare_change ?? "0"
        let reoccurring    = self.reoccurring ?? "0"
        
        let paramsStr = "user_id=\(user_id ?? "")&spare_change=\(spare_change)&reoccurring=\(reoccurring)"

        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + "pausedonations", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        self.showAlert((jsonResult["message"] as? String)!)
                    }
                    else {
                        print(jsonResult)
                        if self.isSelected == true {
                            self.showAlert("Transactions have been successfully paused")
                        } else {
                            self.showAlert("Transactions have been successfully unpaused")
                        }
                        
                    }
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
}
