//
//  FeedbackViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 06/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {

    @IBOutlet weak var excellentButton: UIButton!
    @IBOutlet weak var goodButton: UIButton!
    @IBOutlet weak var notGoodButton: UIButton!
    @IBOutlet weak var issueView: UIView!
    @IBOutlet weak var subjectView: UIView!
    @IBOutlet weak var issueTextview: UITextView!
    @IBOutlet weak var subjectTextfield: UITextField!
    
    var comment : String?
    var rating : String?
    var subject : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.subjectView.dropShadow()
        self.issueView.dropShadow()
        self.title = "Feedback"
        self.excellentButton.isSelected = true
        self.excellentButton.isUserInteractionEnabled = false
    }
    
    @IBAction func sendButton(_ sender: UIButton) {
        if self.subjectTextfield.text == "" || self.subjectTextfield.text == nil {
            self.showAlert("Enter subject")
        } else if issueTextview.text == "" || issueTextview.text == nil {
            self.showAlert("Please enter some suggestions")
        } else {
           if  Reachability.isConnectedToNetwork() == true{
                self.hitFeedbackAPI()
            } else {
                self.showAlert(Constants.commomInternetmsz.checkInternet)
            }
        }
    }
    
    @IBAction func selecExcellentButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if self.excellentButton.isSelected == true {
            self.goodButton.isSelected = false
            self.notGoodButton.isSelected = false
            self.excellentButton.isUserInteractionEnabled = false
            self.notGoodButton.isUserInteractionEnabled = true
            self.goodButton.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func goodButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if self.goodButton.isSelected == true {
            self.excellentButton.isSelected = false
            self.notGoodButton.isSelected = false
            self.excellentButton.isUserInteractionEnabled = true
            self.goodButton.isUserInteractionEnabled = false
            self.notGoodButton.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func notGood(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if self.notGoodButton.isSelected == true {
            self.goodButton.isSelected = false
            self.excellentButton.isSelected = false
            self.notGoodButton.isUserInteractionEnabled = false
            self.excellentButton.isUserInteractionEnabled = true
            self.goodButton.isUserInteractionEnabled = true
        }
    }
    
    func hitFeedbackAPI(){
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        if goodButton.isSelected == true {
            self.rating = "good"
        } else if excellentButton.isSelected == true {
            self.rating = "exceelent"
        } else if notGoodButton.isSelected == true {
            self.rating = "notgood"
        }
        
        
        let user_id  = userInfoModel.id as? String 
        let comment = self.issueTextview.text ?? ""
        let rating  = self.rating ?? ""
        let subject = self.subjectTextfield.text ?? ""
        
        let paramsStr = "user_id=\(user_id ?? "0")&comment=\(comment)&rating=\(rating)&subject=\(subject )"
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + "feedback", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        self.showAlert((jsonResult["message"] as? String)!)
                    }
                    else {
                        print(jsonResult)
                        self.showAlert("Thanks for your valuable feedback")
                        DispatchQueue.main.async {
                            self.subjectTextfield.text = ""
                            self.issueTextview.text = ""
                        }
                    }
                }} else {
                print("Data is not in proper format")
            }
        }
    }
}
