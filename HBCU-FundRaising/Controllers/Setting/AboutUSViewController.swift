//
//  AboutUSViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 09/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class AboutUSViewController: UIViewController {

    @IBOutlet weak var aboutUsWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let url = URL(string: "http://iheartmyhbcu.org/pages/about-us.php") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }

}
