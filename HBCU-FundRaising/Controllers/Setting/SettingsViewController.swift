//
//  SettingsViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 22/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var changePinButton: UIButton!
    @IBOutlet weak var myProfileButton: UIButton!
    @IBOutlet weak var supportView: UIView!
    @IBOutlet weak var myAccountView: UIView!
    @IBOutlet weak var adminView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        borderColor(view: supportView)
        borderColor(view: myAccountView)
        borderColor(view: adminView)
        
        self.title = "Settings"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
        self.navigationItem.hidesBackButton = true
    }
    
    func borderColor(view : UIView) {
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.borderWidth = 0.4
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        borderColor(view: supportView)
        borderColor(view: myAccountView)
        borderColor(view: adminView)
        self.title = "Settings"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
        self.navigationItem.hidesBackButton = true
    }
    
    @objc func leftBar(sender: UIButton) {
        self.menuContainerViewController.toggleLeftSideMenuCompletion{
        }
    }
    
    @IBAction func changePinAction(_ sender: UIButton) {
        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "GeneratePinViewController") as! GeneratePinViewController
            vc.mode = .changePin
            currentNavVC.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func linkedCardActn(_ sender: Any){
        self.performSegue(withIdentifier: "settingToLinkCard", sender: self)
    }
    
    @IBAction func linkedCheckAccount(_ sender: Any){
        self.performSegue(withIdentifier: "settingToLinkAcount", sender: self)
    }
    
    @IBAction func statementsBtnActn(_ sender: Any){
        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "StatementsViewController") as! StatementsViewController
            currentNavVC.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func blockedUserBtnActn(_ sender: Any){
//        self.showToast(message: "Module Under Development")
        self.performSegue(withIdentifier: "settingToBlock", sender: self)
    }
    
    @IBAction func favHBCUBtnActn(_ sender: Any){
        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FavHBCUViewController") as! FavHBCUViewController
            currentNavVC.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func aboutUsBtnActn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AboutUSViewController") as! AboutUSViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func myProfileButtonaction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "EditProfile", sender: self)
    }
    
    @IBAction func changePassword(_ sender: UIButton){
        guard let userDetil = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let signUp_type = userDetil.signup_type
        if signUp_type == "facebook" {
            
            self.showToast(message: "You are looged in from facebook. You cannot change password.")
        } else {
            
            self.performSegue(withIdentifier: "ChangePass", sender: self)
        }
    }

    @IBAction func writeReview(_ sender: UIButton) {
        rateApp(appId: "osvin.HBCU-FundRaising") { success in
            print("RateApp \(success)")
        }
    }
    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destViewController: SignUpStep1ViewController = segue.destination as? SignUpStep1ViewController, segue.identifier == "EditProfile" {
            
            destViewController.enumCase = .EditProfile
        }
    }
}

