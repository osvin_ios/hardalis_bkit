//
//  LinkedAccountTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 24/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class LinkedAccountTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainAccountView: ViewDesign!
    @IBOutlet weak var bankAccountImage: UIImageView!
    @IBOutlet weak var bankNamelabel: UILabel!
    @IBOutlet weak var editButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func editButtonAction(_ sender: Any) {
        
        
    }
}
