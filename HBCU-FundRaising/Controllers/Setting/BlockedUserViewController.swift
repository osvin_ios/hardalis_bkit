//
//  BlockedUserViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 14/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class BlockedUserViewController: UIViewController {

    @IBOutlet weak var titlreLabel: UILabel!
    @IBOutlet var blockUserListTableView: UITableView!
    
    var blockUserData = [BlockedUserListModel]()
    var blockUnblockData = [BlockedUserListModel]()
    var array = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.blockListAPI()
        self.navigationController?.navigationBar.isHidden = true
        
        let userDefaults = UserDefaults.standard
        guard let data = userDefaults.object(forKey: "myArrayKey") as? [String] else {
            return
        }
        array = data
        blockUserListTableView.reloadData()
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.blockListAPI()
        self.navigationController?.navigationBar.isHidden = true
        
        let userDefaults = UserDefaults.standard
        guard let data = userDefaults.object(forKey: "myArrayKey") as? [String] else {
            return
        }
        array = data
        blockUserListTableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnActn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    // Blocked User List API
    
    func blockListAPI() {
        
        WebServiceClass.showLoader(view: self.view)
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let paramsStr = "from_id=\(userInfoModel.id!)"
        print(paramsStr)
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + Constants.APIs.blocked_users, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            
            WebServiceClass.hideLoader(view: self.view)
            
            if success == true {
                
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    
                    print(jsonResult)
                    
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        
                        if responeCode == false {
                            
                            if let blockMessage = jsonResult["message"] as? String {
                                
                                if let blockListData = jsonResult["result"] as? [Dictionary<String,AnyObject>] {
                                   
                                    if let blockInfo = Mapper<BlockedUserListModel>().mapArray(JSONObject: blockListData) {
                                        
                                        self.blockUserData = blockInfo
                                        print(self.blockUserData)
                                    }
                                    if self.blockUserData.count == 0 {
                                        
                                        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
                                        label.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
                                        label.textAlignment = .center
                                        label.textColor = UIColor.lightGray
                                        label.text = "No Blocked User"
                                        self.view.addSubview(label)
                                    }
                                    DispatchQueue.main.async {
                                        self.blockUserListTableView.reloadData()
                                    }
                            } else {
                                print("something went wrong 1")
                            }
                        }
                    } else {
                        print("Something went wrong 2")
                    }
                    
                } else {
                    print("Something went wrong 3")
                }
                
            } else {
                    print("Somthing went wrong 4")
                }
            }
        }
    }
    // Block Unblock User API
    
     func blockUnblockAPI(){

        WebServiceClass.showLoader(view: self.view)

        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
    
        var to_id = blockUserData[0].to_id
        let user_id = userInfoModel.id as? String
        let paramsStr = "from_id=\(user_id ?? "")&to_id=\(to_id!)"
        print(paramsStr)
    
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + Constants.APIs.block_unBlock_User, method: "POST", params: paramsStr) { (success, response, errorMsg) in

            WebServiceClass.hideLoader(view: self.view)

            if success == true {

                if let jsonResult = response as? Dictionary<String, AnyObject> {

                    print(jsonResult)

                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {

                        if responeCode == false {

                            if let blockMessage = jsonResult["message"] as? String {

                                if let blockListData = jsonResult["result"] as? [Dictionary<String,AnyObject>] {

                                    if let blockInfo = Mapper<BlockedUserListModel>().mapArray(JSONObject: blockListData) {

                                        self.blockUserData = blockInfo
                                        print(self.blockUserData)
                                    }
                                    DispatchQueue.main.async {
                                        self.blockListAPI()
                                        self.blockUserListTableView.reloadData()
                                    }
                                }
                            } else {
                                print("something went wrong 1")
                            }
                        }
                    } else {
                        print("Something went wrong 2")
                    }
                } else {
                    print("Something went wrong 3")
                }
            } else {
                print("Somthing went wrong 4")
            }
        }
    }
}

extension BlockedUserViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return blockUserData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "BlockedUserTableViewCell", for: indexPath) as! BlockedUserTableViewCell
        
        cell.blockedPersonName.text = (blockUserData[indexPath.row].first_name ?? "") + (  blockUserData[indexPath.row].last_name ?? "")
     
        let imagebaseURl = Constants.APIs.imageBaseURL + (blockUserData[indexPath.row].profile ?? "")
        let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        cell.dummyProfileImage.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
        cell.blockUnblockBtn.addTarget(self, action: #selector(self.selectorMethod(_:)), for: .touchUpInside)
        cell.blockUnblockBtn.setTitle("Unblock", for: .normal)
        
        if self.blockUserData.count == 0 {
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
            label.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
            label.textAlignment = .center
            label.textColor = UIColor.lightGray
            label.text = "No Blocked User"
            self.view.addSubview(label)
        }
//        if blockUserData[indexPath.row].message == "User blocked successfully"  {
//            self.showToast(message: "User UnBlock Successfully")
//        }else{
//            cell.blockUnblockBtn.setTitle("Block", for: .normal)
//            self.showToast(message: "User Unblock Successfully")
//        }
        return cell
    }
    @objc func selectorMethod(_ Sender : UIButton){
        
        self.alertFunctionsWithCallBack(title: "Are you sure you want to Unblock this User") { (isTrue) in
            if isTrue.boolValue {
                // Check network connection.
                if Reachability.isConnectedToNetwork() == true {
                    self.blockUnblockAPI()
                } else {
                    self.showAlertWithoutCallBack(messageTitle: AKErrorHandler.CommonErrorMessages.NO_INTERNET_AVAILABLE)
                }
            }
        }
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        blockUserData.remove(at: indexPath.row)
        self.blockListAPI()
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            blockUserData.remove(at: indexPath.row)
            blockUserListTableView.deleteRows(at: [indexPath as IndexPath], with: .automatic)
            let userDefaults = UserDefaults.standard
            userDefaults.set(blockUserData, forKey: "myArrayKey")
        }
    }
}


