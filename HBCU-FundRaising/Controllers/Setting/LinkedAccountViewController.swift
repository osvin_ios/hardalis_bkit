//
//  LinkedAccountViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 24/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class LinkedAccountViewController: UIViewController {
    
    @IBOutlet weak var linkedAccountTableView: UITableView!
    //    var institution_id : String?
    //    var public_key : String?
    var linkedAccountName : String?
    var linkedAccountLogo : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
        self.title = "Linked Account"
        
        // Do any additional setup after loading the view.
    }

    @objc func backAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
        self.title = "Linked Accounts"
        hitLinkedAccountsApi()
    }

    @objc func editLinkedAccount(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BankPlaidViewController") as! BankPlaidViewController
        vc.checkPreviousLinkedAccountVCis = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension LinkedAccountViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.linkedAccountName != nil && self.linkedAccountName != ""{
            return 1
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "LinkedAccountTableViewCell", for: indexPath) as! LinkedAccountTableViewCell
        
        cell.bankAccountImage.image = self.linkedAccountLogo
        cell.bankNamelabel.text = self.linkedAccountName
        cell.editButton.addTarget(self, action: #selector(editLinkedAccount(_:)), for: .touchUpInside)
        cell.editButton.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
}

extension LinkedAccountViewController {
    
    func hitLinkedAccountsApi() {
        
        let publicToken = "c74afb41cee71cd888636cc90426fb"
        let institution_id = UserDefaults.standard.value(forKey: "institutionalId") as? String
        
        var dict = Dictionary<String, Any>()
        dict = ["include_display_data" : true]
        
        let parameters = [
            "institution_id": institution_id ?? "" ,
            "public_key" : publicToken ?? "",
            "options": dict
            ] as [String : Any]
        
        
        let url = "https://development.plaid.com/institutions/get_by_id"    //Live
        //let url = "https://sandbox.plaid.com/institutions/get_by_id"
        
        print(url)
        let header = [
            "Content-Type" : "application/json",
            ]
        
        WebServiceClass.showLoader(view: self.view)
        
        Alamofire.request(url, method: .post, parameters : parameters,  encoding: JSONEncoding.default, headers: header)
            .responseJSON { response in
                WebServiceClass.hideLoader(view: self.view)
                switch(response.result) {
            case .success(_):
                
                if let jsonObject = response.result.value as? Dictionary<String, AnyObject> {
                    print(jsonObject)
                    if let bankDetails = jsonObject["institution"] as? Dictionary<String, AnyObject> {
                        self.linkedAccountName = bankDetails["name"] as? String
                        self.linkedAccountLogo =  self.base64Convert(base64String: bankDetails["logo"] as? String)
                        
                        //DispatchQueue.main.async {
                        self.linkedAccountTableView.reloadData()
                        //}
                        
                    }
                }
            case .failure(_):
                WebServiceClass.hideLoader(view: self.view)
                //self.showAlertView(title: "", message: "Error")
                break
                
            }
        }
    }
    
    //convert
    func base64Convert(base64String: String?) -> UIImage{
        
        if (base64String?.isEmpty)! {
            return #imageLiteral(resourceName: "no_image_found")
        }else {
            // !!! Separation part is optional, depends on your Base64String !!!
            let dataDecoded : Data = Data(base64Encoded: base64String!, options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            return decodedimage ?? #imageLiteral(resourceName: "ic_dummy_card")
        }
        
    }
}

