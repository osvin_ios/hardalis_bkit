//
//  FavHBCUViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 19/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class FavHBCUViewController: UIViewController {
    
    @IBOutlet weak var favHBCUTableView: UITableView!
    @IBOutlet var footerView: UIView!
    
    var hbcuFavArray = [HBCUListModel]()
    var comingFromCB : Bool = false
    var comingFromAccout : Bool = false
    var goToHome : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        favHBCUTableView.register(UINib(nibName: "FavTableViewCell", bundle: nil), forCellReuseIdentifier: "favCell")
        
        if Reachability.isConnectedToNetwork() == true {
            self.hitAPIToGetFavHBCU()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
        self.title = " My Favorite HBCU"
                self.barButton()
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
        self.navigationController?.navigationBar.setBackgroundImage(image.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_question_mark"), style: .plain, target: self, action: #selector(rightBar(sender:)))
    }
    
    @objc func backAction(){
        if comingFromAccout == true {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            vc.goToHome = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(vc, animated: true)
           // self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func rightBar(sender: UIButton){
        
        self.performSegue(withIdentifier: "HelpscreenSegue", sender: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Reachability.isConnectedToNetwork() == true{
            self.hitAPIToGetFavHBCU()
        }else{
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
        
        self.title = "My Favorite HBCU"
        //        self.barButton()
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
        self.navigationController?.navigationBar.setBackgroundImage(image.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_question_mark"), style: .plain, target: self, action: #selector(rightBar(sender:)))
    }

    func barButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
    }
    
    @objc func leftBar(sender: UIButton) {
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            print("drawer opened")
        }
    }
    
    func hitAPIToGetFavHBCU(){

        WebServiceClass.showLoader(view: self.view)
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let userId = userInfoModel.id as? String
        let paramsStr = "user_id=\(userId ?? "0")"
        let urlName = Constants.APIs.baseURL + "getUserHBCU?" + paramsStr
        
        WebServiceClass.dataTask(urlName:  urlName, method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                    } else {
                        if let result = jsonResult["result"] as? [Dictionary<String,AnyObject>]{
                            if let hcbuPost = Mapper<HBCUListModel>().mapArray(JSONObject: result){
                                self.hbcuFavArray = hcbuPost
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.favHBCUTableView.reloadData()
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
    @IBAction func editPercentageAction(_ sender: UIButton) {
        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EditPercentagesViewController") as! EditPercentagesViewController
            vc.favVC = hbcuFavArray
            currentNavVC.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func clickToDonatePercentageAction(_ sender: UIButton) {
        
        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DonationViewController") as! DonationViewController
            vc.mode = .ComingFromHBCUDonation
            currentNavVC.pushViewController(vc, animated: true)
        }
        
    }
}
extension FavHBCUViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return hbcuFavArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : FavTableViewCell = tableView.dequeueReusableCell(withIdentifier: "favCell", for: indexPath) as! FavTableViewCell
        
        let data = hbcuFavArray[indexPath.row]
        let image = data.logo
        
        let hbcuImage = Constants.APIs.imageBaseURL + image!
        cell.hbcuImageView.sd_setImage(with: URL(string: hbcuImage ), placeholderImage: nil)
        cell.hbcuTitle.text = data.title
        cell.hbcuDonationPercentage.text = (data.donation_percent ?? "") + "%"
        cell.editButton.addTarget(self, action: #selector(goToSelectHBCU), for: .touchUpInside)
        cell.editButton.tag = indexPath.row
        
        if self.comingFromCB == true {
        }
        
        let whiteRoundedView : UIView = UIView(frame: CGRect(x: 10, y: 8, width: self.view.frame.size.width - 20, height: 90))
        
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 2.0
        whiteRoundedView.layer.shadowOffset = CGSize(width: -1, height: 1)
        whiteRoundedView.layer.shadowOpacity = 0.2
        
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubview(toBack: whiteRoundedView)
        return cell
    }
    
    @objc func goToSelectHBCU(_ sender : UIButton){
        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SelectHBCUToDonateVC") as! SelectHBCUToDonateVC
            vc.donateValue = "0"
            vc.enumCases = .SelectHBCU
            vc.selectedId   = hbcuFavArray[sender.tag].id ?? ""
            currentNavVC.pushViewController(vc, animated: true)
            //self.hitAPIToGetFavHBCU()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView()
        let titleLabel : UILabel = UILabel(frame: CGRect(x: self.view.frame.size.width/40, y: 10, width: 110, height: 30))
        titleLabel.text = "Favorite Five"
        titleLabel.font = UIFont(name: "Gotham-Book", size: 14)
        titleLabel.textColor = UIColor.lightGray
        view.addSubview(titleLabel)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 140
    }
}
