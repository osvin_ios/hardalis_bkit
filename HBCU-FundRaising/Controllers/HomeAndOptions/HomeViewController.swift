//
//  HomeViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 13/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class HomeViewController: UIViewController {
        
    @IBOutlet weak var oneTimeDonation: UILabel!
    @IBOutlet weak var reoccuringDonation: UILabel!
    @IBOutlet weak var spareChangeDonation: UILabel!
    @IBOutlet weak var thankYouLabel: UILabel!
    @IBOutlet weak var attributeDonationLabel: UILabel!
    
    var goToHome : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.hidesBackButton = true
        self.title = "Home"
        self.barButton()
        
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
        self.navigationController?.navigationBar.setBackgroundImage(image.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
    }
    
    override func viewWillAppear(_ animated: Bool){
        
        if Reachability.isConnectedToNetwork() == true {
            self.hitApiOfHome()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
        
        guard let userDetil = Methods.sharedInstance.getloginData() else {
            return
        }
        let name: String = userDetil.first_name ?? ""
        let lastName : String = userDetil.last_name ?? ""
        let fullName = name + lastName
        let amount  : String = userDetil.recent_donations ?? "$0"
        thankYouLabel.text = "Thank you, " + fullName  + ", for showing our HBCU's some love!"
        self.attributeDonationLabel.text = amount + ".00"
    }
    
    func barButton(){
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
    }
        
    @objc func leftBar(sender: UIButton) {
        if goToHome == true{
//            self.menuContainerViewController.toggleLeftSideMenuCompletion {
                print("drawer opened")
//            }
        }else{
            self.menuContainerViewController.toggleLeftSideMenuCompletion {
                print("drawer opened")
            }
        }
    }
    
    //get data of home screen
    func hitApiOfHome() {
        guard let userInfoModel = Methods.sharedInstance.getloginData() else{
            return
        }
        
        let apiName        = "getDonationDetails"
        let user_id        = userInfoModel.id as? String
        let paramsStr = "user_id=\(user_id ?? "0")"
      
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + apiName + "?" + paramsStr, method: "GET", params: "") { (success, response, errorMsg) in
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let resultValue = jsonResult["result"] as? Dictionary<String,AnyObject> {
                        if let _ = Mapper<MyAccountDetailsModel>().map(JSONObject: resultValue) {
                            
                            if let amount = resultValue["recent_donations"] as? String {
                                self.attributeDonationLabel.text = "$" + amount
                            }else if let amount = resultValue["recent_donations"] as? Int {
                                self.attributeDonationLabel.text = "$" + "\(amount)" + ".00"
                            }else if let amount = resultValue["recent_donations"] as? Double {
                                self.attributeDonationLabel.text = "$" + "\(amount)"
                            }
                            
                            if let spareChange = resultValue["spare_change"] as? String {
                                self.spareChangeDonation.text = "$" + spareChange
                            }else if let spareChange = resultValue["spare_change"] as? Int {
                                self.spareChangeDonation.text = "$" + "\(spareChange)" + ".00"
                            }else if let spareChange = resultValue["spare_change"] as? Double {
                                self.spareChangeDonation.text = "$" + "\(spareChange)"
                            }
                            
                            if let reocurring = resultValue["reoccurring"] as? String {
                                self.reoccuringDonation.text = "$" + reocurring
                            }else if let reocurring = resultValue["reoccurring"] as? Int {
                                self.reoccuringDonation.text = "$" + "\(reocurring)" + ".00"
                            }else if let reocurring = resultValue["reoccurring"] as? Double {
                                self.reoccuringDonation.text = "$" + "\(reocurring)"
                            }
                            
                            if let oneTime = resultValue["one_time"] as? String {
                                self.oneTimeDonation.text = "$" + oneTime
                            }else if let oneTime = resultValue["one_time"] as? Int {
                                self.oneTimeDonation.text = "$" + "\(oneTime)" + ".00"
                            }else if let oneTime = resultValue["one_time"] as? Double {
                                self.oneTimeDonation.text = "$" + "\(oneTime)"
                            }
                            
                            if let insId = resultValue["ins_id"] as? String {
                                UserDefaults.standard.set(insId, forKey: "institutionalId")
                            }
                            
                        }
                    }
                }
            }else{
                print("Data is not in proper format")
            }
        }
    }
    
    @IBAction func chatBtnActn(_ sender: Any) {
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainChatViewController") as! MainChatViewController
        secondViewController.comeFromHome = "2"
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func raiseMoneyBtnActn(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChallengeSeeAllViewController") as? ChallengeSeeAllViewController
        vc?.checkChallengeType = "0"
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func earnCashBtnActn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChallengeSeeAllViewController") as? ChallengeSeeAllViewController
        vc?.checkChallengeType = "1"
         self.navigationController?.pushViewController(vc!, animated: true)
    }
}
