//
//  EditPercentageCell.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 19/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class EditPercentageCell: UITableViewCell {

    @IBOutlet weak var percentageText: UITextField!
    @IBOutlet weak var hbcuTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
