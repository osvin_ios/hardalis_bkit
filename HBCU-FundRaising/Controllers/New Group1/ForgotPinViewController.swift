//
//  ForgotPinViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 18/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ForgotPinViewController: UIViewController {
    
    @IBOutlet var emailTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        emailTextField.text = userInfoModel.email ?? ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork() {
            self.forgotPinByUserServiceFunction()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
}

extension ForgotPinViewController {
    
    //API to confirm Pin
    func forgotPinByUserServiceFunction() {
        
        guard let userDetil = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let paramsStr = "email=\(emailTextField.text ?? "")&user_id=\(userDetil.id ?? "")"
        
        WebServiceClass.showLoader(view: self.view)

        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + Constants.APIs.forgotPinByUser, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    
                    if let errorCode = jsonResult["error"] as? Int, errorCode == 0 {
                        self.showAlert("Please check your email-id for new pin.")
                    } else {
                        self.showAlert(jsonResult["message"] as? String ?? "")
                    }
                } else {
                    print("Data is not in proper format")
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
}
