//
//  GeneratePinViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 12/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class GeneratePinViewController: UIViewController {
    
    @IBOutlet weak var viewHeightConstant: NSLayoutConstraint!
    @IBOutlet var firstTextField: UITextField!
    @IBOutlet var secondTextField: UITextField!
    @IBOutlet var thirdTextField: UITextField!
    @IBOutlet var forthTextField: UITextField!
    @IBOutlet weak var enterButtonOutlet: UIButton!
    var mode = PinController.generatePin

        //MARK:- View Start
    override func viewDidLoad(){
        super.viewDidLoad()
        
        self.title = "Create PIN"
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
//        self.viewHeightConstant.constant = 20
        
        if mode == .changePin {
            self.title = "Change Pin"
            
            self.enterButtonOutlet.setTitle("Save Changes", for: .normal)
//            self.viewHeightConstant.constant = 0
        }else{
             self.navigationController?.navigationBar.setBackgroundImage(image.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
        }
    }

    override func viewWillAppear(_ animated: Bool){
        if mode == .changePin {
            self.enterButtonOutlet.setTitle("Save Changes", for: .normal)
        }
    }

    @objc func backAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //IBAction
    @IBAction func enterPinButton(_ sender: UIButton) {
        
        if mode == .changePin {
            if firstTextField.text == "" || secondTextField.text == "" || thirdTextField.text == "" || forthTextField.text == "" {
                    self.showAlert("Please enter 4 digit pin to proceed further.")
            } else {
                if Reachability.isConnectedToNetwork() {
                    self.hitAPIToChangePin()
                } else {
                    self.showAlert(Constants.commomInternetmsz.checkInternet)
                }
            }} else {
            if firstTextField.text == "" || secondTextField.text == "" || thirdTextField.text == "" || forthTextField.text == "" {
                self.showAlert("Please enter 4 digit pin to proceed further.")
            } else {
                
                if Reachability.isConnectedToNetwork() {
                    self.hitAPIToGeneratePin()
                } else {
                    self.showAlert(Constants.commomInternetmsz.checkInternet)
                }
            }
        }
    }
   
    //hit api to change pin
    func hitAPIToChangePin(){
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let user_id          = userInfoModel.id as? String
        let firstPin         = (self.firstTextField.text ?? "") + (self.secondTextField.text ?? "")
        let secondPin        = (self.thirdTextField.text ?? "") + (self.forthTextField.text ?? "")
        let userPin          = firstPin + secondPin
        let paramsStr        = "user_id=\(user_id ?? "")&pin=\(userPin)&"
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + "changePin", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let _ = response as? Dictionary<String, AnyObject> {
                    self.showAlert("Pin updated successfuly")
                } else {
                    print("Data is not in proper format")
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
    @IBAction func keyboardActions(_ sender : UIButton) {
        
        let number = sender.currentTitle
        self.checkWhichTextFieldActive(number: number)
    }
    
    func checkWhichTextFieldActive(number: String?) {
        
        if firstTextField.text?.count == 0 {
            self.firstTextField.text = number
            self.secondTextField.becomeFirstResponder()
        } else if secondTextField.text?.count == 0 {
            self.secondTextField.text = number
            self.thirdTextField.becomeFirstResponder()
        } else if thirdTextField.text?.count == 0{
            self.thirdTextField.text = number
            self.forthTextField.becomeFirstResponder()
        } else if forthTextField.text?.count == 0 {
            self.forthTextField.text = number
        }
    }
    
    @IBAction func deleteTextfieldText(_ sender: UIButton) {
        if forthTextField.text?.count != 0 {
            forthTextField.text = ""
        } else if thirdTextField.text?.count != 0 {
            thirdTextField.text = ""
        } else if secondTextField.text?.count != 0 {
            secondTextField.text = ""
        } else {
            firstTextField.text = ""
        }
    }
    
    func showHCBUFavoriteViews() {
        guard let userDetil = Methods.sharedInstance.getloginData() else {
            return
        }
        let name: String = userDetil.first_name ?? ""
        let viewHBCU = HBCUFavoriteView(frame: CGRect(x: 0, y: 0, width: Constants.ScreenSize.SCREEN_WIDTH, height: Constants.ScreenSize.SCREEN_HEIGHT))
        viewHBCU.nameLabel.text = "Congratulations, \(name), your account is 100% complete."
        viewHBCU.parentVC = self
        CallBackMethods.appDelegate?.window?.addSubview(viewHBCU)
    }
}

extension GeneratePinViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case firstTextField:
            if string.count > 0 {
                DispatchQueue.main.async {
                    self.secondTextField.becomeFirstResponder()
                }
            }
        case secondTextField:
            if string.count > 0 {
                DispatchQueue.main.async {
                    self.thirdTextField.becomeFirstResponder()
                }
            }
        case thirdTextField:
            if string.count > 0 {
                DispatchQueue.main.async {
                    self.forthTextField.becomeFirstResponder()
                }
            }
        default:
            print("final")
        }
        return true
    }
}

extension GeneratePinViewController {
    
    //MARK:- hit api to generate pin
    func hitAPIToGeneratePin() {
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        guard let uuid = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        
        let apiName          = "signup"
        let user_id          = userInfoModel.id as? String
        let token_id         =  UserDefaults.standard.object(forKey: "token") ?? ""
        let unique_device_id =  uuid
        let signup_level     = "5"
        let firstPin = (self.firstTextField.text ?? "") + (self.secondTextField.text ?? "")
        let secondPin = (self.thirdTextField.text ?? "") + (self.forthTextField.text ?? "")
        
        let userPin    = firstPin + secondPin
        
        let paramsStr = "user_id=\(user_id ?? "")&token_id=\(token_id)&login_via=1&pin=\(userPin)&unique_device_id=\(unique_device_id )&signup_level=\(signup_level)"
        
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + apiName, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            
            if success == true {
                if let _ = response as? Dictionary<String, AnyObject> {
                    self.alertFunctionsWithSingleCallBack2(title: "Pin created Successfully", completionHandler: { (isGenerate) in
                        if isGenerate.boolValue {
                            DispatchQueue.main.async {
                                self.showHCBUFavoriteViews()
                            }
                        }
                    })
                } else {
                    print("Data is not in proper format")
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
}
