//
//  ConfirmPinViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 14/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class ConfirmPinViewController: UIViewController {
    
    @IBOutlet var firstTextField: UITextField!
    @IBOutlet var secondTextField: UITextField!
    @IBOutlet var thirdTextField: UITextField!
    @IBOutlet var forthTextField: UITextField!

    var arrayPfPin = [String]()
    var pin   : String?
    var temppinDict : NSDictionary!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Enter PIN"
        navigationItem.hidesBackButton = true
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
        self.navigationController?.navigationBar.setBackgroundImage(image.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
    }
    override func viewWillAppear(_ animated: Bool){
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func pushBackgrondNotification(){
      //  print(notification.userInfo)
        if let userChatInfo = Mapper<ChatMessageModel>().map(JSONObject: temppinDict) {
            let leftSideMenu = self.storyboard!.instantiateViewController(withIdentifier: "MainChatViewController") as? MainChatViewController
            leftSideMenu?.TempString = "2"
            
            leftSideMenu?.comingDataFromAppDelegate = userChatInfo
            self.navigationController?.pushViewController(leftSideMenu!, animated: true)
        }
    }
    
    @IBAction func keyboardActions(_ sender : UIButton) {
        let number = sender.currentTitle
        self.checkWhichTextFieldActive(number: number)
    }
    
    func checkWhichTextFieldActive(number: String?) {
        if firstTextField.text?.count == 0 {
            self.firstTextField.text = number
            self.secondTextField.becomeFirstResponder()
        } else if secondTextField.text?.count == 0 {
            self.secondTextField.text = number
            self.thirdTextField.becomeFirstResponder()
        } else if thirdTextField.text?.count == 0{
            self.thirdTextField.text = number
            self.forthTextField.becomeFirstResponder()
        } else if forthTextField.text?.count == 0 {
            self.forthTextField.text = number
        }
    }
    
    @IBAction func deleteTextfieldText(_ sender: UIButton) {
        if forthTextField.text?.count != 0 {
            forthTextField.text = ""
        } else if thirdTextField.text?.count != 0 {
            thirdTextField.text = ""
        } else if secondTextField.text?.count != 0 {
            secondTextField.text = ""
        } else {
            firstTextField.text = ""
        }
    }
    
    @IBAction func enterPinButton(_ sender: UIButton) {
        
        if firstTextField.text == "" || secondTextField.text == "" || thirdTextField.text == "" || forthTextField.text == "" {
            self.showAlert("Please enter 4 digit pin to proceed further.")
        } else {
            if Reachability.isConnectedToNetwork() {
                self.hitAPIConfirmPI()
            } else {
                self.showAlert(Constants.commomInternetmsz.checkInternet)
            }
        }
    }
    
    //API to confirm Pin
    func hitAPIConfirmPI() {
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let apiName = "enterPin"
        let user_id = userInfoModel.id as! String
        let firstPin = (self.firstTextField.text ?? "") + (self.secondTextField.text ?? "")
        let secondPin = (self.thirdTextField.text ?? "") + (self.forthTextField.text ?? "")
        
        let userPin = firstPin + secondPin
        
        let paramsStr = "user_id=\(user_id)&pin=\(userPin)"
        
        WebServiceClass.showLoader(view: self.view)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + apiName, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
//            if success == true{
//                if let responseDict = response as? Dictionary<String, AnyObject> {
//                    if let result = responseDict["result"]as? Dictionary<String, AnyObject>{
//                        let accessTokenStatus = result["plaid_access_token"] as? Int
//                        print(accessTokenStatus)
//                        if accessTokenStatus == 1 { //accessTokenStatus == 0
//                            self.alertFunctionsWithSingleCallBack(title: "Successfull", completionHandler: { (isGenerate) in
//                                if isGenerate.boolValue {
//                                    DispatchQueue.main.async {
//                                        CallBackMethods.appDelegate?.changeRootViewController()
//                                    }
//                                }
//                            })
//
//                        }else {
//                            let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: "Your ACH account setup is required to proceed further into your account. We are redirecting you to account set up", preferredStyle: .alert)
//                            alertController.addAction(UIAlertAction(title: "Proceed", style: UIAlertActionStyle.default)
//                            { action -> Void in
//
//                                _ = self.navigationController?.popViewController(animated: true)
//                                let verifyPinController = self.storyboard?.instantiateViewController(withIdentifier: "BankPlaidViewController") as! BankPlaidViewController
//                                self.navigationController?.pushViewController(verifyPinController, animated: true)
//
//                            })
//                            self.present(alertController, animated: true, completion:nil)
//                        }
//                    }
//
//                } else {
//                    print("Data is not in proper format")
//                }
//            }
            if let jsonResult = response as? Dictionary<String, AnyObject> {
                 print(jsonResult)
                
                 if let errorCode = jsonResult["error"] as? Int, errorCode == 1 {
                    self.showAlert(jsonResult["message"] as? String ?? "")
                 } else {
                         DispatchQueue.main.async {
                            let valu = comingFromPush
                                if valu == true {
                                    self.pushBackgrondNotification()
                                  //  CallBackMethods.appDelegate?.navigateToChat()
                                } else {
                                    CallBackMethods.appDelegate?.changeRootViewController()
                                }
                            }
                        }
             }else{
                    print("Data is not in proper format")
                }
                 }
//            else {
//                print("Data is not in proper format")
//            }
        }
}

extension ConfirmPinViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case firstTextField:
            
            if string.count > 0 {
                
                DispatchQueue.main.async {
                    self.secondTextField.becomeFirstResponder()
                }
            }
            
            print("first")
        case secondTextField:
            
            if string.count > 0 {
                
                DispatchQueue.main.async {
                    self.thirdTextField.becomeFirstResponder()
                }
            }
            print("second")
        case thirdTextField:
            print("third")
            if string.count > 0 {
                
                DispatchQueue.main.async {
                    self.forthTextField.becomeFirstResponder()
                }
            }
        default:
            print("final")
            
        }
        return true
    }
}
