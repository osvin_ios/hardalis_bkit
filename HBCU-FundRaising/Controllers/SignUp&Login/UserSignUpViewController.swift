 //
//  SignUpViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 05/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class UserSignUpViewController: UIViewController {
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    var userPostArray = [SignInModel]()
    
    var Dict = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func registerUserButton(_ sender: UIButton) {
        self.checkValidation()
    }
    
    //MARK:- Valiadtions
    func checkValidation() {
        if (emailTextfield.text)! == ""  || (passwordTextfield.text)! == "" {
            self.showAlert("Please complete all fields.")
        } else if (emailTextfield.text?.isValidEmail() == false) {
            self.showAlert("Please enter valid email.")
        } else if ((passwordTextfield.text?.count)! < 6){
            self.showAlert("Password is too short!")
        } else {
            if Reachability.isConnectedToNetwork() == true {
                self.signupApi()
            } else {
                self.showAlert(Constants.commomInternetmsz.checkInternet)
            }
        }
    }
    
    //MARK :- hit api to register user
    func signupApi() {
        
        WebServiceClass.showLoader(view: self.view)
        let apiName = "signup"
        let email = emailTextfield.text ?? ""
        let pass = passwordTextfield.text ?? ""
        let signup_level = "1"
        let signup_type  = "email"
        let fb_id = ""
        
        let paramsStr = "email=\(email)&password=\(pass)&signup_level=\(signup_level)&signup_type=\(signup_type)&fb_id=\(fb_id)"
        
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + apiName, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        self.showAlert((jsonResult["message"] as? String)!)
//                        print("Error status - true, signUp unsuccessful")
                    } else {
                        if let resultValue = jsonResult["result"] as? Dictionary<String, AnyObject> {
                            if let userInfo = Mapper<SignInModel>().map(JSONObject: resultValue) {
                                if let JSONString = Mapper().toJSONString(userInfo, prettyPrint: true) {
                                    //User Info:-
                                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: JSONString)
                                    UserDefaults.standard.set(encodedData, forKey: "userinfo")
                                    
//                                    let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: jsonResult["message"] as? String ?? "", preferredStyle: .alert)
//                                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
//                                    { action -> Void in
                                        DispatchQueue.main.async {
                                            self.performSegue(withIdentifier: "manualSignUpSegue", sender: self)
                                        }
//                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                        }
                        
                    }} else {
                    print("Data is not in proper format")
                }} else {
                print("Data is not in proper format")
            }
        }
    }
}
