//
//  ForgotPasswordViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 13/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var popViewButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
//            return
//        }
//
//        emailTextfield.text = userInfoModel.email ?? ""
    }

    @IBAction func popAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        
        if self.emailTextfield.text?.count != 0 {
            if Reachability.isConnectedToNetwork() {
                self.hitForgetAPI()
            } else {
                self.showAlert(Constants.commomInternetmsz.checkInternet)
            }
        } else {
            self.showAlert("Please enter email")
        }
    }
    
    //forgot password api
    func hitForgetAPI(){
        WebServiceClass.showLoader(view: self.view)
        let email = emailTextfield.text
        
        let paramsStr = "email=\(email ?? "")"
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + Constants.APIs.forgotPassword, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            
            if let jsonResult = response as? Dictionary<String, AnyObject> {
                if success == true {
                    if let error = jsonResult["error"] as? Bool{
                        if error == false{
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordEmailViewController")  as? ForgotPasswordEmailViewController
                            self.navigationController?.show(vc!, sender: self)
                        }else{
                            self.showAlert(jsonResult["message"] as? String ?? "")
                        }
                    }
                } else {
                    self.showAlert(jsonResult["message"] as? String ?? "")
                }
             }
          }
    }
}

