//
//  SignUpStep1ViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 06/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import IQKeyboardManager
import CropViewController
import Alamofire
import ObjectMapper
import SDWebImage

class SignUpStep1ViewController: UIViewController {
    
    //MARK:- outlets
    @IBOutlet weak var selectedOrganizationText: UITextField!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var selectedHBCUUnivList: UILabel!
    @IBOutlet weak var firstNameTextfield: UITextField!
    @IBOutlet weak var lastNameTextfield: UITextField!
    @IBOutlet weak var SelectHBCUButton: UIButton!
    @IBOutlet weak var organizationView: UIView!
    @IBOutlet weak var selectUserProfileButton: UIButton!
    @IBOutlet var openGalleryView: UIView!
    @IBOutlet weak var selectOrganizationSwitch: UIButton!
    @IBOutlet weak var topView: MKGradientView!
    @IBOutlet weak var progressIndicatorHeight: NSLayoutConstraint!
    @IBOutlet weak var saveandContinueButton: UIButton!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var saveAndContinueHeight: NSLayoutConstraint!
    @IBOutlet weak var checkboxHeight: NSLayoutConstraint!
   
    @IBOutlet weak var tickTermsAndConditions: UIButton!
    @IBOutlet weak var termsAndConditionButton: UIButton!
    @IBOutlet weak var selectOrgBytton: UIButton!
    @IBOutlet weak var referralLinkTextField: UITextField!
    @IBOutlet weak var checkBoxOfDoationBtn: ButtonDesign!
    
    @IBOutlet weak var wouldYoLikelabel: UILabel!
    @IBOutlet weak var wouldYouLikeLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var termsAndConditionHeight: NSLayoutConstraint!
    @IBOutlet weak var IAgreeLabelOutlet: UILabel!
    @IBOutlet weak var IAgreeTermsHeight: NSLayoutConstraint!
    
    //MARK:-variables
    var userImage: UIImage?
    var selectedUnivID = String()
    var profileURL  : String?
    var greek : String?
    var mode: LoginCase!
    var mineHBCUPostArray = [HBCUListModel]()
    var mineHBCUOrgArray  = [HBCUOrganizationListModel]()
    var selectedOrgId     : String?
    var enumCase = SelectProfileFrom.SignUp
    var boolInteraction : Bool = false
    var hbcuTitle = [String]()
    var barButton : UIBarButtonItem!
    
    //MARK:-viewDidLoad
    override func viewDidLoad() {
        
        organizationView.isHidden = true
        
        //keyboard
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().previousNextDisplayMode = .alwaysShow
        
        if mode == .Facebook {
            profileURL = UserDefaults.standard.object(forKey: "profileUrl") as? String
            firstNameTextfield.text = UserDefaults.standard.object(forKey: "name") as? String
            userProfileImage.sd_setImage(with: URL(string: profileURL ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
        }
        
        topView.type = .linear
        topView.colors = [UIColor.red, UIColor.black, UIColor.green]
        topView.startPoint = CGPoint(x: 0.0, y: 0.5)
        topView.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
        self.title = "My Profile"
        
        barButton = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(editAction(_:)))
        self.navigationItem.rightBarButtonItem = barButton
        self.navigationController?.navigationBar.setBackgroundImage(image.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
        
        if enumCase == .EditProfile {
            
            self.hitAPIToGetFavHBCU()
            guard let userDetil = Methods.sharedInstance.getloginData() else {
                return
            }
            firstNameTextfield.text = userDetil.first_name
            lastNameTextfield.text  = userDetil.last_name
            selectedOrgId = userDetil.organization?.id

            if userDetil.greek_org == "Y" {
                selectOrganizationSwitch.isSelected = true
                organizationView.isHidden = false
                if userDetil.organization != nil {
                    selectedOrganizationText.text = userDetil.organization?.title
                }
            }
            if let image = userDetil.profile{
                if image.contains("https://"){
                    userProfileImage.sd_setImage(with: URL(string: image ), placeholderImage: nil)
                } else {
                    let userImage = Constants.APIs.imageBaseURL + image
                    userProfileImage.sd_setImage(with: URL(string: userImage ), placeholderImage: nil)
                }
            }
        }
    }
    
    //MARK:-viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
        self.title = "My Profile"
        
        if enumCase == .EditProfile {
            self.saveandContinueButton.isHidden = true
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            
            progressIndicatorHeight.constant = 0
            checkboxHeight.constant          = 0
            saveAndContinueHeight.constant   = 0
            topViewHeight.constant           = 0
            termsAndConditionHeight.constant = 0
            termsAndConditionButton.isHidden = true
            IAgreeLabelOutlet.isHidden = true
            IAgreeTermsHeight.constant =  0
            wouldYouLikeLabelHeight.constant = 0
            wouldYoLikelabel.isHidden = true
            
            if boolInteraction == false {
                self.selectUserProfileButton.isUserInteractionEnabled = false
                self.firstNameTextfield.isUserInteractionEnabled = false
                self.lastNameTextfield.isUserInteractionEnabled = false
                self.SelectHBCUButton.isUserInteractionEnabled = false
                self.selectOrganizationSwitch.isUserInteractionEnabled = false
                self.selectOrgBytton.isUserInteractionEnabled = false
                //self.view.isUserInteractionEnabled = false
            }else{
                self.selectUserProfileButton.isUserInteractionEnabled = true
                self.firstNameTextfield.isUserInteractionEnabled = true
                self.lastNameTextfield.isUserInteractionEnabled = true
                self.SelectHBCUButton.isUserInteractionEnabled = true
                self.selectOrganizationSwitch.isUserInteractionEnabled = true
                self.selectOrgBytton.isUserInteractionEnabled = true
                //self.view.isUserInteractionEnabled = true
            }
        }else{
            self.navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }
    
    //MARK:- api function
    func hitAPIToGetFavHBCU(){
        self.selectedHBCUUnivList.text = ""
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let userId = userInfoModel.id as? String
        let paramsStr = "user_id=\(userId ?? "0")"
        let urlName = Constants.APIs.baseURL + "getUserHBCU?" + paramsStr
        
        WebServiceClass.dataTask(urlName:  urlName, method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                    } else {
                        if let result = jsonResult["result"] as? [Dictionary<String,AnyObject>]{
                        for i in 0..<5 {
                            self.hbcuTitle.append((result[i]["title"] as? String)!)
                          }
                            self.selectedHBCUUnivList.text = self.hbcuTitle.joined(separator: ",")
                            self.selectedHBCUUnivList.textColor = UIColor.black
                        }
                    }
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
    //MARK:- functions
    @objc func editAction(_ sender : UIBarButtonItem){
        if sender.title == "Edit"{
            boolInteraction = true
            self.selectUserProfileButton.isUserInteractionEnabled = true
            self.firstNameTextfield.isUserInteractionEnabled = true
            self.lastNameTextfield.isUserInteractionEnabled = true
            self.SelectHBCUButton.isUserInteractionEnabled = true
            self.selectOrganizationSwitch.isUserInteractionEnabled = true
            self.selectOrgBytton.isUserInteractionEnabled = true
            //self.view.isUserInteractionEnabled = true
            sender.title = "Save"
            sender.action = #selector(saveAction)
        } else {
            sender.title = "Edit"
            self.selectUserProfileButton.isUserInteractionEnabled = false
            self.firstNameTextfield.isUserInteractionEnabled = false
            self.lastNameTextfield.isUserInteractionEnabled = false
            self.SelectHBCUButton.isUserInteractionEnabled = false
            self.selectOrganizationSwitch.isUserInteractionEnabled = false
            self.selectOrgBytton.isUserInteractionEnabled = false
            //self.view.isUserInteractionEnabled = false
        }
    }
    
    @objc func saveAction(){
        if Reachability.isConnectedToNetwork() == true{
            self.hitUpdateProfileAPI()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- hit api to update profile
    
    func hitUpdateProfileAPI() {
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else{
            return
        }
        
        let userId = userInfoModel.id as? String
        let apiURL = "http://admin.iheartmyhbcu.org/api/user/updateprofile" //Live
        //let apiURL = "http://phphosting.osvin.net/HBCU/api/user/updateprofile"
        if selectOrganizationSwitch.isSelected == true {
            greek = "Y"
        } else { greek = "N" }
        
        let params : [String: String] = [
            "user_id"       : "\(userId ?? "0")" ,
            "first_name"    : self.firstNameTextfield.text ?? "" ,
            "last_name"     : self.lastNameTextfield.text ?? "",
            "hbcu"          : selectedUnivID,
            "organization"  : selectedOrgId ?? "",
            "greek_org"     : greek!,
            "anonymous"     : "0",
        ]
        
        print(params)
        WebServiceClass.showLoader(view: self.view)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            if let normalImg = self.userImage {
                if  let imgValue = UIImageJPEGRepresentation(normalImg, 0.6) {
                    let r = arc4random()
                    let str = "file"+String(r)+".jpg"
                    let parameterName = "profile"
                    multipartFormData.append(imgValue, withName:parameterName, fileName:str, mimeType: "image/jpeg")
                }
            }
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key )
            }
            
        }, to: apiURL,encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    WebServiceClass.hideLoader(view: self.view)
                    
                    if let Json = response.result.value as? Dictionary<String,AnyObject> {
                        
                        if let responcCode = Json["error"] as? Bool, responcCode {
                            
                        }else {
                            if let responceResult = Json["result"] as? Dictionary<String,AnyObject> {
                                if let userInfo = Mapper<MyAccountDetailsModel>().map(JSONObject: responceResult) {
                                    if let JSONString = Mapper().toJSONString(userInfo, prettyPrint: true) {
                                        //User Info:-
                                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: JSONString)
                                        UserDefaults.standard.set(encodedData, forKey: "userinfo")
                                        
                                        self.alertFunctionsWithSingleCallBack(title: "Profile Updated", completionHandler: { (isObject) in
                                            
                                            if isObject.boolValue {
                                                DispatchQueue.main.async {
                                                    UserDefaults.standard.set(true, forKey: "loginsession")
                                                    self.barButton.title = "Edit"
                                                    self.view.isUserInteractionEnabled = false
                                                    self.navigationController?.popViewController(animated: true)
                                                }
                                            }
                                        })
                                    }
                                }
                            }
                        }
                    }}
            case .failure(let encodingError):
                print(encodingError)
            }})
    }
    
    //MARK:- hit api to upload data
    func signUpPostAPI() {
        
        let apiName = "signup"
        let apiURL = Constants.APIs.baseURL + apiName
        let percent = "20,20,20,20,20"
        let signup_level = "2"
        
        if selectOrganizationSwitch.isSelected == true {
            greek = "Y"
        } else {
            greek = "N"
        }
        
        var anonynoms = String()
        if checkBoxOfDoationBtn.isSelected == true {
            anonynoms = "1"
        }else{
            anonynoms = "0"
        }
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let params : [String: AnyObject] = [
        
            "user_id"       : userInfoModel.id as AnyObject  ,
            "first_name"    : self.firstNameTextfield.text as AnyObject ,
            "last_name"     : self.lastNameTextfield.text as AnyObject,
            "hbcu"          : selectedUnivID as AnyObject,
            "organization"  : selectedOrgId as AnyObject,
            "percent"       : percent as AnyObject,
            "signup_level"  : signup_level as AnyObject,
            "greek_org"     : greek! as AnyObject,
            "anonymous"     : anonynoms as AnyObject,
            "referral_link" : self.referralLinkTextField.text as AnyObject,
//            "referral_link" : "Optional",
            "profile_url"   : profileURL as AnyObject,
        ]
        
        print(params)
        WebServiceClass.showLoader(view: self.view)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            if let normalImg = self.userImage {
                if  let imgValue = UIImageJPEGRepresentation(normalImg, 0.6) {
                    let r = arc4random()
                    let str = "file"+String(r)+".jpg"
                    let parameterName = "profile"
                    multipartFormData.append(imgValue, withName:parameterName, fileName:str, mimeType: "image/jpeg")
                }
            }
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key )
            }
        }, to: apiURL,encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    WebServiceClass.hideLoader(view: self.view)
                    
                    if let Json = response.result.value as? Dictionary<String,AnyObject> {
                        
                        if let responcCode = Json["error"] as? Bool, responcCode {
                            
                        } else {
                            if let responceResult = Json["result"] as? Dictionary<String, AnyObject> {
                                if let userInfo = Mapper<MyAccountDetailsModel>().map(JSONObject: responceResult) {
                                    if let JSONString = Mapper().toJSONString(userInfo, prettyPrint: true) {
                                        //User Info:-
                                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: JSONString)
                                        UserDefaults.standard.set(encodedData, forKey: "userinfo")}
                                    }
                                print(responceResult)
//                                let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: "Registered Successfully", preferredStyle: .alert)
//                                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
//                                { action -> Void in
                                    DispatchQueue.main.async {
                                        self.performSegue(withIdentifier: "BankListSegue", sender: self)
                                    }
//       self.present(alertController, animated: true, completion:nil)
                            }}}}
            case .failure(let encodingError):
                print(encodingError)
            }})
    }
    
    //MARK:- function to validate textfields
    func checkvalidation() {
        
        if mode == .Facebook{
            if (self.lastNameTextfield.text ?? "").count == 0 {
                self.showAlert("Please enter your last Name")
            } else if (self.selectedHBCUUnivList.text)?.count == 0 {
                self.showAlert("Please select HBCU")
            } else if self.tickTermsAndConditions.isSelected == false {
                self.showAlert("Please agree to Terms and Conditions")
            } else if selectOrganizationSwitch.isSelected == true && selectedOrganizationText.text?.count == 0 {
                self.showAlert("Please select organization")
            } else {
                if Reachability.isConnectedToNetwork() == true {
                    self.signUpPostAPI()
                } else {
                    self.showAlert(Constants.commomInternetmsz.checkInternet)
                }
            }
        } else {
            if (self.firstNameTextfield.text ?? "").count == 0 {
                self.showAlert("Please enter your first Name")
            } else if (self.lastNameTextfield.text ?? "").count == 0 {
                self.showAlert("Please enter your last Name")
            } else if self.userImage == nil {
                self.showAlert("Please select image")
            }  else if (self.selectedHBCUUnivList.text == "Select HBCU Universities") {
                self.showAlert("Please select HBCU")
            } else if self.tickTermsAndConditions.isSelected == false {
                self.showAlert("Please agree to terms and conditions")
            } else if selectOrganizationSwitch.isSelected == true && selectedOrganizationText.text?.count == 0 {
                self.showAlert("Please select organization")
            } else {
                if Reachability.isConnectedToNetwork() == true {
                    self.signUpPostAPI()
                } else {
                    self.showAlert(Constants.commomInternetmsz.checkInternet)
                }
            }
        }
    }
    
    //MARK:- IBActions
    
    @IBAction func toggleOrganizationButton(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            organizationView.isHidden = false
        }else{
            organizationView.isHidden = true
        }
    }
    
    @IBAction func saveAndContinue(_ sender: UIButton){
        self.checkvalidation()
    }
    
    @IBAction func openCameraViewButtonAction(_ sender: UIButton){
        openGalleryView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        //openGalleryView?.backgroundColor = UIColor(white: 1, alpha: 0.5)
        self.view.addSubview(openGalleryView)
    }
    
    @IBAction func cancelCameraViewaction(_ sender: UIButton){
        openGalleryView.removeFromSuperview()
    }
    @IBAction func takeAPhotoFromCamera(_ sender: UIButton){
        openCamera()
    }
    @IBAction func selectPhotoFromGalleryAction(_ sender: UIButton){
        openGallery()
    }
    @IBAction func takePhotoImgFromCamera(_ sender: Any){
         openCamera()
    }
    @IBAction func selectFrpmGalleryImgActn(_ sender: Any) {
           openGallery()
    }
    @IBAction func goToHBCUUnivList(_ sender: UIButton) {
        if enumCase == .EditProfile {
            
            barButton.title = "Save"
            self.view.isUserInteractionEnabled = true
            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "FavHBCUViewController") as! FavHBCUViewController
                currentNavVC.pushViewController(vc, animated: true)
            }
        } else {
            
            boolInteraction = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectHBCUCollectionViewController") as! SelectHBCUCollectionViewController
            vc.selectedHBCUPostArray = mineHBCUPostArray
            vc.completionBlock = { [weak self] (modelArrayObject) in
                
                guard let strongSelf = self else { return }
                
                if let hBCUArray = modelArrayObject as? [HBCUListModel] {
                    
                    strongSelf.mineHBCUPostArray = hBCUArray
                    
                    let names = hBCUArray.map({$0.title}) as? [String]
                    let univName = names?.joined(separator: ",")
                    let univId   = hBCUArray.map({$0.id}) as? [String]
                    self?.selectedHBCUUnivList.text = univName
                    self?.selectedHBCUUnivList.sizeToFit()
                    self?.selectedHBCUUnivList.textColor = UIColor.black
                    self?.selectedUnivID = univId?.joined(separator: ",") ?? ""
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func goToOrgList(_ sender: UIButton){
        
        boolInteraction = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectHBCUOrgViewController") as! SelectHBCUOrgViewController
        vc.selectedHBCUPostArray = mineHBCUOrgArray
        vc.completionBlock = { [weak self] (modelArrayObject) in
            
            guard let strongSelf = self else { return }
            
            if let hBCUArray = modelArrayObject as? [HBCUOrganizationListModel] {
                
                strongSelf.mineHBCUOrgArray = hBCUArray
                
                let orgName = hBCUArray.map({$0.title }) as? [String]
                let name = orgName?.joined(separator: ",")
                self?.selectedOrganizationText.text = name
                
                let orgId = hBCUArray.map({$0.id }) as? [String]
                self?.selectedOrgId = orgId?.joined(separator: ",")
            }
        }
        
        if enumCase == .EditProfile {
            barButton.title = "Save"
            self.view.isUserInteractionEnabled = true
        } else {
            
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func termsAndConditionButton(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
    }
    
    //MARK:- delegate function of org hbcu
    func orgName(orgNAme : String){
        selectedOrganizationText.text = orgNAme
    }
}

//MARK:- extension
extension SignUpStep1ViewController : CropViewControllerDelegate{
    
    //MARK:- Camera Fxn
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerControllerSourceType.camera
            //myPickerController.allowsEditing = true
            self.present(myPickerController, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery() {
        
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    //MARK:- CROP FXN
    func presentCropViewController(image : UIImage){
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        cropViewController.aspectRatioPreset = .presetSquare; //Set the initial aspect ratio as a square
        cropViewController.aspectRatioLockEnabled = true // The crop box is locked to the aspect ratio and can't be resized away from it
        cropViewController.resetAspectRatioEnabled = false // When tapping 'reset', the aspect ratio will NOT be reset back to default
        cropViewController.aspectRatioPickerButtonHidden = true
        cropViewController.rotateButtonsHidden = true
        cropViewController.rotateClockwiseButtonHidden = true
        present(cropViewController, animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {

        self.userProfileImage.image = image
        userImage = image
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        cropViewController.dismiss(animated: true, completion: nil)
        openGalleryView.removeFromSuperview()
    }
    
    // for open gallery
    
    func opengallery(){
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // methods of open gallery
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        userImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        dismiss(animated: true, completion: nil)
        self.presentCropViewController(image: userImage!)
    }
    
    override func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        openGalleryView.removeFromSuperview()
    }
}
