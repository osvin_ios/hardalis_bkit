//
//  LoginViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 11/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import ObjectMapper

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var rememberMeButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    var loginTYpe = LoginCase.Email
    var facebookInfoDict = [String:AnyObject]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
//        emailTextfield.delegate = self
//        emailTextfield.tag = 0 //Increment accordingly
//        passwordTextfield.delegate = self
//        passwordTextfield.tag = 0 //Increment accordingly
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let selectedBoolValue = UserDefaults.standard.bool(forKey: "selectedBool")
        
        if selectedBoolValue {
            rememberMeButton.isSelected = true
            emailTextfield.text = UserDefaults.standard.object(forKey: "email") as? String
            passwordTextfield.text = UserDefaults.standard.object(forKey: "password") as? String
        } else {
            rememberMeButton.isSelected = false
        }
    }
    
    //IBAction
    @IBAction func rememberMeAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            UserDefaults.standard.set(true, forKey: "selectedBool")
            
            UserDefaults.standard.set((emailTextfield.text ?? ""), forKey: "email")
            UserDefaults.standard.set((passwordTextfield.text ?? ""), forKey: "password")

        } else {
            UserDefaults.standard.set(false, forKey: "selectedBool")
            UserDefaults.standard.set("", forKey: "email")
            UserDefaults.standard.set("", forKey: "password")
        }
    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool
//    {
//        // Try to find next responder
//        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
//            nextField.becomeFirstResponder()
//        } else {
//            // Not found, so remove keyboard.
//            textField.resignFirstResponder()
//        }
//        // Do not add a line break
//        return false
//
//        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
//            nextField.becomeFirstResponder()
//        } else {
//            // Not found, so remove keyboard.
//            textField.resignFirstResponder()
//        }
//        // Do not add a line break
//        return false
//    }
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        
//        if self.emailTextfield.text?.count != 0 {
//            self.performSegue(withIdentifier: "ForgotPassSegue", sender: self)
//        } else {
//            self.showAlert("Please Enter Email")
//        }
    }
    
    @IBAction func facebookLogin(_ sender: UIButton) {
        
        loginTYpe = .Facebook

        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        //fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self) { (result, error) -> Void in
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email")) {
                    if Reachability.isConnectedToNetwork() {
                        self.getFBUserData()
                    } else {
                        self.showAlert(Constants.commomInternetmsz.checkInternet)
                    }
                }
            }
        }
    }
    
    //function is fetching the user data
    
    func getFBUserData() {
        
        if((FBSDKAccessToken.current()) != nil) {
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.facebookInfoDict = result as! [String : AnyObject]
                    if Reachability.isConnectedToNetwork() {
                        self.loginAPI()
                    } else {
                        self.showAlert(Constants.commomInternetmsz.checkInternet)
                    }
                }
            })
        }
    }
    
    @IBAction func loginButton(_ sender: UIButton) {
        
        if let emailText = self.emailTextfield.text, emailText.count == 0 {
            self.showAlert("Please enter email.")
        } else if let emailText = self.emailTextfield.text, emailText.isValidEmail() == false {
            self.showAlert("Please enter valid email.")
        } else if let passText = self.passwordTextfield.text, passText.count < 6 {
            self.showAlert("password is too short!")
        } else {
            loginTYpe = .Email
            if Reachability.isConnectedToNetwork() {
                self.loginAPI()
            } else {
                self.showAlert(Constants.commomInternetmsz.checkInternet)
            }
        }
    }
    
    //hit API
    func loginAPI() {
        
        var loginEmail = String()
        //var fb_ID = UserDefaults.standard.value(forKey: "fb_id") ?? ""
        var fb_ID = facebookInfoDict["id"] as? String ?? ""

        var password = ""
        
        if loginTYpe == .Facebook {
            loginEmail = facebookInfoDict["email"] as? String ?? ""
        } else {
            loginEmail = emailTextfield.text ?? ""
            password   = passwordTextfield.text ?? ""
            fb_ID = ""
        }
        guard let uuid = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        WebServiceClass.showLoader(view: self.view)
        let apiName          = "login"
        //let password         = passwordTextfield.text
        let unique_device_id = uuid
        let token_id         = UserDefaults.standard.object(forKey: "token") ?? ""
        //let token_id         = "b49c7702d974babe68c7bc33ac583081a5e2a01078aaba5c2e3b58af6478b63d"
        
        let paramsStr = "email=\(loginEmail)&password=\(password)&token_id=\(token_id)&unique_device_id=\(unique_device_id)&fb_id=\(fb_ID)&login_via=1&login_type=\(loginTYpe == .Email ? "email" : "facebook")"
        
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + apiName, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        self.showAlert(jsonResult["message"] as? String ?? "")
                    } else {
                        if let resultValue = jsonResult["result"] as? Dictionary<String, AnyObject> {
                            if let userInfo = Mapper<MyAccountDetailsModel>().map(JSONObject: resultValue) {
                                if let JSONString = Mapper().toJSONString(userInfo, prettyPrint: true) {
                                    //User Info:-
                                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: JSONString)
                                    UserDefaults.standard.set(encodedData, forKey: "userinfo")
                                    
//                                    self.alertFunctionsWithSingleCallBack(title: "Login Successfully", completionHandler: { (isObject) in
//
//                                        if isObject.boolValue {
                                            DispatchQueue.main.async {
                                                UserDefaults.standard.set(true, forKey: "loginsession")
                                                let verifyPinController = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmPinViewController") as! ConfirmPinViewController
                                                self.navigationController?.pushViewController(verifyPinController, animated: true)
                                            }
//                                        }
//                                    })
                                }
                            }
                        }
                    }
                } else {
                    print("Data is not in proper format")
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destViewController: SignUpStep1ViewController = segue.destination as? SignUpStep1ViewController, segue.identifier == "facebookLogin" {
            destViewController.mode = .Facebook
        }
    }
}
