       //
//  SignUpViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 05/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import ObjectMapper

class MainHomeViewController: UIViewController {
    
    var facebookInfoDict = [String:AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        let name = "HomeScreen"
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: name)
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func loginWithFacebook(_ sender: UIButton) {
//        WebServiceClass.showLoader(view: self.view)
//        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
//        fbLoginManager.logOut()
//        //fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) -> Void in
//        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
//
//            if (error == nil){
//                WebServiceClass.hideLoader(view: self.view)
//                let fbloginresult : FBSDKLoginManagerLoginResult = result!
//                // if user cancel the login
//                if (result?.isCancelled)!{
//                    return
//                }
//                if(fbloginresult.grantedPermissions.contains("email")) {
//                    self.getFBUserData()
//                }
//            } else{
//                WebServiceClass.hideLoader(view: self.view)
//                print("ydbgbiu")
//            }
//        }
        
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                
                if fbloginresult.grantedPermissions != nil {
                    
                    if(fbloginresult.grantedPermissions.contains("email")) {
                        
                        if((FBSDKAccessToken.current()) != nil){
                            
                            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                                
                                if (error == nil){
                                    
                                    if let dict = result as? [String : AnyObject]{
//                                        let name = dict["name"] as? String ?? ""
//                                        print(name)
//                                        let email = dict["email"] as? String ?? ""
//                                        print(email)
//                                        let facebooId = dict["id"] as? String ?? ""
//                                        print(facebooId)
//                                        let profile_pic = dict["picture.type(large)"] as? String ?? ""
//                                        print(profile_pic)
//
//                                        self.loginSelectedType = .social
//                                        self.userLoginApi(email: email, password: "", socialid:facebooId, name: name, profile_pic: profile_pic)
//
//                                        self.facebookInfoDict = result as! [String : AnyObject]
//                                        print(self.facebookInfoDict)
                                        self.facebookInfoDict = dict
                                        print(self.facebookInfoDict)
                                        if let profileData = self.facebookInfoDict["picture"]!["data"] as? Dictionary<String, AnyObject> {
                                            let profileUrl = profileData["url"] as? String
                                            let name = self.facebookInfoDict["name"] as? String
                                            UserDefaults.standard.set(self.facebookInfoDict["id"] as? String, forKey: "fb_id")
                                            UserDefaults.standard.set(profileUrl, forKey: "profileUrl")
                                            UserDefaults.standard.set(name, forKey: "name")
                                            UserDefaults.standard.synchronize()
                                        }
                                        self.signUpAPI()
                                        
                                        
                                    }
                                } else {
                                    print("somthing went wrong")
                                    
                                }
                            })
                        }
                    }
                }
            }
        }
       
        
    }
    
//    //function is fetching the user data
//    func getFBUserData() {
//        if((FBSDKAccessToken.current()) != nil){
//            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
//                if (error == nil){
//                    WebServiceClass.hideLoader(view: self.view)
//                    self.facebookInfoDict = result as! [String : AnyObject]
//                    print(self.facebookInfoDict)
//                    if let profileData = self.facebookInfoDict["picture"]!["data"] as? Dictionary<String, AnyObject> {
//                        let profileUrl = profileData["url"] as? String
//                        let name = self.facebookInfoDict["name"] as? String
//                        UserDefaults.standard.set(self.facebookInfoDict["id"] as? String, forKey: "fb_id")
//                        UserDefaults.standard.set(profileUrl, forKey: "profileUrl")
//                        UserDefaults.standard.set(name, forKey: "name")
//                        UserDefaults.standard.synchronize()
//                    }
//                    self.signUpAPI()
//                }else{
//                    print("error")
//                }
//            })
//        }
//    }
//
    //Hit login API
    func signUpAPI() {
        WebServiceClass.showLoader(view: self.view)
        let apiName = "signup"
        let email   = facebookInfoDict["email"] as? String ?? ""
        let pass    = ""
        let signup_level = "1"
        let signup_type  = "facebook"
        
        let paramsStr = "email=\(email ?? "")&password=\(pass)&signup_level=\(signup_level)&signup_type=\(signup_type)&fb_id=\(facebookInfoDict["id"] as? String ?? "")"
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + apiName, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        //print("Error status - true, signUp unsuccessful")
                         self.showAlert((jsonResult["message"] as? String)!)
                        } else {
                            if let resultValue = jsonResult["result"] as? Dictionary<String, AnyObject> {
                                if let userInfo = Mapper<MyAccountDetailsModel>().map(JSONObject: resultValue) {
                                    if let JSONString = Mapper().toJSONString(userInfo, prettyPrint: true) {
                                        //User Info:-
                                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: JSONString )
                                        UserDefaults.standard.set(encodedData , forKey: "userinfo")
                                    }
                                }
                            }
                            let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: jsonResult["message"] as? String ?? "", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                            { action -> Void in
                                DispatchQueue.main.async {
                                    self.performSegue(withIdentifier: "facebookLoginMain", sender: self)
                                }})
                            self.present(alertController, animated: true, completion:nil)
                        }} else {
                    print("Data is not in proper format")
                }} else {
                print("Data is not in proper format")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destViewController: SignUpStep1ViewController = segue.destination as? SignUpStep1ViewController, segue.identifier == "facebookLoginMain" {
            destViewController.mode = .Facebook
        }
    }

}

