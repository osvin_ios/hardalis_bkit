//
//  BankPlaidViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 12/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

//
//  ViewController.swift
//  LinkDemo-Swift
//
//  Copyright © 2017 Plaid Technologies, Inc. All rights reserved.
//

import UIKit
import LinkKit

class BankPlaidViewController : UIViewController {
    var checkPreviousLinkedAccountVCis : Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        #if USE_CUSTOM_CONFIG
        presentPlaidLinkWithCustomConfiguration()
        #else
        presentPlaidLinkWithSharedConfiguration()
        #endif
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func handleSuccessWithToken(_ publicToken: String, metadata: [String : Any]?) {
        presentAlertViewWithTitle("Success", message: "token: \(publicToken)\nmetadata: \(metadata ?? [:])")
    }
    
    func handleError(_ error: Error, metadata: [String : Any]?) {
        presentAlertViewWithTitle("Failure", message: "error: \(error.localizedDescription)\nmetadata: \(metadata ?? [:])")
    }
    
    func handleExitWithMetadata(_ metadata: [String : Any]?) {
        presentAlertViewWithTitle("Exit", message: "metadata: \(metadata ?? [:])")
    }
    
    func presentAlertViewWithTitle(_ title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: Plaid Link setup with shared configuration from Info.plist
    func presentPlaidLinkWithSharedConfiguration(){
        let linkViewDelegate = self
        let linkViewController = PLKPlaidLinkViewController(delegate: linkViewDelegate)
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            linkViewController.modalPresentationStyle = .formSheet;
        }
        present(linkViewController, animated: true)
    }
    
    // MARK: Plaid Link setup with custom configuration
    func presentPlaidLinkWithCustomConfiguration(){
        //development
        //let linkConfiguration = PLKConfiguration(key: "c74afb41cee71cd888636cc90426fb", env: .sandbox, product: .auth)
        
        //live
          //let linkConfiguration = PLKConfiguration(key: "pk_live_egNBOvzPd1DewC3sOGQe4H0c", env: .development, product: .auth)
        let linkConfiguration = PLKConfiguration(key: "c74afb41cee71cd888636cc90426fb", env: .development, product: .auth)
        
        linkConfiguration.clientName = "HBCU Fundraising"
        let linkViewDelegate = self
        let linkViewController = PLKPlaidLinkViewController(configuration: linkConfiguration, delegate: linkViewDelegate)
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            linkViewController.modalPresentationStyle = .formSheet;
        }
        present(linkViewController, animated: true)
    }
    
    // MARK: Start Plaid Link in update mode
    func presentPlaidLinkInUpdateMode(){
        let linkViewDelegate = self
        let linkViewController = PLKPlaidLinkViewController(publicToken: ".", delegate: linkViewDelegate)
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            linkViewController.modalPresentationStyle = .formSheet;
        }
        present(linkViewController, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "openBankAccount" {
            let destiation = segue.destination as! BankAccountDetailViewController
            destiation.checkPreviousLinkedAccountVCis = self.checkPreviousLinkedAccountVCis
        }
    }
}

// MARK: - PLKPlaidLinkViewDelegate Protocol
extension BankPlaidViewController : PLKPlaidLinkViewDelegate {
    
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didSucceedWithPublicToken publicToken: String, metadata: [String : Any]?){
        dismiss(animated: true) {
            // Handle success, e.g. by storing publicToken with your service
            NSLog("Successfully linked account!\npublicToken: \(publicToken)\nmetadata: \(metadata ?? [:])")
            
            if let mandeepDat = metadata {
                if let data = mandeepDat["institution"] as? Dictionary<String,AnyObject> {
                    UserDefaults.standard.set(data["institution_id"] as? String, forKey: "institutionalId")
                    UserDefaults.standard.set(publicToken, forKey: "publicToken")
                    self.performSegue(withIdentifier: "openBankAccount", sender: self)
                }
            }
        }
    }
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didExitWithError error: Error?, metadata: [String : Any]?){
        dismiss(animated: true) {
            if let error = error {
                NSLog("Failed to link account due to: \(error.localizedDescription)\nmetadata: \(metadata ?? [:])")
                self.handleError(error, metadata: metadata)
            } else {
                NSLog("Plaid link exited with metadata: \(metadata ?? [:])")
            }
        }
    }
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didHandleEvent event: String, metadata: [String : Any]?){
        NSLog("Link event: \(event)\nmetadata: \(metadata ?? [:])")
        if event == "EXIT" {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}

