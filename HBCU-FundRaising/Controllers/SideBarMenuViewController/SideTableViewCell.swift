//
//  SideTableViewCell.swift
//  36five
//
//  Created by IosDeveloper on 16/06/17.
//  Copyright © 2017 IosDeveloper. All rights reserved.
//

import UIKit

class SideTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel?
    @IBOutlet weak var cellImageView : UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
