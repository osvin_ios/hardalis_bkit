//
//  SideMenuViewController.swift
//  HBSUApplication
//
//  Created by osvinuser on 11/06/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import UIKit
import MFSideMenu

class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var userProfilePic: UIImageView!
    @IBOutlet weak var sideBarTableView: UITableView!
    @IBOutlet weak var userName: UILabel!
    
//    let tableArray = ["HOME", "CHAT", "CHALLENNGE" , "DONATE", "SPARE CHANGE", "MOST LOVED HBCUs","DIVINE 9", "INVITE FRIENDS", "SETTINGS"]
//    let imagesArray = [#imageLiteral(resourceName: "ic_home"), #imageLiteral(resourceName: "ic_chat"), #imageLiteral(resourceName: "ic_challenge"),#imageLiteral(resourceName: "ic_donate"),#imageLiteral(resourceName: "ic_spare_change"),#imageLiteral(resourceName: "ic_most_loved_hbcu"),#imageLiteral(resourceName: "ic_top_donors"),#imageLiteral(resourceName: "ic_invire_friends"),#imageLiteral(resourceName: "ic_settings")]
  
    let tableArray = ["HOME", "CHAT", "CHALLENGE", "MOST LOVED HBCU's", "DIVINE 9 RANKING", "DONATE", "SPARE CHANGE", "INVITE FRIENDS", "SETTINGS"]
    
    let imagesArray = [#imageLiteral(resourceName: "ic_home"), #imageLiteral(resourceName: "ic_chat"), #imageLiteral(resourceName: "ic_challenge"), #imageLiteral(resourceName: "ic_most_loved_hbcu"), #imageLiteral(resourceName: "ic_top_donors"), #imageLiteral(resourceName: "ic_donate"), #imageLiteral(resourceName: "ic_spare_change"), #imageLiteral(resourceName: "ic_invire_friends") , #imageLiteral(resourceName: "ic_settings")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateImageViewFunction), name: NSNotification.Name(rawValue: "updateImage"), object: nil)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = "Favorite HBCU"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        guard let userDetil = Methods.sharedInstance.getloginData() else {
            return
        }
        userName.text = userDetil.first_name! + " " +  userDetil.last_name!
        if let image = userDetil.profile {
            if image.contains("https://"){
                userProfilePic.sd_setImage(with: URL(string: image ), placeholderImage: nil)
            } else {
                let userImage = Constants.APIs.imageBaseURL + image
                userProfilePic.sd_setImage(with: URL(string: userImage ), placeholderImage: nil)
            }
        }
    }
    
    @objc func updateImageViewFunction() {
        
        self.sideBarTableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellInstance : SideTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideTableViewCell", for: indexPath) as! SideTableViewCell
        
        cellInstance.titleLabel?.text = tableArray[indexPath.row]
        cellInstance.cellImageView?.image = imagesArray[indexPath.row]
        cellInstance.selectionStyle = .none
        return cellInstance
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCell = tableView.cellForRow(at: indexPath)
        selectedCell?.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            // Put your code which should be executed with a delay here
            selectedCell?.isUserInteractionEnabled = true
        })
        
        switch indexPath.row {
            
        case 0:
            
            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                currentNavVC.pushViewController(vc, animated: true)
            }
                self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)
            
        case 1:
            
            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "MainChatViewController") as! MainChatViewController
                vc.TempString = "1"
                currentNavVC.pushViewController(vc, animated: true)
            }
            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)

        case 2:
                  
            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ChallengeViewController") as! ChallengeViewController
                currentNavVC.pushViewController(vc, animated: true)
            }
            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)

//        case 3:
//
//            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "FavHBCUViewController") as! FavHBCUViewController
//                currentNavVC.pushViewController(vc, animated: true)
//            }
//            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)

        case 3:
            
            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "MostLovedHBCU") as! MostLovedHBCU
                currentNavVC.pushViewController(vc, animated: true)
            }
            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)
            
//            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "DonationViewController") as! DonationViewController
//                currentNavVC.pushViewController(vc, animated: true)
//            }
//            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)

        case 4:
            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Divine9ViewController") as! Divine9ViewController
                currentNavVC.pushViewController(vc, animated: true)
            }
            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)
            
//            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "SpareChangeViewController") as! SpareChangeViewController
//                currentNavVC.pushViewController(vc, animated: true)
//            }
//            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)
            
//        case 5:
//
//            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "StatementsViewController") as! StatementsViewController
//                currentNavVC.pushViewController(vc, animated: true)
//            }
//            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)
            
        case 5:
            
            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "DonationViewController") as! DonationViewController
                currentNavVC.pushViewController(vc, animated: true)
            }
            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)
            
//            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "MostLovedHBCU") as! MostLovedHBCU
//                currentNavVC.pushViewController(vc, animated: true)
//            }
//            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)
            
        case 6:
            
            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SpareChangeViewController") as! SpareChangeViewController
                currentNavVC.pushViewController(vc, animated: true)
            }
            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)
            
//            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "Divine9ViewController") as! Divine9ViewController
//                currentNavVC.pushViewController(vc, animated: true)
//            }
//            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)
            
        case 7 :

            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "InviteFriendsVC") as! InviteFriendsVC
                currentNavVC.pushViewController(vc, animated: true)
            }
            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)
            
        default:
            
            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
                currentNavVC.pushViewController(vc, animated: true)
            }
            self.menuContainerViewController.setMenuState(MFSideMenuState(rawValue: 0), completion:nil)
            
        }
    }

    
    @IBAction func logOut(_ sender: UIButton) {
        
        self.alertFunctionsWithCallBack(title: "Are you sure you want to logout?") { (isObject) in
            
            if isObject.boolValue {
                
                if Reachability.isConnectedToNetwork() {
                    self.hitLogOutAPI()
                } else {
                    self.showAlert(Constants.commomInternetmsz.checkInternet)
                }
            }
        }
    }
    
    //sign out
    func hitLogOutAPI() {
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let User_id = userInfoModel.id as? String 
        let apiName        = "logout"
        let unique_deviceId = UIDevice.current.identifierForVendor?.uuidString ?? ""
        WebServiceClass.showLoader(view: self.view)
        
        let paramsStr = "User_id=\(User_id ?? "0")&unique_deviceId=\(unique_deviceId)"
        
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + apiName, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["responseCode"] as? Bool, responeCode {
                        self.showAlert(jsonResult["message"] as? String ?? "")
                    } else {
                        self.clearUserSession()
                    }
                } else {
                    print("Data is not in proper format")
                }
                
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
    //MARK:- User Logout.
    func clearUserSession() {
        
        let loginViewObject = self.storyboard?.instantiateViewController(withIdentifier: "MainHomeViewController") as! MainHomeViewController
        UserDefaults.standard.set(false, forKey: "loginsession")
        let navigationController: UINavigationController = UINavigationController(rootViewController: loginViewObject)
        UIApplication.shared.applicationIconBadgeNumber = 0
        CallBackMethods.appDelegate?.window?.rootViewController = navigationController
    }
}
