//
//  hbcuOrganizationCollectionViewCell.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 07/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class hbcuOrganizationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var hbcuLogoImage: UIImageView!
    @IBOutlet weak var hbcuTitle: UILabel!
    @IBOutlet weak var selectedCellImage: UIImageView!
}
