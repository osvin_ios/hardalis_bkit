//
//  ConfirmPinViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 14/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ConfirmPinViewController: UIViewController {
    
    @IBOutlet var firstTextField: UITextField!
    @IBOutlet var secondTextField: UITextField!
    @IBOutlet var thirdTextField: UITextField!
    @IBOutlet var forthTextField: UITextField!

    var arrayPfPin = [String]()
    var pin        : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Enter PIN"
        navigationItem.hidesBackButton = true
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
        self.navigationController?.navigationBar.setBackgroundImage(image.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func keyboardActions(_ sender : UIButton) {
        let number = sender.currentTitle
        self.checkWhichTextFieldActive(number: number)
    }
    
    func checkWhichTextFieldActive(number: String?) {
        
        if firstTextField.text?.count == 0 {
            self.firstTextField.text = number
            self.secondTextField.becomeFirstResponder()
        } else if secondTextField.text?.count == 0 {
            self.secondTextField.text = number
            self.thirdTextField.becomeFirstResponder()
        } else if thirdTextField.text?.count == 0{
            self.thirdTextField.text = number
            self.forthTextField.becomeFirstResponder()
        } else if forthTextField.text?.count == 0 {
            self.forthTextField.text = number
        }
    }
    
    @IBAction func deleteTextfieldText(_ sender: UIButton) {
        
        if forthTextField.text?.count != 0 {
            forthTextField.text = ""
        } else if thirdTextField.text?.count != 0 {
            thirdTextField.text = ""
        } else if secondTextField.text?.count != 0 {
            secondTextField.text = ""
        }else { firstTextField.text = ""
        }
    }
    
    
    @IBAction func enterPinButton(_ sender: UIButton) {
        if firstTextField.text == "" || secondTextField.text == "" || thirdTextField.text == "" || forthTextField.text == "" {
            self.showAlert("Please enter 4 digit pin to proceed further.")
        }
        else {
            if Reachability.isConnectedToNetwork() {
                self.hitAPIConfirmPI()
            } else {
                self.showAlert(Constants.commomInternetmsz.checkInternet)
            }
        }
    }
    
    //API to confirm Pin
    func hitAPIConfirmPI(){
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let apiName    = "enterPin"
        let user_id    = userInfoModel.id ?? ""
        let firstPin = (self.firstTextField.text ?? "") + (self.secondTextField.text ?? "")
        let secondPin = (self.thirdTextField.text ?? "") + (self.forthTextField.text ?? "")

        let userPin    = firstPin + secondPin
        
        let paramsStr = "user_id=\(user_id)&pin=\(userPin)"
        
        WebServiceClass.showLoader(view: self.view)

        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + apiName, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    
                    print(jsonResult)
                    
                    if let errorCode = jsonResult["error"] as? Int, errorCode == 1 {
                        self.showAlert(jsonResult["message"] as? String ?? "")
                    } else {
                        DispatchQueue.main.async {
                            CallBackMethods.appDelegate?.changeRootViewController()
                        }
                    }
                    
                } else {
                    print("Data is not in proper format")
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
}

extension ConfirmPinViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case firstTextField:
            
            if string.count > 0 {
                
                DispatchQueue.main.async {
                    self.secondTextField.becomeFirstResponder()
                }
            }
            
            print("first")
        case secondTextField:
            
            if string.count > 0 {
                
                DispatchQueue.main.async {
                    self.thirdTextField.becomeFirstResponder()
                }
                
            }
            print("second")
        case thirdTextField:
            print("third")
            if string.count > 0 {
                
                DispatchQueue.main.async {
                    self.forthTextField.becomeFirstResponder()
                }
                
            }
        default:
            print("final")
            
        }
        return true
        
    }
}
