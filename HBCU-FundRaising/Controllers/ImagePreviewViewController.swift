//
//  ImagePreviewViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 09/10/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ImagePreviewViewController: UIViewController {

    // Outlet
    @IBOutlet weak var imageObj: UIImageView!
    var scrollView: UIScrollView!
    var fileUrl:String?
    var imageGetArray = [ChatMessageModel]()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let image = fileUrl {
            if image.contains("http://"){
                imageObj.sd_setImage(with: URL(string: image ), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                imageObj.contentMode = .scaleAspectFit
            } else {
                let userImage = Constants.APIs.imageBaseURL + image
                let urlString = userImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                imageObj.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
            }
        }
//        print(fileUrl ?? "")
//        imageObj.image =  self.fixOrientation(img:self.load(fileName: fileUrl!)!)
//        imageObj.image =  self.load(fileName: fileUrl!)
//        imageObj.contentMode = UIViewContentMode.scaleAspectFit
    }
    
    func load(fileName: String) -> UIImage?{
        let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            return UIImage(data: imageData)
        } catch {
            print("Error loading image : \(error)")
        }
        return nil
    }
    
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    //MARK:- Cross Action
  
    @IBAction func crossAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

var documentsUrl: URL {
    return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
}
