//
//  BonusChallengeDetailViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 11/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage
import Branch
import IQKeyboardManager

class BonusChallengeDetailViewController: UIViewController {

    @IBOutlet weak var bonusChallengeImageView: UIImageView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var amountRaisedLabel: UILabel!
    
    @IBOutlet weak var challengeTitle: UILabel!
    @IBOutlet weak var challengeDescriptionLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    
    @IBOutlet weak var bonusAboutAndTermsCollectionView: UICollectionView!
    @IBOutlet weak var bonusLeaderboardCollectionView: UICollectionView!
    @IBOutlet weak var bonusAboutTermsTextView: UITextView!
    
    struct Storyboard {
        static let BonusDetailFooterView = "BonusDetailFooterView"
    }
    
    var challengesDetailModel = [ChallengesDetailModel]()
    
    var titleArray = ["Leaderboard","About","Terms & Conditions"]
    
    var getResultDict : ChallengesDetailModel?
    var hbcuDetailModel = [hbcuDetailsModel]()
    
    var selectedTitleIndex = Int ()
    var id : String?
    var Donate_Now_View1 : DonateNowView?
    var checkChallengeType = "1"
    
    //Timer:
    var seconds = Int()
    var timer = Timer()
    //var isTimerRunning = false
    
    var serverDateTimeStr : String?
    var serverDateInDateFormat : Date?
    var startDateInDateFormat : Date?
    var endtDateInDateFormat : Date?
    var timerStr : String?
    
    var fromAppDelegate = false
    var fromAppDelegateEventID : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bonusAboutAndTermsCollectionView.delegate = self
        bonusAboutAndTermsCollectionView.dataSource = self
        bonusLeaderboardCollectionView.delegate = self
        bonusLeaderboardCollectionView.dataSource = self
        bonusAboutAndTermsCollectionView.register(UINib(nibName: "AboutAndTermsAndConditionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AboutAndTermsAndConditionCollectionViewCell")
        bonusLeaderboardCollectionView.register(UINib(nibName: "LeaderboardDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeaderboardDetailCollectionViewCell")
        bonusLeaderboardCollectionView.isHidden = false
        bonusAboutTermsTextView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.getChallengeDetailsAPI()
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        self.bonusAboutAndTermsCollectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: [])
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        
        timer.invalidate()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        timer.invalidate()
    }
    
    @IBAction func shareButtonPressed(_ sender: UIButton) {
        
        guard getResultDict != nil else{return}

        let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
        buo.title = Constants.appTitle.alertTitle
        buo.contentDescription = self.getResultDict?.name ?? ""
        buo.imageUrl = Constants.APIs.imageBaseURL + (self.getResultDict?.image ?? "" )
        buo.contentMetadata.customMetadata["challenge_id"] = self.getResultDict?.id ?? ""
        buo.contentMetadata.customMetadata["screen_type"] = Constants.screen_type.BONUS_CHALLENGE
        
        let lp: BranchLinkProperties = BranchLinkProperties()
        
        //lp.channel = "facebook"
        lp.feature = "sharing"
        lp.campaign = "content 123 launch"
        lp.stage = "new user"
        lp.tags = ["one", "two", "three"]
        
//        lp.addControlParam("$desktop_url", withValue: "http://example.com/desktop")
//        lp.addControlParam("$ios_url", withValue: "http://example.com/ios")
//        lp.addControlParam("$ipad_url", withValue: "http://example.com/ios")
//        lp.addControlParam("$android_url", withValue: "http://example.com/android")
//        lp.addControlParam("$match_duration", withValue: "2000")
        
        lp.addControlParam("custom_data", withValue: "yes")
        lp.addControlParam("look_at", withValue: "this")
        lp.addControlParam("nav_to", withValue: "over here")
        lp.addControlParam("random", withValue: UUID.init().uuidString)
        
        buo.getShortUrl(with: lp) { (url, error) in
            if error == nil {
                print("got my Branch link to share: %@", url ?? "not found")
                
                //let message = "Check out this link"
                buo.showShareSheet(with: lp, andShareText: nil, from: self) { (activityType, completed) in
                    print(activityType ?? "")
                }
            }
        }
    }
    
    @IBAction func donateNowButtonPressed(_ sender: UIButton) {
        
        //For loading add perticueler vc
        
        Donate_Now_View1 = DonateNowView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        //Donate_Now_View?.
        Donate_Now_View1?.showDonateViewDelegate1 = self
        self.view.addSubview((Donate_Now_View1!))
    }
    
    func setTopBannerData(){
        
        if getResultDict != nil{
            bonusChallengeImageView.sd_setImage(with: URL(string: Constants.APIs.imageBaseURL + (self.getResultDict?.image ?? "")), placeholderImage: UIImage(named: "ic_image_thumb_small@x"))
            
            let amountString = self.getResultDict?.amount ?? ""
            let valueInDouble : Double = Double(amountString as? String ?? "0") ?? 0.0
            let amountResult = self.formatNumber(valueInDouble)
            
            amountLabel.text = "$\(amountResult ?? "")"
            amountRaisedLabel.text = "Amount Raised"
            
            challengeTitle.text = self.getResultDict?.name ?? ""
            challengeDescriptionLabel.text = self.getResultDict?.description ?? ""
            //endTimeLabel.text
            
//            let startDateString = self.UTCToLocalStringValue(date: self.getResultDict?.startDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "dd MMM, HH:mm a")
//            let endtDateString = self.UTCToLocalStringValue(date: self.getResultDict?.endDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "dd MMM, HH:mm a")

            let startDateString = self.convertTimeFormaters(self.getResultDict?.startDate ?? "")
            let endtDateString = self.convertTimeFormaters(self.getResultDict?.endDate ?? "")
            
            startDateInDateFormat = self.getDateFormatterDate(date: self.getResultDict?.startDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            endtDateInDateFormat = self.getDateFormatterDate(date: self.getResultDict?.endDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
            if let serverDateInDateFormat = serverDateInDateFormat, let startDateInDateFormat = startDateInDateFormat {
                
                let startDateComparisionResult:ComparisonResult = serverDateInDateFormat.compare(startDateInDateFormat)
                
                if startDateComparisionResult == ComparisonResult.orderedAscending
                {
                    // Server date is smaller than Start date.
                    
                    //self.runTimer(serverDate: serverDateInDateFormat ?? Date(), grabDate: startDateInDateFormat ?? Date())
                    
                    endTimeLabel.text = "Starts: \(startDateString ?? "")"
                }
                else if startDateComparisionResult == ComparisonResult.orderedDescending
                {
                    // Server date is greater than Start date.
                    
                    //self.runTimer(serverDate: startDateInDateFormat ?? Date(), grabDate: endtDateInDateFormat ?? Date())
                    
                    endTimeLabel.text = "Ends: \(endtDateString ?? "")"
                }
                else if startDateComparisionResult == ComparisonResult.orderedSame
                {
                    // Server date and Start date are same
                    
                    //self.runTimer(serverDate: startDateInDateFormat ?? Date(), grabDate: endtDateInDateFormat ?? Date())
                    
                    endTimeLabel.text = "Ends: \(endtDateString ?? "")"
                }else{}
            }

        }else{}
    }
    
    func formatNumber(_ num: Double) ->String{
        let thousandNum = num/1000
        let millionNum = num/1000000
        if num >= 1000 && num < 1000000{
            if(thousandNum == thousandNum){
                //let result = thousandNum.truncate(places: 1)
                let result = thousandNum.truncate(places: 2)
                return("\(result)K")
            }
            //let result = thousandNum.truncate(places: 1)
            let result = thousandNum.truncate(places: 2)
            return("\(result)K")
        }
        if num > 1000000{
            if(millionNum == millionNum){
                //let result = millionNum.truncate(places: 1)
                let result = thousandNum.truncate(places: 2)
                return("\(result)M")
            }
            //let result = millionNum.truncate(places: 1)
            let result = thousandNum.truncate(places: 2)
            return("\(result)M")
        }
        else{
            if(num == num){
                
                let no = num.truncate(places: 2)
                let number = "\(no)"
                let result = number.split(separator: ".")
                if result.count >= 2{
                    if result[1] == "0" || result[1] == "00"{
                        return ("\(result[0]).00")
                    }else if result[1].count == 1{
                        return ("\(num)0")
                    }else if result[1].count == 2{
                        return ("\(num)")
                    }
                }
                
            }
            return ("\(num)")
        }
        
    }
    
    func serverTime(){
        if self.serverDateTimeStr != nil && self.serverDateTimeStr != ""{
            //serverDateTime = self.UTCToLocalStringValue(date: self.serverDateTimeStr ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "dd MMMM,yyyy h:mm a")
            let serverDateString = self.UTCToLocalStringValue(date: self.serverDateTimeStr ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
            serverDateInDateFormat = self.getDateFormatterDate(date: serverDateString, incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
        }else{}
    }
    
    
    func runTimer(serverDate : Date, grabDate : Date) {
        
        let mingap = Calendar.current.dateComponents([.second], from: serverDate ?? Date(), to: grabDate ?? Date())
        print("gap is\(mingap.second!)")
        
        seconds = mingap.second!
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(ChallengeTableViewCell.updateTimer)), userInfo: nil, repeats: true)
        
    }
    
    func timeString(time:TimeInterval) -> String {
        
        let days = ((Int(time) / 86400))
        let hours = ((Int(time) % 86400) / 3600)
        let minutes = ((Int(time) % 3600) / 60)
        let seconds = ((Int(time) % 3600) % 60)
        return String(format:"%02i Days, %02i:%02i:%02i", days, hours, minutes, seconds)
    }
    
    @objc func updateTimer() {
        
        if seconds < 1 {
            timer.invalidate()
            //Send alert to indicate "time's up!"
        } else {
            seconds -= 1    //This will decrement(count down)the seconds
            self.timerStr = timeString(time: TimeInterval(seconds))  //This will update the label.
            
//            if let serverDateInDateFormat = serverDateInDateFormat, let startDateInDateFormat = startDateInDateFormat {
//
//                let startDateComparisionResult:ComparisonResult = serverDateInDateFormat.compare(startDateInDateFormat)
//
//                if startDateComparisionResult == ComparisonResult.orderedAscending
//                {
//                    // Server date is smaller than Start date.
//
//                    endTimeLabel.text = "Starts in: \(timerStr ?? "")"
//                }
//                else if startDateComparisionResult == ComparisonResult.orderedDescending
//                {
//                    // Server date is greater than Start date.
//
//                    endTimeLabel.text = "Ends in: \(timerStr ?? "")"
//                }
//                else if startDateComparisionResult == ComparisonResult.orderedSame
//                {
//                    // Server date and Start date are same
//
//                    endTimeLabel.text = "Ends in: \(timerStr ?? "")"
//                }else{
//
//                }
//            }
        }
    }
    
    func convertTimeFormaters(_ date: String) -> String? {
        
        if date.isEmpty == false {
            let myDateString = date
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.amSymbol = "AM"
            dateFormatter.pmSymbol = "PM"
            
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormatter.date(from: myDateString)!
            
            //dateFormatter.dateFormat = "dd MMM, h:mm a"
            dateFormatter.dateFormat = "MMMM, dd, h:mm a"
            
            let somedateString = dateFormatter.string(from: myDate as Date)
            return somedateString
        }
        return ""
        
    }
    
    
}

extension BonusChallengeDetailViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == bonusAboutAndTermsCollectionView{
            return titleArray.count
        }else{
            return hbcuDetailModel.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == bonusAboutAndTermsCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutAndTermsAndConditionCollectionViewCell", for: indexPath) as! AboutAndTermsAndConditionCollectionViewCell
            
            cell.MainTitleLabel.text = titleArray[indexPath.row]
            
            if selectedTitleIndex == indexPath.row
            {
                cell.backView.backgroundColor = UIColor.white
                cell.backView.borderwidth = 1
                cell.MainTitleLabel.textColor = UIColor.black
            }
            else
            {
                cell.backView.backgroundColor = UIColor(red: 244/255, green: 246/255, blue: 246/255, alpha: 1.0)
                cell.backView.borderwidth = 0
                cell.MainTitleLabel.textColor = UIColor.lightGray
            }
            
            return cell
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeaderboardDetailCollectionViewCell", for: indexPath) as! LeaderboardDetailCollectionViewCell
            
            cell.leaderboardImageView.sd_setImage(with: URL(string: Constants.APIs.imageBaseURL + (self.hbcuDetailModel[indexPath.row].logo ?? "")), placeholderImage: UIImage(named: "ic_image_thumb_small@x"))
            cell.leaderboardTitleLabel.text = self.hbcuDetailModel[indexPath.row].title ?? ""
            cell.leaderboardSubtitleLabel.text = "Amount Raised"
            cell.AmountLabel.text = "$\(self.hbcuDetailModel[indexPath.item].amount ?? "")"
            
            if hbcuDetailModel.count > 0 {
                switch self.hbcuDetailModel[indexPath.item].level ?? 0 {
                case 1:
                    cell.throphyImageView.image = UIImage(named: "ic_medal_gold.png")
                case 2:
                    cell.throphyImageView.image = UIImage(named: "ic_medal_silver.png")
                case 3:
                    cell.throphyImageView.image = UIImage(named: "ic_medal_bronze.png")
                default:
                    print("Testing")
                }
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if collectionView == bonusAboutAndTermsCollectionView{
            
            return CGSize(width: 0, height: 0)
        }else{
            return CGSize(width: collectionView.frame.size.width, height: 100)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if collectionView == bonusAboutAndTermsCollectionView{
            
            return UICollectionReusableView()
        }else{
            let bonusDetailFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Storyboard.BonusDetailFooterView , for: indexPath) as! BonusDetailFooterView
            
            bonusDetailFooterView.BonusDetailViewAllButton.addTarget(self, action: #selector(viewAllPressed(_:)), for: .touchUpInside)
            
            return bonusDetailFooterView
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == bonusAboutAndTermsCollectionView{
            
            return CGSize(width: (collectionView.frame.size.width/3)-5, height: 40)
        }else{
            //return CGSize(width: collectionView.frame.size.width/3.428, height: collectionView.frame.size.height/2)
            return CGSize(width: (collectionView.frame.size.width/3)-5, height: collectionView.frame.size.height/2)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == bonusAboutAndTermsCollectionView{
            
            return 10
        }else{
            //return 15
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == bonusAboutAndTermsCollectionView{
            
            return 0
        }else{
            //return 15
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == bonusAboutAndTermsCollectionView{
            
            switch indexPath.row {
            case 0 :
                bonusLeaderboardCollectionView.isHidden = false
                bonusAboutTermsTextView.isHidden = true
                
            case 1 :
                bonusLeaderboardCollectionView.isHidden = true
                bonusAboutTermsTextView.isHidden = false
                if getResultDict != nil{
                    bonusAboutTermsTextView.text = getResultDict?.about ?? ""
                }else{}
                
            case 2 :
                bonusLeaderboardCollectionView.isHidden = true
                bonusAboutTermsTextView.isHidden = false
                if getResultDict != nil{
                    bonusAboutTermsTextView.text = getResultDict?.termsConditions ?? ""
                }else{}
                
                
            default :
                print("default")
            }
            
            selectedTitleIndex = indexPath.row
            self.bonusAboutAndTermsCollectionView.reloadData()
            
        }else{
            
        }
        
    }
    
    @objc func viewAllPressed(_ : ButtonDesign){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewAllViewController") as! ViewAllViewController
        
        if challengesDetailModel != nil && challengesDetailModel.count > 0{
            vc.challengesDetailModel = self.challengesDetailModel
        }else{}
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension BonusChallengeDetailViewController {
    
    func getChallengeDetailsAPI(){
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id = userInfoModel.id as? String
        //let id = self.challengesDetailModel[0].id ?? ""
  
        if fromAppDelegate == true{
            id = self.fromAppDelegateEventID
        }else{
            id = self.challengesDetailModel[0].id ?? ""
        }
        
        WebServiceClass.showLoader(view: self.view)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.challengeBaseURL + Constants.APIs.challengeDetails + "?id=\(id ?? "")&user_id=\(user_id ?? "0")", method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult    = response as? Dictionary<String, AnyObject> {
                    
                    if let error = jsonResult["error"] as? Bool {
                        if error == true  {
                        }else {
                            
                                if let serverDateTime = jsonResult["server"] as? String{
                                    self.serverDateTimeStr = serverDateTime
                                    self.serverTime()
                                }
                                if let result = jsonResult["result"] as? Dictionary<String, AnyObject>{
                                
                                    if let modelData = Mapper<ChallengesDetailModel>().map(JSONObject: result){
                                        self.getResultDict = modelData
                                        self.setTopBannerData()
                                        
                                    }
                                
                                    if let bonusData = result["hbcu"] as? [Dictionary<String, AnyObject>]{
                                        if let modelData = Mapper<hbcuDetailsModel>().mapArray(JSONObject: bonusData) {
                                            self.hbcuDetailModel = modelData
                                            
                                        }
                                    }
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.bonusLeaderboardCollectionView.reloadData()
                }
            }else {
                print("Data is not in proper format")
            }
        }
    }
    
}

// for delegate

extension BonusChallengeDetailViewController : ShowDonateViewDismissProtocol {
    
    func dismiss(hit : Bool) {
        self.Donate_Now_View1?.removeFromSuperview()
    }
    
    func donateButtonPressed(hit : Bool) {
        
        if Donate_Now_View1?.amountTextField.text == "" || Donate_Now_View1?.amountTextField.text == nil  {
            self.showAlert("Please enter some amount")
        }else if Donate_Now_View1?.amountTextField.text == "0"{
            self.showAlert("Minimun amount is $1")
        }else{
            
            if getResultDict != nil{
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectHBCUToDonateVC") as! SelectHBCUToDonateVC
                
                vc.challenge_id = getResultDict?.id ?? ""
                vc.challengeDonateAmount = Donate_Now_View1?.amountTextField.text
                vc.enumCases = SelectHBCUEnum.ChallengeDonation
                
                self.Donate_Now_View1?.removeFromSuperview()
                self.navigationController?.pushViewController(vc, animated: true)
            }else{}
            
        }
    }
}
