//
//  JoinNowViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 11/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage
import Branch
import IQKeyboardManager
import Foundation

class JoinNowViewController: UIViewController {

    @IBOutlet weak var joinNowImageView: UIImageView!
    @IBOutlet weak var millionDollarChallengeLabel: UILabel!
    @IBOutlet weak var overMillionRaisedLabel: UILabel!
    @IBOutlet weak var LoremDescriptionLabel: UILabel!
    @IBOutlet weak var termsAndConditionCollectionView: UICollectionView!
    @IBOutlet weak var leaderboardDetailCollectionView: UICollectionView!
    @IBOutlet weak var aboutAndTermsTextView: UITextView!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    
    struct Storyboard {
        static let ViewAllButtonFooterView = "ViewAllButtonFooterView"
    }
    
    var titleArray = ["Leaderboard","About","Terms & Conditions"]
    //var titleArray = ["Leaderboard","About","Terms & Conditions","Terms & Conditions Terms & Conditions Terms & Conditions"]
    
    var topBannerChallengeDetailDict : ChallengesDetailModel?
    var getResultDict : ChallengesDetailModel?
    var hbcuDetailModel = [hbcuDetailsModel]()
    
    var selectedTitleIndex = Int ()
    
    var Donate_Now_View : DonateNowView?
    //var mode = SelectHBCUEnum.ChallengeDonation
    
    var fromAppDelegate = false
    var fromAppDelegateEventID : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        termsAndConditionCollectionView.delegate = self
        termsAndConditionCollectionView.dataSource = self
        leaderboardDetailCollectionView.delegate = self
        leaderboardDetailCollectionView.dataSource = self
    
        termsAndConditionCollectionView.register(UINib(nibName: "AboutAndTermsAndConditionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AboutAndTermsAndConditionCollectionViewCell")
        leaderboardDetailCollectionView.register(UINib(nibName: "LeaderboardDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeaderboardDetailCollectionViewCell")
        
        leaderboardDetailCollectionView.isHidden = false
        aboutAndTermsTextView.isHidden = true
       // NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        getChallengeDetailsAPI()
        
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        self.termsAndConditionCollectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: [])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func shareButtonPressed(_ sender: UIButton) {
        
        guard getResultDict != nil else{return}
        
        let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
        
        buo.title = Constants.appTitle.alertTitle
        buo.contentDescription = self.getResultDict?.name ?? ""
        buo.imageUrl = Constants.APIs.imageBaseURL + (self.getResultDict?.image ?? "" )
        buo.contentMetadata.customMetadata["challenge_id"] = self.getResultDict?.id ?? ""
        buo.contentMetadata.customMetadata["screen_type"] = Constants.screen_type.MILLION_DOLLAR_CHALLENGE
       
        let lp: BranchLinkProperties = BranchLinkProperties()
        
        //lp.channel = "facebook"
        lp.feature = "sharing"
        lp.campaign = "content 123 launch"
        lp.stage = "new user"
        lp.tags = ["one", "two", "three"]
        
//        lp.addControlParam("$desktop_url", withValue: "http://example.com/desktop")
//        lp.addControlParam("$ios_url", withValue: "http://example.com/ios")
//        lp.addControlParam("$ipad_url", withValue: "http://example.com/ios")
//        lp.addControlParam("$android_url", withValue: "http://example.com/android")
//        lp.addControlParam("$match_duration", withValue: "2000")
        
        lp.addControlParam("custom_data", withValue: "yes")
        lp.addControlParam("look_at", withValue: "this")
        lp.addControlParam("nav_to", withValue: "over here")
        lp.addControlParam("random", withValue: UUID.init().uuidString)
        
        buo.getShortUrl(with: lp) { (url, error) in
            if error == nil {
                print("got my Branch link to share: %@", url ?? "not found")
                
                //let message = "Check out this link"
                buo.showShareSheet(with: lp, andShareText: nil, from: self) { (activityType, completed) in
                    print(activityType ?? "")
                }
            }
        }
        
    }
    
    @IBAction func donateNowButtonPressed(_ sender: UIButton) {
        
        //For loading add perticueler vc
        Donate_Now_View = DonateNowView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        //Donate_Now_View?.amountTextField
        
        Donate_Now_View?.donateButton.setTitle("Next", for: .normal)
        
        Donate_Now_View?.showDonateViewDelegate = self
        self.view.addSubview((Donate_Now_View!))
        
//          let window = UIApplication.shared.keyWindow!
//        let v = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
//        window.addSubview(v);
//        v.backgroundColor = UIColor.black
//        let v2 = UIView(frame: CGRect(x: 50, y: 50, width: 100, height: 50))
//        v2.backgroundColor = UIColor.white
//        window.addSubview(v2)
        
    }
    
    func setTopBannerData(){
        
        if getResultDict != nil{
            joinNowImageView.sd_setImage(with: URL(string: Constants.APIs.imageBaseURL + (self.getResultDict?.image ?? "")), placeholderImage: UIImage(named: "ic_image_thumb_small@x"))
            
            let titleString = self.getResultDict?.name ?? ""
            if titleString.count > 0 && titleString != ""{
                millionDollarChallengeLabel.text = titleString.uppercased()
            }else{}
            
            let amountString = self.getResultDict?.amount ?? ""
            let valueInDouble : Double = Double(amountString as? String ?? "0") ?? 0.0
            let amountResult = self.formatNumber(valueInDouble)
            
            overMillionRaisedLabel.text = amountResult ?? ""
            LoremDescriptionLabel.text = self.getResultDict?.description ?? ""
        }else{}
        
    }
    
    func formatNumber(_ num: Double) ->String {
        let thousandNum = num/1000
        let millionNum = num/1000000
        if num >= 1000 && num < 1000000{
            if(thousandNum == thousandNum){
                let result =  String(format: "%.2f", ceil(num*100)/100)
                //     let result = num.truncate(places: 2)
                return ("\(result)K")
            }
            let result =  String(format: "%.2f", ceil(num*100)/100)
            //     let result = num.truncate(places: 2)
            return ("\(result)K")
        }
        if num > 1000000 {
            if(millionNum == millionNum){
                let result =  String(format: "%.2f", ceil(num*100)/100)
                //     let result = num.truncate(places: 2)
                return ("\(result)M")
            }
            let result = millionNum.truncate(places: 1)
            return("\(result)M")
        }else{
            if(num == num){
                let result =  String(format: "%.2f", ceil(num*100)/100)
                //     let result = num.truncate(places: 2)
                return ("\(result)")
            }
            //  }
            //  let result = num.truncate(places: 2)
            // return("\(result)")
        }
        let result =  String(format: "%.2f", ceil(num*100)/100)
        return ("\(result)")
    }
    
//    func formatNumber(_ num: Double) ->String {
//        let thousandNum = num/1000
//        let millionNum = num/1000000
//        if num >= 1000 && num < 1000000{
//            if(thousandNum == thousandNum){
//                let result = thousandNum.truncate(places: 1)
//                return("\(result)K")
//            }
//            let result = thousandNum.truncate(places: 1)
//            return("\(result)K")
//        }
//        if num > 1000000 {
//            if(millionNum == millionNum){
//                let result = millionNum.truncate(places: 1)
//                return("\(result)M")
//            }
//            let result = millionNum.truncate(places: 1)
//            return("\(result)M")
//        }else{
//            if(num == num){
//                let result =  String(format: "%.2f", ceil(num*100)/100)
//                //     let result = num.truncate(places: 2)
//                return ("\(result)")
//            }
//            //  }
//            //  let result = num.truncate(places: 2)
//            // return("\(result)")
//        }
//        let result =  String(format: "%.2f", ceil(num*100)/100)
//        return ("\(result)")
//    }
    
//    //MARK:- Keyboard Observer
//    @objc func keyboardNotification(notification: NSNotification) {
//
//        if let userInfo = notification.userInfo {
//            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
//            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
//            let _ = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
//            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
//                self.bottomLayoutConstraint?.constant = 0.0
//            } else {
//                self.bottomLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
//            }
//            self.view.layoutIfNeeded()
//        }
//    }
    
}

extension JoinNowViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == termsAndConditionCollectionView{
            return titleArray.count
        }else{
            return hbcuDetailModel.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == termsAndConditionCollectionView{
            
            let cell : AboutAndTermsAndConditionCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutAndTermsAndConditionCollectionViewCell", for: indexPath) as! AboutAndTermsAndConditionCollectionViewCell

            cell.MainTitleLabel.text = titleArray[indexPath.row]
            //cell.MainTitleLabel.sizeToFit()

                if selectedTitleIndex == indexPath.row{
                    cell.backView.backgroundColor = UIColor.white
                    cell.backView.borderwidth = 1
                    cell.MainTitleLabel.textColor = UIColor.black
                }else{
                    cell.backView.backgroundColor = UIColor(red: 244/255, green: 246/255, blue: 246/255, alpha: 1.0)
                    cell.backView.borderwidth = 0
                    cell.MainTitleLabel.textColor = UIColor.lightGray
                }
 
            return cell
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeaderboardDetailCollectionViewCell", for: indexPath) as! LeaderboardDetailCollectionViewCell
            
            cell.leaderboardImageView.sd_setImage(with: URL(string: Constants.APIs.imageBaseURL + (self.hbcuDetailModel[indexPath.row].logo ?? "")), placeholderImage: UIImage(named: "ic_image_thumb_small@x"))
            cell.leaderboardTitleLabel.text = self.hbcuDetailModel[indexPath.row].title ?? ""
            cell.leaderboardSubtitleLabel.text = "Amount Raised"
            let amountString = self.hbcuDetailModel[indexPath.item].amount ?? ""
            let valueInDouble : Double = Double(amountString as? String ?? "0") ?? 0.0
            let amountResult = self.formatNumber(valueInDouble)
            
            cell.AmountLabel.text = "$\(amountResult)"
           // cell.AmountLabel.text = "$\(self.hbcuDetailModel[indexPath.item].amount ?? "")"
            
            if hbcuDetailModel.count > 0{
                
                    if self.hbcuDetailModel[indexPath.item].level == 1{
                        cell.throphyImageView.image = UIImage(named: "ic_medal_gold.png")
                    }else if self.hbcuDetailModel[indexPath.item].level == 2{
                        cell.throphyImageView.image = UIImage(named: "ic_medal_silver.png")
                    }else if self.hbcuDetailModel[indexPath.item].level == 3{
                        cell.throphyImageView.image = UIImage(named: "ic_medal_bronze.png")
                    }else{}
                
            }else{}

            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {

        if collectionView == termsAndConditionCollectionView{

            return CGSize(width: 0, height: 0)
        }else{
            return CGSize(width: collectionView.frame.size.width, height: 100)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if collectionView == termsAndConditionCollectionView{
            
            return UICollectionReusableView()
        }else{
            let viewAllButtonFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Storyboard.ViewAllButtonFooterView , for: indexPath) as! ViewAllButtonFooterView
            
            viewAllButtonFooterView.viewAllButton.addTarget(self, action: #selector(viewAllPressed(_:)), for: .touchUpInside)
            
            return viewAllButtonFooterView
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == termsAndConditionCollectionView{
            
            return CGSize(width: (collectionView.frame.size.width/3)-5, height: 40)
            //return UICollectionViewFlowLayoutAutomaticSize
            
//            let text = titleArray[indexPath.row].trimmingCharacters(in: .whitespacesAndNewlines)
//            //let newText = NSAttributedString(string: text)
//            let newText = NSAttributedString(string:text,attributes:[NSAttributedString.Key.foregroundColor: UIColor.lightGray,NSAttributedString.Key.font: UIFont(name: "Arial", size: 18.0) as Any])
//
//            let result = text.components(separatedBy: " ")
//            if result.count > 1{
//                return CGSize(width: newText.size().width, height: 40)
//            }else{
//                return CGSize(width: newText.size().width+20, height: 40)
//            }
            
//            let lbl = UILabel()
//            lbl.text = titleArray[indexPath.row]
//
//            var rect: CGRect = lbl.frame //get frame of label
//            //rect.size = (label.text?.size(attributes: [NSFontAttributeName: UIFont(name: label.font.fontName , size: label.font.pointSize)!]))! //Calculate as per label font
//            //labelWidth.constant = rect.width // set width to Constraint outlet
//
//            //rect.size = (lbl.text?.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: lbl.font.fontName , size: lbl.font.pointSize)!]))!
//            //rect.size = (lbl.text?.size(withAttributes: [NSAttributedStringKey.font: UIFont(name: "Arial" , size: 20.0)!]))!
//           rect.size = lbl.intrinsicContentSize
//
//            return CGSize(width: rect.width, height: 40)

        }else{
            //return CGSize(width: collectionView.frame.size.width/3.428, height: collectionView.frame.size.height/2.2)
            return CGSize(width: (collectionView.frame.size.width/3)-5, height: collectionView.frame.size.height/2.2)
        }

     }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

        if collectionView == termsAndConditionCollectionView{
            
            return 10
        }else{
            //return 15
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        if collectionView == termsAndConditionCollectionView{
            
            return 0
        }else{
            //return 15
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == termsAndConditionCollectionView{
            
            switch indexPath.row {
            case 0 :
                leaderboardDetailCollectionView.isHidden = false
                aboutAndTermsTextView.isHidden = true
                
            case 1 :
                leaderboardDetailCollectionView.isHidden = true
                aboutAndTermsTextView.isHidden = false
                if getResultDict != nil{
                    aboutAndTermsTextView.text = getResultDict?.about ?? ""
                }else{}
                
            case 2 :
                leaderboardDetailCollectionView.isHidden = true
                aboutAndTermsTextView.isHidden = false
                if getResultDict != nil{
                    aboutAndTermsTextView.text = getResultDict?.termsConditions ?? ""
                }else{}
                
            default :
                print("default")
            }
            
            selectedTitleIndex = indexPath.row
            self.termsAndConditionCollectionView.reloadData()
            
        }else{}
        
    }
    
    @objc func viewAllPressed(_ : ButtonDesign){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewAllViewController") as! ViewAllViewController
        
        if topBannerChallengeDetailDict != nil{
            vc.topBannerChallengeDetailDict = self.topBannerChallengeDetailDict
        }else{}
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension JoinNowViewController {
    
    func getChallengeDetailsAPI(){
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id = userInfoModel.id as? String 
        
        let id : String?
        
        if fromAppDelegate == true{
            id = self.fromAppDelegateEventID
        }else{
            id = self.topBannerChallengeDetailDict?.id ?? ""
        }
        
        WebServiceClass.showLoader(view: self.view)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.challengeBaseURL + Constants.APIs.challengeDetails + "?id=\(id ?? "")&user_id=\(user_id ?? "0")", method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                   
                    if let error = jsonResult["error"] as? Bool {
                        if error == true  {
                            
                        }else {
                            
                            if let result = jsonResult["result"] as? Dictionary<String, AnyObject>{
                                
                                    if let modelData = Mapper<ChallengesDetailModel>().map(JSONObject: result){
                                        self.getResultDict = modelData
                                        self.setTopBannerData()

                                    }
                
                                    if let bonusData = result["hbcu"] as? [Dictionary<String, AnyObject>]{
                                        if let modelData = Mapper<hbcuDetailsModel>().mapArray(JSONObject: bonusData) {
                                            self.hbcuDetailModel = modelData
  
                                        }
                                    }
                            }
                            
                        }
                    }
                }
                //DispatchQueue.main.async {
                    self.leaderboardDetailCollectionView.reloadData()
                //}
            }else {
                print("Data is not in proper format")
            }
        }
    }
    
}

// for delegate

extension JoinNowViewController : ShowDonateViewDismissProtocol {
    
    func dismiss(hit : Bool) {
        self.Donate_Now_View?.removeFromSuperview()
    }
    
    func donateButtonPressed(hit : Bool) {
    
        if Donate_Now_View?.amountTextField.text == "" || Donate_Now_View?.amountTextField.text == nil  {
            self.showAlert("Please enter some amount")
        }else if Donate_Now_View?.amountTextField.text == "0"{
            self.showAlert("Minimun amount is $1")
        }else{
            
            if getResultDict != nil{
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectHBCUToDonateVC") as! SelectHBCUToDonateVC
                
                vc.challenge_id = getResultDict?.id ?? ""
                vc.challengeDonateAmount = Donate_Now_View?.amountTextField.text
                vc.enumCases = SelectHBCUEnum.ChallengeDonation
                
                self.Donate_Now_View?.removeFromSuperview()
                self.navigationController?.pushViewController(vc, animated: true)
            }else{}
            
        }
   
    }
}

extension JoinNowViewController : UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        //textField.text = amount
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //Disable iQkeyboarad
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
      //  NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        //Disable iQkeyboarad
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        return true
    }
}

extension UILabel {
    func textWidth() -> CGFloat {
        return UILabel.textWidth(label: self)
    }
    
    class func textWidth(label: UILabel) -> CGFloat {
        return textWidth(label: label, text: label.text!)
    }
    
    class func textWidth(label: UILabel, text: String) -> CGFloat {
        return textWidth(font: label.font, text: text)
    }
    
    class func textWidth(font: UIFont, text: String) -> CGFloat {
        
        let myText = text as NSString
        
        let rect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: 50)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let labelSize = myText.boundingRect(with: rect, options: option, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(labelSize.width)
    }
}

extension String {
    var stringWidth: CGFloat {
        let constraintRect = CGSize(width: UIScreen.main.bounds.width, height: .greatestFiniteMagnitude)
        let boundingBox = self.trimmingCharacters(in: .whitespacesAndNewlines).boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)], context: nil)
        return boundingBox.width
    }
    
    var stringHeight: CGFloat {
        let constraintRect = CGSize(width: UIScreen.main.bounds.width, height: .greatestFiniteMagnitude)
        let boundingBox = self.trimmingCharacters(in: .whitespacesAndNewlines).boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)], context: nil)
        return boundingBox.height
    }
}

class PaddingLabel: UILabel {
    
    var topInset: CGFloat
    var bottomInset: CGFloat
    var leftInset: CGFloat
    var rightInset: CGFloat
    
    required init(withInsets top: CGFloat, _ bottom: CGFloat,_ left: CGFloat,_ right: CGFloat) {
        self.topInset = top
        self.bottomInset = bottom
        self.leftInset = left
        self.rightInset = right
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}
