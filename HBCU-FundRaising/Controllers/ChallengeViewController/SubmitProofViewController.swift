//
//  SubmitProofViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 11/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import Alamofire
//import AlamofireImage
import CropViewController
import ObjectMapper
import SDWebImage

class SubmitProofViewController: UIViewController {

    @IBOutlet weak var submitImageView: UIImageView!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var phoneNoTextField: UITextField!
    @IBOutlet weak var captionTextField: UITextField!
    @IBOutlet weak var shortDescriptionTextView: TextViewDesign!
    @IBOutlet weak var submitBtnOutlet: UIButton!
    
    var getImage: UIImage?
    var imagePicker = UIImagePickerController()
    
    var getResultDict : ChallengesDetailModel?
    var proofDetailModel = [ChallengeDetailProofModel]()
    

    
    var isAddingProof = false
    
    override func viewDidLoad(){
        super.viewDidLoad()

        // image delegate
        imagePicker.delegate = self
        if isAddingProof == true {
            if proofDetailModel.count > 0 {
                //self.submitImageView.sd_setImage(with: URL(string:(proofDetailModel[0].photo ?? "")), placeholderImage: nil)
                self.submitImageView.sd_setImage(with: URL(string: Constants.APIs.imageBaseURL + (self.proofDetailModel[0].photo ?? "")), placeholderImage: UIImage(named: "ic_upload_image@x"))
                self.phoneNoTextField.text = proofDetailModel[0].phone ?? ""
                self.captionTextField.text = proofDetailModel[0].caption ?? ""
                self.shortDescriptionTextView.text = proofDetailModel[0].description ?? ""
                self.submitBtnOutlet.setTitle("Update", for: .normal)
            }else{
                  self.submitBtnOutlet.setTitle("Submit", for: .normal)
            }
        }else{}
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
       
    }

    @IBAction func crossButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func addImageButtonPressed(_ sender: UIButton) {
        self.addImage()
    }
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork() == true {
            
            if validate() {
                
                if isAddingProof == true {
                    
                    if proofDetailModel.count > 0 {
                        self.editProofApi()
                    }
                    
                }else{
                    self.addProofApi()
                }
            
            }
        } else {
            self.showAlert("NO_INTERNET_AVAILABLE")
        }
    }
 
    
    // MARK: - Validation method
    func validate() -> Bool {
        
        /* if submitImageView.image == #imageLiteral(resourceName: "Add Item") {
         
         self.showTextAlertLabel(error: "Please Select Image")
         
         }*/
      
        if phoneNoTextField.text == "" && captionTextField.text == "" && shortDescriptionTextView.text == "" {
            
            self.showAlert("All fields are empty")
        } else if phoneNoTextField.text == "" {
            
            self.showAlert("Please Enter Phone Number")
        } else if captionTextField.text! == "" {
            
            self.showAlert("Please Enter Caption")
        }else if shortDescriptionTextView.text! == "" {
            
            self.showAlert("Please Enter Description")
        } else {
            
            return true
        }
        return false
    }
}

//MARK:- Image Picker Delegate

extension SubmitProofViewController :  CropViewControllerDelegate {
    
    func addImage() {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let myImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        //        userImage =
        dismiss(animated: true, completion: nil)
        self.presentCropViewController(image: myImage!)
    }
    
    // new Cropper Function
    
    func presentCropViewController(image : UIImage) {
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self as CropViewControllerDelegate
        
        //cropViewController.aspectRatioPreset = .presetCustom //Set the initial aspect ratio as a square
        cropViewController.aspectRatioPreset = .presetSquare
        cropViewController.aspectRatioLockEnabled = true // The crop box is locked to the aspect ratio and can't be resized away from it
        cropViewController.resetAspectRatioEnabled = false // When tapping 'reset', the aspect ratio will NOT be reset back to default
        cropViewController.aspectRatioPickerButtonHidden = true
        
        cropViewController.rotateButtonsHidden = true
        cropViewController.rotateClockwiseButtonHidden = true
        
        present(cropViewController, animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        self.getImage = self.submitImageView.image
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        
        self.submitImageView.image = image
        self.getImage = image
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
    
    
    override func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
}

extension SubmitProofViewController{
    
    func addProofApi() {
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let params: [String: String] = [
            "challenge_id"     : getResultDict?.id ?? "",
            "user_id"        : userInfoModel.id as? String ?? "0" ,
            "phone"          : phoneNoTextField.text ?? "",
            "caption"        : captionTextField.text ?? "",
            "description"   : shortDescriptionTextView.text ?? "",
        ]
        
        print(params)
        ///LoaderClass.sharedInstance.showLoader()
        let updateProfileUrl = Constants.APIs.challengeBaseURL + Constants.APIs.challengesProof
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            WebServiceClass.showLoader(view: self.view)
            
            if let proofImage = self.getImage {
                
                if let imageData = UIImageJPEGRepresentation(proofImage , 0.6) {
                    let r = arc4random()
                    let str = "file"+String(r)+".jpg"
                    
                    let parameterName = "photo"
                    
                    multipartFormData.append(imageData, withName:parameterName, fileName:str, mimeType: "image/jpeg")
                    
                    for (key, value) in params {
                        let stringValue = String(describing: value)
                        multipartFormData.append(stringValue.data(using: String.Encoding.utf8)!, withName: key )
                        
                    }
                }
            } else {
                
                for (key, value) in params {
                    let stringValue = String(describing: value)
                    multipartFormData.append(stringValue.data(using: String.Encoding.utf8)!, withName: key )
                }
            }
            
        }, to: updateProfileUrl,encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    WebServiceClass.hideLoader(view: self.view)
                    //                    ARSLineProgress.hide()
                    //LoaderClass.sharedInstance.hideLoader()
                    
                    debugPrint(response)
                    
                    print(response.result.value ?? "Not Found")
                    
                    if let httpStatus = response.response , httpStatus.statusCode == 200 {
                        
                        if let responseObject = response.result.value as? Dictionary<String, AnyObject> {
                            
                            if let dataResponse = responseObject["response"] as? Dictionary<String, AnyObject>{
                                guard let userInfo = Mapper<ChallengeDetailProofModel>().map(JSONObject: dataResponse) else { return }
                                
//                                if let cbBlock = self.completionBlock {
//                                    cbBlock(userInfo)
//                                }
                            }
                            //self.showTextAlertLabelWithOnePop(error: responseObject["MessageWhatHappen"] as? String ?? "")
                            self.showAlertWithActions(responseObject["message"] as? String ?? "")
                        }
                        
                        //UserDefaults.standard.set(true, forKey: "SetProfileApiStatus")
                        
                        if response.result.isSuccess {
                            print(response)
                            
                        }
                    }}
                
            case .failure(let encodingError):
                
                print(encodingError)
                
            }})
        
        
    }
    
    
    func editProofApi() {
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
      
        let params: [String: String] = [
            
            //"challenge_id"     : getResultDict?.id ?? "",
            //"user_id"        : userInfoModel.id as? String ?? "",
       
            "proof_id"      : self.proofDetailModel[0].id ?? "",
            "phone"         : phoneNoTextField.text ?? "",
            "caption"       : captionTextField.text ?? "",
            "description"   : shortDescriptionTextView.text ?? "",
            ]
        
        print(params)
        ///LoaderClass.sharedInstance.showLoader()
        let updateProfileUrl = Constants.APIs.challengeBaseURL + Constants.APIs.editProof
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            WebServiceClass.showLoader(view: self.view)
            
            if let proofImage = self.getImage {
                
                if let imageData = UIImageJPEGRepresentation(proofImage , 0.6) {
                    let r = arc4random()
                    let str = "file"+String(r)+".jpg"
                    
                    let parameterName = "photo"
                    
                    multipartFormData.append(imageData, withName:parameterName, fileName:str, mimeType: "image/jpeg")
                    
                    for (key, value) in params {
                        let stringValue = String(describing: value)
                        multipartFormData.append(stringValue.data(using: String.Encoding.utf8)!, withName: key )
                        
                    }
                }
            } else {
                
                for (key, value) in params {
                    let stringValue = String(describing: value)
                    multipartFormData.append(stringValue.data(using: String.Encoding.utf8)!, withName: key )
                }
            }
            
        }, to: updateProfileUrl,encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    WebServiceClass.hideLoader(view: self.view)
                    //                    ARSLineProgress.hide()
                    //LoaderClass.sharedInstance.hideLoader()
                    
                    debugPrint(response)
                    
                    print(response.result.value ?? "Not Found")
                    
                    if let httpStatus = response.response , httpStatus.statusCode == 200 {
                        
                        if let responseObject = response.result.value as? Dictionary<String, AnyObject> {
                            
                            if let dataResponse = responseObject["response"] as? Dictionary<String, AnyObject>{
                                guard let userInfo = Mapper<ChallengeDetailProofModel>().map(JSONObject: dataResponse) else { return }
                                
                                //                                if let cbBlock = self.completionBlock {
                                //                                    cbBlock(userInfo)
                                //                                }
                            }
                            //self.showTextAlertLabelWithOnePop(error: responseObject["MessageWhatHappen"] as? String ?? "")
                            self.showAlertWithActions(responseObject["message"] as? String ?? "")
                        }
                        
                        //UserDefaults.standard.set(true, forKey: "SetProfileApiStatus")
                        
                        if response.result.isSuccess {
                            print(response)
                            
                        }
                    }}
                
            case .failure(let encodingError):
                
                print(encodingError)
                
            }})
        
        
    }
    
    
}
