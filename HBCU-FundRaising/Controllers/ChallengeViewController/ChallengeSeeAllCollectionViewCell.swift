//
//  ChallengeSeeAllCollectionViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 04/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ChallengeSeeAllCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var challengeImageView: UIImageView!
    @IBOutlet weak var challengeNameLabel: UILabel!
    @IBOutlet weak var challengeDetailsLabel: UILabel!
    @IBOutlet weak var challengeEndTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
