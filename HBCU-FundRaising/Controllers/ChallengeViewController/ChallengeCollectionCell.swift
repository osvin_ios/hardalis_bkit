//
//  ChallengeCollectionCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 08/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ChallengeCollectionCell: UICollectionViewCell {
    
    //Collection View Cell
    
    @IBOutlet weak var mainCollectionBackView: UIView!
    @IBOutlet weak var challengeImageView: UIImageView!
    @IBOutlet weak var challengeNumberLabel: UILabel!
    @IBOutlet weak var challengeDetailLabel: UILabel!
    @IBOutlet weak var challengeEndTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
}
