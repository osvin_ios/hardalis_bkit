//
//  ViewAllViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 22/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//
import UIKit
import ObjectMapper
import SDWebImage

class ViewAllViewController: UIViewController {

    @IBOutlet weak var viewAllCollectionView: UICollectionView!
   
    @IBOutlet weak var topMoveView: ViewDesign!
    @IBOutlet weak var topMoveButton: ButtonDesign!
    
    var gradientLayer: CAGradientLayer!
    var topBannerChallengeDetailDict : ChallengesDetailModel?
    var hbcuDetailModel = [hbcuDetailsModel]()
    var challengesDetailModel = [ChallengesDetailModel]()
    
    var refreshControlTop : UIRefreshControl!
    var refreshControlBottom : UIRefreshControl!
    var skipCount : Int = 0
    var postsCountOFArray : Int = 0
    
    override func viewDidLoad(){
        super.viewDidLoad()

        viewAllCollectionView.dataSource = self
        viewAllCollectionView.delegate = self
        
        viewAllCollectionView.register(UINib(nibName: "LeaderboardDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LeaderboardDetailCollectionViewCell")
        self.title = "Leaderboard"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .plain, target: self, action: #selector(leftBar(sender:)))
        self.navigationItem.hidesBackButton = true
        
        self.navigationTitleColor()
        self.createGradientLayer()
        
        topMoveView.isHidden = true
        topMoveButton.isHidden = true
        
        if Reachability.isConnectedToNetwork() == true {
            self.refreshControlAPI()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    
    override func viewWillAppear(_ animated: Bool){
    }
    
    // MARK: - refresh control For API
    internal func refreshControlAPI(){
        
        refreshControlTop = UIRefreshControl()
        refreshControlTop.addTarget(self, action: #selector(self.reloadAPI), for: .valueChanged)
        self.viewAllCollectionView.addSubview(refreshControlTop) // not required when using UITableViewController
        
        refreshControlBottom = UIRefreshControl()
        refreshControlBottom.addTarget(self, action: #selector(self.reloadAPICount), for: .valueChanged)
        self.viewAllCollectionView.bottomRefreshControl = refreshControlBottom
        
        self.manuallyRefreshTheTab()
    }
    
    func manuallyRefreshTheTab(){
        //First time automatically refreshing.
        refreshControlTop.beginRefreshingManually()
        self.perform(#selector(self.reloadAPI), with: nil, afterDelay: 0)
    }
    
    //MARK:- Reload API
    @objc func reloadAPI(){
        // Check network connection.
        self.skipCount = 1
        if Reachability.isConnectedToNetwork() == true {
            
            self.getHbcuDetailsAPI()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    
    //MARK:- Reload API
    @objc func reloadAPICount(){
        
//      if postsCountOFArray != 0 {
        // Check network connection.
        self.skipCount = skipCount + 1
        if Reachability.isConnectedToNetwork() == true {
            
            self.getHbcuDetailsAPI()
            
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
//        }else{
//            self.refreshControlBottom.endRefreshing()
//        }
    }
    
    @objc func leftBar(sender: UIButton) {
        //        self.menuContainerViewController.toggleLeftSideMenuCompletion {
        //        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func navigationTitleColor() {
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func createGradientLayer() {
        
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor(hexString: "#538795").cgColor, UIColor(hexString: "#09203f").cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func formatNumber(_ num: Double) ->String {
        let thousandNum = num/1000
        let millionNum = num/1000000
        if num >= 1000 && num < 1000000{
            if(thousandNum == thousandNum){
                let result =  String(format: "%.2f", ceil(num*100)/100)
                //     let result = num.truncate(places: 2)
                return ("\(result)K")
            }
            let result =  String(format: "%.2f", ceil(num*100)/100)
            //     let result = num.truncate(places: 2)
            return ("\(result)K")
        }
        if num > 1000000 {
            if(millionNum == millionNum){
                let result =  String(format: "%.2f", ceil(num*100)/100)
                //     let result = num.truncate(places: 2)
                return ("\(result)M")
            }
            let result = millionNum.truncate(places: 1)
            return("\(result)M")
        }else{
            if(num == num){
                let result =  String(format: "%.2f", ceil(num*100)/100)
                //     let result = num.truncate(places: 2)
                return ("\(result)")
            }
            //  }
            //  let result = num.truncate(places: 2)
            // return("\(result)")
        }
        let result =  String(format: "%.2f", ceil(num*100)/100)
        return ("\(result)")
    }

    @IBAction func topMoveButtonPressed(_ sender: ButtonDesign) {
        
        self.viewAllCollectionView.setContentOffset(.zero, animated: true)
    }
}

extension ViewAllViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hbcuDetailModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeaderboardDetailCollectionViewCell", for: indexPath) as! LeaderboardDetailCollectionViewCell
        
        cell.leaderboardImageView.sd_setImage(with: URL(string: Constants.APIs.imageBaseURL + (self.hbcuDetailModel[indexPath.row].logo ?? "")), placeholderImage: UIImage(named: "ic_image_thumb_small@x"))
        cell.leaderboardTitleLabel.text = self.hbcuDetailModel[indexPath.row].title ?? ""
        cell.leaderboardSubtitleLabel.text = "Amount Raised"
        let amountString = self.hbcuDetailModel[indexPath.item].amount ?? ""
        let valueInDouble : Double = Double(amountString as? String ?? "0") ?? 0.0
        let amountResult = self.formatNumber(valueInDouble)
        
        cell.AmountLabel.text = "$\(amountResult)"
       // cell.AmountLabel.text = "$\(self.hbcuDetailModel[indexPath.item].amount ?? "")"
        
        if hbcuDetailModel.count > 0 {
            switch self.hbcuDetailModel[indexPath.item].level ?? 0 {
                case 1:
                    cell.throphyImageView.image = UIImage(named: "ic_medal_gold.png")
                case 2:
                    cell.throphyImageView.image = UIImage(named: "ic_medal_silver.png")
                case 3:
                    cell.throphyImageView.image = UIImage(named: "ic_medal_bronze.png")
                default:
                    print("Testing")
            }
        }
        
//        if hbcuDetailModel.count > 0{
//
//            if self.hbcuDetailModel[indexPath.item].level == 1{
//                cell.throphyImageView.image = UIImage(named: "ic_medal_gold.png")
//            }else if self.hbcuDetailModel[indexPath.item].level == 2{
//                cell.throphyImageView.image = UIImage(named: "ic_medal_silver.png")
//            }else if self.hbcuDetailModel[indexPath.item].level == 3{
//                cell.throphyImageView.image = UIImage(named: "ic_medal_bronze.png")
//            }else{}
//
//        }else{}
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        let padding: CGFloat =  10
        //        let collectionViewSize = collectionView.frame.size.width - padding
        //
        //        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2.5)
        //        //return CGSize(width: collectionView.frame.size.width/2, height: collectionView.frame.size.width/2)
        //return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.width/2)
        return CGSize(width: (collectionView.frame.size.width/2.08)+3, height: collectionView.frame.size.height/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
//    {
//        topMoveButton.isHidden = (indexPath.row == collectionView.numberOfItems(inSection: 0) - 1)
//        topMoveButton.isHidden = (indexPath.row == 0)
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (viewAllCollectionView.contentOffset.y <= 0) {
            topMoveView.isHidden = true
            topMoveButton.isHidden = true
        }else{
            topMoveView.isHidden = false
            topMoveButton.isHidden = false
        }
    }
}

extension ViewAllViewController {
    
    func getHbcuDetailsAPI(){
        
//        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
//            return
//        }
//        let user_id = userInfoModel.id  ?? ""
//        WebServiceClass.showLoader(view: self.view)
//        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + Constants.APIs.awardedhbcu + "?id=\(user_id)", method: "GET", params: "") { (success, response, errorMsg) in
        
        WebServiceClass.showLoader(view: self.view)
        
        //Param:- page
        let page = self.skipCount as AnyObject
        
        //let id = self.topBannerChallengeDetailDict?.id ?? ""
        let id = self.topBannerChallengeDetailDict?.id != nil ? self.topBannerChallengeDetailDict?.id : challengesDetailModel[0].id ?? ""
        
        let paramsStr = "page=\(page)&id=\(id ?? "")"
        
        WebServiceClass.dataTask(urlName: Constants.APIs.challengeBaseURL + Constants.APIs.allchallengeDetails, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            
            self.refreshControlTop.endRefreshing()
            self.refreshControlBottom.endRefreshing()
            
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject>{
                    if let error = jsonResult["error"] as? Bool{
                        if error == true{
                        }else {
                            if let getmodelData = jsonResult["result"] as? [Dictionary<String, AnyObject>]{
                                if let modelData = Mapper<hbcuDetailsModel>().mapArray(JSONObject: getmodelData){
                                    
                                    if self.skipCount == 1{
                                        self.hbcuDetailModel = modelData
                                    }else{
                                        self.hbcuDetailModel += modelData
                                    }
                                }
                            }
                        }
                    }
                }
                //DispatchQueue.main.async {
                //self.commentsTableView.reloadData()
                self.viewAllCollectionView.reloadData()
                //}
            }else {
                print("Data is not in proper format")
            }
        }
    }
}
