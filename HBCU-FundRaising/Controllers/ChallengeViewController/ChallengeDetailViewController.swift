//
//  ChallengeDetailViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 17/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage
import Branch
import IQKeyboardManager

class ChallengeDetailViewController: UIViewController {
    
    // Background view
    @IBOutlet weak var mainBackgroundView: UIView!
    
    // Upper Image View
    @IBOutlet weak var challengeImageView: UIImageView!
    @IBOutlet weak var aboutHBCULabel: UILabel!
    @IBOutlet weak var joinHbcuBtnDetailLabel: UILabel!
    
    // Second View
    @IBOutlet weak var secondView: ViewDesign!
    @IBOutlet weak var challengeNameLabel: UILabel!
    @IBOutlet weak var challengeDetailLabel: UILabel!
    @IBOutlet weak var challengeEndTimeLabel: UILabel!
    @IBOutlet weak var collegeCoveredCollectionView: UICollectionView!
    @IBOutlet weak var collegeCoveredTextView: UITextView!
    @IBOutlet weak var continueButton: UIButton!
    
    var gradientLayer: CAGradientLayer!
    
    var challengesDetailModel = [ChallengesDetailModel]()
    
    var getResultDict : ChallengesDetailModel?
    var challengesImageTitleDict = [String : String]()
    
    var proofDetailModel = [ChallengeDetailProofModel]()
    
    var titleArray = ["About Challenge","Terms & Conditions"]
    
    var selectedTitleIndex = Int ()

    //Timer:
    var seconds = Int()
    var timer = Timer()
    //var isTimerRunning = false
    
    var serverDateTimeStr : String?
    var serverDateInDateFormat : Date?
    var startDateInDateFormat : Date?
    var endtDateInDateFormat : Date?
    var timerStr : String?
    
    var fromAppDelegate = false
    var fromAppDelegateEventID : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collegeCoveredTextView.isEditable = false

        collegeCoveredCollectionView.delegate = self
        collegeCoveredCollectionView.dataSource = self
     
        collegeCoveredCollectionView.register(UINib(nibName: "AboutAndTermsAndConditionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AboutAndTermsAndConditionCollectionViewCell")
    }
   
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.getChallengeDetailsAPI()
        
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        self.collegeCoveredCollectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: [])
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        
        timer.invalidate()
    }
    
    // MARK: - Button Action
  
    @IBAction func backBtnActn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        timer.invalidate()
    }
    
    @IBAction func shareBtnActn(_ sender: Any) {

        guard getResultDict != nil else{return}
        
        let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
        
        buo.title = Constants.appTitle.alertTitle
        buo.contentDescription = self.getResultDict?.name ?? ""
        buo.imageUrl = Constants.APIs.imageBaseURL + (self.getResultDict?.image ?? "" )
        buo.contentMetadata.customMetadata["challenge_id"] = self.getResultDict?.id ?? ""
        buo.contentMetadata.customMetadata["screen_type"] = Constants.screen_type.ACTION_CHALLENGE
        
        let lp: BranchLinkProperties = BranchLinkProperties()
        
        //lp.channel = "facebook"
        lp.feature = "sharing"
        lp.campaign = "content 123 launch"
        lp.stage = "new user"
        lp.tags = ["one", "two", "three"]
        
        //        lp.addControlParam("$desktop_url", withValue: "http://example.com/desktop")
        //        lp.addControlParam("$ios_url", withValue: "http://example.com/ios")
        //        lp.addControlParam("$ipad_url", withValue: "http://example.com/ios")
        //        lp.addControlParam("$android_url", withValue: "http://example.com/android")
        //        lp.addControlParam("$match_duration", withValue: "2000")
        
        lp.addControlParam("custom_data", withValue: "yes")
        lp.addControlParam("look_at", withValue: "this")
        lp.addControlParam("nav_to", withValue: "over here")
        lp.addControlParam("random", withValue: UUID.init().uuidString)
        
        buo.getShortUrl(with: lp) { (url, error) in
            if error == nil {
                print("got my Branch link to share: %@", url ?? "not found")
                
                //let message = "Check out this link"
                buo.showShareSheet(with: lp, andShareText: nil, from: self) { (activityType, completed) in
                    print(activityType ?? "")
                }
            }
        }
        
    }
    
    @IBAction func continueButtonPressed(_ sender: UIButton){

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubmitProofViewController") as! SubmitProofViewController
        if self.getResultDict != nil {
            vc.getResultDict = self.getResultDict
        }
        if self.proofDetailModel.count > 0 {
            vc.isAddingProof = true
            vc.proofDetailModel = self.proofDetailModel
           
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setTopBannerData(){
        
        if getResultDict != nil{
          
            challengeImageView.sd_setImage(with: URL(string: Constants.APIs.imageBaseURL + (self.getResultDict?.image ?? "")), placeholderImage: UIImage(named: "ic_image_thumb_small@x"))
            aboutHBCULabel.text = self.challengesImageTitleDict["image_title"]
            joinHbcuBtnDetailLabel.text = self.challengesImageTitleDict["image_subtitle"]
           
            challengeNameLabel.text = self.getResultDict?.name ?? ""
            challengeDetailLabel.text = self.getResultDict?.description ?? ""
            //challengeEndTimeLabel.text
            
//            let startDateString = self.UTCToLocalStringValue(date: self.getResultDict?.startDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "yyyy-MM-dd HH:mm:ss")
//            let endtDateString = self.UTCToLocalStringValue(date: self.getResultDict?.endDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
            let startDateString = self.convertTimeInFormater(self.getResultDict?.startDate ?? "")
            let endtDateString = self.convertTimeInFormater(self.getResultDict?.endDate ?? "")
            
            startDateInDateFormat = self.getDateFormatterDate(date: self.getResultDict?.startDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            endtDateInDateFormat = self.getDateFormatterDate(date: self.getResultDict?.endDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
           
            if let serverDateInDateFormat = serverDateInDateFormat, let startDateInDateFormat = startDateInDateFormat {
                
                let startDateComparisionResult:ComparisonResult = serverDateInDateFormat.compare(startDateInDateFormat)
                
                if startDateComparisionResult == ComparisonResult.orderedAscending
                {
                    // Server date is smaller than Start date.
                    
                    //self.runTimer(serverDate: serverDateInDateFormat ?? Date(), grabDate: startDateInDateFormat ?? Date())
                    
                    challengeEndTimeLabel.text = "Starts: \(startDateString ?? "")"
                }
                else if startDateComparisionResult == ComparisonResult.orderedDescending
                {
                    // Server date is greater than Start date.
                    
                    //self.runTimer(serverDate: startDateInDateFormat ?? Date(), grabDate: endtDateInDateFormat ?? Date())
                    
                    challengeEndTimeLabel.text = "Ends: \(endtDateString ?? "")"
                }
                else if startDateComparisionResult == ComparisonResult.orderedSame
                {
                    // Server date and Start date are same
                    
                    //self.runTimer(serverDate: startDateInDateFormat ?? Date(), grabDate: endtDateInDateFormat ?? Date())
                    
                    challengeEndTimeLabel.text = "Ends: \(endtDateString ?? "")"
                }else{}
            }
            
            collegeCoveredTextView.text = getResultDict?.about ?? ""
            
        }else{}
    }
    
    func formatNumber(_ num: Double) ->String{
        let thousandNum = num/1000
        let millionNum = num/1000000
        if num >= 1000 && num < 1000000{
            if(thousandNum == thousandNum){
                let result = thousandNum.truncate(places: 1)
                return("\(result)K")
            }
            let result = thousandNum.truncate(places: 1)
            return("\(result)K")
        }
        if num > 1000000{
            if(millionNum == millionNum){
                let result = millionNum.truncate(places: 1)
                return("\(result)M")
            }
            let result = millionNum.truncate(places: 1)
            return("\(result)M")
        }
        else{
            if(num == num){
                return ("\(num)")
            }
            return ("\(num)")
        }
        
    }
    
    func serverTime(){
        if self.serverDateTimeStr != nil && self.serverDateTimeStr != ""{
            
            let serverDateString = self.UTCToLocalStringValue(date: self.serverDateTimeStr ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
            serverDateInDateFormat = self.getDateFormatterDate(date: serverDateString, incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
        }else{}
    }

    
    func runTimer(serverDate : Date, grabDate : Date) {
        
        let mingap = Calendar.current.dateComponents([.second], from: serverDate ?? Date(), to: grabDate ?? Date())
        print("gap is\(mingap.second!)")
        
        seconds = mingap.second!
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(ChallengeTableViewCell.updateTimer)), userInfo: nil, repeats: true)
        
    }
    
    func timeString(time:TimeInterval) -> String {
        
        let days = ((Int(time) / 86400))
        let hours = ((Int(time) % 86400) / 3600)
        let minutes = ((Int(time) % 3600) / 60)
        let seconds = ((Int(time) % 3600) % 60)
        return String(format:"%02i Days, %02i:%02i:%02i", days, hours, minutes, seconds)
        
    }
    
    @objc func updateTimer() {
        
        if seconds < 1 {
            timer.invalidate()
            //Send alert to indicate "time's up!"
        } else {
            seconds -= 1    //This will decrement(count down)the seconds
            self.timerStr = timeString(time: TimeInterval(seconds))  //This will update the label.
            
            
//            if let serverDateInDateFormat = serverDateInDateFormat, let startDateInDateFormat = startDateInDateFormat {
//
//                let startDateComparisionResult:ComparisonResult = serverDateInDateFormat.compare(startDateInDateFormat)
//
//                if startDateComparisionResult == ComparisonResult.orderedAscending
//                {
//                    // Server date is smaller than Start date.
//
//                    challengeEndTimeLabel.text = "Starts in: \(timerStr ?? "")"
//                }
//                else if startDateComparisionResult == ComparisonResult.orderedDescending
//                {
//                    // Server date is greater than Start date.
//
//                    challengeEndTimeLabel.text = "Ends in: \(timerStr ?? "")"
//                }
//                else if startDateComparisionResult == ComparisonResult.orderedSame
//                {
//                    // Server date and Start date are same
//
//                    challengeEndTimeLabel.text = "Ends in: \(timerStr ?? "")"
//                }else{}
//            }
            
            
            
        }
        
    }
    
    func convertTimeInFormater(_ date: String) -> String? {
        
        if date.isEmpty == false {
            let myDateString = date
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.amSymbol = "AM"
            dateFormatter.pmSymbol = "PM"
            
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormatter.date(from: myDateString)!
            
            //dateFormatter.dateFormat = "dd MMM, h:mm a"
            dateFormatter.dateFormat = "MMMM, dd, h:mm a"
            
            let somedateString = dateFormatter.string(from: myDate as Date)
            return somedateString
        }
        return ""
        
    }
    
}

extension ChallengeDetailViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return titleArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutAndTermsAndConditionCollectionViewCell", for: indexPath) as! AboutAndTermsAndConditionCollectionViewCell
            
            cell.MainTitleLabel.text = titleArray[indexPath.row]
            
            if selectedTitleIndex == indexPath.row
            {
                cell.backView.backgroundColor = UIColor.white
                cell.backView.borderwidth = 1
                cell.MainTitleLabel.textColor = UIColor.black
            }
            else
            {
                cell.backView.backgroundColor = UIColor(red: 244/255, green: 246/255, blue: 246/255, alpha: 1.0)
                cell.backView.borderwidth = 0
                cell.MainTitleLabel.textColor = UIColor.lightGray
            }
            
            return cell
        
   }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            return CGSize(width: (collectionView.frame.size.width/2)-5, height: 40)
     
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
         return 10
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            switch indexPath.row {
            case 0 :
               
                if getResultDict != nil{
                    collegeCoveredTextView.text = getResultDict?.about ?? ""
                }else{}
                
            case 1 :
               
                if getResultDict != nil{
                    collegeCoveredTextView.text = getResultDict?.termsConditions ?? ""
                }else{}
                
                
            default :
                print("default")
            }
            
            selectedTitleIndex = indexPath.row
            self.collegeCoveredCollectionView.reloadData()
        
    }
    
}

extension ChallengeDetailViewController {
    
    func getChallengeDetailsAPI(){
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id = userInfoModel.id as? String
        //let id = self.challengesDetailModel[0].id ?? ""
        
        let id : String?
        
        if fromAppDelegate == true{
            id = self.fromAppDelegateEventID
        }else{
            id = self.challengesDetailModel[0].id ?? ""
        }
        
        WebServiceClass.showLoader(view: self.view)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.challengeBaseURL + Constants.APIs.challengeDetails + "?id=\(id ?? "")&user_id=\(user_id ?? "0")", method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult    = response as? Dictionary<String, AnyObject> {
                    
                    if let error = jsonResult["error"] as? Bool {
                        if error == true  {
                            
                        }else {
                                if let serverDateTime = jsonResult["server"] as? String{
                                    self.serverDateTimeStr = serverDateTime
                                    self.serverTime()
                                }
                            
                                if let settings = jsonResult["settings"] as? Dictionary<String, AnyObject>{
                                    if let collegeData = settings["college"] as? Dictionary<String, AnyObject> {
                                        self.challengesImageTitleDict["image_title"] = collegeData["image_title"] as? String ?? ""
                                        self.challengesImageTitleDict["image_subtitle"] = collegeData["image_subtitle"] as? String ?? ""
                                    }
                                }
                            
                            if let result = jsonResult["result"] as? Dictionary<String, AnyObject>{
                                
                                if let modelData = Mapper<ChallengesDetailModel>().map(JSONObject: result){
                                    self.getResultDict = modelData
                                    self.setTopBannerData()
                                    
                                }
                                
                                if let proofData = result["proof"] as? [Dictionary<String, AnyObject>]{
                                    if let modelData = Mapper<ChallengeDetailProofModel>().mapArray(JSONObject: proofData) {
                                        self.proofDetailModel = modelData
                                        
                                        if self.proofDetailModel.count > 0{
                                           //self.continueButton.setTitle("View Proof", for: .normal)
                                            self.continueButton.setTitle("Proof Submitted", for: .normal)
                                            self.continueButton.backgroundColor = UIColor.lightGray
                                            self.continueButton.isUserInteractionEnabled = false
                                        }else{
                                            self.continueButton.setTitle("Continue", for: .normal)
                                            self.continueButton.backgroundColor = UIColor(red: 56/255, green: 173/255, blue: 62/255, alpha: 1)
                                            self.continueButton.isUserInteractionEnabled = true
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self.collegeCoveredCollectionView.reloadData()
                }
            }else {
                print("Data is not in proper format")
            }
        }
    }
    
}
