//
//  ChallengeViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 08/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage

class ChallengeViewController: UIViewController {

    // Upper View
    @IBOutlet weak var upperView1: UIView!
    @IBOutlet weak var topBannerImageView: UIImageView!
    @IBOutlet weak var dollarChallangeTitleLbl: UILabel!
    @IBOutlet weak var countingLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    // Table View
    @IBOutlet weak var challengeTableView: UITableView!

    var gradientLayer: CAGradientLayer!
    var titleTypeCheckInt : Int?
    
    var topBannerChallengeDetailDict : ChallengesDetailModel?
    var bonusChallengesDetailArr = [ChallengesDetailModel]()
    var collegeChallengesDetailArr = [ChallengesDetailModel]()
    var countChallengesDetailDict : ChallengesDetailModel?
    
    var serverDateTimeStr : String?
    
    var challengesTitleDict = [String : String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        challengeTableView.dataSource = self
        challengeTableView.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.title = "Challenge"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
        self.navigationItem.hidesBackButton = true
        
        self.navigationTitleColor()
        self.createGradientLayer()
        self.getChallengesDetailsAPI()
    }
    
    @objc func leftBar(sender: UIButton){
        self.menuContainerViewController.toggleLeftSideMenuCompletion{
        }
    }
    
    func navigationTitleColor(){
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    func createGradientLayer(){
        
        gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.view.bounds
        
        gradientLayer.colors = [UIColor(hexString: "#538795").cgColor, UIColor(hexString: "#09203f").cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func setTopBannerData(){
        if topBannerChallengeDetailDict != nil{
            topBannerImageView.sd_setImage(with: URL(string: Constants.APIs.imageBaseURL + (self.topBannerChallengeDetailDict?.image ?? "")), placeholderImage: UIImage(named: "ic_image_thumb_small@x"))
            
            let titleString = self.topBannerChallengeDetailDict?.name ?? ""
            if titleString.count > 0 && titleString != ""{
                dollarChallangeTitleLbl.text = titleString.uppercased()
            }else{}
            
            let amountString = self.topBannerChallengeDetailDict?.amount ?? "0.00"
            countingLabel.text = amountString
//            let valueInDouble : Double = Double(amountString) ?? 0.0
//            let amountResult = self.formatNumber(valueInDouble)
//            countingLabel.text = amountResult ?? ""
            detailLabel.text = self.topBannerChallengeDetailDict?.description ?? ""
        }else{}
    }
    
    func formatNumber(_ num: Double) ->String{
        let thousandNum = num/1000
        let millionNum = num/1000000
        if num >= 1000 && num < 1000000{
            if(thousandNum == thousandNum){
                let result = thousandNum.truncate(places: 1)
                return("\(result)K")
            }
            let result = thousandNum.truncate(places: 1)
            return("\(result)K")
        }
        if num > 1000000{
            if(millionNum == millionNum){
                let result = millionNum.truncate(places: 1)
                return("\(result)M")
            }
            let result = millionNum.truncate(places: 1)
            return("\(result)M")
        } else{
            if(num == num){
            let result =  String(format: "%.2f", ceil(num*100)/100)
       //     let result = num.truncate(places: 2)
                return ("\(result)")
            }
          //  }
          //  let result = num.truncate(places: 2)
           // return("\(result)")
        }
        let result =  String(format: "%.2f", ceil(num*100)/100)
        return ("\(result)")
    }
    
//    func createGradientLayer() {
//
//        gradientLayer = CAGradientLayer()
//
//        gradientLayer.frame = self.view.bounds
//
//        gradientLayer.colors = [UIColor(hexString: "#538795").cgColor, UIColor(hexString: "#09203f").cgColor]
//        self.view.layer.insertSublayer(gradientLayer, at: 0)
//    }
//
//    func setTopBannerData() {
//
//        if topBannerChallengeDetailDict != nil{
//            topBannerImageView.sd_setImage(with: URL(string: Constants.APIs.imageBaseURL + (self.topBannerChallengeDetailDict?.image ?? "")), placeholderImage: UIImage(named: "ic_image_thumb_small@x"))
//
//            let titleString = self.topBannerChallengeDetailDict?.name ?? ""
//            if titleString.count > 0 && titleString != ""{
//                dollarChallangeTitleLbl.text = titleString.uppercased()
//            }else{}
//
//            let amountString = self.topBannerChallengeDetailDict?.amount ?? ""
//            let valueInDouble : Double = Double(amountString as? String ?? "0") ?? 0.0
//            let amountResult = self.formatNumber(valueInDouble)
//
//            countingLabel.text = amountResult ?? ""
//            detailLabel.text = self.topBannerChallengeDetailDict?.description ?? ""
//        }else{}
//
//    }
//
//    func formatNumber(_ num: Double) ->String{
//        let thousandNum = num/1000
//        let millionNum = num/1000000
//        if num >= 1000 && num < 1000000{
//            if(thousandNum == thousandNum){
//                let result = thousandNum.truncate(places: 1)
//                return("\(result)K")
//            }
//            let result = thousandNum.truncate(places: 1)
//            return("\(result)K")
//        }
//        if num > 1000000{
//            if(millionNum == millionNum){
//                let result = millionNum.truncate(places: 1)
//                return("\(result)M")
//            }
//            let result = millionNum.truncate(places: 1)
//            return("\(result)M")
//        }
//        else{
//            if(num == num){
//                return ("\(num)")
//            }
//            return ("\(num)")
//        }
//
//    }
//
    @IBAction func joinNowBtnActn(_ sender: Any){
        performSegue(withIdentifier: "joinNow", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        
        if segue.identifier == "joinNow" {
            let destiation = segue.destination as! JoinNowViewController
            if topBannerChallengeDetailDict != nil{
                destiation.topBannerChallengeDetailDict = self.topBannerChallengeDetailDict
            }else{}
        }else if segue.identifier == "seeAll" {
            let destination = segue.destination as! ChallengeSeeAllViewController
            if sender as? Int == 0 {
                if self.challengesTitleDict.count > 0{
                    //if self.countChallengesDetailDict?.bonusCount ?? 0 > 5{
                        destination.challengesTitleDict = self.challengesTitleDict
                        destination.checkChallengeType = "0"
                    //}else{}
                }else{}
            }else if sender as? Int == 1{
                if self.challengesTitleDict.count > 0{
                    //if self.countChallengesDetailDict?.collegeCount ?? 0 > 5{
                        destination.challengesTitleDict = self.challengesTitleDict
                        destination.checkChallengeType = "1"
                    //}else{}
                }else{}
            }else{}
        }
    }
}

extension ChallengeViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "challengeTableViewCell1", for: indexPath) as! ChallengeTableViewCell
        
        switch indexPath.row {
            case 0:
//                if countChallengesDetailDict?.bonusCount == 0{
//                    cell.challegeTitleLabel.text = self.challengesTitleDict["bonus_title"]
//                    cell.challengeDetailLbl.text = self.challengesTitleDict["bonus_sub_title"]
//                }else{
//                    cell.challegeTitleLabel.text = self.challengesTitleDict["bonus_title"]
//                    cell.challengeDetailLbl.text = self.challengesTitleDict["bonus_sub_title"]
//                }
             
             //   if bonusChallengesDetailArr.count > 0 && indexPath.row == 0{
               
                    //titleTypeCheckInt = 0
                    cell.challegeTitleLabel.text = self.challengesTitleDict["bonus_title"]
                    cell.challengeDetailLbl.text = self.challengesTitleDict["bonus_sub_title"]
                    cell.seeAllButton.isHidden = self.countChallengesDetailDict?.bonusCount ?? 0 > 5 ? false : true
                    cell.seeAllButton.addTarget(self, action: #selector(challengeSeeAll(_:)), for: .touchUpInside)
                    cell.seeAllButton.tag = indexPath.row
                    
                    cell.parentController = self
                    cell.delegate1 = self
                    
                    if bonusChallengesDetailArr.count != 0{
                        cell.challengesDetailModel = bonusChallengesDetailArr
                        cell.serverDateTimeStr = self.serverDateTimeStr
                    }else{}
//                    else if bonusChallengesDetailArr.count == 0{
//                        cell.challengesDetailModel = bonusChallengesDetailArr
//                         cell.bonusDetailModel = collegeChallengesDetailArr
//                    }else if collegeChallengesDetailArr.count == 0{
//                        cell.bonusDetailModel = collegeChallengesDetailArr
//                         cell.challengesDetailModel = bonusChallengesDetailArr
//                    }else{}
                    
                    cell.tableViewRow = indexPath.row
                    //cell.checkButon()
                    
                    //DispatchQueue.main.async{
                        cell.reloadCollectionView()
                    //}
                    
                    return cell
                //}
            case 1:

//                if countChallengesDetailDict?.collegeCount == 0 {
//                    cell.challegeTitleLabel.text = "COLLEGE COVERED"
//                    cell.challengeDetailLbl.text = "Lorem Ipsum is simple dummy text of printing and typesetting industry"
//                }else{
//                    cell.challegeTitleLabel.text = self.challengesTitleDict["college_title"]
//                    cell.challengeDetailLbl.text = self.challengesTitleDict["college_sub_title"]
//                }
            //    if collegeChallengesDetailArr.count > 0 && indexPath.row == 1{
                    cell.challegeTitleLabel.text = self.challengesTitleDict["college_title"]
                    cell.challengeDetailLbl.text = self.challengesTitleDict["college_sub_title"]
       
                    //titleTypeCheckInt = 1

                    cell.seeAllButton.isHidden = self.countChallengesDetailDict?.collegeCount ?? 0 > 5 ? false : true
                    cell.seeAllButton.addTarget(self, action: #selector(challengeSeeAll(_:)), for: .touchUpInside)
                    cell.seeAllButton.tag = indexPath.row
                    
                    cell.parentController = self
                    cell.delegate1 = self
                    
                    if collegeChallengesDetailArr.count != 0 {
                        cell.challengesDetailModel = collegeChallengesDetailArr
                        cell.serverDateTimeStr = self.serverDateTimeStr
                    }else{}
                    
                    cell.tableViewRow = indexPath.row
                    //cell.checkButon()
                    
                    //DispatchQueue.main.async {
                        cell.reloadCollectionView()
                    //}
                    
                    return cell
               // }else{}
            
            default:
                print("default")
            }
        return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        switch indexPath.row {
//        case 0:
//            return self.bonusChallengesDetailArr.count > 0 ? 230 : 0
//        case 1:
//            return self.collegeChallengesDetailArr.count > 0 ? 230 : 0
//        default:
            return 230
//        }
    }
    
    @objc func challengeSeeAll(_ sender : UIButton) {
        let indexPathRow = sender.tag
        //let object = self.bonusChallengesDetailArr[indexPathRow]
        performSegue(withIdentifier: "seeAll", sender: indexPathRow)
    }
    
//    @objc func collegeChallengeSeeAll(_ sender : UIButton){
//        let indexPathRow = sender.tag
//        //let object = self.collegeChallengesDetailArr[indexPathRow]
//        performSegue(withIdentifier: "seeAll", sender: indexPathRow)
//    }
}

extension ChallengeViewController {
    
    func getChallengesDetailsAPI(){
        
        WebServiceClass.showLoader(view: self.view)
        print(Constants.APIs.challengeBaseURL + Constants.APIs.getChallenges)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.challengeBaseURL + Constants.APIs.getChallenges, method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let error = jsonResult["error"] as? Bool {
                        if error == true  {
                            
                        }else {
                                if let serverDateTime = jsonResult["server"] as? String{
                                    self.serverDateTimeStr = serverDateTime
                                }
                                if let settings = jsonResult["settings"] as? Dictionary<String, AnyObject>{
                                    if let bonusData = settings["bonus"] as? Dictionary<String, AnyObject> {
                                        self.challengesTitleDict["bonus_title"] = bonusData["title"] as? String ?? ""
                                        self.challengesTitleDict["bonus_sub_title"] = bonusData["sub_title"] as? String ?? ""
                                    }
                                    if let collegeData = settings["college"] as? Dictionary<String, AnyObject> {
                                        self.challengesTitleDict["college_title"] = collegeData["title"] as? String ?? ""
                                        self.challengesTitleDict["college_sub_title"] = collegeData["sub_title"] as? String ?? ""
                                    }
                                }
                            
                            if let result = jsonResult["result"] as? Dictionary<String, AnyObject>{
                                if let topBannerData = result["top_banner"] as? Dictionary<String, AnyObject> {
                                    if let modelData = Mapper<ChallengesDetailModel>().map(JSONObject: topBannerData){
                                        self.topBannerChallengeDetailDict = modelData
                                        self.setTopBannerData()
                                        
                                    }
                                }
                                if let bonusData = result["bonus"] as? [Dictionary<String, AnyObject>]{
                                    if let modelData = Mapper<ChallengesDetailModel>().mapArray(JSONObject: bonusData) {
                                        self.bonusChallengesDetailArr = modelData
                                        
                                    }
                                }
                                if let collegeData = result["college"] as? [Dictionary<String, AnyObject>]{
                                    if let modelData = Mapper<ChallengesDetailModel>().mapArray(JSONObject: collegeData) {
                                        self.collegeChallengesDetailArr = modelData
                                        
                                    }
                                }
                                if let count = result["count"] as? Dictionary<String, AnyObject> {
                                    
                                    if let modelData = Mapper<ChallengesDetailModel>().map(JSONObject: count){
                                        self.countChallengesDetailDict = modelData
                                    }
                                   
                                }
                                
                            }
                            
                        self.challengeTableView.reloadData()
                            
                        }
                    }
                }
//                DispatchQueue.main.async {
//                    //self.commentsTableView.reloadData()
//                    self.challengeTableView.reloadData()
//                }
            }else {
                print("Data is not in proper format")
            }
        }
    }
}

extension ChallengeViewController : detailApiProtocol {
    
    func didSelect(ID: AnyObject) {
        
        if let selectedObject = ID as? ChallengesDetailModel {
            if let checkChallengeType = selectedObject.type{
                if checkChallengeType == "0"{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "BonusChallengeDetailViewController") as! BonusChallengeDetailViewController
                    vc.challengesDetailModel = [selectedObject]
                    self.navigationController?.pushViewController(vc, animated: true)

                }else if checkChallengeType == "1"{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChallengeDetailViewController") as! ChallengeDetailViewController
                    vc.challengesDetailModel = [selectedObject]
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{}
            }else{}
        }else{}
    }
}

