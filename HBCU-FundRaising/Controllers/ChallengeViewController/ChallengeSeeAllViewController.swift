//
//  ChallengeSeeAllViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 04/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage
import CCBottomRefreshControl

class ChallengeSeeAllViewController: UIViewController {
    
    @IBOutlet weak var challengeTitleLabel: UILabel!
    @IBOutlet weak var challengeDescriptionLabel: UILabel!
    @IBOutlet weak var challengeSeeAllCollectionView: UICollectionView!
    
    @IBOutlet weak var topMoveView: ViewDesign!
    @IBOutlet weak var topMoveButton: ButtonDesign!
    
    var gradientLayer: CAGradientLayer!
    
    var challengesDetailModel = [ChallengesDetailModel]()
    var checkChallengeType : String?
    var challengesTitleDict = [String : String]()
    
    //Timer:
    var seconds = Int()
    var timer = Timer()
    //var isTimerRunning = false
    
    var serverDateTimeStr : String?
    var serverDateInDateFormat : Date?
    var startDateInDateFormat : Date?
    var endtDateInDateFormat : Date?
    var timerStr : String?
    
    var refreshControlTop : UIRefreshControl!
    var refreshControlBottom : UIRefreshControl!
    var skipCount : Int = 0
    var postsCountOFArray : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        challengeSeeAllCollectionView.dataSource = self
        challengeSeeAllCollectionView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .plain, target: self, action: #selector(leftBar(sender:)))
        //self.navigationItem.hidesBackButton = true
       // self.navigationTitleColor()
       // self.createGradientLayer()
       
        topMoveView.isHidden = true
        topMoveButton.isHidden = true
        
        if Reachability.isConnectedToNetwork() == true {
            //self.hitAllSpareChangeAPI(offset: 0)
            self.refreshControlAPI()
        } else{
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
        
        
    }
    
    func setTopViewData(){
        if checkChallengeType == "0"{
            self.title = self.challengesTitleDict["bonus_title"]
            challengeTitleLabel.text = self.challengesTitleDict["bonus_title"]
            challengeDescriptionLabel.text = self.challengesTitleDict["bonus_sub_title"]
            
            //self.getBonusChallengesDetailsAPI()
            
        }else if checkChallengeType == "1"{
            self.title = self.challengesTitleDict["college_title"]
            challengeTitleLabel.text = self.challengesTitleDict["college_title"]
            challengeDescriptionLabel.text = self.challengesTitleDict["college_sub_title"]
            
            //self.getCollegeChallengesDetailsAPI()
            
        }else{}
    }

    // MARK: - refresh control For API
    internal func refreshControlAPI() {
        
        refreshControlTop = UIRefreshControl()
        refreshControlTop.addTarget(self, action: #selector(self.reloadAPI), for: .valueChanged)
        self.challengeSeeAllCollectionView.addSubview(refreshControlTop) // not required when using UITableViewController
        
        refreshControlBottom = UIRefreshControl()
        refreshControlBottom.addTarget(self, action: #selector(self.reloadAPICount), for: .valueChanged)
        self.challengeSeeAllCollectionView.bottomRefreshControl = refreshControlBottom
        
        self.manuallyRefreshTheTab()
    }
    
    func manuallyRefreshTheTab() {
        //First time automatically refreshing.
        refreshControlTop.beginRefreshingManually()
        self.perform(#selector(self.reloadAPI), with: nil, afterDelay: 0)
    }
    
    //MARK:- Reload API
    @objc func reloadAPI() {
        // Check network connection.
        self.skipCount = 1
        if Reachability.isConnectedToNetwork() == true {
            //self.hitAllSpareChangeAPI(offset: skipCount)
            
            if checkChallengeType == "0"{
                self.getBonusChallengesDetailsAPI()
            }else if checkChallengeType == "1"{
                self.getCollegeChallengesDetailsAPI()
            }else{}
            
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    
    //MARK:- Reload API
    @objc func reloadAPICount() {
        
  //      if postsCountOFArray != 0 {
            // Check network connection.
            self.skipCount = skipCount + 1
            if Reachability.isConnectedToNetwork() == true {
                //self.hitAllSpareChangeAPI(offset: skipCount)
                
                if checkChallengeType == "0"{
                    self.getBonusChallengesDetailsAPI()
                }else if checkChallengeType == "1"{
                    self.getCollegeChallengesDetailsAPI()
                }else{}
                
            } else {
                self.showAlert(Constants.commomInternetmsz.checkInternet)
            }
//        }else{
//            self.refreshControlBottom.endRefreshing()
//        }
        
    }
    
    @objc func leftBar(sender: UIButton) {
//        self.menuContainerViewController.toggleLeftSideMenuCompletion {
//        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func navigationTitleColor() {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor(hexString: "#538795").cgColor, UIColor(hexString: "#09203f").cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func serverTime(){
        if self.serverDateTimeStr != nil && self.serverDateTimeStr != ""{
            //serverDateTime = self.UTCToLocalStringValue(date: self.serverDateTimeStr ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "dd MMMM,yyyy h:mm a")
            let serverDateString = self.UTCToLocalStringValue(date: self.serverDateTimeStr ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
            serverDateInDateFormat = self.getDateFormatterDate(date: serverDateString, incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
        }else{}
    }
    
    
    func runTimer(serverDate : Date, grabDate : Date) {
        
        let mingap = Calendar.current.dateComponents([.second], from: serverDate ?? Date(), to: grabDate ?? Date())
        print("gap is\(mingap.second!)")
        
        seconds = mingap.second!
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(ChallengeTableViewCell.updateTimer)), userInfo: nil, repeats: true)
        
    }
    
    func timeString(time:TimeInterval) -> String {
        
        let days = ((Int(time) / 86400))
        let hours = ((Int(time) % 86400) / 3600)
        let minutes = ((Int(time) % 3600) / 60)
        let seconds = ((Int(time) % 3600) % 60)
        return String(format:"%02i Days, %02i:%02i:%02i", days, hours, minutes, seconds)
        
    }
    
    @objc func updateTimer() {
        
        if seconds < 1 {
            timer.invalidate()
            //Send alert to indicate "time's up!"
        } else {
            seconds -= 1    //This will decrement(count down)the seconds
            self.timerStr = timeString(time: TimeInterval(seconds))  //This will update the label.
            
            
            //print("==========",self.timerStr)
            
          
//        let visibleIndexPaths = challengeSeeAllCollectionView.indexPathsForVisibleItems
//        //var visibleCellsArray: [UICollectionViewCell] = []
//        for indexPath in visibleIndexPaths {
//            //  You now have visible cells in visibleCellsArray
//            //visibleCellsArray.append(challengeCollectionView.cellForItem(at: currentIndextPath)!)
//            let cell = challengeSeeAllCollectionView.cellForItem(at: indexPath) as! ChallengeSeeAllCollectionViewCell
//            
//            if let serverDateInDateFormat = serverDateInDateFormat, let startDateInDateFormat = startDateInDateFormat {
//                
//                let startDateComparisionResult:ComparisonResult = serverDateInDateFormat.compare(startDateInDateFormat)
//                
//                if startDateComparisionResult == ComparisonResult.orderedAscending
//                {
//                    // Server date is smaller than Start date.
//                    
//                    cell.challengeEndTimeLabel.text = "Starts in: \(timerStr ?? "")"
//                }
//                else if startDateComparisionResult == ComparisonResult.orderedDescending
//                {
//                    // Server date is greater than Start date.
//                    
//                    cell.challengeEndTimeLabel.text = "Ends in: \(timerStr ?? "")"
//                }
//                else if startDateComparisionResult == ComparisonResult.orderedSame
//                {
//                    // Server date and Start date are same
//                    
//                    cell.challengeEndTimeLabel.text = "Ends in: \(timerStr ?? "")"
//                }else{}
//            }
//        }
        
            
        }
        
    }
    
   
    @IBAction func topMoveButtonPressed(_ sender: ButtonDesign) {
        self.challengeSeeAllCollectionView.setContentOffset(.zero, animated: true)
    }
   
    
    func convertTimeFormater2(_ date: String) -> String? {
        
        if date.isEmpty == false {
            let myDateString = date
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.amSymbol = "AM"
            dateFormatter.pmSymbol = "PM"
            
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormatter.date(from: myDateString)!
            
            //dateFormatter.dateFormat = "dd MMM, h:mm a"
            dateFormatter.dateFormat = "MMMM, dd, h:mm a"
            
            let somedateString = dateFormatter.string(from: myDate as Date)
            return somedateString
        }
        return ""
        
    }
    
}

extension ChallengeSeeAllViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return challengesDetailModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeeAllCollectionCell", for: indexPath) as! ChallengeSeeAllCollectionViewCell
  
        cell.challengeImageView.sd_setImage(with: URL(string: Constants.APIs.imageBaseURL + (self.challengesDetailModel[indexPath.row].image ?? "")), placeholderImage: UIImage(named: "ic_image_thumb_small@x"))
        cell.challengeNameLabel.text = self.challengesDetailModel[indexPath.row].name ?? ""
        cell.challengeDetailsLabel.text = self.challengesDetailModel[indexPath.item].description ?? ""
        
//        let startDateString = self.UTCToLocalStringValue(date: self.challengesDetailModel[indexPath.item].startDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "dd MMM, h:mm a")
//        let endtDateString = self.UTCToLocalStringValue(date: self.challengesDetailModel[indexPath.item].endDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "dd MMM, h:mm a")
        
        let startDateString = self.convertTimeFormater2(self.challengesDetailModel[indexPath.item].startDate ?? "")
        let endtDateString = self.convertTimeFormater2(self.challengesDetailModel[indexPath.item].endDate ?? "")
        
        startDateInDateFormat = self.getDateFormatterDate(date: self.challengesDetailModel[indexPath.item].startDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
        endtDateInDateFormat = self.getDateFormatterDate(date: self.challengesDetailModel[indexPath.item].endDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
        
        if let serverDateInDateFormat = serverDateInDateFormat, let startDateInDateFormat = startDateInDateFormat, let endtDateInDateFormat = endtDateInDateFormat {
            
            let startDateComparisionResult:ComparisonResult = serverDateInDateFormat.compare(startDateInDateFormat)
            
            self.timer.invalidate()
            
            if startDateComparisionResult == ComparisonResult.orderedAscending
            {
                // Server date is smaller than Start date.
                
               
                //DispatchQueue.main.async {
                    //self.runTimer(serverDate: serverDateInDateFormat, grabDate: startDateInDateFormat)
                //}
                
                cell.challengeEndTimeLabel.text = "Starts: \(startDateString ?? "")"
            }
            else if startDateComparisionResult == ComparisonResult.orderedDescending
            {
                // Server date is greater than Start date.
                
               
                //DispatchQueue.main.async {
                    //self.runTimer(serverDate: startDateInDateFormat, grabDate: endtDateInDateFormat)
                //}
                
                cell.challengeEndTimeLabel.text = "Ends: \(endtDateString ?? "")"
            }
            else if startDateComparisionResult == ComparisonResult.orderedSame
            {
                // Server date and Start date are same.
                
                //DispatchQueue.main.async {
                    //self.runTimer(serverDate: startDateInDateFormat, grabDate: endtDateInDateFormat)
                //}
                
                cell.challengeEndTimeLabel.text = "Ends: \(endtDateString ?? "")"
            }else{}
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/2.08, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let selectedObject = self.challengesDetailModel[indexPath.row] as? ChallengesDetailModel {
            if let checkChallengeType = selectedObject.type{
                if checkChallengeType == "0"{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "BonusChallengeDetailViewController") as! BonusChallengeDetailViewController
                    vc.challengesDetailModel = [selectedObject]
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if checkChallengeType == "1"{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChallengeDetailViewController") as! ChallengeDetailViewController
                    vc.challengesDetailModel = [selectedObject]
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    
                }
            }else{
                
            }
        }else{
            
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (challengeSeeAllCollectionView.contentOffset.y <= 0) {
            topMoveView.isHidden = true
            topMoveButton.isHidden = true
        }else{
            topMoveView.isHidden = false
            topMoveButton.isHidden = false
        }
    }
}

extension ChallengeSeeAllViewController {
    
    func getBonusChallengesDetailsAPI(){
    
        //Param:- page
        let page = self.skipCount as AnyObject
        let paramsStr = "page=\(page)"
        
        WebServiceClass.showLoader(view: self.view)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.challengeBaseURL + Constants.APIs.getBonusChallenges, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            
            self.refreshControlTop.endRefreshing()
            self.refreshControlBottom.endRefreshing()
            
            if success == true {
                if let jsonResult  = response as? Dictionary<String, AnyObject> {
                    if let error = jsonResult["error"] as? Bool {
                        if error == true {
                        
                            if let settings = jsonResult["settings"] as? Dictionary<String, AnyObject>{
                                if let bonusData = settings["bonus"] as? Dictionary<String, AnyObject> {
                                    self.challengesTitleDict["bonus_title"] = bonusData["title"] as? String ?? ""
                                    self.challengesTitleDict["bonus_sub_title"] = bonusData["sub_title"] as? String ?? ""
                                }
                                if let collegeData = settings["college"] as? Dictionary<String, AnyObject> {
                                    self.challengesTitleDict["college_title"] = collegeData["title"] as? String ?? ""
                                    self.challengesTitleDict["college_sub_title"] = collegeData["sub_title"] as? String ?? ""
                                }
                                self.setTopViewData()
                                
                            }
                        }else {
                            if let serverDateTime = jsonResult["server"] as? String{
                                self.serverDateTimeStr = serverDateTime
                                self.serverTime()
                            }
                            
                            if let settings = jsonResult["settings"] as? Dictionary<String, AnyObject>{
                                if let bonusData = settings["bonus"] as? Dictionary<String, AnyObject> {
                                    self.challengesTitleDict["bonus_title"] = bonusData["title"] as? String ?? ""
                                    self.challengesTitleDict["bonus_sub_title"] = bonusData["sub_title"] as? String ?? ""
                                }
                                if let collegeData = settings["college"] as? Dictionary<String, AnyObject> {
                                    self.challengesTitleDict["college_title"] = collegeData["title"] as? String ?? ""
                                    self.challengesTitleDict["college_sub_title"] = collegeData["sub_title"] as? String ?? ""
                                }
                                self.setTopViewData()
                            }
                            if let getmodelData = jsonResult["result"] as? [Dictionary<String, AnyObject>]{
                                if let modelData = Mapper<ChallengesDetailModel>().mapArray(JSONObject: getmodelData) {
                                    
                                    if self.skipCount == 1{
                                        self.challengesDetailModel = modelData
                                    }else{
                                        self.challengesDetailModel += modelData
                                    }
                                
                                }
                            }
                            
                        }
                    }
                    
                    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
                    label.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
                    label.textAlignment = .center
                    label.textColor = UIColor.lightGray
                    label.text = "No Challenges."
                    if self.skipCount == 1 && self.challengesDetailModel.count <= 0{
                        self.view.addSubview(label)
                    }else{
                        label.removeFromSuperview()
                    }
                    
                }
                //DispatchQueue.main.async {
                    //self.commentsTableView.reloadData()
                    self.challengeSeeAllCollectionView.reloadData()
                //}
            }else {
                print("Data is not in proper format")
            }
        }
    }
    
    
    func getCollegeChallengesDetailsAPI(){

        //Param:- page
        let page = self.skipCount as AnyObject
        let paramsStr = "page=\(page)"
        
        WebServiceClass.showLoader(view: self.view)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.challengeBaseURL + Constants.APIs.getCollegeChallenges, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            
            self.refreshControlTop.endRefreshing()
            self.refreshControlBottom.endRefreshing()
            
            if success == true {
                if let jsonResult    = response as? Dictionary<String, AnyObject> {
                    if let error = jsonResult["error"] as? Bool {
                        if error == true  {
                            
                            if let settings = jsonResult["settings"] as? Dictionary<String, AnyObject>{
                                
                                if let bonusData = settings["bonus"] as? Dictionary<String, AnyObject>{
                                    self.challengesTitleDict["bonus_title"] = bonusData["title"] as? String ?? ""
                                    self.challengesTitleDict["bonus_sub_title"] = bonusData["sub_title"] as? String ?? ""
                                }
                                if let collegeData = settings["college"] as? Dictionary<String, AnyObject>{
                                    self.challengesTitleDict["college_title"] = collegeData["title"] as? String ?? ""
                                    self.challengesTitleDict["college_sub_title"] = collegeData["sub_title"] as? String ?? ""
                                }
                                
                                self.setTopViewData()
                                
                            }
                        }else {
                            
                            if let serverDateTime = jsonResult["server"] as? String{
                                self.serverDateTimeStr = serverDateTime
                                self.serverTime()
                            }
                            
                            if let settings = jsonResult["settings"] as? Dictionary<String, AnyObject>{
                                
                                if let bonusData = settings["bonus"] as? Dictionary<String, AnyObject> {
                                    self.challengesTitleDict["bonus_title"] = bonusData["title"] as? String ?? ""
                                    self.challengesTitleDict["bonus_sub_title"] = bonusData["sub_title"] as? String ?? ""
                                }
                                if let collegeData = settings["college"] as? Dictionary<String, AnyObject> {
                                    self.challengesTitleDict["college_title"] = collegeData["title"] as? String ?? ""
                                    self.challengesTitleDict["college_sub_title"] = collegeData["sub_title"] as? String ?? ""
                                }
                                
                                self.setTopViewData()
                            }
                            if let getmodelData = jsonResult["result"] as? [Dictionary<String, AnyObject>]{
                                if let modelData = Mapper<ChallengesDetailModel>().mapArray(JSONObject: getmodelData) {
                                    
                                    if self.skipCount == 1{
                                        self.challengesDetailModel = modelData
                                    }else{
                                        self.challengesDetailModel += modelData
                                    }
                                    
                                }
                            }
                            
                        }
                    }
                    
                    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
                    label.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
                    label.textAlignment = .center
                    label.textColor = UIColor.lightGray
                    label.text = "No Challenges."
                    if self.skipCount == 1 && self.challengesDetailModel.count <= 0{
                        self.view.addSubview(label)
                    }else{
                        label.removeFromSuperview()
                    }
                    
                }
                //DispatchQueue.main.async {
                    //self.commentsTableView.reloadData()
                    self.challengeSeeAllCollectionView.reloadData()
                //}
            }else {
                print("Data is not in proper format")
            }
        }
    }
    
    
}
