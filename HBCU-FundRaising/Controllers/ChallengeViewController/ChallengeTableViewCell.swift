//
//  ChallengeTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 09/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

protocol SeeAllProtocol {
    func seeAll(status  : Bool)
}

protocol detailApiProtocol {
    //func apiHit(status : Bool)
    func didSelect(ID : AnyObject)
}

import UIKit

class ChallengeTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //TableView Data
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var challegeTitleLabel: UILabel!
    @IBOutlet weak var challengeDetailLbl: UILabel!
    @IBOutlet weak var seeAllButton: UIButton!
    // Challenge Collection View
    @IBOutlet weak var challengeCollectionView: UICollectionView!
    @IBOutlet weak var noBounsLabel: UILabel!
    @IBOutlet weak var noBounsView: UIView!
    
    var challengesDetailModel = [ChallengesDetailModel]()
//    var bonusDetailModel = [ChallengesDetailModel]()
    var delegate : SeeAllProtocol?
    var delegate1 : detailApiProtocol?
    var parentController : UIViewController?
    
    //Timer:
    var seconds = Int()
    var timer = Timer()
    //var isTimerRunning = false
    
    var serverDateTimeStr : String?
    var serverDateInDateFormat : Date?
    var startDateInDateFormat : Date?
    var endtDateInDateFormat : Date?
    var timerStr : String?
    
    var getIndexPathRow : Int?
   
    var tableViewRow : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        challengeCollectionView.dataSource = self
        challengeCollectionView.delegate = self
        
        serverTime()
        
//            if challengesDetailModel.count == 0 {
//                challengeCollectionView.isHidden = true
//                self.noBounsView.isHidden = false
//                self.noBounsLabel.text = "No challenge found"
//
//            }else{
//                 challengeCollectionView.isHidden = false
//                self.noBounsView.isHidden = true
//            }
//            if bonusDetailModel.count == 0 {
//                challengeCollectionView.isHidden = true
//                self.noBounsView.isHidden = false
//                self.noBounsLabel.text = "No challenge found"
//            }else{
//                 challengeCollectionView.isHidden = false
//            }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
        serverTime()
    }
    
    deinit {
        timer.invalidate()
    }
    
    @IBAction func seeAllButtonPressed(_ sender: UIButton) {
//        guard let parentview = self.parentController else {
//            return
//        }
        //parentview.performSegue(withIdentifier: "seeAllSegue", sender: self)
    }
    
//    func checkButon(){
//        self.seeAllButton.isHidden = challengesDetailModel.count > 0 ? false : true
//    }
    
    func reloadCollectionView(){
        self.challengeCollectionView.reloadData()
    }

    func serverTime(){
      if self.serverDateTimeStr != nil && self.serverDateTimeStr != ""{
         //serverDateTime = self.UTCToLocalStringValue(date: self.serverDateTimeStr ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "dd MMMM,yyyy h:mm a")
        let serverDateString = self.UTCToLocalStringValue(date: self.serverDateTimeStr ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "yyyy-MM-dd HH:mm:ss")
        
        serverDateInDateFormat = self.getDateFormatterDate(date: serverDateString, incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
        
      }else{}
    }
    
    func runTimer(serverDate : Date, grabDate : Date) {
        let mingap = Calendar.current.dateComponents([.second], from: serverDate ?? Date(), to: grabDate ?? Date())
        print("gap is\(mingap.second!)")
        seconds = mingap.second!
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(ChallengeTableViewCell.updateTimer)), userInfo: nil, repeats: true)
    }
    
    func timeString(time:TimeInterval) -> String {
        
        let days = ((Int(time) / 86400))
        let hours = ((Int(time) % 86400) / 3600)
        let minutes = ((Int(time) % 3600) / 60)
        let seconds = ((Int(time) % 3600) % 60)
        return String(format:"%02i Days, %02i:%02i:%02i", days, hours, minutes, seconds)
        
    }
    
    @objc func updateTimer() {
        
        if seconds < 1 {
            timer.invalidate()
            //Send alert to indicate "time's up!"
        }else{
            seconds -= 1    //This will decrement(count down)the seconds
            self.timerStr = timeString(time: TimeInterval(seconds))  //This will update the label.
            
            print("==========",self.timerStr)
            
//            challengeCollectionView.reloadInputViews()
            
//            self.challengeCollectionView.reloadData()
//            chatMessageTableView.reloadRows(at: [IndexPath(row: self.selectedIndex, section: 0)], with: .automatic)
            
//            let getIndexPath = IndexPath(item: getIndexPathRow ?? 0, section: 0)
//            let cell = challengeCollectionView.cellForItem(at: getIndexPath) as! ChallengeCollectionCell
           
//            let visibleIndexPaths = challengeCollectionView.indexPathsForVisibleItems
//            var visibleCellsArray: [UICollectionViewCell] = []
//            for currentIndextPath in visibleIndexPaths {
//                //  You now have visible cells in visibleCellsArray
//                visibleCellsArray.append(challengeCollectionView.cellForItem(at: currentIndextPath)!)
//            }
            
//            let visibleIndexPaths = challengeCollectionView.indexPathsForVisibleItems
//            //var visibleCellsArray: [UICollectionViewCell] = []
//            for indexPath in visibleIndexPaths {
//                //  You now have visible cells in visibleCellsArray
//                //visibleCellsArray.append(challengeCollectionView.cellForItem(at: currentIndextPath)!)
//                let cell = challengeCollectionView.cellForItem(at: indexPath) as! ChallengeCollectionCell
//
//                if let serverDateInDateFormat = serverDateInDateFormat, let startDateInDateFormat = startDateInDateFormat {
//
//                    let startDateComparisionResult:ComparisonResult = serverDateInDateFormat.compare(startDateInDateFormat)
//
//                    if startDateComparisionResult == ComparisonResult.orderedAscending
//                    {
//                        // Server date is smaller than Start date.
//
//                        cell.challengeEndTimeLabel.text = "Starts in: \(timerStr ?? "")"
//                    }
//                    else if startDateComparisionResult == ComparisonResult.orderedDescending
//                    {
//                        // Server date is greater than Start date.
//
//                        cell.challengeEndTimeLabel.text = "Ends in: \(timerStr ?? "")"
//                    }
//                    else if startDateComparisionResult == ComparisonResult.orderedSame
//                    {
//                        // Server date and Start date are same
//
//                        cell.challengeEndTimeLabel.text = "Ends in: \(timerStr ?? "")"
//                    }else{}
//                }
//            }
        }
    }
    
    //Collection View Methods:
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

//        if challengesDetailModel.count > 2 {
//            return 5
//        } else {
        
//        }        
        if tableViewRow == 0{
            if challengesDetailModel.count == 0 {
                challengeCollectionView.isHidden = true
                self.noBounsView.isHidden = false
                self.noBounsLabel.text = "No challenge found"
            }else{
                challengeCollectionView.isHidden = false
                self.noBounsView.isHidden = true
            }
        }else if tableViewRow == 1{
            if challengesDetailModel.count == 0 {
                challengeCollectionView.isHidden = true
                self.noBounsView.isHidden = false
                self.noBounsLabel.text = "No challenge found"
            }else{
                challengeCollectionView.isHidden = false
                self.noBounsView.isHidden = true
            }
        }else{}
        
        return challengesDetailModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChallengeCollectionCell1", for: indexPath) as! ChallengeCollectionCell
 
            cell.challengeImageView.sd_setImage(with: URL(string: Constants.APIs.imageBaseURL + (self.challengesDetailModel[indexPath.row].image ?? "")), placeholderImage: UIImage(named: "ic_image_thumb_small@x"))
            cell.challengeNumberLabel.text = self.challengesDetailModel[indexPath.row].name ?? ""
            cell.challengeDetailLabel.text = self.challengesDetailModel[indexPath.item].description ?? ""
            
            let startDateString = self.convertTimeFormater(self.challengesDetailModel[indexPath.item].startDate ?? "")
            let endtDateString = self.convertTimeFormater(self.challengesDetailModel[indexPath.item].endDate ?? "")
            
            startDateInDateFormat = self.getDateFormatterDate(date: self.challengesDetailModel[indexPath.item].startDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            endtDateInDateFormat = self.getDateFormatterDate(date: self.challengesDetailModel[indexPath.item].endDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss")
            
            if let serverDateInDateFormat = serverDateInDateFormat, let startDateInDateFormat = startDateInDateFormat, let endtDateInDateFormat = endtDateInDateFormat {
                let startDateComparisionResult:ComparisonResult = serverDateInDateFormat.compare(startDateInDateFormat)
                self.timer.invalidate()
                if startDateComparisionResult == ComparisonResult.orderedAscending {
                    // Server date is smaller than Start date.
                    getIndexPathRow = indexPath.row
                    //self.runTimer(serverDate: serverDateInDateFormat, grabDate: startDateInDateFormat)
                    cell.challengeEndTimeLabel.text = "Starts: \(startDateString ?? "")"
                }
                else if startDateComparisionResult == ComparisonResult.orderedDescending
                {
                    // Server date is greater than Start date.
                    
                    getIndexPathRow = indexPath.row
                    
                    //self.runTimer(serverDate: startDateInDateFormat, grabDate: endtDateInDateFormat)
                    
                    cell.challengeEndTimeLabel.text = "Ends: \(endtDateString ?? "")"
                }
                else if startDateComparisionResult == ComparisonResult.orderedSame
                {
                    // Server date and Start date are same.
                    
                    getIndexPathRow = indexPath.row
                    
                    //self.runTimer(serverDate: startDateInDateFormat, grabDate: endtDateInDateFormat)
                    
                    cell.challengeEndTimeLabel.text = "Ends: \(endtDateString ?? "")"
                }else{}
            }
            //        if serverDate >= startDate{
            //            //self.runTimer()
            //            print("Ends in:")
            //        }else if serverDate < startDate{
            //            //self.runTimer()
            //            print("Starts in:")
            //        }else{}
            guard let parentview = self.parentController else {
                return cell
            }
     
//        let startDateString = self.UTCToLocalStringValue(date: self.challengesDetailModel[indexPath.item].startDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "dd MMM, h:mm a")
//        let endtDateString = self.UTCToLocalStringValue(date: self.challengesDetailModel[indexPath.item].endDate ?? "", incomingDateFormat: "yyyy-MM-dd HH:mm:ss", outgoingDateFormat: "dd MMM, h:mm a")
               return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        //return CGSize(width: self.view.frame.width / 3, height: self.challengeCollectionView.frame.height)
        //return CGSize(width: (self.window?.frame.size.width)! / 3, height: self.challengeCollectionView.frame.height)
        return CGSize(width: collectionView.frame.size.width/2.2, height: self.challengeCollectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        timer.invalidate()
        self.delegate1?.didSelect(ID: self.challengesDetailModel[indexPath.row] as AnyObject)
    }
}

extension ChallengeTableViewCell {
    
    func UTCToLocalStringValue(date:String, incomingDateFormat: String, outgoingDateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = outgoingDateFormat
        
        return dateFormatter.string(from: dt ?? Date())
    }
    
    func getDateFormatterDate(date:String, incomingDateFormat: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        return dt
    }
    
    func convertTimeFormater(_ date: String) -> String? {
        
        if date.isEmpty == false {
            let myDateString = date
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.amSymbol = "AM"
            dateFormatter.pmSymbol = "PM"
            
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormatter.date(from: myDateString)!
            
            //dateFormatter.dateFormat = "dd MMM, h:mm a"
            dateFormatter.dateFormat = "MMMM, dd, h:mm a"
            
            let somedateString = dateFormatter.string(from: myDate as Date)
            return somedateString
        }
        return ""
    }
}
