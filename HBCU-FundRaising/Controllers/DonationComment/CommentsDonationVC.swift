//
//  CommentsDonationVC.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 03/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManager

class CommentsDonationVC: UIViewController {
    
    @IBOutlet weak var donateNow: UIButton!
    @IBOutlet weak var donatedByNumber: UITextField!
    @IBOutlet weak var favNumber: UITextField!
    @IBOutlet weak var commentNumber: UITextField!
    @IBOutlet weak var totalDonationText: UITextField!
    @IBOutlet weak var univImage: UIImageView!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var commentTextfield: UITextField!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    
    let donationView = DonationView()
    var hbcu : String?
    var hbcuTitle : String?
    var modelArray = [CommentDataModel]()
    var amount : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        self.title = hbcuTitle
        
    }
    
    //MARK:- Keyboard Observer
    @objc func keyboardNotification(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let _ = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.bottomLayoutConstraint?.constant = 0.0
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
            } else {
                self.bottomLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        guard let userDetil = Methods.sharedInstance.getloginData() else {
            return
        }
        if let image = userDetil.profile {
            if image.contains("https://"){
                userProfileImage.sd_setImage(with: URL(string: image ), placeholderImage: nil)
            } else {
                let userImage = Constants.APIs.imageBaseURL + image
                userProfileImage.sd_setImage(with: URL(string: userImage ), placeholderImage: nil)
            }
        }
        self.donateNow.layer.borderWidth = 1
        self.donateNow.layer.borderColor = UIColor(red: 2/255, green: 182/255, blue: 91/255, alpha: 1).cgColor
        self.donateNow.backgroundColor = .clear
          //self.hitGetDonationDetailsAPI()
        //self.view.layoutIfNeeded()
        if Reachability.isConnectedToNetwork() == true {
            self.hitGetDonationDetailsAPI()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    @objc func backAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func hitGetDonationDetailsAPI(){
        
        WebServiceClass.showLoader(view: self.view)
        let hbcu = self.hbcu ?? ""
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + "getHbcuDetails?hbcu=\(hbcu)", method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult    = response as? Dictionary<String, AnyObject> {
                    if let result = jsonResult["result"] as? Dictionary<String, AnyObject>{
                        if let modelData = Mapper<CommentDataModel>().map(JSONObject: result) {
                            self.modelArray.append(modelData)
                            if let image = result["image"] as? String {
                                let hbcuImage = Constants.APIs.imageBaseURL + image
                                self.univImage.sd_setImage(with: URL(string: hbcuImage ), placeholderImage: nil)
                            }
                            
                            if let totalDonation = result["total_donation"] as? String {
                                self.totalDonationText.text = " $" + totalDonation
                            }else if let totalDonation = result["total_donation"] as? Int {
                                self.totalDonationText.text = " $" + "\(totalDonation)"
                            }else if let totalDonation = result["total_donation"] as? Double {
                                self.totalDonationText.text = " $" + "\(totalDonation)"
                            }
                            
                            if let totalComment = result["total_comment"] as? String{
                                self.commentNumber.text = totalComment
                            }
                            if let favNumber = result["total_favourite"] as? String{
                                self.favNumber.text = favNumber
                            }
                            if let donatedBy = result["total_donatedby"] as? String {
                                self.donatedByNumber.text = donatedBy
                            }
                        }
                    }
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
    @IBAction func donateNowButtonAction(_ sender: UIButton) {
        
//        donationView.frame = self.view.frame
//        donationView.layer.cornerRadius = 0.2
//        donationView.donateButton.addTarget(self, action: #selector(hitOneTimeDonation), for: .touchUpInside)
//        donationView.donationBorderView.layer.cornerRadius = 5
//        donationView.donationBorderView.layer.borderColor = UIColor.black.cgColor
//        self.view.addSubview(donationView)
        
        // Safe Present
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DontionVC") as? DonationViewAddController {
            vc.hbcu = self.hbcu ?? ""
            self.present(vc, animated: false, completion: nil)
        }
        
    }
    
    @IBAction func addCommentAction(_ sender: UIButton) {
        if commentTextfield.text == "" || commentTextfield.text == nil {
            self.showAlert("Please enter some text")
        } else {
            if Reachability.isConnectedToNetwork() == true {
                self.hitAddCommentAPI()
            } else {
                self.showAlert(Constants.commomInternetmsz.checkInternet)
            }
        }
        
    }
    
    @IBAction func goToCommentSection(_ sender: UIButton) {
        
        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CommentsTableView") as! CommentsTableView
            vc.hbcu = self.hbcu!
            vc.mode = .viewComments
            vc.headerTitleLabelStr = "Comments"
            currentNavVC.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func goToFavSection(_ sender: UIButton) {
        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CommentsTableView") as! CommentsTableView
            vc.hbcu = self.hbcu!
            vc.mode = .viewFav
            vc.headerTitleLabelStr = "Favorites"
            currentNavVC.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func goToDonationSection(_ sender: UIButton) {
        if self.donatedByNumber.text == "0" {
            self.showAlert("No one has donated to this HBCU yet, be the first one to donate.")
        } else {
            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CommentsTableView") as! CommentsTableView
                vc.hbcu = self.hbcu!
                vc.mode = .viewDonation
                vc.headerTitleLabelStr = "Donated By"
                currentNavVC.pushViewController(vc, animated: true)
            }
        }
    }
    
    func hitAddCommentAPI() {
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let user_id        = userInfoModel.id as? String
        let hbcu           = self.hbcu ?? ""
        let comment        = commentTextfield.text ?? ""
        let paramsStr = "user_id=\(user_id ?? "")&hbcu=\(hbcu)&comment=\(comment)"
        
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + "addHBCUComment", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        self.showAlert((jsonResult["message"] as? String)!)
                    }
                    else {
                        print(jsonResult)
                        self.showAlert((jsonResult["message"] as? String)!)
                    }
                }
                DispatchQueue.main.async {
                    self.commentTextfield.text = ""
                    self.hitGetDonationDetailsAPI()
                }
            } else {
                print("Data is not in proper format")
            }
        }
        
    }
}
extension CommentsDonationVC : UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = amount
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //Disable iQkeyboarad
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }
}


