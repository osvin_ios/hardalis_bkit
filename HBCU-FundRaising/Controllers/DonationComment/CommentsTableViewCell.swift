//
//  CommentsTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 04/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class CommentsTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var commentLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentUserName: UITextField!
    @IBOutlet weak var commentsProfileImage: UIImageView!
    
    @IBOutlet weak var commentProfileButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
