//
//  CommentsTableView.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 04/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManager

class CommentsTableView: UIViewController {

    @IBOutlet weak var userProfile: UIImageView!
    @IBOutlet weak var noCommenImage: UIImageView!
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var nillLabel: UILabel!
    @IBOutlet weak var addeComentHeight: NSLayoutConstraint!
    @IBOutlet weak var buttomLayoutConstraint: NSLayoutConstraint!

    @IBOutlet weak var commentTextfield: UITextField!
    @IBOutlet weak var headerTitleLabel: UILabel!
    
    var hbcu = String()
    var modelArray = [CommentDataModel]()
    var mode  = Comment.viewComments
    var tag = Int()
    
    var headerTitleLabelStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let userDetil = Methods.sharedInstance.getloginData() else {
            return
        }
        if let image = userDetil.profile {
            if image.contains("https://"){
               userProfile.sd_setImage(with: URL(string: image ), placeholderImage: nil)
            } else {
                let userImage = Constants.APIs.imageBaseURL + image
                userProfile.sd_setImage(with: URL(string: userImage ), placeholderImage: nil)
            }
        }
        addeComentHeight.constant = 0
        noCommenImage.isHidden = true
        nillLabel.isHidden = true
        
        commentsTableView.register(UINib(nibName: "CommentsTableViewCell", bundle: nil), forCellReuseIdentifier: "cellReuseIdentifier")
        self.callServiceFunction()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        
        //self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        if self.headerTitleLabelStr != ""{
            self.headerTitleLabel.text = self.headerTitleLabelStr
        }else{}
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    //MARK:- Keyboard Observer
    @objc func keyboardNotification(notification: NSNotification){
        
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let _ = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.buttomLayoutConstraint?.constant = 0.0
            } else {
                self.buttomLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            self.view.layoutIfNeeded()
        }
    }
    
    func callServiceFunction(){
        if Reachability.isConnectedToNetwork() == true {
            switch mode {
            case .viewFav:
                self.getHBCUFavAPI()
            case .viewDonation:
                self.getHBCUDonationAPI()
            default:
                self.getHBCUCommentAPI()
            }
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    @IBAction func backButtonPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func sendCommentButtonAction(_ sender: UIButton) {
        if commentTextfield.text?.count == 0 {
            self.showAlert("Please enter some text")
        } else {
            if Reachability.isConnectedToNetwork() == true {
                self.hitAddCommentAPI()
            } else {
                self.showAlert(Constants.commomInternetmsz.checkInternet)
            }
        }
    }
    
    func hitAddCommentAPI() {
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id        = userInfoModel.id as? String 
        let hbcu           = self.hbcu ?? ""
        let comment        = commentTextfield.text ?? ""
        
        let paramsStr = "user_id=\(user_id ?? "0")&hbcu=\(hbcu)&comment=\(comment)"
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL +  "addHBCUComment", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        self.showAlert((jsonResult["message"] as? String)!)
                    } else {
                        print(jsonResult)
                        self.showAlert((jsonResult["message"] as? String)!)
                    }
                }
                DispatchQueue.main.async {
                    self.getHBCUCommentAPI()
                    self.commentTextfield.text = ""
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
    func getHBCUCommentAPI(){
        self.addeComentHeight.constant = 50
        WebServiceClass.showLoader(view: self.view)
        let hbcu = self.hbcu ?? ""
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL +  "getHbcuCommentsBy?hbcu=\(hbcu)", method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            
            if success == true {
                
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                       let message = jsonResult["message"] as? String
                        if message == "No record found" {
                            self.commentsTableView.isHidden = true
                            self.nillLabel.isHidden = false
                            self.noCommenImage.isHidden = false
                            self.nillLabel.text = "No comments yet, be the first one to share your thoughts."
                            self.nillLabel.textColor = UIColor.lightGray
                        }else{
                                self.commentsTableView.isHidden = false
                                self.nillLabel.isHidden = true
                                self.noCommenImage.isHidden = true
                            if let result = jsonResult["result"] as? [Dictionary<String, AnyObject>]{
                                if let modelData = Mapper<CommentDataModel>().mapArray(JSONObject: result) {
                                    self.modelArray = modelData
                                }
                            }
                        }
                    }
                
                DispatchQueue.main.async {
                    self.mode = .viewComments
                    self.dismissKeyboard()
                    self.commentsTableView.reloadData()
                }
            }else {
                print("Data is not in proper format")
            }
        }
    }
    
    func convertDateFormater(_ date: String) -> String {
        if date.isEmpty == false {
            let myDateString = date
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormatter.date(from: myDateString)!
            
            dateFormatter.dateFormat = "dd MMM"
            let somedateString = dateFormatter.string(from: myDate)
            return somedateString
            
        }
        return ""
    }
    
    func getHBCUFavAPI() {

        WebServiceClass.showLoader(view: self.view)
        let hbcu = self.hbcu ?? ""
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + "getHbcuFavouriteBy?hbcu=\(hbcu)", method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true{
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let error = jsonResult["error"] as? Bool {
                        if error == true{
                            self.addeComentHeight.constant = 0
                            self.view.layoutIfNeeded()
                            self.commentsTableView.isHidden = true
                            self.nillLabel.isHidden = false
                            self.nillLabel.text = "Be the first to favorite this school!"
                        }else{
                            self.addeComentHeight.constant = 0
                            self.view.layoutIfNeeded()

                        if let result = jsonResult["result"] as? [Dictionary<String, AnyObject>]{
                            if let modelData = Mapper<CommentDataModel>().mapArray(JSONObject: result){
                                self.modelArray = modelData
                            }
                         }
                      }
                   }
                }
                DispatchQueue.main.async {
                    self.commentsTableView.reloadData()
                }
            }else{
                print("Data is not in proper format")
            }
        }
    }
    
    func getHBCUDonationAPI(){
 
        WebServiceClass.showLoader(view: self.view)
        let hbcu = self.hbcu ?? ""

        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + "getHbcuDonatedBy?hbcu=\(hbcu)", method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                self.addeComentHeight.constant = 0
                if let jsonResult    = response as? Dictionary<String, AnyObject> {
                    if let result = jsonResult["result"] as? [Dictionary<String, AnyObject>]{
                        if let modelData = Mapper<CommentDataModel>().mapArray(JSONObject: result) {
                            self.modelArray = modelData }
                    }
                }
                DispatchQueue.main.async {
                    self.commentsTableView.reloadData()
                }
            }else {
                print("Data is not in proper format")
            }
        }
    }
}

extension CommentsTableView : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return modelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell : CommentsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifier", for: indexPath) as! CommentsTableViewCell
        let value = modelArray[indexPath.row]
        
        if mode == .viewComments {
            
            tag = indexPath.row
            let date = convertDateFormater(value.created!)
            cell.dateLabel.text = date
            cell.commentLabel.text = value.comment
            cell.commentUserName.text = value.first_name! + value.last_name!
          
            let image = Constants.APIs.imageBaseURL + value.profile!
            cell.commentsProfileImage.sd_setImage(with: URL(string: image ), placeholderImage: nil)
            cell.commentProfileButton.tag = indexPath.row
            cell.commentUserName.tag = indexPath.row
            cell.commentProfileButton.addTarget(self, action: #selector(profileImageView(_:)), for: .touchUpInside)
            
        } else if mode == .viewFav {
            
            tag = indexPath.row
            cell.commentLabelHeight.constant = 0
            cell.dateLabel.isHidden = true
            cell.commentUserName.text = value.first_name! + value.last_name!
          
            let image = value.profile!
            if image.contains("https://"){
                cell.commentsProfileImage.sd_setImage(with: URL(string: image ), placeholderImage: nil)
            }else{
                let image = Constants.APIs.imageBaseURL + value.profile!
                cell.commentsProfileImage.sd_setImage(with: URL(string: image ), placeholderImage: nil)
            }
             cell.commentProfileButton.tag = indexPath.row
             cell.commentUserName.tag = indexPath.row
             cell.commentProfileButton.addTarget(self, action: #selector(profileImageView(_:)), for: .touchUpInside)
            
        } else {
            
            tag = indexPath.row
            cell.commentLabelHeight.constant = 0
            cell.commentUserName.text = value.first_name! + value.last_name!
            let image = Constants.APIs.imageBaseURL + value.profile!
            cell.commentsProfileImage.sd_setImage(with: URL(string: image ), placeholderImage: nil)
            cell.dateLabel.text = "$" + value.total_amount!
            cell.dateLabel.textColor = UIColor.black
            cell.commentProfileButton.tag = indexPath.row
            cell.commentUserName.tag = indexPath.row
            cell.commentProfileButton.addTarget(self, action: #selector(profileImageView(_:)), for: .touchUpInside)
        }
        return cell
    }

    @objc func profileImageView(_ sender : UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatMemberDetailViewController") as! ChatMemberDetailViewController
       // vc.commentUserId = modelArray[tag].id ?? ""
        vc.mostLovedId = true
        vc.commentUserId = modelArray[sender.tag].id ?? ""
     //   vc.profile = modelArray[sender.tag].profile!
      //  vc.labelName = modelArray[sender.tag].first_name!
    //    vc.lastname = modelArray[sender.tag].last_name!
        self.navigationController?.pushViewController(vc, animated: true)
    //    self.present(vc, animated: true, completion: nil)
    }
}

