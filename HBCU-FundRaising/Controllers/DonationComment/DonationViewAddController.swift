//
//  DonationViewAddController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 16/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import Stripe
import PassKit
import Alamofire

class DonationViewAddController: UIViewController {

    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var donateWithApplePayButton: ButtonDesign!
    
    var hbcu = String()
    var str = ""
    var hbcu1 = String()
    var str1 = ""
    var comingVC = Bool()
    var hbcuDetailData = [ChatPersonDetailModel]()
    var groupId = String()
    
    //Apple Pay
    var subscriptionId : String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        str = hbcu
        
        //view.isOpaque = false
        //view.backgroundColor = .clear
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func buttonActionDonation(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func donateWithApplePayButtonPressed(_ sender: ButtonDesign) {
        let donationAmount = Int(self.amountTextField.text ?? "")
        if self.amountTextField.text == "" || self.amountTextField.text == nil  {
            self.showAlert("Please enter some amount")
        } else if donationAmount == 0{
            self.showAlert("Minimun amount is $1")
        }  else {
            self.alertFunctionsWithCallBack(title: "Do you really want to donate this amount?") { (isObject) in
                if isObject.boolValue {
                   self.setupApplePay()
                }else{}
            }
        }
    }
    
    @IBAction func DonateNow(_ sender: UIButton) {
        let donationAmount = Int(self.amountTextField.text ?? "")
        if self.amountTextField.text == "" || self.amountTextField.text == nil  {
            self.showAlert("Please enter some amount")
        } else if donationAmount == 0{
            self.showAlert("Minimun amount is $1")
        }  else {
            self.alertFunctionsWithCallBack(title: "Do you really want to donate this amount?") { (isObject) in
                if isObject.boolValue {
                    self.hitOneTimeDonationAPI()
                } else {print("rejected")}
            }
        }
    }
    
    func hitOneTimeDonationAPI(){
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id    = userInfoModel.id as? String
        let amount     = self.amountTextField.text ?? ""
        let card_num   = ""
        let comment    = "vc"
        var hbcu2 = String()
        if comingVC == true{
            hbcu2 = groupId
        }else{
            hbcu2 = self.str
        }
        
        //let paramsStr = "user_id=\(user_id ?? "")&amount=\(amount)&card_num=\(card_num)&comment=\(comment)&hbcu=\(hbcu2)"
        let paramsStr = "user_id=\(user_id ?? "")&amount=\(amount)&card_id=\(card_num)&hbcu=\(hbcu2)"
        print(paramsStr)
        
        WebServiceClass.showLoader(view: self.view)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL +  "oneTimeDonation", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true{
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    if let responseCode = jsonResult["error"] as? Bool ,responseCode{
                        self.showAlert(jsonResult["message"] as! String)
                    } else {
                        
                        let view = AlertView()
                        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
                        view.layer.cornerRadius = 0.2
                        view.alertView.layer.cornerRadius = 10
                        view.alertView.layer.borderColor = UIColor.black.cgColor
                        self.view.addSubview(view)
                    }
                } else {
                    print("Data is not in proper format")
                }
                DispatchQueue.main.async {
                    self.amountTextField.text = ""
                }
        }
            else {
                //self.showAlert("Something went wrong, try again after some time")
                self.showAlert("Something is wrong. Please try again later.")
                print("Rejected - 500")
            }
        }
    }
}

extension DonationViewAddController : PKPaymentAuthorizationViewControllerDelegate{
    
    func setupApplePay(){
        
        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: Constants.ApplePayMerchantID)
        paymentRequest?.currencyCode = "USD"
        paymentRequest?.countryCode = "US"
        
        if let amount = Int(self.amountTextField.text ?? ""){
            paymentRequest?.paymentSummaryItems = [
                PKPaymentSummaryItem(label: "", amount: NSDecimalNumber(value: amount))
            ]
            
            //Payment networks array
            let paymentNetworks = [PKPaymentNetwork.amex, .visa, .masterCard, .discover]
            paymentRequest?.supportedNetworks = paymentNetworks
            paymentRequest?.merchantCapabilities = .capability3DS
        }
        
        if Stripe.canSubmitPaymentRequest(paymentRequest) {
            var paymentController: PKPaymentAuthorizationViewController?
            paymentController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest!)
            paymentController?.delegate = self
            paymentController?.modalPresentationStyle = .formSheet
            
            if let paymentController = paymentController {
                present(paymentController, animated: true, completion: nil)
            }
        } else {
            //self.showAlert("Apple Pay is not available on this device or May needs to set up the Apple Pay on the device")
            self.showAlert("You need to set up the Apple Pay on the device")
        }
        
    }
    
    //This function is called when Apple Pay payment will be authorized.
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void){
        // Payment token can be found like this
        print(payment.token)
        // Get response from the server and set the PKPaymentAuthorizationStatus
        let status = PKPaymentAuthorizationStatus(rawValue: 0)!
        switch status.rawValue {
        case 0:
          
            //Stripe.setDefaultPublishableKey("pk_test_ho0D4zAZULYWZcvNHEHFasyE")
            Stripe.setDefaultPublishableKey("pk_live_egNBOvzPd1DewC3sOGQe4H0c") //live
            
            STPAPIClient.shared().createToken(with: payment) {
                (token, error) -> Void in
                
                if (error != nil) {
                    print("%@", error!)
                    completion(PKPaymentAuthorizationStatus.failure)
                    return
                }
                
                if let token = token{
                    self.hitOneTimeAPI(token: token, VC: controller, completion: completion)
                }
                
            }
            
        default:
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
    //This function is called when Apple Pay payment authorization is finished.
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController){
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension DonationViewAddController{
    
    //MARK:- one time charges
    func hitOneTimeAPI(token: STPToken, VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)) {
        
        let url            = "https://api.stripe.com/v1/charges"
        let description    = "One-off"
        let source         = token
        var price = 0
        if let firstPrice = self.amountTextField.text{
            let newFirstPrice = Int(firstPrice) ?? 0
            price = newFirstPrice * 100
        }
        
        var dict = Dictionary<String, Any>()
        
        dict = ["amount"        : price ,
                "currency"  : "usd" ,
                "description" : description  , "source" : source ]
        
        //"sk_test_0HEIX5gE8pRO5X5xuSPrPczU"
        //"sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        
        let header = [
            "Content-Type"   : "application/x-www-form-urlencoded",
            "Authorization"  : "Bearer sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        ]
        
        WebServiceClass.showLoader(view: self.view)
        Alamofire.request(url , method: .post, parameters: dict, encoding:  URLEncoding.httpBody, headers: header ).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                if let json = response.result.value as? [String: Any] {
                    print("JSON: \(json)") // serialized json response
                    if let id = json["id"] as? String{
                        self.subscriptionId = id
                        
                        if let txnId = json["balance_transaction"] as? String{
                            if let source = json["source"] as? [String: Any]{
                                if let cardNum = source["last4"] as? String{
                                    if let cardId = source["id"] as? String{
                                        self.saveOneTimeDonationWithApplePayDetail(card_num: cardNum, card_id: cardId, txnId: txnId, VC: VC, completion: completion)
                                    }
                                }
                            }
                        }
                    }else{
                        resultFailure()
                    }
                }else{
                    resultFailure()
                }
                
            case.failure(let error):
                print("Not Success",error)
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
    func saveOneTimeDonationWithApplePayDetail(card_num: String, card_id: String, txnId: String, VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)){
        
        guard let userInfoModel = Methods.sharedInstance.getloginData()else{return}
        let user_id = userInfoModel.id as? String ?? ""
        let amount = self.amountTextField.text ?? ""
        var hbcu2 = String()
        if comingVC == true{
            hbcu2 = groupId
        }else{
            hbcu2 = self.str
        }
        
        let paramsStr = "user_id=\(user_id)&amount=\(amount)&donation_type=\(2)&hbcu=\(hbcu2)&card_num=\(card_num)&card_id=\(card_id)&txnId=\(txnId)"
        print(paramsStr)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL +  Constants.APIs.paymentsDoneWithAppleI, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true{
                if let jsonResult = response as? Dictionary<String, AnyObject>{
                    print(jsonResult)
                    if let responseCode = jsonResult["error"] as? Bool ,responseCode{
                        //self.showAlert(jsonResult["message"] as! String)
                        resultFailure()
                    }else{
//                        let view = AlertView()
//                        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
//                        view.layer.cornerRadius = 0.2
//                        view.alertView.layer.cornerRadius = 10
//                        view.alertView.layer.borderColor = UIColor.black.cgColor
//                        self.view.addSubview(view)
                        
                        completion(PKPaymentAuthorizationStatus.success)
                        return
                    }
                }else{
                    print("Data is not in proper format")
                    resultFailure()
                }
                DispatchQueue.main.async {
                    self.amountTextField.text = ""
                }
            }else{
                //self.showAlert("Something is wrong. Please try again later.")
                //print("Rejected - 500")
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
    
}
