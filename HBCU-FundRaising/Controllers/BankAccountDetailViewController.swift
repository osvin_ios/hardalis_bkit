//
//  BankAccountDetailViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 12/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class BankAccountDetailViewController: UIViewController {
    
    //variables
    var plaidAccessToken : String?
    var accountId = [String]()
    var maskArray = [String]()
    var selectedBank = String()
    var moveAccountID = [MyAccountDetailsModel]()
    var accountMoveId = String()
    
    var checkPreviousLinkedAccountVCis : Bool = false
    var selectedIndexes = [[IndexPath.init(row: 0, section: 0)], [IndexPath.init(row: 0, section: 1)]]
    
    //IBoutlet
    @IBOutlet weak var accountsTableView: UITableView!
    @IBOutlet weak var saveAndContinueButton: ButtonDesign!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            if checkPreviousLinkedAccountVCis == true {
                self.hitAPIForLinkedAccountsData()
            }else {
                self.hitAPI()
            }
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
        saveAndContinueButton.isUserInteractionEnabled = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.title = "Select Your Bank"
    }
    
    //Hit api number 5
    func hitAPI() {
        WebServiceClass.showLoader(view: self.view)
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let apiName        = "signup"
        let user_id        = userInfoModel.id as? String
        let public_token   = UserDefaults.standard.object(forKey: "publicToken")
        let institution_id = UserDefaults.standard.object(forKey: "institutionalId")
        let signup_level   =  "3"
        
        let paramsStr = "user_id=\(user_id ?? "0")&public_token=\(public_token ?? "")&institution_id=\(institution_id ?? "")&signup_level=\(signup_level)"
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + apiName, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        print(jsonResult["message"] as? String )
                    } else {
                        if let resultValue = jsonResult["result"] as? Dictionary<String, AnyObject> {
                            self.plaidAccessToken = resultValue["plaid_access_token"] as? String
                            UserDefaults.standard.set(self.plaidAccessToken, forKey: "plaid_access_token")
                        }
                        self.hitAPIgetPlaidAccounts()
                    }
                } else {
                    print("Data is not in proper format")
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
    //MARK:- hit api to get all checking accounts from plaid
    func hitAPIgetPlaidAccounts() {
        
      
        let urlName = "https://development.plaid.com/"  //Live
            //let urlName = "https://sandbox.plaid.com/auth/get"
        //let urlName = "https://sandbox.plaid.com/"
       
        let accessToken = UserDefaults.standard.value(forKey: "plaid_access_token") as? String
        
        let parameters = [
            "client_id": "596cd0c14e95b810ac887df6",
            "secret": "36ca7ae963c88b05111f1246a5df69",
            "access_token": accessToken
            ] as! [String : String]
        
        print("Parameter string is:-\(parameters)")
        
        WebServiceClass.uploadArrayToServer(urlName: urlName, array: parameters as Dictionary<String, AnyObject>) { (success, object, errorMsg) in
            if success == true {
                if let jsonResult = object as? Dictionary<String, AnyObject> {
                    if let dta = jsonResult["accounts"] as? [Dictionary<String,AnyObject>]{
                        print(dta)
                        if let accountInfo = Mapper<MyAccountDetailsModel>().mapArray(JSONObject: dta){
                            self.moveAccountID = accountInfo
                            print(self.moveAccountID)
                            
                        }
                        for i in 0..<dta.count {
                            let subtype = dta[i]["subtype"] as? String
                            if subtype == "checking"{
                                print("yes")
                                self.accountId.append((dta[i]["account_id"] as? String) ?? "")
                                
                            }else{
                                print("no")
                            }
                        }
                        
                        DispatchQueue.main.async(execute: {
                            self.accountsTableView.reloadData()
                        })
                    }
                } else {
                    print("Data is not in proper format")
                }} else {
                
                print("Data is not in proper format")
            }
        }
    }
    
    @IBAction func saveAndContinueButtonPressed(_ sender: ButtonDesign) {
        
        if checkPreviousLinkedAccountVCis == true {
            //performSegue(withIdentifier: "linkedAccountSegue", sender: nil)
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is LinkedAccountViewController {
                    self.navigationController!.popToViewController(aViewController, animated: true)
                    break
                }
            }
        }else{ 
            self.hitCreateAccountIdAPI()
        }
    }
}

extension BankAccountDetailViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return accountId.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "AccountDetailsTableViewCell", for: indexPath) as! AccountDetailsTableViewCell
        cell.selectionStyle = .none
        
        if accountId.count == 1 {
            
            cell.toggleSelectionButton.isHidden = false
            self.saveAndContinueButton.isUserInteractionEnabled = true
            self.accountMoveId  = moveAccountID[indexPath.row].account_id as? String ?? ""
        } else {
            
            cell.toggleSelectionButton.isHidden = true
            self.saveAndContinueButton.isUserInteractionEnabled = false
            self.accountMoveId  = moveAccountID[indexPath.row].account_id as? String ?? ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedIndex = indexPath
    
        let cell = tableView.cellForRow(at: selectedIndex) as? AccountDetailsTableViewCell
        
       
        if cell?.toggleSelectionButton.isHidden == true{
            
            cell?.toggleSelectionButton.isHidden = false
            self.saveAndContinueButton.isUserInteractionEnabled = true
            self.accountMoveId  = moveAccountID[indexPath.row].account_id as? String ?? ""
        } else {
            
            cell?.toggleSelectionButton.isHidden = true
            self.saveAndContinueButton.isUserInteractionEnabled = false
            self.accountMoveId  = moveAccountID[indexPath.row].account_id as? String ?? ""
        }
    }
    
    
    //APIForLinkedAccounts
    func hitAPIForLinkedAccountsData() {
        
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let apiName        = "createaccesstoken"
        let user_id        = userInfoModel.id as? String
        let public_token   = UserDefaults.standard.object(forKey: "publicToken")
        let institution_id = UserDefaults.standard.object(forKey: "institutionalId")
        
        let paramsStr = "user_id=\(user_id ?? "")&public_token=\(public_token ?? "")&institution_id=\(institution_id ?? "")"
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + apiName, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        print(jsonResult["message"] as? String )
                    } else {
                        if let resultValue = jsonResult["result"] as? Dictionary<String, AnyObject> {
                            self.plaidAccessToken = resultValue["plaid_access_token"] as? String
                            UserDefaults.standard.set(self.plaidAccessToken, forKey: "plaid_access_token")
                            
                        }
                        self.hitAPIgetPlaidAccounts()
                    }
                } else {
                    print("Data is not in proper format")
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
    //MARK:- hit create account Id API
    func hitCreateAccountIdAPI() {
        WebServiceClass.showLoader(view: self.view)
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id = userInfoModel.id as? String
        let abc = moveAccountID
        print(abc)
        let selectedAcoount = accountMoveId

        let access_token = UserDefaults.standard.value(forKeyPath: "plaid_access_token") as? String ?? ""
        let paramsStr = "user_id=\(user_id ?? "0")&account_id=\(selectedAcoount)&access_token=\(access_token )"
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL+"createAccountId" , method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        print(jsonResult["message"] as? String )
                        self.showAlert((jsonResult["message"] as? String)!)
                    } else {
                        if let _ = jsonResult["result"] as? Dictionary<String, AnyObject> {
                            DispatchQueue.main.async {
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "GeneratePinViewController") as! GeneratePinViewController
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
}
