//
//  footerViewFile.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 19/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//



////----- not used in project------------
//import Foundation
//
//class footerViewFile: UIView {
//    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        loadViewFromNib ()
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        loadViewFromNib ()
//    }
//    
//    func loadViewFromNib() {
//        let view = UINib(nibName: "footerView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
//        view.frame = bounds
//        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        self.addSubview(view)
//    }
//    
//    @IBAction func clickToDonatePercentageAction(_ sender: UIButton) {
//        
//        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "DonationViewController") as! DonationViewController
//            vc.mode = .ComingFromHBCUDonation
//            currentNavVC.pushViewController(vc, animated: true)
//        }
//        
//    }
//    
//    
//    
//    
//    @IBAction func editPercentageAction(_ sender: UIButton) {
//    
//        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "EditPercentagesViewController") as! EditPercentagesViewController
//            
//            currentNavVC.pushViewController(vc, animated: true)
//        }
//    }
//}
