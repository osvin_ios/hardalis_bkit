//
//  LikedPersonCollectionViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 07/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class LikedPersonCollectionViewCell: UICollectionViewCell {

    @IBOutlet var likedPersonImageView: ImageViewDesign!
    @IBOutlet var likePersonNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
