//
//  ChatAudioTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 05/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ChatImageableViewCell: UITableViewCell {
    
    //Receiver View
    @IBOutlet var receiverView: UIView!
    @IBOutlet var receiverImgView: UIImageView!
    @IBOutlet var subViewofReceiver: UIView!
    @IBOutlet var reciverMsgLbl: UILabel!
    @IBOutlet var receiverNameLbl: UILabel!
    @IBOutlet var buttonSubView: UIView!
    @IBOutlet var noOfLikesBtnOutletR: UIButton!
    @IBOutlet var heartBtnOutletR: UIButton!
    
    // Sender View
    
    @IBOutlet var senderView: UIView!
    @IBOutlet var senderImgView: UIImageView!
    @IBOutlet var subViewofSenderView: UIView!
    @IBOutlet var senderMsgLbl: UILabel!
    @IBOutlet var senderNameLbl: UILabel!
    @IBOutlet var buttonSubViewSender: UIView!
    @IBOutlet var noOfLikesBtnOutletS: UIButton!
    @IBOutlet var heartBtnOutletS: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // Receiver View
    @IBAction func noOfLikesBtnActnR(_ sender: Any) {
    }
    
    @IBAction func heartBtnActnR(_ sender: Any) {
        
        if heartBtnOutletR.isSelected == true {
            self.heartBtnOutletR.imageView?.image = #imageLiteral(resourceName: "ic_like_unselected")
        }else{
            self.heartBtnOutletR.imageView?.image = #imageLiteral(resourceName: "ic_like_selected")
        }
        
    }
    
    // Sender View
    @IBAction func noOfLikesBtnActnS(_ sender: Any) {
    }
    
    @IBAction func heartBtnOutletS(_ sender: Any) {
        if heartBtnOutletS.isSelected == true {
            self.heartBtnOutletS.imageView?.image = #imageLiteral(resourceName: "ic_like_unselected")
        }else{
            self.heartBtnOutletS.imageView?.image = #imageLiteral(resourceName: "ic_like_selected")
        }
    }

}





