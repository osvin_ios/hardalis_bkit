//
//  VideoTableViewCell.swift
//  XIBSChat
//
//  Created by Chaithu on 12/10/18.
//  Copyright © 2018 osvin. All rights reserved.
//

import UIKit

class VideoTableViewCell: UITableViewCell {

    // MARK: Sender View
    @IBOutlet weak var senderViewS: UIView!
    @IBOutlet weak var senderImageViewS: UIView!
    @IBOutlet weak var senderImgS: UIImageView!
    @IBOutlet weak var senderImgBtnS: UIButton!
    @IBOutlet weak var mainUpperViewS: UIView!
    @IBOutlet weak var likeBtnViewS: UIView!
    @IBOutlet weak var noOfLikeBtnOutletS: UIButton!
    @IBOutlet weak var heartBtnOUtletS: UIButton!
    @IBOutlet weak var senderNameLabelS: UILabel!
    @IBOutlet weak var senderMsgLabelS: UILabel!
    @IBOutlet weak var msgImageShowViewS: UIView!
    @IBOutlet weak var msgImageViewS: UIImageView!
    @IBOutlet weak var playPauseBtnImg: UIImageView!
    @IBOutlet weak var likePersonListViewS: UIView!
    @IBOutlet weak var likePersonLIstHeight: NSLayoutConstraint!
    @IBOutlet weak var likePersonTableViewCell: UITableViewCell!
    @IBOutlet weak var msgTimeViewS: UIView!
    @IBOutlet weak var timeLabelS: UILabel!
    
    //MARK: Receiver View

    
    @IBOutlet weak var receiverViewR: UIView!
    @IBOutlet weak var receiverImageViewR: UIView!
    @IBOutlet weak var receiverImgR: UIImageView!
    @IBOutlet weak var receiverImgBtnR: UIButton!
    @IBOutlet weak var mainUpperViewR: UIView!
    @IBOutlet weak var likeBtnViewR: UIView!
    @IBOutlet weak var noOfLikesBtnOutletR: UIButton!
    @IBOutlet weak var heartBtnOutletR: UIButton!
    @IBOutlet weak var msgImageShowViewR: UIView!
    @IBOutlet weak var msgImageViewR: UIImageView!
    @IBOutlet weak var playPauseBtnImgR: UIImageView!
    @IBOutlet weak var receiverNameLabelR: UILabel!
    @IBOutlet weak var msgLabelR: UILabel!
    @IBOutlet weak var likePersonListViewR: UIView!
    @IBOutlet weak var likePersonListHeightR: NSLayoutConstraint!
    @IBOutlet weak var msgTimeViewR: UIView!
    @IBOutlet weak var timeLabelR: UILabel!
    
    
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
