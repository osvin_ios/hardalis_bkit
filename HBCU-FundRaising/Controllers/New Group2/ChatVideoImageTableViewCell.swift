//
//  ChatVideoImageTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 06/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ChatVideoImageTableViewCell: UITableViewCell {
    
    // Sender view
    @IBOutlet var senderView: UIView!
    @IBOutlet var subViewofSenderView: UIView!
    @IBOutlet var senderNameLbl: UILabel!
    @IBOutlet var senderMsgLbl: UILabel!
    @IBOutlet var btnOutletView: UIView!
    @IBOutlet var noOfLikesBtnOutletS: UIButton!
    @IBOutlet var heartBtnOutletS: UIButton!
    @IBOutlet var MsgImgViewS: UIImageView!
    @IBOutlet var msgImgViewHeightS: NSLayoutConstraint!
    @IBOutlet var senderImgView: UIImageView!
    @IBOutlet var senderImgBtn: UIButton!
    @IBOutlet weak var senderViewVideoImgHeight: NSLayoutConstraint!
    @IBOutlet weak var senderTimeLabel: UILabel!
    @IBOutlet weak var likePersonView: UIView!
    @IBOutlet var likeViewDetailHeightS: NSLayoutConstraint!
    
    // Receiver View
    @IBOutlet var reciverView: UIView!
    @IBOutlet var receiverImgView: UIImageView!
    @IBOutlet var receiverSubView: UIView!
    @IBOutlet var receiverNameLbl: UILabel!
    @IBOutlet var reciverMsgLbl: UILabel!
    @IBOutlet var btnOutletViewReceiver: UIView!
    @IBOutlet var noOfLikesBtnOutletR: UIButton!
    @IBOutlet var heartBtnOutletR: UIButton!
    @IBOutlet var msgImgViewR: UIImageView!
    @IBOutlet var msgImgHeightR: NSLayoutConstraint!
    @IBOutlet var receiverImgBtn: UIButton!
    @IBOutlet weak var receiverTimeLabel: UILabel!
    @IBOutlet var likeViewDetailHeight: NSLayoutConstraint!
    @IBOutlet weak var likePersonViewR: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // Sender View
    
    @IBAction func noOfLikesBtnActnS(_ sender: Any) {
    }
    
    @IBAction func heartBtnActnS(_ sender: Any) {
    }
    
    // Receiver View
    
    @IBAction func noOfLikesBtnActnR(_ sender: Any) {
    }
    
    @IBAction func heartBtnActnR(_ sender: Any) {
    }
}
