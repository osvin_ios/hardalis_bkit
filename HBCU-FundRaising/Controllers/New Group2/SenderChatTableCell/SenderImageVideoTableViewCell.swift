//
//  SenderImageVideoTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 03/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class SenderImageVideoTableViewCell: UITableViewCell {
    
    // Side View
    @IBOutlet var sideView: UIView!
    @IBOutlet var noofLikeBtn: UIButton!
    @IBOutlet var heartBtnOutlet: UIButton!
    
    // Sender Name
    @IBOutlet var senderNameLbl: UILabel!
    @IBOutlet var videoThumbnailImgVIew: UIImageView!
    @IBOutlet var playPauseBtnOutlet: UIButton!
    @IBOutlet var senderImgView: UIImageView!
    @IBOutlet var horizontalSpaceView: UIView!
    
    @IBOutlet var TextShowingLbl: UILabel!
    @IBOutlet var TextShowingLblHeight: NSLayoutConstraint!
    
    // Bottom View
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet var bottomViewHeight: NSLayoutConstraint!
    
    @IBOutlet var firstLikedPersonImage: UIImageView!
    @IBOutlet var secondLikedPersonImage: UIImageView!
    @IBOutlet var thirdLikedPersonImage: UIImageView!
    @IBOutlet var forthLikedPersonImage: UIImageView!
    @IBOutlet var moreLikedPersonImage: UIImageView!
    
    @IBOutlet var firstLikedPersonNameLbl: UILabel!
    @IBOutlet var secondLikedPersonNameLbl: UILabel!
    @IBOutlet var thirdLikedPersonNameLbl: UILabel!
    @IBOutlet var forthLikedPersonNameLbl: UILabel!
    
    // Gesture for more Img Action
    @IBOutlet weak var moreImgGestureRecognizer: UITapGestureRecognizer!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func noofLikeBtnActn(_ sender: Any) {
    }

    @IBAction func heartBtnActn(_ sender: Any) {
    }
    
    @IBAction func playPauseBtnActn(_ sender: Any) {
    }
    
    @IBAction func moreImgGestureRecognizer(_ sender: Any) {
    }
  
}







