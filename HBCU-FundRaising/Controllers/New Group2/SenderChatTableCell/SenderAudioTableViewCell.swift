//
//  SenderAudioTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 03/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class SenderAudioTableViewCell: UITableViewCell {
    
    // Upper View
    @IBOutlet var upperView: UIView!
    
    // Side View inside the Upper View
    
    @IBOutlet var sideView: UIView!
    @IBOutlet var noOfPersonLikeBtn: UIButton!
    @IBOutlet var heartBtnOulet: UIButton!
    
    // Sender Name Inside the Upper View
    @IBOutlet var senderNameLabel: UILabel!
   
    // Audio Play Inside the upper View
    @IBOutlet var audioPlayView: UIView!
    
    @IBOutlet var playPauseBtnOutlet: UIButton!
    @IBOutlet var audioPlaySlider: UISlider!
    
    @IBOutlet var audioDurationLabel: UILabel!
    @IBOutlet var audioAttachtextMsgLabel: UILabel!
    
    @IBOutlet var senderImgView: UIImageView!
    
    @IBOutlet var horizontalSpaceView: UIView!
    
    // Bottom View
    @IBOutlet var bottomView: UIView!
    

    @IBOutlet var bottomViewHeight: NSLayoutConstraint!
    
    @IBOutlet var firstLikedPersonImage: UIImageView!
    @IBOutlet var secondLikedPersonImage: UIImageView!
    @IBOutlet var thirdLikedPersonImage: UIImageView!
    @IBOutlet var forthLikedPersonImage: UIImageView!
    @IBOutlet var moreLikedPersonImage: UIImageView!
    
    @IBOutlet var firstLikedPersonNameLbl: UILabel!
    @IBOutlet var secondLikedPersonNameLbl: UILabel!
    @IBOutlet var thirdLikedPersonNameLbl: UILabel!
    @IBOutlet var forthLikedPersonNameLbl: UILabel!
    
    // Gesture for more Img Action
    @IBOutlet weak var moreImgGestureRecognizer: UITapGestureRecognizer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func noOfPersonLikeBtnActn(_ sender: Any) {
    }
    
    @IBAction func heartBtnActn(_ sender: Any) {
    }

    @IBAction func playPauseBtnActn(_ sender: Any) {
    }
    
    @IBAction func moreImgGestureRecognizerActn(_ sender: Any) {
    }
    
}









