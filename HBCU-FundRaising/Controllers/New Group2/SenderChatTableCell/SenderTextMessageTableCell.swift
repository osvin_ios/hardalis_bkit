//
//  SenderTextMessageTableCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 03/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class SenderTextMessageTableCell: UITableViewCell {
    
    @IBOutlet var sideView: UIView!
    @IBOutlet var senderNameLabel: UILabel!
    
    @IBOutlet var senderMsgLabel: UILabel!
    
    @IBOutlet var senderImageView: UIImageView!
    
    @IBOutlet var horizontalSpaceView: UIView!
    @IBOutlet var noofLikeBtn: UIButton!
    
    @IBOutlet var heratBtnOutlet: UIButton!
    
    
    // Bottom View
    
    @IBOutlet var bottomViewHeight: NSLayoutConstraint!
    
    @IBOutlet var firstLikedPersonImage: UIImageView!
    @IBOutlet var secondLikedPersonImage: UIImageView!
    @IBOutlet var thirdLikedPersonImage: UIImageView!
    @IBOutlet var forthLikedPersonImage: UIImageView!
    @IBOutlet var moreLikedPersonImage: UIImageView!
  
    // Gesture for more Img Action
    @IBOutlet weak var moreImageGestureRecognizer: UITapGestureRecognizer!
    
    @IBOutlet var firstLikedPersonNameLbl: UILabel!
    @IBOutlet var secondLikedPersonNameLbl: UILabel!
    @IBOutlet var thirdLikedPersonNameLbl: UILabel!
    @IBOutlet var forthLikedPersonNameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func noOfChatLikeBtnActn(_ sender: Any) {
    }
    
    @IBAction func heartBtnActn(_ sender: Any) {
    }
    
    @IBAction func moreImgGestureReogniizerActn(_ sender: Any) {
    }
}
