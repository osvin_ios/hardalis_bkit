//
//  ChatVoiceTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 06/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ChatVoiceTableViewCell: UITableViewCell {
    
    // Receiver View
    
    @IBOutlet var receiverView: UIView!
    @IBOutlet var receiverImgView: UIImageView!
    @IBOutlet var subViewofReceiverView: UIView!
    @IBOutlet var receiverNameLbl: UILabel!
    @IBOutlet var audioPlayView: UIView!
    @IBOutlet var playPauseBtnOutletR: UIButton!
    @IBOutlet var audioPlaySliderOutletR: UISlider!
    @IBOutlet var audioDurationTimeLblR: UILabel!
    @IBOutlet var receiverMsgLbl: UILabel!
    @IBOutlet var likeBtnView: UIView!
    @IBOutlet var noofLikesOutletR: UIButton!
    @IBOutlet var heartImgOutletR: UIButton!
    @IBOutlet var receiverImgBtn: UIButton!
    @IBOutlet weak var receiverViewHeightVoiceCell: NSLayoutConstraint!
    @IBOutlet weak var receiverTimeLabel: UILabel!
    
    // Sender View
    
    @IBOutlet var senderView: UIView!

    @IBOutlet var senderNameLbl: UILabel!
    @IBOutlet var subViewofSenderView: UIView!
    @IBOutlet var playPauseBtnOutletS: UIButton!
    @IBOutlet var audioDurationSliderS: UISlider!
    @IBOutlet var audioDurationLblS: UILabel!
    @IBOutlet var senderMsgLbl: UILabel!
    @IBOutlet var likeBtnViewS: UIView!
    @IBOutlet var noOfLikesBtnOutletS: UIButton!
    @IBOutlet var heartImgBtnOutletS: UIButton!
    @IBOutlet var senderImgView: UIImageView!
    @IBOutlet var senderImgBtn: UIButton!
    @IBOutlet weak var senderViewHeightVoiceCell: NSLayoutConstraint!
    @IBOutlet weak var senderTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // Receiver End
    @IBAction func playPauseBtnActnR(_ sender: Any) {
    }
    @IBAction func audioPlaySliderActnR(_ sender: Any) {
    }
    @IBAction func noofLikesActionR(_ sender: Any) {
    }
    @IBAction func heartImgActnR(_ sender: Any) {
    }
    
    // Sender End
    
    @IBAction func playPauseBtnActnS(_ sender: Any) {
    }
    @IBAction func audioDurationSliderActnS(_ sender: Any) {
    }
    @IBAction func noOfLikesBtnActnS(_ sender: Any) {
    }
    @IBAction func heartImgBtnActnS(_ sender: Any) {
    }
    
}

