//
//  ReceiverAudioTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 03/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ReceiverAudioTableViewCell: UITableViewCell {
    
    @IBOutlet var receiverImageView: UIImageView!
  
    //Upper View
    @IBOutlet var upperView: UIView!
    @IBOutlet var receiverNameLbl: UILabel!
    
    // SubView of Upper View
    @IBOutlet var sideView: UIView!
    @IBOutlet var noOfLikesBtnOutlet: UIButton!
    @IBOutlet var heartBtnOutlet: UIButton!
    
    @IBOutlet var upperViewHeight: NSLayoutConstraint!
    // MiddleSubView of Upper View
    @IBOutlet var middleSubView: UIView!
    @IBOutlet var playPauseBtnOutlet: UIButton!
    @IBOutlet var volumeSliderOutlet: UISlider!
    @IBOutlet var audioDurationLbl: UILabel!
   
    // Audio Attach Message
    @IBOutlet var audioAttachTextMsgLbl: UILabel!
    
    // Middle Space
    @IBOutlet var horizontalSpaceView: UIView!
    
    // Bottom View
    @IBOutlet var bottomView: UIView!
    @IBOutlet var bottomViewHeight: NSLayoutConstraint!
    
    @IBOutlet var firstPersonImgView: UIImageView!
    @IBOutlet var secondPersonImgView: UIImageView!
    @IBOutlet var thirdPersonImgView: UIImageView!
    @IBOutlet var forthPersonImgView: UIImageView!
    @IBOutlet var moreImgView: UIImageView!
   
    @IBOutlet var firstPersonNameLbl: UILabel!
    @IBOutlet var secondPersonNameLbl: UILabel!
    @IBOutlet var thirdPersonNameLbl: UILabel!
    @IBOutlet var forthPersonNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bottomViewHeight.constant = 0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func noOfLikesBtnActn(_ sender: Any) {
        
        if noOfLikesBtnOutlet.isSelected == true {
            self.bottomViewHeight.constant = 20
        } else {
            self.bottomViewHeight.constant = 0
        }
    }
    
    @IBAction func heartBtnActn(_ sender: Any) {
        if heartBtnOutlet.isSelected == true {
            self.heartBtnOutlet.imageView?.image = #imageLiteral(resourceName: "ic_like_selected")
        }else {
            self.heartBtnOutlet.imageView?.image = #imageLiteral(resourceName: "ic_like_unselected")
        }
    }
    
    @IBAction func playPauseBtnActn(_ sender: Any) {
    }
    
    @IBAction func volumeSliderActn(_ sender: Any) {
    }
}




