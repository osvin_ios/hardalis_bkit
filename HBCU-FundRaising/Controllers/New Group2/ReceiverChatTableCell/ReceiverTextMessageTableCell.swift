//
//  ReceiverTextMessageTableCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 03/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ReceiverTextMessageTableCell: UITableViewCell {
    
    @IBOutlet var receiverImgView: UIImageView!
    
    // Upper View
    @IBOutlet var upperView: UIView!
    @IBOutlet var receiverNameLbl: UILabel!
    @IBOutlet var receiverSideMsgLbl: UILabel!
    @IBOutlet var upperViewHeight: NSLayoutConstraint!
    
    // Side sub view inside Upper view
    
    @IBOutlet var sideView: UIView!
    @IBOutlet var noofLikesCountBtn: UIButton!
    @IBOutlet var heartBtnOutlet: UIButton!
    
    // Space View
    @IBOutlet var horizontalSpaceView: UIView!
    
    // Bottom View
    
    @IBOutlet var bottomView: UIView!
    @IBOutlet var bottomViewHeight: NSLayoutConstraint!

    @IBOutlet var firstReceiverImgView: UIImageView!
    @IBOutlet var secondReceiverImgView: UIImageView!
    @IBOutlet var thirdReceiverImgView: UIImageView!
    @IBOutlet var forthReceiverImgView: UIImageView!
    @IBOutlet var moreImageView: UIImageView!
    
    @IBOutlet var firstReceiverNameLabel: UILabel!
    @IBOutlet var secondReceiverNameLabel: UILabel!
    @IBOutlet var thirdReceiverNameLabel: UILabel!
    @IBOutlet var forthReceiverNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bottomViewHeight.constant = 0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func noofLikesCountBtnActn(_ sender: Any) {
        
        if noofLikesCountBtn.isSelected == true {
            self.bottomViewHeight.constant = 20
        }else{
            self.bottomViewHeight.constant = 0
        }
    }
    
    @IBAction func heartBtnActn(_ sender: Any) {
        
        if heartBtnOutlet.isSelected == true {
            self.heartBtnOutlet.imageView?.image = #imageLiteral(resourceName: "ic_like_selected")
        }else{
            self.heartBtnOutlet.imageView?.image = #imageLiteral(resourceName: "ic_like_unselected")
        }
    }
}




