//
//  ReceiverImageVideoTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 03/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ReceiverImageVideoTableViewCell: UITableViewCell {
    
    @IBOutlet var receiverImageView: UIImageView!
    
    //Upper View
    @IBOutlet var upperView: UIView!
    @IBOutlet var receiverNameLbl: UILabel!
    @IBOutlet var receiveImage: UIImageView!
    @IBOutlet var upperViewHeight: NSLayoutConstraint!
    
    // Side View inside Upper View
    @IBOutlet var sideView: UIView!
    @IBOutlet var heartBtnOutlet: UIButton!
    @IBOutlet var noOfLikesBtnOutlet: UIButton!
    @IBOutlet var noOfLikesLbl: UILabel!
    
    @IBOutlet var receiverMsgLbl: UILabel!

    // Bottom View
    @IBOutlet var bottomView: UIView!
    @IBOutlet var bottomViewHeight: NSLayoutConstraint!
    @IBOutlet var firstPersonImgView: UIImageView!
    @IBOutlet var firstpersonNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bottomViewHeight.constant = 0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func heartBtnActn(_ sender: Any) {
        
        if heartBtnOutlet.isSelected == true {
            self.heartBtnOutlet.imageView?.image = #imageLiteral(resourceName: "ic_like_selected")
        } else {
            self.heartBtnOutlet.imageView?.image = #imageLiteral(resourceName: "ic_like_unselected")
        }
    }
    
    @IBAction func noOfLikesBtnActn(_ sender: Any) {
        if noOfLikesBtnOutlet.isSelected == true {
            self.bottomViewHeight.constant = 20
        }else{
            self.bottomViewHeight.constant = 0
        }
    }
}
