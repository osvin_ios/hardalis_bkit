//
//  AccountDetailsTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 13/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class AccountDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var toggleSelectionButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
