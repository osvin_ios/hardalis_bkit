//
//  ChatAudioTableViewCell.swift
//  XIBSChat
//
//  Created by Chaithu on 12/10/18.
//  Copyright © 2018 osvin. All rights reserved.
//

import UIKit

class ChatAudioTableViewCell: UITableViewCell {

    // MARK:- Receiver View
    @IBOutlet weak var receiverImageViewR: UIView!
    @IBOutlet weak var receiverViewR: UIView!
    @IBOutlet weak var receiverImgR: UIImageView!
    @IBOutlet weak var receiverImgBtnR: UIButton!
    @IBOutlet weak var upperMaiViewR: UIView!
    @IBOutlet weak var likeBtnViewR: UIView!
    @IBOutlet weak var heartBtnOutletR: UIButton!
    @IBOutlet weak var noOfLikesBtnOutletR: UIButton!
    @IBOutlet weak var userNameLabelR: UILabel!
    @IBOutlet weak var userMsgLabelR: UILabel!
    @IBOutlet weak var likePersonListViewR: UIView!
    @IBOutlet weak var likePErsonTableViewR: UITableViewCell!
    @IBOutlet weak var msgTimeViewR: UIView!
    @IBOutlet weak var timeLabelR: UILabel!
    @IBOutlet weak var likePersonListViewHeightR: NSLayoutConstraint!
    
    //MARK: Sender View
    @IBOutlet weak var senderViewS: UIView!
    @IBOutlet weak var upperMainViewS: UIView!
    @IBOutlet weak var senderImageViewS: UIView!
    
    @IBOutlet weak var senderImgBtnS: UIButton!
    @IBOutlet weak var senderImgS: UIImageView!
    @IBOutlet weak var likeBtnViewS: UIView!
    @IBOutlet weak var noOfLikesBtnOutletS: UIButton!
    @IBOutlet weak var heartBtnOutletS: UIButton!
    @IBOutlet weak var userNameLabelS: UILabel!
    @IBOutlet weak var userMsgLabelS: UILabel!
    @IBOutlet weak var likePersonTableViewS: UITableViewCell!
    @IBOutlet weak var likePersonListViewS: UIView!
    @IBOutlet weak var likePersonListViewHeightS: NSLayoutConstraint!
    @IBOutlet weak var msgTimeViewS: UIView!
    @IBOutlet weak var timeLabelS: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
