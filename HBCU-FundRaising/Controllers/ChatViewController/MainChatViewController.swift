//
//  MainChatViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 24/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import Cloudinary
import MobileCoreServices
import StoreKit
import MFSideMenu

class MainChatViewController: UIViewController {
    
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var upperBackImageView: UIImageView!
    @IBOutlet weak var groupButtonOutlet: UIButton!
    @IBOutlet weak var directButtonOutlet: UIButton!
    @IBOutlet weak var groupView: UIView!
    @IBOutlet weak var directView: UIView!
    @IBOutlet weak var mainChatTableView: UITableView!
    
    var gradientLayer: CAGradientLayer!
    var chatModelArray = [ChatModel]()
    var groupChatArray = [ChatGroupModel]()
    var likedUnliked : [ChatModel]?
    var groupChatModelAray : ChatGroupModel?
    var singleChatArray : ChatModel?
    var BtnStatus : Int?
    var notificationPush = true
    var window: UIWindow?
    var sideContainer : MFSideMenuContainerViewController?
    var comingDataFromAppDelegate : ChatMessageModel?
    var valu = false
    var pushStr : String?
    var TempString = ""
    var label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
    var comeFromHome = "1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Chat"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isHidden = false
        self.navigationTitleColor()
        self.createGradientLayer()
        //self.groupViewColor()
        self.groupChatListAPI()
        //self.mainChatTableView.reloadData()
        BackForNoti = true
        
        if comeFromHome == "1"{
            if TempString == "1"{
            }else{
                // DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                // your code here
                self.pushBackgrondNotification1()
                //  }
            }
        }else{
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateTable1(_:)), name: NSNotification.Name("reloadTheTable"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(pushBackgrondNotification), name: NSNotification.Name(rawValue: "handleback"), object: nil)
    }
   
    @objc func updateTable1(_ notification: Notification?){
        self.groupChatListAPI()
        self.listUsreAPI()
    }
    override func viewWillAppear(_ animated: Bool){
        print(TempString)
        self.title = "CHAT"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isHidden = false
        self.navigationTitleColor()
        self.createGradientLayer()
        //self.groupViewColor()
        if BtnStatus == 2 {
            self.directViewColor()
        }else{
            self.groupViewColor()
        }
        self.label.text = ""
        self.listUsreAPI()
        self.groupChatListAPI()
    //    self.mainChatTableView.reloadData()
    }
    
     func pushBackgrondNotification1(){
      
        let leftSideMenu = self.storyboard!.instantiateViewController(withIdentifier: "ChatMessageViewController") as? ChatMessageViewController
        leftSideMenu?.TempString = "2"
        leftSideMenu?.chatNotificationArray = comingDataFromAppDelegate
        leftSideMenu?.notificationCheck = false
        if comingDataFromAppDelegate?.type == "Personal"{
             leftSideMenu?.comingBtnStatus = false
        }else{
             leftSideMenu?.comingBtnStatus = true
        }
        self.navigationController?.pushViewController(leftSideMenu!, animated: true)
    }
    
    @objc func leftBar(sender: UIButton){
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
        }
    }
    
    func navigationTitleColor(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func createGradientLayer(){
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor(hexString: "#538795").cgColor, UIColor(hexString: "#09203f").cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    func groupViewColor(){
        self.groupView.backgroundColor = UIColor.white
        self.directView.backgroundColor = UIColor.clear
    }
    func directViewColor(){
        self.groupView.backgroundColor = UIColor.clear
        self.directView.backgroundColor = UIColor.white
    }
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func groupBtnActn(_ sender: Any){
        self.label.text = ""
        BtnStatus = 1
        self.groupChatListAPI()
        self.groupViewColor()
    }
    @IBAction func directBtnActn(_ sender: Any){
        self.label.text = ""
        BtnStatus = 2
        self.listUsreAPI()
        self.directViewColor()
    }
}

extension MainChatViewController {
    
    // Direct tab API data
    func listUsreAPI(){
        
        WebServiceClass.showLoader(view: self.view)
        guard let userInfoModel = Methods.sharedInstance.getloginData() else{
            return
        }
        let user_id        = userInfoModel.id as? String
        let paramsStr = "user_id=\(user_id ?? "")"
        print(paramsStr)
        WebServiceClass.dataTask(urlName: Constants.APIs.chatBaseURL + Constants.APIs.listusers, method: "POST", params: paramsStr) { (success, response, errorMsg) in
        WebServiceClass.hideLoader(view: self.view)
        if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                print(jsonResult)
                // do whatever with jsonResult
                if let responeCode = jsonResult["error"] as? Bool {
                    if responeCode == false {
                            if let chatMessage = jsonResult["message"] as? String {
                                    if let ChatData = jsonResult["result"] as? [Dictionary<String,AnyObject>] {
                                        if let ChatInfo = Mapper<ChatModel>().mapArray(JSONObject: ChatData) {
                                            self.chatModelArray = ChatInfo
                                            print(self.chatModelArray)
                                            let sortedByFirstNameSwifty =  self.chatModelArray.sorted(by: { $0.date_created! > $1.date_created! })
                                            print(sortedByFirstNameSwifty)
                                            self.chatModelArray =  sortedByFirstNameSwifty
                                            print(self.chatModelArray)
                                        }
                                        if self.directView.backgroundColor == UIColor.white{
                                            if self.chatModelArray.count == 0 {
                                                self.label.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
                                                self.label.textAlignment = .center
                                                self.label.textColor = UIColor.lightGray
                                                self.label.text = "No Conversation Found.."
                                                self.view.addSubview(self.label)
                                            }
                                        }
                                     //   self.mainChatTableView.reloadData()
                                    }
                            }else{
                                print("something went wrong 1")
                            }
                        }
                    }else{
                        print("Something went wrong 2")
                    }
                }else{
                    print("Something went wrong 3")
                }
            DispatchQueue.main.async{
                self.mainChatTableView.reloadData()
            }
        }else{
                print("Something went wrong 4")
            }
        }
    }
    
    // Group Tab API Data
    func groupChatListAPI(){
        
        WebServiceClass.showLoader(view: self.view)
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let user_id = userInfoModel.id as? String
        let paramsStr = "user_id=\(user_id ?? "")"
        print(paramsStr)
        WebServiceClass.dataTask(urlName: Constants.APIs.chatBaseURL + Constants.APIs.listorgs, method: "POST", params: paramsStr) {(success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject>{
                    print(jsonResult)
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        if responeCode == false {
                            if let chatMessage = jsonResult["message"] as? String {
                                if let ChatData = jsonResult["result"] as? [Dictionary<String,AnyObject>]{
                                    if let ChatInfo = Mapper<ChatGroupModel>().mapArray(JSONObject: ChatData){
                                        self.groupChatArray = ChatInfo
                                        print(self.groupChatArray)
                                        let sortedByFirstNameSwifty =  self.groupChatArray.sorted(by: { $0.date_created! > $1.date_created! })
                                        print(sortedByFirstNameSwifty)
                                        self.groupChatArray =  sortedByFirstNameSwifty
                                        print(self.groupChatArray)
                                    }
                                    if self.groupView.backgroundColor == UIColor.white{
                                        if self.groupChatArray.count == 0{
                                            self.label.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
                                            self.label.textAlignment = .center
                                            self.label.textColor = UIColor.lightGray
                                            self.label.text = "No Conversation Found.."
                                            self.view.addSubview(self.label)
                                        }
                                    }
                                }
                            } else {
                                print("something went wrong 1")
                            }
                        }
                    } else {
                        print("Something went wrong 2")
                    }
                } else {
                    print("Something went wrong 3")
                }
            } else {
                print("Somthing went wrong 4")
            }
            DispatchQueue.main.async{
                self.mainChatTableView.reloadData()
            }
        }
    }
}

extension Array where Element == [String:String]{
    func sorted(by key: String) -> [[String:String]] {
        return sorted { $0[key] ?? "" < $1[key] ?? "" }
    }
    func filterDuplicates(includeElement: @escaping (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element] {
        var results = [Element]()
        forEach { (element) in
            let existingElements = results.filter {
                return includeElement(element, $0)
            }
            if existingElements.count == 0 {
                results.append(element)
            }
        }
        return results
    }
}

extension MainChatViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if BtnStatus == 2 {
             return chatModelArray.count
        }else{
             return groupChatArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell  = tableView.dequeueReusableCell(withIdentifier: "MainChatTableViewCell", for: indexPath) as! MainChatTableViewCell
        if BtnStatus == 2 {
            cell.userTitleLabel.text = chatModelArray[indexPath.row].name ?? "null"
            if chatModelArray[indexPath.row].media_type == "text"{
                cell.messgeLabel.text = chatModelArray[indexPath.row].post_title ?? ""
            } else if chatModelArray[indexPath.row].media_type == "Text"{
                cell.messgeLabel.text = chatModelArray[indexPath.row].post_title ?? ""
            }else if chatModelArray[indexPath.row].media_type == "video"{
                let fullString = NSMutableAttributedString(string: "")
                let image1Attachment = NSTextAttachment()
                image1Attachment.image = UIImage(named: "ic_video")
                let image1String = NSAttributedString(attachment: image1Attachment)
                fullString.append(image1String)
                fullString.append(NSAttributedString(string: " Video"))
                cell.messgeLabel.attributedText = fullString
            }else{
                let fullString = NSMutableAttributedString(string: "")
                let image1Attachment = NSTextAttachment()
                image1Attachment.image = UIImage(named: "ic_image")
                let image1String = NSAttributedString(attachment: image1Attachment)
                fullString.append(image1String)
                fullString.append(NSAttributedString(string: " Image"))
                cell.messgeLabel.attributedText = fullString
            }
            cell.countOfnewMsgLabel.text = chatModelArray[indexPath.row].counting ?? ""
            cell.countOfnewMsgLabel.textColor = UIColor.init(red: 49/255, green: 163/255, blue: 54/255, alpha: 1)
            if cell.countOfnewMsgLabel.text == "0"{
                cell.countOfnewMsgLabel.text = ""
            }else{
                
            }
            let imagebaseURl = Constants.APIs.imageBaseURL + (chatModelArray[indexPath.row].image ?? "")
            let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            cell.userImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
            if let dateValue = chatModelArray[indexPath.row].date_created {
                cell.chatTimeLabel.text = self.convertTimeFormater(dateValue)
            }else{
                cell.chatTimeLabel.text = ""
            }
        } else {
            cell.userTitleLabel.text = groupChatArray[indexPath.row].name ?? "null"
            if groupChatArray[indexPath.row].media_type == "text"{
                cell.messgeLabel.text = groupChatArray[indexPath.row].post_title ?? ""
            }else if groupChatArray[indexPath.row].media_type == "video"{
                let fullString = NSMutableAttributedString(string: "")
                let image1Attachment = NSTextAttachment()
                image1Attachment.image = UIImage(named: "ic_video")
                let image1String = NSAttributedString(attachment: image1Attachment)
                fullString.append(image1String)
                fullString.append(NSAttributedString(string: " Video"))
                cell.messgeLabel.attributedText = fullString
            }else if groupChatArray[indexPath.row].media_type == "image"{
                let fullString = NSMutableAttributedString(string: "")
                let image1Attachment = NSTextAttachment()
                image1Attachment.image = UIImage(named: "ic_image")
                let image1String = NSAttributedString(attachment: image1Attachment)
                fullString.append(image1String)
                fullString.append(NSAttributedString(string: " Image"))
                cell.messgeLabel.attributedText = fullString
            }else{
            }
                cell.countOfnewMsgLabel.text = groupChatArray[indexPath.row].counting ?? ""
                cell.countOfnewMsgLabel.textColor = UIColor.init(red: 49/255, green: 163/255, blue: 54/255, alpha: 1)
            if cell.countOfnewMsgLabel.text == "0"{
                cell.countOfnewMsgLabel.text = ""
            }else{
            }
            let imagebaseURl = Constants.APIs.imageBaseURL + (groupChatArray[indexPath.row].image ?? "")
            let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            cell.userImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
            if let dateValue = groupChatArray[indexPath.row].date_created  {
                if dateValue == ""{
                    cell.chatTimeLabel.text = ""
                }else{
                    cell.chatTimeLabel.text = self.convertTimeFormater(dateValue)
                }
            }else{
                cell.chatTimeLabel.text = ""
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatMessageViewController") as! ChatMessageViewController
        if BtnStatus == 2 {
            vc.singleChatArray1 = self.chatModelArray[indexPath.row]
            vc.tag = indexPath.row
            vc.comingBtnStatus = false
        }else{
            vc.groupChatArray1 = self.groupChatArray[indexPath.row]
            vc.tag = indexPath.row
            vc.comingBtnStatus = true
        }
        vc.TempString = "1"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }
}

