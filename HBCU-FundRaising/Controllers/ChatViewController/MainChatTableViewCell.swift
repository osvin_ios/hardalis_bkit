//
//  MainChatTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 24/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class MainChatTableViewCell: UITableViewCell {
    
    @IBOutlet var MainView: UIView!
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var userTitleLabel: UILabel!
    @IBOutlet var chatTimeLabel: UILabel!
    @IBOutlet var messgeLabel: UILabel!
    @IBOutlet var countOfnewMsgLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
