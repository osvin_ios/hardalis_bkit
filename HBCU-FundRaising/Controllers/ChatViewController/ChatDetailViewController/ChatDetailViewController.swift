//
//  ChatDetailViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 10/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

class ChatDetailViewController: UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var uniOrgnImageView: UIImageView!
    
    // Upper View
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var uniOrgnTitleLbl: UILabel!
    
    @IBOutlet weak var donateNowButtonOutlet: UIButton!
    // Donation View
    @IBOutlet weak var donationView: UIView!
    @IBOutlet weak var totalDonationLbl: UILabel!
    @IBOutlet weak var totalDonationAmtLbl: UILabel!
    
    // Notification View
    @IBOutlet weak var notificationView: UIView!
    
    // Notification View 1
    @IBOutlet weak var notificationView1: ViewDesign!
    @IBOutlet weak var commentImgView: UIImageView!
    @IBOutlet weak var noOfPostLbl: UILabel!
    @IBOutlet weak var noOfPostBtnOutlet: UIButton!
    
    // Notification View 2
    @IBOutlet weak var notificationView2: ViewDesign!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var noOfuserLbl: UILabel!
    
    // Mute Notification
    @IBOutlet weak var muteNotificationView: ViewDesign!
    @IBOutlet weak var muteNotificationOutlet: UIButton!
    @IBOutlet weak var muteNotiBtnImageOutlet: UIButton!
    
    var muteNotificationData : MuteNotificationModel?
    var hbcuDetailData = [ChatPersonDetailModel]()
    var hbcuDetailShowData : ChatPersonDetailModel?
    var hbcuFavArray = [HBCUListModel]()
    var groupChatArray : ChatGroupModel?
    var singleChatArray : ChatModel?
    var muteStatus = String()
    var chatMsgId : ChatMessageModel?
    var comingVCStatus = Bool()
    var groupId = String()
    var Status = String()
    var group_id = String()
    var selectedHBCUId : String?
    var selectedHBCU : String?
    var Donate_Now_View1 : DonateNowView?
    var hbcu : String?
    var  hbcuType = String()
    var notificationCheck : ChatMessageModel?
    var notificationBackUserID = true
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.hbcdDetailAPI()
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
   
    // total member button action
    @IBAction func totalMemberActnBtn(_ sender: Any){
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChatMemberViewController" ) as! ChatMemberViewController
        vc.groupChatArray = groupChatArray
        vc.hbcuDetailShowData = hbcuDetailShowData
        self.navigationController?.show(vc, sender: self)
      //  self.performSegue(withIdentifier: "chatMember", sender: self)
    }
    
    // Top View
    @IBAction func backBtnActn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.navigationBar.isHidden = false
//        self.navigationController?.navigationBar.isHidden = true
    }
    
    // Mute Notification
    @IBAction func muteNotiBtnActn(_ sender: Any) {
    }
    
    @IBAction func muteNotiBtnImg(_ sender: UIButton){
//        if hbcuDetailShowData?.notification_status == "0"{
//            self.muteStatus = "Do you really want to Mute the notification for this group?"
//        }else{
//            //self.muteStatus = "Do you really want to Unmute the notification for this group?"
//            self.muteStatus = "Are you sure you want to Unmute the notification for this group?"
//        }
        
        var msg = ""
        if self.muteNotificationOutlet.currentTitle == "Mute Notification"{
            msg = "Do you really want to Mute the notification for this group?"
        }else{
            msg = "Are you sure you want to Unmute the notification for this group?"
        }
        
        self.alertFunctionsWithCallBacks(title: msg) { (isTrue) in
            if isTrue.boolValue {
                // Check network connection.
                if Reachability.isConnectedToNetwork() == true {
                    self.muteNotification()
                }else{
                    self.showAlertWithoutCallBack(messageTitle: AKErrorHandler.CommonErrorMessages.NO_INTERNET_AVAILABLE)
                }
            }
        }
    }

    // no of post button action
    @IBAction func noOfPostBtnActn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    // Donate Now
    
    @IBAction func donateNowBtnActn(_ sender: Any) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DontionVC") as? DonationViewAddController {
            vc.hbcuDetailData = self.hbcuDetailData
            if comingVCStatus == true{
                vc.groupId = self.groupChatArray?.to_id ?? ""
            }else{
                vc.groupId = self.singleChatArray?.to_id ?? ""
            }
            vc.comingVC = true
            self.present(vc, animated: false, completion: nil)
        }
    }
}

extension ChatDetailViewController {
    // Mute Notification
    func muteNotification() {
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
  
        if comingVCStatus == true{
            self.group_id = self.groupChatArray?.to_id ?? "0"
             self.hbcuType = self.groupChatArray?.type ?? ""
        }else{
            self.group_id = self.singleChatArray?.to_id ?? "0"
             self.hbcuType = self.singleChatArray?.type ?? ""
        }
        
        var paramStr = ""
        if let userId = userInfoModel.id as? Int{
            paramStr = "user_id=\(userId)&group_id=\(group_id)&type=\(hbcuType)"
        }else if let userId = userInfoModel.id as? String{
            paramStr = "user_id=\(userId)&group_id=\(group_id)&type=\(hbcuType)"
        }else{}
        
        print(paramStr)
       
//            WebServiceClass.showLoader(view: self.view)
        WebServiceClass.dataTask(urlName: Constants.APIs.chatBaseURL + Constants.APIs.mutenotification, method: "POST", params: paramStr) { (success, response, errorMsg) in
//            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        if responeCode == false {
                            if let muteMessage = jsonResult["message"] as? String {
                                if let muteData = jsonResult["result"] as? Dictionary<String,AnyObject> {
                                    if let muteDataInfo = Mapper<MuteNotificationModel>().map(JSONObject: muteData) {
                                        self.muteNotificationData = muteDataInfo
                                        print(self.muteNotificationData)
                                      
                                        if self.muteNotificationData?.notification_status == "1" {
                                            self.muteNotificationOutlet.setTitle("Unmute Notification", for: .normal)
                                            let img = UIImage(named: "ic_unmute_chat")
                                            self.muteNotiBtnImageOutlet.setImage(img , for: .normal)
                                        } else {
                                            self.muteNotificationOutlet.setTitle("Mute Notification", for: .normal)
                                            let img = UIImage(named: "ic_mute_chat")
                                            self.muteNotiBtnImageOutlet.setImage(img , for: .normal)
                                        }
                                    }
                                }
                            } else {
                                print("something went wrong 1")
                            }
                        }
                    } else {
                        print("Something went wrong 2")
                    }
                } else {
                    print("Something went wrong 3")
                }
            } else {
                print("Somthing went wrong 4")
            }
        }
    }
    
    // Get Detail of HBCU's
    func hbcdDetailAPI() {
        WebServiceClass.showLoader(view: self.view)
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        if notificationBackUserID == true{
            if comingVCStatus == true{
                self.groupId = self.groupChatArray?.to_id ?? ""
                self.Status = self.groupChatArray?.type ?? ""
            }else{
                self.groupId = self.singleChatArray?.to_id ?? ""
                self.Status = self.singleChatArray?.type ?? ""
            }
        }else{
            self.groupId = self.notificationCheck?.to_id ?? ""
            self.Status = self.notificationCheck?.type ?? ""
        }
        
        var urlName = ""
        if let userId = userInfoModel.id as? Int{
            urlName = "http://admin.iheartmyhbcu.org/api/user/getPostHbcuDetails?hbcu=\(groupId)&user_id=\(userId)&type=\(Status)"  //Live
            //urlName = "http://phphosting.osvin.net/HBCU/api/user/getPostHbcuDetails?hbcu=\(groupId)&user_id=\(userId)&type=\(Status)"
        }else if let userId = userInfoModel.id as? String{
            urlName = "http://admin.iheartmyhbcu.org/api/user/getPostHbcuDetails?hbcu=\(groupId)&user_id=\(userId)&type=\(Status)"  //Live
            //urlName = "http://phphosting.osvin.net/HBCU/api/user/getPostHbcuDetails?hbcu=\(groupId)&user_id=\(userId)&type=\(Status)"
        }else{}

        WebServiceClass.dataTask(urlName: urlName,  method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            
            if success == true{
                if let jsonResult = response as? Dictionary<String, AnyObject>{
                    print(jsonResult)
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool{
                        if responeCode == false {
                            if let detailMessage = jsonResult["message"] as? String{
                                if let detailData = jsonResult["result"] as? Dictionary<String,AnyObject>{
                                    if let detailDataInfo = Mapper<ChatPersonDetailModel>().map(JSONObject: detailData){
                                        self.hbcuDetailData = [detailDataInfo]
                                        self.hbcuDetailShowData = detailDataInfo
                                        print(self.hbcuDetailData)
                                        
                                        if self.hbcuDetailShowData?.total_donation == "0"{
                                             self.totalDonationAmtLbl.text = ("$ 0.00" )
                                        }else{
                                             self.totalDonationAmtLbl.text = ("$ " + (self.hbcuDetailShowData?.total_donation ?? "0.00"))
                                        }
                                        self.uniOrgnTitleLbl.text = self.hbcuDetailShowData?.title  ?? ""
                                        self.noOfPostLbl.text = self.hbcuDetailShowData?.total_post_count ?? "0"
                                        self.noOfuserLbl.text = self.hbcuDetailShowData?.total_favourite ?? "0"
                                        
                                        if self.Status == "organisation"{
                                            let imagebaseURl = Constants.APIs.imageBaseURL + (self.hbcuDetailShowData?.logo ?? "")
                                            let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                            self.uniOrgnImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                                            self.donationView.isHidden = true
                                            self.donateNowButtonOutlet.isHidden = true
                                        }else{
                                            let imagebaseURl = Constants.APIs.imageBaseURL + (self.hbcuDetailShowData?.image ?? "")
                                            let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                            self.uniOrgnImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                                            self.donationView.isHidden = false
                                            self.donateNowButtonOutlet.isHidden = false
                                        }
                                      
                                        if self.hbcuDetailShowData?.notification_status == "1"{
                                            self.muteNotificationOutlet.setTitle("Unmute Notification", for: .normal)
                                            let img = UIImage(named: "ic_unmute_chat")
                                            self.muteNotiBtnImageOutlet.setImage(img , for: .normal)
                                        }else{
                                            self.muteNotificationOutlet.setTitle("Mute Notification", for: .normal)
                                            let img = UIImage(named: "ic_mute_chat")
                                            self.muteNotiBtnImageOutlet.setImage(img , for: .normal)
                                        }
                                    }
                                }
                            }else{
                                print("something went wrong 1")
                            }
                        }
                    }else{
                        print("Something went wrong 2")
                    }
                }else{
                    print("Something went wrong 3")
                }
            }else{
                print("Somthing went wrong 4")
            }
        }
    }
}
