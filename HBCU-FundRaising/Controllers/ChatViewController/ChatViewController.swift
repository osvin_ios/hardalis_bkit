//
//  ChatViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 08/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {

    @IBOutlet var chatTableView: UITableView!
    
    var gradientLayer: CAGradientLayer!
    
    var userNameArray = ["Grambling State University", "Joaquin Henandez", "Grambling State University", "Joaquin Henandez", "Grambling State University", "Joaquin Henandez", "Grambling State University", "Joaquin Henandez" ]
    
    var messageArray = ["Hey, let's meet me for lunch, Catch you there!", "Hey, let's meet me for lunch, Catch you there!", "Hey, let's meet me for lunch, Catch you there!", "Hey, let's meet me for lunch, Catch you there!, see you there", "Hey, let's meet me for lunch, Catch you there!", "Hey, let's meet me for lunch, Catch you there!", "Hey, let's meet me for lunch, Catch you there!", "Hey, let's meet me for lunch, Catch you there!"]
    
    var timeArray  = ["1:30 PM", "1:30 PM", "1:30 PM", "1:30 PM", "1:30 PM", "1:30 PM", "1:30 PM", "1:30 PM"]
    
    var countArray = ["1", "2" ,"3", "4", "5", "6", "7", "8"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
   
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.title = "CHAT"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
        self.navigationItem.hidesBackButton = true
        
        self.navigationTitleColor()
        self.createGradientLayer()
    }
    
    @objc func leftBar(sender: UIButton) {
        
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            
        }
    }
    
    func navigationTitleColor() {
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func createGradientLayer() {
        
        gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.view.bounds
        
        gradientLayer.colors = [UIColor(hexString: "#538795").cgColor, UIColor(hexString: "#09203f").cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
}

extension ChatViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return userNameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
        cell.userTitleLabel.text = self.userNameArray[indexPath.row]
        cell.messgeLabel.text = self.messageArray[indexPath.row]
        cell.countOfnewMsgLabel.text = self.countArray[indexPath.row]
        cell.chatTimeLabel.text = self.timeArray[indexPath.row]
        
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "chatDetail", sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView()
        view.backgroundColor = UIColor.white
        return  view
    }
}
