       //
//  ChatMessageViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 27/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import Cloudinary
import MobileCoreServices
import MediaPlayer
import AVFoundation
import AVKit
import AssetsLibrary
import CoreData
//import IQKeyboardManagerSwift
import CropViewController
import MFSideMenu
import SDWebImage
       
class ChatMessageViewController: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var chatMessageTableView: UITableView!
    @IBOutlet weak var tableviewBottomLayout: NSLayoutConstraint!
    @IBOutlet weak var buttomLayoutConstraint: NSLayoutConstraint!
    // Audio Sub View
    @IBOutlet var audioViewSubView: UIView!
    @IBOutlet weak var bottomAudioView: UIView!
    @IBOutlet weak var audioStartTimeLabel: UILabel!
    @IBOutlet weak var audioTotalTimeLabel: UILabel!
    @IBOutlet weak var audioPlayingView: UIView!
    @IBOutlet weak var audioButtonView: UIView!

    @IBOutlet weak var audioPreviousBtnOutlet: UIButton!
    @IBOutlet weak var audiobackBtnOutlet: UIButton!
    @IBOutlet weak var playAudioBtnOutlet: UIButton!
    @IBOutlet weak var audioNextBtnOutlet: UIButton!
    @IBOutlet weak var audioforwardBtnOutlet: UIButton!
    
    // Upper View
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var chatUserImageView: ImageViewDesign!
    @IBOutlet var chatPersonNameLabel: UILabel!
    @IBOutlet weak var organizationImgBtnOutlet: UIButton!
    
    // Sub View
    @IBOutlet var recordButton: UIButton!
    @IBOutlet var stopButton: UIButton!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var addSubView: UIView!
    
    // Bottom View
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var typeMessageTextField: UITextField!
    @IBOutlet weak var mediaCaptureBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var previewImageIcon: UIView!
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var previewImgCrossBtn: UIButton!
    @IBOutlet weak var moveToBottomOutlet: UIButton!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    
    var TempString : String!
    var readMsgDict : NSDictionary!
    var sideContainer : MFSideMenuContainerViewController?
    var params = [String: String]()
    var gradientLayer: CAGradientLayer!
    var imageFullSize = UIImage()
    var imageThumbnailSize = UIImage()
    var uploadImageThumnailSize = UIImage()
    var imagePreview = UIImage()
    var videoThumbnailSize = String()
    var audioThumbnailSize = String()
    var urlGetCloudinary : URL?
    var urlAudioGetCloudinary : URL?
    var urlImageGetCloudinary : URL?
    var urlTextGetCloudinary : URL?
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    // Audio URL
    var recorder: AVAudioRecorder!
    var player: AVAudioPlayer!
    var audioPlayer : AVAudioPlayer!
    var meterTimer: Timer!
    var soundFileURL: URL!
    var paramsStr = String()
    var userInfoChat = [AUserInfoModel]()
    var mediaTypeVariable : AUserInfoModel?
    var chatGetVariable : ChatMessageModel?
    var likedPersonDetailArray = [ChatMessageModel]()
    var likeUnlikePersonDetailArray = [ChatMessageModel]()
    var totalPersonLikeCount : ChatMessageModel?
    var mediaType = String()
    var userInfoModel = String()
    var likeCount = Int()
    var aString : String = ""
    var likes = [String]()
    var sum = Int()
    var a = Int()
    var b = Int()
    var temp = String()
    var message  = String()
    var textURL : URL!
    var row = Int()
    var dict = String()
    var refreshControlTop: UIRefreshControl!
    var post_Count: Int = 1
    var groupChatArray1 : ChatGroupModel?
    var singleChatArray1 : ChatModel?
    var tag = Int()
    var badgeCount: Int = 1
    var chatGetArray = [ChatMessageModel]()
    var blockStatusDict = [ChatMessageModel]()
    var blockUnblockStatus : [Dictionary<String,AnyObject>] = []
    var comingBtnStatus = Bool()
    var onButtonTapped : (() -> Void)? = nil
    var blockStatus = String()
    var likeCount1 = String()
    var blockData1 : ChatMessageModel?
    var blockStatus1 = ""
    var chatMemberToChat = true
    var otherUserDetail : otherUserProfileModel?
    var userMedal = [otherUserProfileModel]()
    var groupChatArrayDetailPage : ChatGroupModel?
    var singleChatArrayDetailPage : ChatModel?
    var hbcuDetailShowData : ChatPersonDetailModel?
    var chatMemberDetail = true
    var chatNotificationArray : ChatMessageModel?
    var notificationCheck = true
    var window: UIWindow?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateTable(_:)), name: NSNotification.Name("reloadTheTable"), object: nil)
     // [self.typeMessageTextField.keyboardToolbar.doneBarButton setTarget:self action:@selector(doneAction:)];
        self.navigationController?.navigationBar.isHidden = true
        self.upperViewData()
        self.loadXibs()
        //self.chatMessagesAPI()
        self.resetViewProperties()
        //stopButton.isEnabled = false
        //playButton.isEnabled = false
        askForNotifications()
        self.typeMessageTextField.delegate = self
        self.userInfoModel = getUserInfoData()?.id as! String
        self.refreshControlAPI()
       // moveToBottomOutlet.isHidden = true
        self.typeMessageTextField.delegate = self
        BackForNoti = false
        self.chatMessageTableView.alwaysBounceVertical = false
        self.moveToBottomOutlet.isHidden = true
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
//    @objc func keyboardWillShow(sender: NSNotification){
//        self.view.frame.origin.y = -100 // Move view 150 points upward
//    }
//    @objc func keyboardWillHide(sender: NSNotification){
//        self.view.frame.origin.y = 0 // Move view to original position
//    }
    @objc func updateTable(_ notification: Notification?){
        self.chatMessagesAPI()
        self.readMsgAPI()
    }
    // MARK: - refresh control For API
    internal func refreshControlAPI(){
        refreshControlTop = UIRefreshControl()
        refreshControlTop.addTarget(self, action: #selector(self.reloadAPICount), for: .valueChanged)
        self.chatMessageTableView.addSubview(refreshControlTop) // not required when using UITableViewController
        // First time automatically refreshing.
        //refreshControlTop.beginRefreshingManually()
        self.perform(#selector(self.reloadAPI), with: nil, afterDelay: 0)
    }
    // MARK: - Reload API
    @objc func reloadAPI(){
        // Check network connection.
        self.post_Count = 1
        if Reachability.isConnectedToNetwork() == true{
            self.refreshControlTop.endRefreshing()
            self.chatMessagesAPI()
        }else{
            self.showAlertWithoutCallBack(messageTitle: AKErrorHandler.CommonErrorMessages.NO_INTERNET_AVAILABLE)
        }
    }
    //MARK:- Reload API
    @objc func reloadAPICount(){
//  if postsCountOFArray != 0 {
        // Check network connection.
        self.post_Count = post_Count + 1
        if Reachability.isConnectedToNetwork() == true{
            self.chatMessagesAPI()
        }else{
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    func incrementBadgeNumberBy(badgeNumberDecrement: Int){
        let currentBadgeNumber = UIApplication.shared.applicationIconBadgeNumber
        let updatedBadgeNumber = currentBadgeNumber - badgeNumberDecrement
        UIApplication.shared.applicationIconBadgeNumber = updatedBadgeNumber
    }
    override func viewWillAppear(_ animated: Bool){
        self.refreshControlAPI()
        self.navigationController?.navigationBar.isHidden = true
        self.moveToBottomOutlet.isHidden = true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.upperView.isHidden = false
        if textField == typeMessageTextField {
            if previewImage.image == nil {
                self.mediaType = "text"
            }else{
                // self.mediaType = "image"
            }
            self.buttonSelected()
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.message = self.typeMessageTextField.text!
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        self.view.endEditing(true)
        return false
    }
    func upperViewData(){
        if notificationCheck == true{
            if chatMemberDetail == true{
                if comingBtnStatus == true{
                    self.chatPersonNameLabel.text = groupChatArray1?.name
                    let imagebaseURl = Constants.APIs.imageBaseURL + (groupChatArray1?.image ?? "")
                    let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    self.chatUserImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                }else{
                    self.chatPersonNameLabel.text = singleChatArray1?.name
                    let imagebaseURl = Constants.APIs.imageBaseURL + (singleChatArray1?.image ?? "")
                    let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    self.chatUserImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                }
            }else{
                self.chatPersonNameLabel.text = (otherUserDetail?.first_name ?? "")! + (otherUserDetail?.last_name ?? "")!
                let imagebaseURl = Constants.APIs.imageBaseURL + (otherUserDetail?.profile ?? "")
                let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.chatUserImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
            }
            
        }else{
            if chatNotificationArray?.type == "Personal"{
                self.chatPersonNameLabel.text = chatNotificationArray?.name
                let imagebaseURl = Constants.APIs.imageBaseURL + (chatNotificationArray?.image ?? "")
                let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.chatUserImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
            }else{
                self.chatPersonNameLabel.text = chatNotificationArray?.groupName
                let imagebaseURl = Constants.APIs.imageBaseURL + (chatNotificationArray?.groupImage ?? "")
                let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.chatUserImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
            }
        }
    }
    
    func navigationTitleColor(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    func createGradientLayer(){
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor(hexString: "#538795").cgColor, UIColor(hexString: "#09203f").cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    func getUserInfoData() -> MyAccountDetailsModel?{
        let decoded                         = UserDefaults.standard.object(forKey: "userinfo") as! Data
        let userDataStr: String             = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! String
        guard let userInfoModel = Mapper<MyAccountDetailsModel>().map(JSONString: userDataStr) else {
            return nil
        }
        return userInfoModel
    }
    func loadXibs(){
        // Created by Divyani
        // ChatAudioTableViewCell is using for Text Purpose not for Audio Purpose
        chatMessageTableView.register(UINib(nibName: "ChatAudioTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatAudioTableViewCell")
        chatMessageTableView.register(UINib(nibName: "VideoTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoTableViewCell")
        chatMessageTableView.register(UINib(nibName: "ChatVoiceTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatVoiceTableViewCell")
        chatMessageTableView.register(UINib(nibName: "LikedPersonCollectionViewCell", bundle: nil), forCellReuseIdentifier: "LikedPersonCollectionViewCell")
        chatMessageTableView.register(UINib(nibName: "likedPersonTableViewCell", bundle: nil), forCellReuseIdentifier: "likedPersonTableViewCell")
          self.chatMessageTableView.delegate = self
          self.chatMessageTableView.dataSource = self
    }
    // Do any additional setup after loading the view.
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        recorder = nil
        player = nil
        // Dispose of any resources that can be recreated.
    }
 
    @IBAction func subviewCrossBtnActn(_ sender: UIButton) {
        self.addSubView.removeFromSuperview()
    }
    @IBAction func backButtonActionOfAudioSubview(_ sender: UIButton) {
        self.audioViewSubView.removeFromSuperview()
    }
    @IBAction func audioNextforwardbtnActn(_ sender: UIButton) {
    }
    @IBAction func audioPlayPreviousBtnActn(_ sender: UIButton){
    }
    @IBAction func playPauseAudioSubViewBtnActn(_ sender: UIButton){
        var url: String?
        let indexPath = IndexPath(row: (sender).tag , section: 0)
            _ = self.chatMessageTableView.cellForRow(at: indexPath) as? ChatVoiceTableViewCell
            let fileUrl = NSURL(string: self.chatGetArray[indexPath.row].post_media!)
            print(fileUrl ?? "")
                url = "http://res.cloudinary.com/djbpcv7ut/video/upload/v1538637882/jz1utrjtemnvqbfl8ul7.m4a"
            let urlS = NSURL(string: url!)
            print("Playing \(urlS ?? nil)")
            do {
                self.audioPlayer = try AVAudioPlayer(contentsOf: (urlS as URL?)!)
                audioPlayer.delegate = self
                audioPlayer.prepareToPlay()
                audioPlayer.volume = 1.0
                audioPlayer.play()

            }catch{
                self.audioPlayer = nil
                print(error.localizedDescription)
            }
    }
    @IBAction func audioBackBtnActn(_ sender: UIButton){
    }
    @IBAction func audionextBtnActn(_ sender: UIButton){
    }
    @IBAction func fileShareBtnActn(_ sender: Any){
        self.uploadProfilePicClick()
    }
    @IBAction func messageSendBtnActn(_ sender: UIButton){
       
        print("test")
        if self.mediaType == "image" {
         //   self.typeMessageTextField.text = ""
            self.previewImage.image = nil
            self.previewImageIcon.removeFromSuperview()
            self.uploadProfilePicName()
        }else if self.mediaType == "video"{
            self.previewImageIcon.removeFromSuperview()
            self.previewImage.image = nil
          //  self.typeMessageTextField.text = ""
            self.postViewController()
        }else if self.mediaType == "text"{
            if self.typeMessageTextField.text == "" {
                self.showAlertWithoutCallBack(messageTitle: "Please Enter Message")
            }else{
                self.sendtextMsgDetailsOnServer()
                self.typeMessageTextField.text = ""
            //  self.dismissKeyboard()
            }
        }else{
            showAlertWithoutCallBack(messageTitle: "Please enter message")
        }
    }
    
    // Subview
    @IBAction func recordBtnACtn(_ sender: Any){
        if player != nil && player.isPlaying {
            print("stopping")
            player.stop()
        }
        if recorder == nil {
            print("recording. recorder nil")
            recordButton.setTitle("Pause", for: .normal)
            playButton.isEnabled = false
            stopButton.isEnabled = true
            recordWithPermission(true)
            return
        }
        if recorder != nil && recorder.isRecording {
            print("pausing")
            recorder.pause()
            recordButton.setTitle("Continue", for: .normal)
        } else {
            print("recording")
            recordButton.setTitle("Pause", for: .normal)
            playButton.isEnabled = false
            stopButton.isEnabled = true
            //            recorder.record()
            recordWithPermission(false)
        }
    }
    
    @IBAction func stopBtnActn(_ sender: Any){
        recorder?.stop()
        player?.stop()
        meterTimer.invalidate()
        recordButton.setTitle("Record", for: .normal)
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setActive(false)
            playButton.isEnabled = true
            stopButton.isEnabled = false
            recordButton.isEnabled = true
        } catch {
            print("could not make session inactive")
            print(error.localizedDescription)
        }
//    self.audioPostViewController()
    }
    
    @IBAction func playBtnACtn(_ sender: Any){
        play()
    }
    @IBAction func showRecording(_ sender: Any){
    }
    @IBAction func removeRecording(_ sender: Any){
         deleteAllRecordings()
    }

    // upper view
    @IBAction func backBtnActn(_ sender: Any)
    {
        if TempString == "1" {
             self.navigationController?.popViewController(animated: true)
        }else{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MainChatViewController.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    @IBAction func OrginzaionPersonImgBtn(_ sender: UIButton){
        organizationImgBtnOutlet.addTarget(self, action: #selector(self.OrganicationDetail(_:)), for: .touchUpInside)
    }
    @IBAction func trimBtnActn(_ sender: Any){
        print("\(#function)")
        if self.soundFileURL == nil {
            print("no sound file")
            return
        }
        print("trimming \(soundFileURL!.absoluteString)")
        print("trimming path \(soundFileURL!.lastPathComponent)")
        let asset = AVAsset(url: self.soundFileURL!)
        exportAsset(asset, fileName: "trimmed.m4a")
    }
    
    @IBAction func moveToBottomButton(_ sender: UIButton){
        self.tableViewScrollToBottom()
    }
}

extension ChatMessageViewController : UITableViewDelegate, UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return chatGetArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let hearderView: UIView = UIView()
        hearderView.backgroundColor = UIColor.clear
        hearderView.frame = CGRect(x: 0, y: 0, width: Constants.ScreenSize.SCREEN_WIDTH, height: 20)
        
        let dict = chatGetArray[section]
        let dateString  =  dict.date_created ?? ""
        var finalDate = dateString.dateformatter()
        var finalDate1 = dateString.dateformatter()
        if finalDate != "Today" && finalDate != "Yesterday" {
            finalDate = dateString.dateFromStringForChat()
        }
        if section == 0{
              return 20
        }else{
            let headerName = UILabel()
            // Set Date on label
            let firstDate = dict.date_created ?? ""
            finalDate1 = firstDate.dateFromStringForChat()
            print(finalDate1)
            let dict1 = chatGetArray[section - 1]
            let secondDate = dict1.date_created ?? ""
            finalDate = secondDate.dateFromStringForChat()
           // print(finalDate)
            if finalDate1 == finalDate{
               return 0
            }else{
                return 20
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let hearderView: UIView = UIView()
        hearderView.backgroundColor = UIColor.clear
        hearderView.frame = CGRect(x: 0, y: 0, width: Constants.ScreenSize.SCREEN_WIDTH, height: 20)
      
        let dict = chatGetArray[section]
        let dateString  =  dict.date_created ?? ""
        var finalDate = dateString.dateformatter()
        var finalDate1 = dateString.dateformatter()
        if finalDate != "Today" && finalDate != "Yesterday"{
            finalDate = dateString.dateFromStringForChat()
        }
        
        if section == 0 {
            // Set Date on label
            let firstDate = dict.date_created ?? ""
            finalDate1 = firstDate.dateFromStringForChat()
           // print(finalDate1)
            var sizeOfString = CGSize()
            if let font = UIFont(name: "Gotham-Book", size: 14.0) {
                let fontAttributes = [NSAttributedStringKey.font: font] // it says name, but a UIFont works
                sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
            }
            // Header View Label.
             let headerName = UILabel()
            headerName.frame = CGRect(x: 0, y: 0, width: sizeOfString.width + 20, height: 20)
            headerName.center = hearderView.center
            headerName.backgroundColor = UIColor.lightGray
            headerName.textAlignment = .center
            headerName.lineBreakMode = NSLineBreakMode.byWordWrapping
            headerName.layer.cornerRadius = 7
            headerName.clipsToBounds = true
            headerName.font =  UIFont(name: "Gotham-Book", size: 12.0)
            headerName.textColor = UIColor.white
            headerName.adjustsFontSizeToFitWidth = true
            headerName.numberOfLines = 0
            headerName.text = finalDate1
            hearderView.addSubview(headerName)
            return hearderView
         //   print(firstDate)
        } else {
            let headerName = UILabel()
            // Set Date on label
            let firstDate = dict.date_created ?? ""
            finalDate1 = firstDate.dateFromStringForChat()
           // print(finalDate1)
            let dict1 = chatGetArray[section - 1]
            let secondDate = dict1.date_created ?? ""
            finalDate = secondDate.dateFromStringForChat()
           // print(finalDate)
            
            if finalDate1 == finalDate{
             //   print("abc")
            }else{
                //Get the width of String
                var sizeOfString = CGSize()
                if let font = UIFont(name: "Gotham-Book", size: 14.0) {
                    let fontAttributes = [NSAttributedStringKey.font: font] // it says name, but a UIFont works
                    sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
                }
                // Header View Label.
                
                headerName.frame = CGRect(x: 0, y: 0, width: sizeOfString.width + 20, height: 20)
                headerName.center = hearderView.center
                headerName.backgroundColor = UIColor.lightGray
                headerName.textAlignment = .center
                headerName.lineBreakMode = NSLineBreakMode.byWordWrapping
                headerName.layer.cornerRadius = 7
                headerName.clipsToBounds = true
                headerName.font =  UIFont(name: "Gotham-Book", size: 12.0)
                headerName.textColor = UIColor.white
                headerName.adjustsFontSizeToFitWidth = true
                headerName.numberOfLines = 0
                headerName.text = finalDate1
               
            }
            hearderView.addSubview(headerName)
            return hearderView
        }
        return hearderView
    }
    func sliderChange(sender: UISlider){
        let currentValue = sender.value    // get slider's value
        let row = sender.tag               // get slider's row in table
        print("Slider in row \(row) has a value of \(currentValue)")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Text Message
        let a : String = chatGetArray[indexPath.section].media_type ?? ""
        let b : String = "text"
        
        if(a.caseInsensitiveCompare(b) == .orderedSame) {
            let dict = chatGetArray[indexPath.section]
            let cell  = tableView.dequeueReusableCell(withIdentifier: "ChatAudioTableViewCell", for: indexPath) as! ChatAudioTableViewCell
          
            if Int(userInfoModel)! == Int(chatGetArray[indexPath.section].user_id ?? "0")! {
                // Sender View
                cell.senderViewS.isHidden = false
                cell.receiverViewR.isHidden = true
                cell.userNameLabelS.text = chatGetArray[indexPath.section].name ?? "null"
                cell.userMsgLabelS.text = chatGetArray[indexPath.section].post_title ?? ""
                if let image = chatGetArray[indexPath.section].image{
                    if image.contains("https://"){
                        cell.senderImgS.sd_setImage(with: URL(string: image ), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                       // cell.senderImgS.contentMode = .scaleToFill
                    }else{
                        let userImage = Constants.APIs.imageBaseURL + image
                        let urlString = userImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                        cell.senderImgS.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                    }
                }else{}
//                let imagebaseURl = Constants.APIs.imageBaseURL + (chatGetArray[indexPath.section].image ?? "")
//                let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                cell.senderImgS.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                cell.senderImgBtnS.addTarget(self, action: #selector(chatPersonDetail(_:)), for: .touchUpInside)
                cell.senderImgBtnS.tag = indexPath.section
                if let dateValue = chatGetArray[indexPath.section].date_created  {
                    let fullString = NSMutableAttributedString(string: "")
                    let image1Attachment = NSTextAttachment()
                    image1Attachment.image = UIImage(named: "ic_clock_small")
                    let image1String = NSAttributedString(attachment: image1Attachment)
                    fullString.append(image1String)
                    fullString.append(NSAttributedString(string: self.convertTimeFormater(dateValue)!))
                    cell.timeLabelS.attributedText = fullString
                }else{}
                
                cell.noOfLikesBtnOutletS.tag = indexPath.section
                cell.heartBtnOutletS.tag = indexPath.section
                cell.heartBtnOutletS.addTarget(self, action: #selector(favoriteOrUnFavoriteEventByUser(_:)), for: .touchUpInside)
                chatGetArray[indexPath.section].is_liked_you == "0" ? cell.heartBtnOutletS.setImage(#imageLiteral(resourceName: "ic_like_unselected"), for: .normal) :  cell.heartBtnOutletS.setImage(#imageLiteral(resourceName: "ic_like_selected"), for: .normal)
                cell.noOfLikesBtnOutletS.addTarget(self, action: #selector(likePersonList(_:)), for: .touchUpInside)
                self.sum = Int(chatGetArray[indexPath.section].total_like_count ?? "0")!
                if chatGetArray[indexPath.section].total_like_count == "1"{
                     cell.noOfLikesBtnOutletS.setTitle("\(chatGetArray[indexPath.section].total_like_count ?? "0")" + (" Like"), for: .normal)
                }else{
                     cell.noOfLikesBtnOutletS.setTitle("\(chatGetArray[indexPath.section].total_like_count ?? "0")" + (" Likes"), for: .normal)
                }
               
                UserDefaults.standard.setValue(self.sum, forKeyPath: "total_like_count")
                UserDefaults.standard.set(chatGetArray[indexPath.section].is_liked_you, forKey: "is_liked_you")

                return cell
            } else {
                // Receiver View
                cell.senderViewS.isHidden = true
                cell.receiverViewR.isHidden = false
                cell.userNameLabelR.text = chatGetArray[indexPath.section].name ?? "null"
                cell.userMsgLabelR.text = chatGetArray[indexPath.section].post_title ?? ""
                if let image = chatGetArray[indexPath.section].image {
                    if image.contains("https://"){
                        cell.receiverImgR.sd_setImage(with: URL(string: image ), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                        //cell.receiverImgR.contentMode = .scaleToFill
                    }else{
                        let userImage = Constants.APIs.imageBaseURL + image
                        let urlString = userImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                        cell.receiverImgR.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                    }
                }else{}
//                let imagebaseURl = Constants.APIs.imageBaseURL + (chatGetArray[indexPath.section].image ?? "")
//                let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                cell.receiverImgR.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                cell.receiverImgBtnR.addTarget(self, action: #selector(chatPersonDetail(_:)), for: .touchUpInside)
                cell.receiverImgBtnR.tag = indexPath.section
                if let dateValue = chatGetArray[indexPath.section].date_created  {
                    let fullString = NSMutableAttributedString(string: "")
                    let image1Attachment = NSTextAttachment()
                    image1Attachment.image = UIImage(named: "ic_clock_small")
                    let image1String = NSAttributedString(attachment: image1Attachment)
                    fullString.append(image1String)
                    fullString.append(NSAttributedString(string: self.convertTimeFormater(dateValue)!))
                    cell.timeLabelR.attributedText = fullString
                }else{}
                
                cell.noOfLikesBtnOutletR.tag = indexPath.section
                cell.heartBtnOutletR.tag = indexPath.section
                cell.heartBtnOutletR.addTarget(self, action: #selector(favoriteOrUnFavoriteEventByUser(_:)), for: .touchUpInside)
                chatGetArray[indexPath.section].is_liked_you == "0" ? cell.heartBtnOutletR.setImage(#imageLiteral(resourceName: "ic_like_unselected"), for: .normal) :  cell.heartBtnOutletR.setImage(#imageLiteral(resourceName: "ic_like_selected"), for: .normal)
                cell.noOfLikesBtnOutletR.addTarget(self, action: #selector(likePersonList(_:)), for: .touchUpInside)
                self.sum = Int(chatGetArray[indexPath.section].total_like_count ?? "0")!
                if chatGetArray[indexPath.section].total_like_count == "1"{
                    cell.noOfLikesBtnOutletR.setTitle("\(chatGetArray[indexPath.section].total_like_count ?? "0")" + (" Like"), for: .normal)
                }else{
                    cell.noOfLikesBtnOutletR.setTitle("\(chatGetArray[indexPath.section].total_like_count ?? "0")" + (" Likes"), for: .normal)
                }
              
                UserDefaults.standard.setValue(self.sum, forKeyPath: "total_like_count")
                UserDefaults.standard.set(chatGetArray[indexPath.section].is_liked_you, forKey: "is_liked_you")
                return cell
            }
            // Video and Image File
        } else {
            let cell  = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell", for: indexPath) as! VideoTableViewCell
            
            if Int(userInfoModel)! == Int(chatGetArray[indexPath.section].user_id ?? "0")! {
         
                // Sender View
                cell.senderViewS.isHidden = false
                cell.receiverViewR.isHidden = true
                cell.senderNameLabelS.text = chatGetArray[indexPath.section].name ?? "null"
                cell.senderMsgLabelS.text = chatGetArray[indexPath.section].post_title
                cell.senderNameLabelS.text = chatGetArray[indexPath.section].name ?? "null"
                cell.senderMsgLabelS.text = chatGetArray[indexPath.section].post_title
                cell.senderViewS.isHidden = false
                cell.receiverViewR.isHidden = true
                cell.senderNameLabelS.text = chatGetArray[indexPath.section].name
                cell.senderMsgLabelS.text = chatGetArray[indexPath.section].post_title
                self.aString = self.chatGetArray[indexPath.section].media_duration ?? ""
                let newString = aString.replacingOccurrences(of: ", ", with: ".")
                if let dateValue = chatGetArray[indexPath.section].date_created {
                    cell.timeLabelS.text = self.convertDateToUTC(datestring: dateValue)
                }else{
                    cell.timeLabelS.text = ""
                }
                
                if let image = chatGetArray[indexPath.section].image{
                    if image.contains("https://"){
                        cell.senderImgS.sd_setImage(with: URL(string: image ), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                        // cell.senderImgS.contentMode = .scaleToFill
                    }else{
                        let userImage = Constants.APIs.imageBaseURL + image
                        let urlString = userImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                        cell.senderImgS.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                    }
                }else{}
//                let imagebaseURl = Constants.APIs.imageBaseURL + (chatGetArray[indexPath.section].image ?? "")
//                let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                cell.senderImgS.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                
                if let dateValue = chatGetArray[indexPath.section].date_created  {
                    let fullString = NSMutableAttributedString(string: "")
                    let image1Attachment = NSTextAttachment()
                    image1Attachment.image = UIImage(named: "ic_clock_small")
                    let image1String = NSAttributedString(attachment: image1Attachment)
                    fullString.append(image1String)
                    fullString.append(NSAttributedString(string: self.convertTimeFormater(dateValue)!))
                    cell.timeLabelS.attributedText = fullString
                } else {
                    cell.timeLabelS.text = ""
                }
                
                cell.msgImageViewS.image = nil
                
                if chatGetArray[indexPath.section].media_type == "image" {
                    cell.playPauseBtnImg.isHidden = true
                    cell.playPauseBtnImg.image = #imageLiteral(resourceName: "ic_video_play")
                    if let image = chatGetArray[indexPath.section].post_media {
                        if image.contains("http://"){
                            
                            cell.msgImageViewS.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "ic_username"))
                    
                            //cell.msgImageViewS.contentMode = .scaleToFill
                        }else{
                            let userImage = Constants.APIs.imageBaseURL + image
                            let urlString = userImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                            cell.msgImageViewS.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: UIImage(named: "ic_username"))
                        }
                    }else{}
                } else {
                    // Generate Thumbnail for Videos
                    cell.playPauseBtnImg.isHidden = false
                    cell.playPauseBtnImg.image = #imageLiteral(resourceName: "ic_video_play")
                    let fileUrl = NSURL(string: chatGetArray[indexPath.section].post_media ?? "")
                    DispatchQueue.global().async {
                        let asset = AVAsset(url: fileUrl! as URL)
                        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                        assetImgGenerate.appliesPreferredTrackTransform = true
                        let time = CMTimeMake(1, 2)
                        let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                        if img != nil {
                            let frameImg  = UIImage(cgImage: img!)
                            
                            DispatchQueue.main.async(execute: {
                                cell.msgImageViewS.image = frameImg
                            })
                        }else{
                            DispatchQueue.main.async(execute: {
                                cell.msgImageViewS.image = nil
                            })
                        }
                    }
                }
                cell.senderImgBtnS.tag = indexPath.section
                cell.senderImgBtnS.addTarget(self, action: #selector(chatPersonDetail(_:)), for: .touchUpInside)
                cell.noOfLikeBtnOutletS.tag = indexPath.section
                cell.heartBtnOUtletS.tag = indexPath.section
                cell.heartBtnOUtletS.addTarget(self, action: #selector(favoriteOrUnFavoriteEventByUser(_:)), for: .touchUpInside)
                chatGetArray[indexPath.section].is_liked_you == "0" ? cell.heartBtnOUtletS.setImage(#imageLiteral(resourceName: "ic_like_unselected"), for: .normal) :  cell.heartBtnOUtletS.setImage(#imageLiteral(resourceName: "ic_like_selected"), for: .normal)
                cell.noOfLikeBtnOutletS.addTarget(self, action: #selector(likePersonList(_:)), for: .touchUpInside)
                self.sum = Int(chatGetArray[indexPath.section].total_like_count ?? "0")!
                if chatGetArray[indexPath.section].total_like_count == "1"{
                    cell.noOfLikeBtnOutletS.setTitle("\(chatGetArray[indexPath.section].total_like_count ?? "")" + (" Like"), for: .normal)
                }else{
                    cell.noOfLikeBtnOutletS.setTitle("\(chatGetArray[indexPath.section].total_like_count ?? "")" + (" Likes"), for: .normal)
                }
                UserDefaults.standard.setValue(self.sum, forKeyPath: "total_like_count")
                UserDefaults.standard.set(chatGetArray[indexPath.section].is_liked_you, forKey: "is_liked_you")
                return cell
            }else{
                // Receiver View
                cell.senderViewS.isHidden = true
                cell.receiverViewR.isHidden = false
                cell.receiverNameLabelR.text = chatGetArray[indexPath.section].name ?? "null"
                cell.msgLabelR.text = chatGetArray[indexPath.section].post_title ?? ""
                
                if let image = chatGetArray[indexPath.section].image {
                    if image.contains("https://"){
                        cell.receiverImgR.sd_setImage(with: URL(string: image ), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                        //cell.receiverImgR.contentMode = .scaleToFill
                    }else{
                        let userImage = Constants.APIs.imageBaseURL + image
                        let urlString = userImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                        cell.receiverImgR.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                    }
                }else{}
//                let imagebaseURl = Constants.APIs.imageBaseURL + (chatGetArray[indexPath.section].image ?? "")
//                let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                cell.receiverImgR.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                
                if let dateValue = chatGetArray[indexPath.section].date_created  {
                    let fullString = NSMutableAttributedString(string: "")
                    let image1Attachment = NSTextAttachment()
                    image1Attachment.image = UIImage(named: "ic_clock_small")
                    let image1String = NSAttributedString(attachment: image1Attachment)
                    fullString.append(image1String)
                    fullString.append(NSAttributedString(string: self.convertTimeFormater(dateValue)!))
                    cell.timeLabelR.attributedText = fullString
                } else {
                    cell.timeLabelR.text = ""
                }
                
                cell.msgImageViewR.image = nil
                
                if chatGetArray[indexPath.section].media_type == "image" {
                    cell.playPauseBtnImgR.isHidden = true
                    cell.playPauseBtnImgR.image = #imageLiteral(resourceName: "ic_video_play")
                    if let image = chatGetArray[indexPath.section].post_media {
                        if image.contains("http://"){
                            cell.msgImageViewR.sd_setImage(with: URL(string: image ), placeholderImage: UIImage(named: "ic_username"))
                            //cell.msgImageViewR.contentMode = .scaleToFill
                        } else {
                            let userImage = Constants.APIs.imageBaseURL + image
                            let urlString = userImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                            cell.msgImageViewR.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: UIImage(named: "ic_username"))
                        }
                    }else{}
                }else{
                    // Generate Thumbnail for Videos
                    cell.playPauseBtnImgR.isHidden = false
                    cell.playPauseBtnImgR.image = #imageLiteral(resourceName: "ic_video_play")
                    let fileUrl = NSURL(string: chatGetArray[indexPath.section].post_media ?? "")
                    DispatchQueue.global().async {
                        let asset = AVAsset(url: fileUrl! as URL)
                        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                        assetImgGenerate.appliesPreferredTrackTransform = true
                        let time = CMTimeMake(1, 2)
                        let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                        if img != nil {
                            let frameImg  = UIImage(cgImage: img!)
                            DispatchQueue.main.async(execute: {
                                cell.msgImageViewR.image = frameImg
                            })
                        }else{
                            DispatchQueue.main.async(execute: {
                                cell.msgImageViewR.image = nil
                            })
                        }
                    }
                }
                cell.receiverImgBtnR.tag = indexPath.section
                cell.receiverImgBtnR.addTarget(self, action: #selector(chatPersonDetail(_:)), for: .touchUpInside)
                cell.noOfLikesBtnOutletR.tag = indexPath.section
                cell.heartBtnOutletR.tag = indexPath.section
                cell.heartBtnOutletR.addTarget(self, action: #selector(favoriteOrUnFavoriteEventByUser(_:)), for: .touchUpInside)
                chatGetArray[indexPath.section].is_liked_you == "0" ? cell.heartBtnOutletR.setImage(#imageLiteral(resourceName: "ic_like_unselected"), for: .normal) :  cell.heartBtnOutletR.setImage(#imageLiteral(resourceName: "ic_like_selected"), for: .normal)
                cell.noOfLikeBtnOutletS.addTarget(self, action: #selector(likePersonList(_:)), for: .touchUpInside)
                self.sum = Int(chatGetArray[indexPath.section].total_like_count ?? "0")!
                if chatGetArray[indexPath.section].total_like_count == "1"{
                    cell.noOfLikesBtnOutletR.setTitle("\(chatGetArray[indexPath.section].total_like_count ?? "0")" + (" Like"), for: .normal)
                }else{
                    cell.noOfLikesBtnOutletR.setTitle("\(chatGetArray[indexPath.section].total_like_count ?? "0")" + (" Likes"), for: .normal)
                }
                UserDefaults.standard.setValue(self.sum, forKeyPath: "total_like_count")
                UserDefaults.standard.set(chatGetArray[indexPath.section].is_liked_you, forKey: "is_liked_you")
             
                return cell
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
     
        let indexPath = IndexPath(row: 0 , section: indexPath.section)
        let cell = self.chatMessageTableView.cellForRow(at: indexPath) as? VideoTableViewCell
        let indexPath1 = IndexPath(row: 0, section: indexPath.section)
        let cell1 = self.chatMessageTableView.cellForRow(at: indexPath1) as? ChatAudioTableViewCell
        self.temp = chatGetArray[indexPath.section].total_like_count ?? "0"
        let a : String = chatGetArray[indexPath.section].media_type ?? ""
        let b  : String = "text"

        if(a.caseInsensitiveCompare(b) == .orderedSame){
            return 130
        }else{
            return 240
        }
//        if(a.caseInsensitiveCompare(b) == .orderedSame) {
//            if Int(userInfoModel)! == Int(chatGetArray[indexPath.section].user_id ?? "0")! {
//                if cell1?.noOfLikesBtnOutletS.isTouchInside == true {
//                    if temp == "0"{
//                        cell?.likePersonLIstHeight.constant = 0
//                        return 130
//                    }else{
//                        cell?.likePersonLIstHeight.constant = 70
//                        return 200
//                    }
//                }else{
//                    return 130
//                }
//            }else{
//
//                if cell1?.noOfLikesBtnOutletR.isTouchInside == true {
//                    if temp == "0"{
//                        cell?.likePersonListHeightR.constant = 0
//                        return 130
//                    }else{
//                        cell?.likePersonListHeightR.constant = 70
//                        return 200
//                    }
//
//                }else{
//                    return 130
//                }
//            }
//        } else {
//            if Int(userInfoModel)! == Int(chatGetArray[indexPath.section].user_id ?? "0")!{
//                if cell?.noOfLikeBtnOutletS.isSelected == true {
//                    if temp == "0"{
//                        cell?.likePersonLIstHeight.constant = 0
//                        return 230
//                    }else{
//                        cell?.likePersonLIstHeight.constant = 70
//                        return 300
//                    }
//                }else{
//                    return 230
//                }
//            }else{
//                if cell?.noOfLikesBtnOutletR.isSelected == true {
//                    if temp == "0"{
//                        cell?.likePersonListHeightR.constant = 0
//                        return 230
//                    }else{
//                        cell?.likePersonListHeightR.constant = 70
//                        return 300
//                    }
//                }else{
//                    return 230
//                }
//            }
//        }
    }
    
    //MARK: - Play Video
    func playVideo(url:String) {
        let videoURL = URL(string: url)
        let player = AVPlayer(url: (videoURL ?? nil)!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if chatGetArray[indexPath.section].media_type  == "video"{
            let tmpDirURL = FileManager.default.temporaryDirectory
            let fileURL = NSURL(string: chatGetArray[indexPath.section].post_media ?? "")
            self.playVideo(url: (fileURL?.absoluteString)!)
        }else if chatGetArray[indexPath.section].media_type == "image"{
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ImagePreviewViewController") as! ImagePreviewViewController
            viewController.fileUrl = chatGetArray[indexPath.section].post_media ?? ""
            self.present(viewController, animated: true, completion: nil)
        }else{
        }
    }
    
    @objc func likePersonListView(_ sender : UIButton) {
        let indexPath = IndexPath(row: 0, section: sender.tag)
        let cell = self.chatMessageTableView.cellForRow(at: indexPath) as? VideoTableViewCell
        var squareFrame: CGRect {
            let midX = self.view.bounds.midX
            let midY = self.view.bounds.midY
            let size: CGFloat = 50
            return CGRect(x: midX-size/2, y: midY-size/2, width: self.view.bounds.width, height: size)
        }
    }
    
    @objc func playSlider(_ sender : UIButton){
        let theHeight = self.view.frame.height
        self.audioViewSubView.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width, height: theHeight)
        self.view.addSubview(self.audioViewSubView)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(1)
    }
    
    @objc func OrganicationDetail(_ sender : UIButton){
        
        if chatMemberDetail == true{
            if comingBtnStatus == true{
                let vc = storyboard?.instantiateViewController(withIdentifier: "ChatDetailViewController" ) as! ChatDetailViewController
                vc.singleChatArray = singleChatArray1
                vc.groupChatArray = groupChatArray1
                vc.comingVCStatus = true
                vc.notificationCheck = chatNotificationArray
                
                if notificationCheck == false{
                    vc.notificationBackUserID = false
                }else{
                    vc.notificationBackUserID = true
                }
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                
                let vc = storyboard?.instantiateViewController(withIdentifier: "ChatMemberDetailViewController" ) as! ChatMemberDetailViewController
                vc.chatMsgId = chatGetArray[sender.tag]
                vc.comingVCStatus = false
                // Single
                vc.checkStatus = false
                vc.memberCheckStatus = false
                vc.groupChatArray = groupChatArray1
                vc.singleChatArray = singleChatArray1
                vc.notificationCheck = chatNotificationArray
                if notificationCheck == false{
                    vc.notificationBackUserID = false
                }else{
                    vc.notificationBackUserID = true
                }
                vc.userIDStatus = false
                vc.msgChatBtn = false
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            
            self.navigationController?.popViewController(animated: true)
//            let vc = storyboard?.instantiateViewController(withIdentifier: "ChatMemberDetailViewController" ) as! ChatMemberDetailViewController
//            vc.otherUserDetail1 = otherUserDetail
//            vc.comingUserCheck = false
//            vc.comingVCStatus = false
//            // Single
//            vc.checkStatus = false
//            vc.memberCheckStatus = false
//            vc.groupChatArray = groupChatArray1
//            vc.singleChatArray = singleChatArray1
//            vc.userIDStatus = false
//            vc.msgChatBtn = false
//            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func likePersonList(_ sender : UIButton){
        let vc = storyboard?.instantiateViewController(withIdentifier: "LikePersonListViewController" ) as! LikePersonListViewController
        if comingBtnStatus == true{
            vc.groupChatArray1 = chatGetArray[sender.tag]
            vc.comingVC = true
           // vc.groupChatArray1 = groupChatArray1
        }else{
            vc.comingVC = false
            vc.singleChatArray1 = chatGetArray[sender.tag]
          // vc.singleChatArray1 = singleChatArray1
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func chatPersonDetail(_ sender : UIButton){
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChatMemberDetailViewController" ) as! ChatMemberDetailViewController
        vc.chatMsgId = chatGetArray[sender.tag]
        vc.comingVCStatus = false
        vc.checkStatus = true
        vc.userIDStatus = true
        vc.groupChatArray = groupChatArray1
        vc.singleChatArray = singleChatArray1
        self.navigationController?.pushViewController(vc, animated: true)
        
        if Int(userInfoModel)! == Int(chatGetArray[sender.tag].user_id ?? "0")!{
            vc.chatMsgId = chatGetArray[sender.tag]
        }else{
            vc.chatMsgId = chatGetArray[sender.tag]
        }
    }
    
    //MARK:- TableView Set To bottom
    func tableViewScrollToBottoms(animated: Bool, milliseconds: Int){
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(milliseconds)) {
            if let indexPath = self.lastIndexPath(self.chatMessageTableView) {
                self.chatMessageTableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
            }else{
            }
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastCellForRowAtIndex = 0
        var isScrollingUp = false
        isScrollingUp = indexPath.row > lastCellForRowAtIndex
        lastCellForRowAtIndex = indexPath.row
        if indexPath.section == chatGetArray.count - 1 {
            print(indexPath.section)
            print(chatGetArray.count - 1)
             moveToBottomOutlet.isHidden = true
        }else{
             moveToBottomOutlet.isHidden = false
        }
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView){
//
//        guard let lastCell = chatMessageTableView.cellForRow(at: IndexPath(row: chatMessageTableView.numberOfRows(inSection: 0)-1, section: 0)) else{
//            return
//        }
//        let lastCellBottomY = lastCell.frame.origin.y
//        let delta = self.chatMessageTableView.frame.height - view.frame.origin.y
//        let bottomValue = Constants.ScreenSize.SCREEN_HEIGHT - 40
//
//
//        if (chatMessageTableView.frame.maxY <= bottomValue) {
//            moveToBottomOutlet.isHidden = false
//        }else{
//            moveToBottomOutlet.isHidden = true
//        }
//    }
}

 extension ChatMessageViewController : CropViewControllerDelegate{
        
    func uploadProfilePicClick(){
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "", message: "Option to select", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        let saveActionButton = UIAlertAction(title: "Take Photo", style: .default) { _ in
            self.mediaType = "image"
            self.openImagePickerViewController(sourceType: .camera, mediaTypes: [kUTTypeImage as String])
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        let libraryActionButton = UIAlertAction(title: "Library", style: .default) { _ in
            self.mediaType = "video"
            self.openImagePickerViewController(sourceType: .photoLibrary, mediaTypes: [kUTTypeMovie as String])
        }
        actionSheetControllerIOS8.addAction(libraryActionButton)
        // Audio
        let audioActionButton = UIAlertAction(title: "Gallery", style: .default) { _ in
            self.mediaType = "image"
            self.openImagePickerViewController(sourceType: .photoLibrary, mediaTypes: [kUTTypeImage as String])
        }
        actionSheetControllerIOS8.addAction(audioActionButton)
        // Video
        let videoActionButton = UIAlertAction(title: "Video", style: .default) { _ in
            self.mediaType = "video"
            self.openImagePickerViewController(sourceType: .camera, mediaTypes: [kUTTypeMovie as String])
        }
        actionSheetControllerIOS8.addAction(videoActionButton)
        
//        // Audio
//        let audioRecordActionButton = UIAlertAction(title: "Audio", style: .default)
//        { _ in
//            self.mediaType = "audio"
//            let theHeight = self.view.frame.height
//            self.addSubView.frame = CGRect(x: 0, y: theHeight - 200 , width: self.view.frame.width, height: 200)
//            self.view.addSubview(self.addSubView)
//            self.view.backgroundColor = UIColor.gray.withAlphaComponent(1)
//
//        }
//        actionSheetControllerIOS8.addAction(audioRecordActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        let videoURL = info[UIImagePickerControllerMediaURL] as? URL
     
       
        if videoURL != nil{
                videoThumbnailSize = "\(videoURL!)"
                let decoded                     = UserDefaults.standard.object(forKey: "userinfo") as! Data
                let userDataStr: String         = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! String
                self.urlGetCloudinary = videoURL
                if Mapper<AUserInfoModel>().map(JSONString: userDataStr) != nil {
                //    self.mediaType = "video"
                    self.view.addSubview(self.previewImageIcon)
                    self.view.backgroundColor = UIColor.white
                    self.previewImageIcon.frame = CGRect(x: 10, y: Constants.ScreenSize.SCREEN_HEIGHT - 150 , width: 80, height: 80)
                    self.previewImageIcon.layer.cornerRadius = 5
                    self.previewImageIcon.layer.masksToBounds = false
                    self.previewImageIcon.layer.borderWidth  = 0.5
                    self.previewImageIcon.clipsToBounds = true
                    self.previewImgCrossBtn.addTarget(self, action: #selector(crossBtn), for: .touchUpInside)
                    let asset = AVAsset(url: videoURL as! URL)
                    let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                    assetImgGenerate.appliesPreferredTrackTransform = true
                    let time = CMTimeMake(1, 2)
                    let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                    if img != nil {
                        let frameImg  = UIImage(cgImage: img!)
                        self.previewImage.image = frameImg
                    }
                }
                self.buttonSelected()
                picker.dismiss(animated: true, completion: nil)
            //   self.presentCropViewController(image: previewImage!)
            
        } else {

//            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
//
//            // self.mediaType = "image"
////                 self.urlImageGetCloudinary = NSURL(string: videoThumbnailSize)! as URL
//                self.view.addSubview(self.previewImageIcon)
//           //     imagePreview = pickedImage
//          //      imageFullSize = pickedImage
//                let imageThumbnail = (pickedImage).createImageThumbnailFromImage()
//                imageThumbnailSize = (imageThumbnail)
//
//                self.buttonSelected()
//                picker.dismiss(animated: true, completion: nil)
//                self.previewImageIcon.isHidden = false
//                self.previewImage.image = imageThumbnailSize
//                self.previewImage.contentMode = .scaleToFill
//                self.previewImageIcon.frame = CGRect(x: 10, y: Constants.ScreenSize.SCREEN_HEIGHT - 150 , width: 80, height: 80)
//                self.previewImageIcon.layer.cornerRadius = 5
//                self.previewImageIcon.layer.masksToBounds = false
//                self.previewImageIcon.clipsToBounds = true
//                self.previewImageIcon.layer.borderWidth  = 0.5
//                self.previewImgCrossBtn.addTarget(self, action: #selector(crossBtn), for: .touchUpInside)
//
//           //     self.presentCropViewController(image: imageThumbnailSize)
//                self.uploadImageThumnailSize = imageThumbnailSize
//            }else
                if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
                
                // self.mediaType = "image"
                //                 self.urlImageGetCloudinary = NSURL(string: videoThumbnailSize)! as URL
                self.view.addSubview(self.previewImageIcon)
                //     imagePreview = pickedImage
                //      imageFullSize = pickedImage
                let imageThumbnail = (pickedImage).createImageThumbnailFromImage()
                imageThumbnailSize = (imageThumbnail)
                
                self.buttonSelected()
                
                self.previewImageIcon.isHidden = false
                self.previewImage.image = imageThumbnailSize
                self.previewImage.contentMode = .scaleToFill
                self.previewImageIcon.frame = CGRect(x: 10, y: Constants.ScreenSize.SCREEN_HEIGHT - 150 , width: 80, height: 80)
                self.previewImageIcon.layer.cornerRadius = 5
                self.previewImageIcon.layer.masksToBounds = false
                self.previewImageIcon.clipsToBounds = true
                self.previewImageIcon.layer.borderWidth  = 0.5
                self.previewImgCrossBtn.addTarget(self, action: #selector(crossBtn), for: .touchUpInside)
                //     self.presentCropViewController(image: imageThumbnailSize)
                self.uploadImageThumnailSize = imageThumbnailSize
                picker.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //MARK:- CROP FXN
    
    func presentCropViewController(image : UIImage){
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        cropViewController.aspectRatioPreset = .presetSquare; // Set the initial aspect ratio as a square
        cropViewController.aspectRatioLockEnabled = true // The crop box is locked to the aspect ratio and can't be resized away from it
        cropViewController.resetAspectRatioEnabled = false // When tapping 'reset', the aspect ratio will NOT be reset back to default
        cropViewController.aspectRatioPickerButtonHidden = true
        cropViewController.rotateButtonsHidden = true
        cropViewController.rotateClockwiseButtonHidden = true
        present(cropViewController, animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        self.previewImage.image = image
       // previewImage.image = image
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
    @objc func crossBtn(){
        self.previewImage.image = nil
        self.previewImageIcon.isHidden = true
        self.mediaType = ""
        self.resetViewProperties()
    }
}

// MARK: AVAudioRecorderDelegate
extension ChatMessageViewController: AVAudioRecorderDelegate , AVAudioPlayerDelegate{
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool){
        print("finished recording \(flag)")
        stopButton.isEnabled = false
        playButton.isEnabled = true
        recordButton.setTitle("Record", for: UIControlState())
        // iOS8 and later
        let alert = UIAlertController(title: "Recorder",
                                      message: "Finished Recording",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Keep", style: .default) {[unowned self] _ in
            print("keep was tapped")
            self.recorder = nil
        })
        alert.addAction(UIAlertAction(title: "Delete", style: .default) {[unowned self] _ in
            print("delete was tapped")
            self.recorder.deleteRecording()
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        if let e = error {
            print("\(e.localizedDescription)")
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("finished playing \(flag)")
        recordButton.isEnabled = true
        stopButton.isEnabled = false
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        if let e = error {
            print("\(e.localizedDescription)")
        }
    }
    
    @objc func updateAudioMeter(_ timer: Timer) {
        if let recorder = self.recorder {
            if recorder.isRecording {
                let min = Int(recorder.currentTime / 60)
                let sec = Int(recorder.currentTime.truncatingRemainder(dividingBy: 60))
                let s = String(format: "%02d:%02d", min, sec)
                statusLabel.text = s
                recorder.updateMeters()
                // if you want to draw some graphics...
                //var apc0 = recorder.averagePowerForChannel(0)
                //var peak0 = recorder.peakPowerForChannel(0)
            }
        }
    }
    
    // Mark:- Reset View Properties
    func resetViewProperties(){
        self.buttonUnSelected()
    }
    
    internal func buttonSelected(){
        nextBtn.imageView?.image = #imageLiteral(resourceName: "ic_send_enabled")
    }
    
    internal func buttonUnSelected(){
        nextBtn.imageView?.image = #imageLiteral(resourceName: "ic_send_disabled")
    }
    
    func play() {
        print("\(#function)")
        
        var url: URL?
        if self.recorder != nil{
            url = self.recorder.url
        } else {
            url = self.soundFileURL!
        }
        print("playing \(String(describing: url))")
        do{
            self.player = try AVAudioPlayer(contentsOf: url!)
            stopButton.isEnabled = true
            player.delegate = self
            player.prepareToPlay()
            player.volume = 1.0
            player.play()
        }catch{
            self.player = nil
            print(error.localizedDescription)
        }
    }
    
    func setupRecorder() {
        print("\(#function)")
        
        let format = DateFormatter()
        format.dateFormat="yyyy-MM-dd-HH-mm-ss"
        let currentFileName = "recording-\(format.string(from: Date())).m4a"
        print(currentFileName)
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        self.soundFileURL = documentsDirectory.appendingPathComponent(currentFileName)
        print("writing to soundfile url: '\(soundFileURL!)'")
        
        if FileManager.default.fileExists(atPath: soundFileURL.absoluteString) {
            // probably won't happen. want to do something about it?
            print("soundfile \(soundFileURL.absoluteString) exists")
        }
        
        let recordSettings: [String: Any] = [
            AVFormatIDKey: kAudioFormatAppleLossless,
            AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue,
            AVEncoderBitRateKey: 32000,
            AVNumberOfChannelsKey: 2,
            AVSampleRateKey: 44100.0
        ]
        do {
            recorder = try AVAudioRecorder(url: soundFileURL, settings: recordSettings)
            recorder.delegate = self
            recorder.isMeteringEnabled = true
            recorder.prepareToRecord() // creates/overwrites the file at soundFileURL
        } catch {
            recorder = nil
            print(error.localizedDescription)
        }
        self.urlAudioGetCloudinary = soundFileURL
    }
    
    func recordWithPermission(_ setup: Bool) {
        print("\(#function)")
        
        AVAudioSession.sharedInstance().requestRecordPermission {
            [unowned self] granted in
            if granted {
                DispatchQueue.main.async {
                    print("Permission to record granted")
                    self.setSessionPlayAndRecord()
                    if setup {
                        self.setupRecorder()
                    }
                    self.recorder.record()
                    self.meterTimer = Timer.scheduledTimer(timeInterval: 0.1,
                                                           target: self,
                                                           selector: #selector(self.updateAudioMeter(_:)),
                                                           userInfo: nil,
                                                           repeats: true)
                }
            }else{
                print("Permission to record not granted")
            }
        }
        if AVAudioSession.sharedInstance().recordPermission() == .denied {
            print("permission denied")
        }
    }
    
    func setSessionPlayback() {
        print("\(#function)")
        
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback, with: .defaultToSpeaker)
        } catch {
            print("could not set session category")
            print(error.localizedDescription)
        }
        do {
            try session.setActive(true)
        } catch {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }
    
    func setSessionPlayAndRecord(){
        print("\(#function)")
        
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
        } catch {
            print("could not set session category")
            print(error.localizedDescription)
        }
        
        do {
            try session.setActive(true)
        } catch {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }
    
    func deleteAllRecordings() {
        print("\(#function)")
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileManager = FileManager.default
        do {
            let files = try fileManager.contentsOfDirectory(at: documentsDirectory,
                                                            includingPropertiesForKeys: nil,
                                                            options: .skipsHiddenFiles)
            //                let files = try fileManager.contentsOfDirectory(at: documentsDirectory)
            var recordings = files.filter({ (name: URL) -> Bool in
                return name.pathExtension == "m4a"
                //                    return name.hasSuffix("m4a")
            })
            for i in 0 ..< recordings.count {
                //                    let path = documentsDirectory.appendPathComponent(recordings[i], inDirectory: true)
                //                    let path = docsDir + "/" + recordings[i]
                
                //                    print("removing \(path)")
                print("removing \(recordings[i])")
                do {
                    try fileManager.removeItem(at: recordings[i])
                } catch {
                    print("could not remove \(recordings[i])")
                    print(error.localizedDescription)
                }
            }
        } catch {
            print("could not get contents of directory at \(documentsDirectory)")
            print(error.localizedDescription)
        }
    }
    
    func askForNotifications() {
        print("\(#function)")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatMessageViewController.background(_:)),
                                               name: NSNotification.Name.UIApplicationWillResignActive,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatMessageViewController.foreground(_:)),
                                               name: NSNotification.Name.UIApplicationWillEnterForeground,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChatMessageViewController.routeChange(_:)),
                                               name: NSNotification.Name.AVAudioSessionRouteChange,
                                               object: nil)
    }
    
    @objc func background(_ notification: Notification) {
        print("\(#function)")
    }
    
    @objc func foreground(_ notification: Notification) {
        print("\(#function)")
    }
    
    @objc func routeChange(_ notification: Notification) {
        print("\(#function)")
        
        if let userInfo = (notification as NSNotification).userInfo {
            print("routeChange \(userInfo)")
            
            //print("userInfo \(userInfo)")
            if let reason = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt {
                //print("reason \(reason)")
                switch AVAudioSessionRouteChangeReason(rawValue: reason)! {
                case AVAudioSessionRouteChangeReason.newDeviceAvailable:
                    print("NewDeviceAvailable")
                    print("did you plug in headphones?")
                    checkHeadphones()
                case AVAudioSessionRouteChangeReason.oldDeviceUnavailable:
                    print("OldDeviceUnavailable")
                    print("did you unplug headphones?")
                    checkHeadphones()
                case AVAudioSessionRouteChangeReason.categoryChange:
                    print("CategoryChange")
                case AVAudioSessionRouteChangeReason.override:
                    print("Override")
                case AVAudioSessionRouteChangeReason.wakeFromSleep:
                    print("WakeFromSleep")
                case AVAudioSessionRouteChangeReason.unknown:
                    print("Unknown")
                case AVAudioSessionRouteChangeReason.noSuitableRouteForCategory:
                    print("NoSuitableRouteForCategory")
                case AVAudioSessionRouteChangeReason.routeConfigurationChange:
                    print("RouteConfigurationChange")
                }
            }
        }
    }
    
    func checkHeadphones() {
        print("\(#function)")
        // check NewDeviceAvailable and OldDeviceUnavailable for them being plugged in/unplugged
        let currentRoute = AVAudioSession.sharedInstance().currentRoute
        if !currentRoute.outputs.isEmpty {
            for description in currentRoute.outputs {
                if description.portType == AVAudioSessionPortHeadphones {
                    print("headphones are plugged in")
                    break
                } else {
                    print("headphones are unplugged")
                }
            }
        } else {
            print("checking headphones requires a connection to a device")
        }
    }
    
    func exportAsset(_ asset: AVAsset, fileName: String) {
        print("\(#function)")
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let trimmedSoundFileURL = documentsDirectory.appendingPathComponent(fileName)
        print("saving to \(trimmedSoundFileURL.absoluteString)")
        
        if FileManager.default.fileExists(atPath: trimmedSoundFileURL.absoluteString) {
            print("sound exists, removing \(trimmedSoundFileURL.absoluteString)")
            do {
                if try trimmedSoundFileURL.checkResourceIsReachable() {
                    print("is reachable")
                }
                
                try FileManager.default.removeItem(atPath: trimmedSoundFileURL.absoluteString)
            } catch {
                print("could not remove \(trimmedSoundFileURL)")
                print(error.localizedDescription)
            }
        }
        print("creating export session for \(asset)")
        
        if let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) {
            exporter.outputFileType = AVFileType.m4a
            exporter.outputURL = trimmedSoundFileURL
            
            let duration = CMTimeGetSeconds(asset.duration)
            if duration < 5.0 {
                print("sound is not long enough")
                return
            }
            // e.g. the first 5 seconds
            let startTime = CMTimeMake(0, 1)
            let stopTime = CMTimeMake(5, 1)
            exporter.timeRange = CMTimeRangeFromTimeToTime(startTime, stopTime)
         
            // do it
            exporter.exportAsynchronously(completionHandler: {
                print("export complete \(exporter.status)")
                
                switch exporter.status {
                case  AVAssetExportSessionStatus.failed:
                    
                    if let e = exporter.error {
                        print("export failed \(e)")
                    }
                    
                case AVAssetExportSessionStatus.cancelled:
                    print("export cancelled \(String(describing: exporter.error))")
                default:
                    print("export complete")
                }
            })
        } else {
            print("cannot create AVAssetExportSession for asset \(asset)")
        }
    }
    
    @IBAction
    func speed() {
        let asset = AVAsset(url: self.soundFileURL!)
        exportSpeedAsset(asset, fileName: "trimmed.m4a")
    }
    
    func exportSpeedAsset(_ asset: AVAsset, fileName: String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let trimmedSoundFileURL = documentsDirectory.appendingPathComponent(fileName)
        
        let filemanager = FileManager.default
        if filemanager.fileExists(atPath: trimmedSoundFileURL.absoluteString) {
            print("sound exists")
        }
        
        print("creating export session for \(asset)")
        
        if let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) {
            exporter.outputFileType = AVFileType.m4a
            exporter.outputURL = trimmedSoundFileURL

            exporter.audioTimePitchAlgorithm = AVAudioTimePitchAlgorithm.varispeed
            
            let duration = CMTimeGetSeconds(asset.duration)
            if duration < 5.0 {
                print("sound is not long enough")
                return
            }
            
            // do it
            exporter.exportAsynchronously(completionHandler: {
                switch exporter.status {
                case  AVAssetExportSessionStatus.failed:
                    print("export failed \(String(describing: exporter.error))")
                case AVAssetExportSessionStatus.cancelled:
                    print("export cancelled \(String(describing: exporter.error))")
                default:
                    print("export complete")
                }
            })
        }
    }
}
       // Audio File
       //        } else if chatGetArray[indexPath.row].media_type == "audio"{
       //            let cell  = tableView.dequeueReusableCell(withIdentifier: "ChatVoiceTableViewCell", for: indexPath) as! ChatVoiceTableViewCell
       //            if Int(userInfoModel)! == Int(chatGetArray[indexPath.row].user_id ?? "0")! {
       //                // Sender View
       //                cell.senderView.isHidden = false
       //                cell.receiverView.isHidden = true
       //                cell.senderNameLbl.text = chatGetArray[indexPath.row].name
       //                cell.senderMsgLbl.text = chatGetArray[indexPath.row].post_title
       //                self.aString = self.chatGetArray[indexPath.row].media_duration ?? ""
       //                let newString = aString.replacingOccurrences(of: ", ", with: ".")
       //                let bNewString = newString.replacingOccurrences(of: "()", with: " ")
       //                cell.audioDurationLblS.text = "\(bNewString)"
       //                if let dateValue = chatGetArray[indexPath.row].date_created {
       //                    cell.senderTimeLabel.text = self.convertDateToUTC(datestring: dateValue)
       //                }else{
       //                    cell.senderTimeLabel.text = ""
       //                }
       //                cell.audioDurationSliderS.setValue(0.0, animated: true)
       //                cell.audioDurationSliderS.tag = indexPath.row
       //                cell.audioDurationSliderS.addTarget(self, action: Selector(("sliderChange:")), for: .valueChanged)
       //                cell.playPauseBtnOutletS.addTarget(self, action: #selector(playSlider(_:)), for: .touchUpInside)
       //                cell.noOfLikesBtnOutletS.setTitle((chatGetArray[indexPath.row].total_like_count ?? "0") + (" Likes"), for: .normal)
       //                let imagebaseURl = Constants.APIs.imageBaseURL + (chatGetArray[indexPath.row].image ?? "")
       //                let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
       //                cell.senderImgView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
       //                cell.noOfLikesBtnOutletS.addTarget(self, action: #selector(LoadCollectionview(_:)), for: .touchUpInside)
       //                cell.senderImgBtn.addTarget(self, action: #selector(chatPersonDetail(_:)), for: .touchUpInside)
       //                cell.heartImgBtnOutletS.addTarget(self, action: #selector(likeUnlikeAPIS(_:)), for: .touchUpInside)
       //                cell.noOfLikesBtnOutletS.addTarget(self, action: #selector(likePersonListAPI(_:)), for: .touchUpInside)
       //                UserDefaults.standard.setValue(chatGetArray[indexPath.row].is_liked_you, forKey: "is_liked_you")
       //                if chatGetArray[indexPath.row].is_liked_you == "0" {
       //                    cell.heartImgBtnOutletS.setImage(#imageLiteral(resourceName: "ic_like_unselected"), for: .normal)
       //                }else{
       //                    cell.heartImgBtnOutletS.setImage(#imageLiteral(resourceName: "ic_like_selected"), for: .normal)
       //                }
       //
       //                return cell
       //            }else{
       //            //      Receiver View
       //                cell.senderView.isHidden = true
       //                cell.receiverView.isHidden = false
       //                cell.receiverNameLbl.text = chatGetArray[indexPath.row].name
       //                cell.receiverMsgLbl.text = chatGetArray[indexPath.row].post_title
       //                self.aString = self.chatGetArray[indexPath.row].media_duration ?? ""
       //                let newString = aString.replacingOccurrences(of: ", ", with: ".")
       //                let bNewString = newString.replacingOccurrences(of: "()", with: " ")
       //                cell.audioDurationTimeLblR.text = "\(bNewString)"
       //                if let dateValue = chatGetArray[indexPath.row].date_created {
       //                    cell.receiverTimeLabel.text = self.convertDateToUTC(datestring: dateValue)
       //                }else{
       //                    cell.receiverTimeLabel.text = ""
       //                }
       //                cell.audioPlaySliderOutletR.setValue(0.0, animated: true)
       //                cell.audioPlaySliderOutletR.tag = indexPath.row
       //                cell.audioPlaySliderOutletR.addTarget(self, action: Selector(("sliderChange:")), for: .valueChanged)
       //                cell.playPauseBtnOutletR.tag = indexPath.row
       //                cell.playPauseBtnOutletR.addTarget(self, action: #selector(playSlider(_:)), for: .touchUpInside)
       //                cell.noofLikesOutletR.setTitle((chatGetArray[indexPath.row].total_like_count ?? "0") + (" Likes"), for: .normal)
       //                let imagebaseURl = Constants.APIs.imageBaseURL + (chatGetArray[indexPath.row].image ?? "")
       //                let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
       //                cell.receiverImgView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
       //                cell.noofLikesOutletR.addTarget(self, action: #selector(LoadCollectionview(_:)), for: .touchUpInside)
       //                cell.receiverImgBtn.addTarget(self, action: #selector(chatPersonDetail(_:)), for: .touchUpInside)
       //                cell.heartImgOutletR.addTarget(self, action: #selector(likeUnlikeAPIS(_:)), for: .touchUpInside)
       //                cell.noofLikesOutletR.addTarget(self, action: #selector(likePersonListAPI(_:)), for: .touchUpInside)
       //
       //                if chatGetArray[indexPath.row].is_liked_you == "0" {
       //                    cell.heartImgOutletR.setImage(#imageLiteral(resourceName: "ic_like_unselected"), for: .normal)
       //                }else{
       //                    cell.heartImgOutletR.setImage(#imageLiteral(resourceName: "ic_like_selected"), for: .normal)
       //                }
       //                return cell
       //            }
       
