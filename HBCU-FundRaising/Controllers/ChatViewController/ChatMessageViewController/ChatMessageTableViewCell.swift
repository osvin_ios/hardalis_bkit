//
//  ChatMessageTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 27/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

protocol TableViewCellSignUpProfilePicDelegate {
    
    func uploadProfilePicClick(sender: UIButton)
}

protocol RecordAudioViewDelegate {
    
    func reloadeTable()
}


class ChatMessageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var uploadimageView: UIImageView!
    
     var delegate: TableViewCellSignUpProfilePicDelegate?
    
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
            // Configure the view for the selected state
        }
   
}
