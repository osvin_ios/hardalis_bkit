//
//  LikePersonListViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 24/10/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class LikePersonListViewController: UIViewController {
    @IBOutlet weak var likePersonListTableView: UITableView!
    var groupChatArray1 : ChatMessageModel?
    var singleChatArray1 : ChatMessageModel?
    var comingVC = Bool()
    var likedPersonDetailArray = [ChatMessageModel]()
    var refreshControlTop : UIRefreshControl!
    var refreshControlBottom : UIRefreshControl!
    var skipCount : Int = 0
    var postsCountOFArray : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.likePersonListAPI()
        self.navigationController?.navigationBar.isHidden = true
//        if Reachability.isConnectedToNetwork() == true {
////            self.refreshControlAPI()
//        } else{
//            self.showAlert(Constants.commomInternetmsz.checkInternet)
//        }
      
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
         self.navigationController?.navigationBar.isHidden = false
    }

    // MARK: - refresh control For API
    internal func refreshControlAPI() {
        
        refreshControlTop = UIRefreshControl()
        refreshControlTop.addTarget(self, action: #selector(self.reloadAPI), for: .valueChanged)
        self.likePersonListTableView.addSubview(refreshControlTop) // not required when using UITableViewController
        
        refreshControlBottom = UIRefreshControl()
        refreshControlBottom.addTarget(self, action: #selector(self.reloadAPICount), for: .valueChanged)
        self.likePersonListTableView.bottomRefreshControl = refreshControlBottom
        self.manuallyRefreshTheTab()
    }
    
    func manuallyRefreshTheTab() {
        //First time automatically refreshing.
        refreshControlTop.beginRefreshingManually()
        self.perform(#selector(self.reloadAPI), with: nil, afterDelay: 0)
    }
    
    //MARK:- Reload API
    @objc func reloadAPI() {
        // Check network connection.
        self.skipCount = 1
        if Reachability.isConnectedToNetwork() == true {
              self.likePersonListAPI()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    
    //MARK:- Reload API
    @objc func reloadAPICount() {
        //      if postsCountOFArray != 0 {
        // Check network connection.
        self.skipCount = skipCount + 1
        if Reachability.isConnectedToNetwork() == true {
            self.likePersonListAPI()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    
    @IBAction func backButtonActn(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
            self.navigationController?.navigationBar.isHidden = true
        }
    }
    
    // Like Person List API
    func likePersonListAPI(){
        WebServiceClass.showLoader(view: self.view)
        var post_id = String()
        
        if comingVC == true{
            post_id = (groupChatArray1?.id ?? "0")!
        } else {
            post_id = (singleChatArray1?.id ?? "0")!
        }
        let page = self.skipCount as AnyObject
        
        let paramsStr = "page=\("1")&post_id=\(post_id)"
        print(paramsStr)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.chatBaseURL + Constants.APIs.likePersonList, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject>{
                    print(jsonResult)
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        
                        if responeCode == false {
                            
                            if (jsonResult["message"] as? String) != nil {
                                
                                if let ChatData = jsonResult["result"] as? [Dictionary<String,AnyObject>] {
                                    
                                    if let ChatInfo = Mapper<ChatMessageModel>().mapArray(JSONObject: ChatData) {
                                        self.likedPersonDetailArray = ChatInfo
                                    }
                                    if self.likedPersonDetailArray.count == 0 {
                                        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
                                        label.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
                                        label.textAlignment = .center
                                        label.textColor = UIColor.lightGray
                                        label.text = "No Liked Person Found"
                                        self.view.addSubview(label)
                                    }
                                }
                            } else {
                                print("something went wrong 1")
                            }
                        }
                    }else{
                        print("Something went wrong 2")
                    }
                } else {
                    print("Something went wrong 3")
                }
            } else {
                print("Somthing went wrong 4")
            }
            DispatchQueue.main.async {
                self.likePersonListTableView.reloadData()
            }
        }
    }
}

extension LikePersonListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return likedPersonDetailArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "LikePersonListTableVieCell", for: indexPath) as! LikePersonListTableVieCell
        cell.likePersonNameLabel.text = (likedPersonDetailArray[indexPath.row].name ?? "")
        if let image = likedPersonDetailArray[indexPath.row].image {
            if image.contains("https://"){
                cell.likePersonImageView.sd_setImage(with: URL(string: image ), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                cell.likePersonImageView.contentMode = .scaleToFill
            } else {
                let userImage = Constants.APIs.imageBaseURL + image
                let urlString = userImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                cell.likePersonImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
            }
        }
//        let imagebaseURl = Constants.APIs.imageBaseURL + (likedPersonDetailArray[indexPath.row].image ?? "")
//        let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//        cell.likePersonImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChatMemberDetailViewController") as! ChatMemberDetailViewController
        vc.likeUnlikeCheck = true
        vc.msgChatBtn = true
        vc.likeUnlikeData = likedPersonDetailArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

