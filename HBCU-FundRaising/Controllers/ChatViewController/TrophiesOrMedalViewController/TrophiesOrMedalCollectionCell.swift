//
//  TrophiesOrMedalCollectionCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 10/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class TrophiesOrMedalCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var trophiesDetailLabel: UILabel!
    @IBOutlet weak var trophyImageView: UIImageView!
    @IBOutlet weak var mainView: ViewDesign!
    
}
