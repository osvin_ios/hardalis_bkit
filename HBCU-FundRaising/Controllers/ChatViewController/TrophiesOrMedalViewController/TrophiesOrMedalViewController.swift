//
//  TrophiesOrMedalViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 10/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class TrophiesOrMedalViewController: UIViewController {
    
    // Upper view
    
    @IBOutlet weak var naviBarImagView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var trophiesCollectionView: UICollectionView!
   
    var trophiesAndMedal = [TrophiesAndMedalModel]()
    var comingVC : Bool = false
    var comingVCTrophie  : otherUserProfileModel?
    var user_id = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        trophiesCollectionView.dataSource = self
        trophiesCollectionView.delegate = self
        self.navigationController?.navigationBar.isHidden = true
        self.trophiesMedalDetailAPI()
        // Do any additional setup after loading the view.
    }

    @IBAction func backBtnActn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.navigationBar.isHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension TrophiesOrMedalViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trophiesAndMedal.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrophiesOrMedalCollectionCell", for: indexPath) as! TrophiesOrMedalCollectionCell
        cell.trophiesDetailLabel.text = trophiesAndMedal[indexPath.item].title
        
        if trophiesAndMedal[indexPath.item].type == "medal"{
            if trophiesAndMedal[indexPath.item].medal == "gold"{
                cell.trophyImageView.image = UIImage(named: "ic_medal_gold.png")
            }else if trophiesAndMedal[indexPath.item].medal == "silver"{
                cell.trophyImageView.image = UIImage(named: "ic_medal_silver.png")
            }else{
                cell.trophyImageView.image = UIImage(named: "ic_medal_bronze.png")
            }
        } else if trophiesAndMedal[indexPath.item].type == "trophy"{
            if trophiesAndMedal[indexPath.item].medal == "gold"{
                cell.trophyImageView.image = UIImage(named: "ic_trophy_three")
            }else if trophiesAndMedal[indexPath.item].medal == "silver"{
                cell.trophyImageView.image = UIImage(named: "ic_trophy_two")
            }else{
                cell.trophyImageView.image = UIImage(named: "ic_trophy_one")
            }
        }else{
            if trophiesAndMedal[indexPath.item].medal == "ribbonyellow"{
                cell.trophyImageView.image = UIImage(named: "ic_ribbon_yellow")
            }else{
                cell.trophyImageView.image = UIImage(named: "ic_ribbon_blue")
            }
        }
        //        let imagebaseURl = Constants.APIs.imageBaseURL + (trophiesAndMedal[indexPath.item].logo ?? "")
        //        let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        //        cell.trophyImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_trophy_one"))
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: (trophiesCollectionView.frame.size.width/2.08)+3, height: trophiesCollectionView.frame.size.height/3)
       // return  CGSize(width:trophiesCollectionView.frame.size.width/2, height: trophiesCollectionView.frame.size.height/3)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 2
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 2
//    }
}

extension TrophiesOrMedalViewController {
    
    // Get Trophies Detail of HBCU's
    func trophiesMedalDetailAPI() {
        
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        if comingVC == true{
            self.user_id = userInfoModel.id as? String ?? "0"
        }else{
            self.user_id = comingVCTrophie?.id ?? ""
        }
       
      
        let urlName = "http://admin.iheartmyhbcu.org/api/user/awardedhbcu?id=\(user_id)"    //Live
        //let urlName = "http://phphosting.osvin.net/HBCU/api/user/awardedhbcu?id=\(user_id)"
        
        WebServiceClass.dataTask(urlName: urlName,  method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        if responeCode == false {
                            if let detailMessage = jsonResult["message"] as? String {
                                
                                if let detailData = jsonResult["result"] as? [Dictionary<String,AnyObject>] {
                                    
                                    if let detailDataInfo = Mapper<TrophiesAndMedalModel>().mapArray(JSONObject: detailData) {
                                        self.trophiesAndMedal = detailDataInfo
                                        print(self.trophiesAndMedal)
                                    }
                                }
                            } else {
                                print("something went wrong 1")
                            }
                        }
                    } else {
                        print("Something went wrong 2")
                    }
                } else {
                    print("Something went wrong 3")
                }
                self.trophiesCollectionView.reloadData()
            } else {
                print("Somthing went wrong 4")
            }
        }
    }
}
