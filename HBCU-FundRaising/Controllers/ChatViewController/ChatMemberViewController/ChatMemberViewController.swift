//
//  ChatMemberViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 10/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class ChatMemberViewController: UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var chatMemberTableView: UITableView!
    // Upper view
    @IBOutlet weak var mainUpperView: UIView!
    @IBOutlet weak var upperImgView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    var memberDetail = [listOfAllMemberModel]()
    var groupChatArray : ChatGroupModel?
    var chatMsgId : ChatMessageModel?
    var singleChatArray : ChatModel?
    
    var refreshControlTop : UIRefreshControl!
    var refreshControlBottom : UIRefreshControl!
    var skipCount : Int = 1
     var hbcuDetailShowData : ChatPersonDetailModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.refreshControlAPI()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
         self.navigationController?.navigationBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - refresh control For API
    internal func refreshControlAPI() {
        refreshControlTop = UIRefreshControl()
        refreshControlTop.addTarget(self, action: #selector(self.reloadAPI), for: .valueChanged)
        //        self.chatMemberTableView.addSubview(refreshControlTop) // not required when using UITableViewController
        refreshControlBottom = UIRefreshControl()
        refreshControlBottom.addTarget(self, action: #selector(self.reloadAPICount), for: .valueChanged)
        self.chatMemberTableView.bottomRefreshControl = refreshControlBottom
        self.manuallyRefreshTheTab()
    }
    
    func manuallyRefreshTheTab() {
        //First time automatically refreshing.
        refreshControlTop.beginRefreshingManually()
        self.perform(#selector(self.reloadAPI), with: nil, afterDelay: 0)
    }
    
    //MARK:- Reload API
    @objc func reloadAPI() {
        // Check network connection.
        self.skipCount = 1
        if Reachability.isConnectedToNetwork() == true {
            self.chatMemberList()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }
    
    //MARK:- Reload API
    @objc func reloadAPICount() {
        // Check network connection.
        self.skipCount = skipCount + 1
        if Reachability.isConnectedToNetwork() == true {
            self.chatMemberList()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
    }

    @IBAction func backBtnActn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.navigationBar.isHidden = false
    }
}

extension ChatMemberViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return memberDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "ChatMemberTableCell", for: indexPath) as! ChatMemberTableCell
        cell.userNameLbl.text = memberDetail[indexPath.row].name ?? ""
        
        if let image = memberDetail[indexPath.row].image {
            if image.contains("https://"){
                cell.userImagView.sd_setImage(with: URL(string: image ), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                cell.userImagView.contentMode = .scaleToFill
            } else {
                let userImage = Constants.APIs.imageBaseURL + image
                let urlString = userImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                cell.userImagView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.performSegue(withIdentifier: "memberDetail", sender: self)
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChatMemberDetailViewController" ) as! ChatMemberDetailViewController
        vc.memberMsgId = memberDetail[indexPath.row]
        vc.singleChatArray = self.singleChatArray
        vc.hbcuDetailShowData = hbcuDetailShowData
        vc.memberCheckStatus = true
        vc.comingVCStatus = false
        vc.checkStatus = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ChatMemberViewController {

    func chatMemberList() {
        
     //   WebServiceClass.showLoader(view: self.view)
        guard Methods.sharedInstance.getloginData() != nil else{
            return
        }
        let page = self.skipCount as AnyObject
        let paramsStr =  "hbcu=\(groupChatArray?.to_id ?? "0")&page=\(page)&type=\(groupChatArray?.type ?? "")"
        print(paramsStr)
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + Constants.APIs.allMembers, method: "POST", params: paramsStr) { (success, response, errorMsg) in
         //   WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        if responeCode == false {
                            if (jsonResult["message"] as? String) != nil {
                                if let memberData = jsonResult["result"] as? [Dictionary<String,AnyObject>] {
                                    if let memberInfo = Mapper<listOfAllMemberModel>().mapArray(JSONObject: memberData) {
                                        if self.skipCount == 1{
                                            self.memberDetail = memberInfo
                                        }else{
                                            self.memberDetail += memberInfo
                                        }
                                    }
                                    self.chatMemberTableView.reloadData()
                                }
                            } else {
                                print("something went wrong 1")
                            }
                        }
                    } else {
                        print("Something went wrong 2")
                    }
                    
                } else {
                    print("Something went wrong 3")
                }
            } else {
                print("Somthing went wrong 4")
            }
            DispatchQueue.main.async {
                self.chatMemberTableView.reloadData()
            }
        }
    }
}
