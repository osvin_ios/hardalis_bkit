//
//  TrophyCollectionViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 10/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class TrophyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var trophyImgView: ImageViewDesign!
    @IBOutlet var trophyDetailLabel: UILabel!
}
