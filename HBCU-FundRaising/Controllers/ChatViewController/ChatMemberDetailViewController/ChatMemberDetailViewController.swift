//
//  ChatMemberDetailViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 10/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class ChatMemberDetailViewController: UIViewController {
    
    @IBOutlet weak var upperCollectionView: UICollectionView!
    @IBOutlet weak var trophyCollectionView: UICollectionView!
   
    //main Backgroud View
    @IBOutlet weak var mainBackgroundView: UIView!
    @IBOutlet weak var upperImageView: UIImageView!
    @IBOutlet var userImageView: ImageViewDesign!
    @IBOutlet weak var TitleNameLabel: UILabel!
    
    // First View
    @IBOutlet weak var firstView: ViewDesign!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageButtonOutlet: ButtonDesign!
     @IBOutlet weak var seeAllBtnOutlet: ButtonDesign!
    // HBCU Label
    @IBOutlet weak var sharedhbcuLabel: UILabel!
    @IBOutlet weak var blockUserButtonOutlet: UIButton!
    @IBOutlet weak var blockUserView: ViewDesign!
    
    var otherUserDetail : otherUserProfileModel?
    var hbcuDetail = [otherUserProfileModel]()
    var userMedal = [otherUserProfileModel]()
    var userOrgDetail = [otherUserProfileModel]()
    var groupChatArray : ChatGroupModel?
    var singleChatArray : ChatModel?
    var chatMsgId : ChatMessageModel?
    var memberMsgId : listOfAllMemberModel?
    var chatId = String()
    var notificationID = String()
    var blockUserDataVlue : BlockedUserListModel?
    var blockUnblockDataVlue = [BlockedUserListModel]()
    var comingVCStatus : Bool = false
    var groupId = String()
    var checkStatus = Bool()
    var memberCheckStatus = Bool()
    var blockStatus = String()
    var userIDStatus = Bool()
    var likeUnlikeCheck = Bool()
    var msgChatBtn = true
    var likeUnlikeData : ChatMessageModel?
    var singleChatData : ChatModel?
    var hbcuDetailShowData : ChatPersonDetailModel?
    var chatMemberDetailId : otherUserProfileModel?
    var otherUserDetail1 : otherUserProfileModel?
    var comingUserCheck = true
    var notificationCheck : ChatMessageModel?
    var notificationBackUserID = true
    var commentUserId = String()
    var mostLovedId = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        self.otherUserProfileAPI()
        print(notificationBackUserID)
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool){
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool){
        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func blockUserBtnActn(_ sender: UIButton){
        if otherUserDetail?.blocked_status == "1"{
           // self.otherUserDetail?.blocked_status = "0"
            self.blockStatus = "Are you sure you want to Unblock this User"
        }else{
         //   self.otherUserDetail?.blocked_status = "1"
            self.blockStatus = "Are you sure you want to Block this User"
        }
        self.alertFunctionsWithCallBacks(title: blockStatus) { (isTrue) in
            if isTrue.boolValue{
                // Check network connection.
                if Reachability.isConnectedToNetwork() == true{
                    self.blockUnblockAPI()
                }else{
                    self.showAlertWithoutCallBack(messageTitle: AKErrorHandler.CommonErrorMessages.NO_INTERNET_AVAILABLE)
                }
            }
        }
    }
    
    @IBAction func backBtnActn(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func chatBtnActn(_ sender: UIButton){
        if msgChatBtn == true {
            let vc = storyboard?.instantiateViewController(withIdentifier: "ChatMessageViewController" ) as! ChatMessageViewController
            vc.chatMemberToChat = false
            vc.otherUserDetail = self.otherUserDetail
            vc.userMedal = self.userMedal
            vc.groupChatArrayDetailPage = self.groupChatArray
            vc.singleChatArrayDetailPage = self.singleChatData
            vc.chatMemberDetail = false
            self.navigationController?.navigationBar.isHidden = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.navigationController?.popViewController(animated: true)
            self.navigationController?.navigationBar.isHidden = true
        }
    }

    @IBAction func seeAllBtnActn(_ sender: Any){
        let vc = storyboard?.instantiateViewController(withIdentifier: "TrophiesOrMedalViewController" ) as! TrophiesOrMedalViewController
        vc.comingVC = false
        vc.comingVCTrophie = self.otherUserDetail
        self.navigationController?.pushViewController(vc, animated: true)
//      self.performSegue(withIdentifier: "trophiesAndMedal", sender: self)
    }
    
    @objc func GoToComment(_ sender : UIButton){
        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CommentsDonationVC") as! CommentsDonationVC
            currentNavVC.pushViewController(vc, animated: true)
        }
    }
}

extension ChatMemberDetailViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == upperCollectionView {
            return hbcuDetail.count
        } else if collectionView == trophyCollectionView {
            return userMedal.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == upperCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatMemberDetailUpperCollectionCell", for: indexPath) as! ChatMemberDetailUpperCollectionCell
            let imagebaseURl = Constants.APIs.imageBaseURL + (hbcuDetail[indexPath.item].logo ?? "")
            let urlString = imagebaseURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            cell.organizationImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic") )
            cell.organizationNameLabel.text = self.hbcuDetail[indexPath.item].title
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrophyCollectionViewCell", for: indexPath) as! TrophyCollectionViewCell
            
            if userMedal[indexPath.item].type == "medal"{
                if userMedal[indexPath.item].medal == "gold"{
                    cell.trophyImgView.image = UIImage(named: "ic_medal_gold.png")
                }else if userMedal[indexPath.item].medal == "silver"{
                    cell.trophyImgView.image = UIImage(named: "ic_medal_silver.png")
                }else{
                    cell.trophyImgView.image = UIImage(named: "ic_medal_bronze.png")
                }
            }else if userMedal[indexPath.item].type == "trophy" {
                if userMedal[indexPath.item].medal == "gold"{
                    cell.trophyImgView.image = UIImage(named: "ic_trophy_three")
                }else if userMedal[indexPath.item].medal == "silver"{
                    cell.trophyImgView.image = UIImage(named: "ic_trophy_two")
                }else{
                    cell.trophyImgView.image = UIImage(named: "ic_trophy_one")
                }
            }else{
                if userMedal[indexPath.item].medal == "ribbonyellow"{
                    cell.trophyImgView.image = UIImage(named: "ic_ribbon_yellow")
                }else{
                    cell.trophyImgView.image = UIImage(named: "ic_ribbon_blue")
                }
            }
            cell.trophyDetailLabel.text = userMedal[indexPath.row].title
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: self.view.frame.size.width/3, height: self.upperCollectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
}

extension ChatMemberDetailViewController {
    
    func otherUserProfileAPI(){
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let url = Constants.APIs.baseURL + "otherUserProfile"
     
        let user_id = userInfoModel.id as? String
        if notificationBackUserID == true {
            if likeUnlikeCheck == true {
                
                self.chatId = likeUnlikeData?.user_id ?? "0"
            }else{
                if mostLovedId == false {
                    if memberCheckStatus == false {
                        if checkStatus == true {
                            
                            self.chatId = chatMsgId?.user_id ?? "0"
                        }else{
                            if userIDStatus == true {
                                self.chatId = groupChatArray?.to_id ?? "0"
                            }else{
                                self.chatId = singleChatArray?.to_id ?? "0"
                            }
                        }
                    } else {
                        self.chatId = memberMsgId?.user_id ?? "0"
                    }
                }else{
                    self.chatId = commentUserId ?? "0"
                }
               
            }
        }else{
            self.chatId = notificationCheck?.user_id ?? "0"
        }
     
        let paramsStr = "user_id=\(user_id ?? "")&other_user_id=\(chatId)"       
       
        print(paramsStr)
        
        WebServiceClass.dataTask(urlName: url, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        if responeCode == false {
                            if (jsonResult["message"] as? String) != nil {
                                if let userHBCUData = jsonResult["result"] as? Dictionary<String,AnyObject> {
                                    
                                    if let userHBCUInfo = Mapper<otherUserProfileModel>().map(JSONObject: userHBCUData) {
                                        self.otherUserDetail = userHBCUInfo
                                        print(self.otherUserDetail ?? "")
                                    }
                                    if let HBCUDetail = userHBCUData["hbcu"] as? [Dictionary<String,AnyObject>] {
                                        
                                        if let hbcuDetails = Mapper<otherUserProfileModel>().mapArray(JSONObject : HBCUDetail){
                                            
                                            self.hbcuDetail  = hbcuDetails
                                            //                                            self.TitleNameLabel.text = self.hbcuDetail.title ?? ""
                                            print(self.hbcuDetail)
                                        }
                                    }
                                    if let otherUserMedal = userHBCUData["medal"] as? [Dictionary<String,AnyObject>] {
                                        if let medalDetails = Mapper<otherUserProfileModel>().mapArray(JSONObject : otherUserMedal){
                                            self.userMedal  = medalDetails
                                            if self.userMedal.count > 3 {
                                                self.seeAllBtnOutlet.isHidden = false
                                            }else{
                                                self.seeAllBtnOutlet.isHidden = true
                                            }
                                        }
                                    }
                                    if let orgUserDetail = userHBCUData["org"] as? [Dictionary<String,AnyObject>] {
                                        if let orgDetails = Mapper<otherUserProfileModel>().mapArray(JSONObject : orgUserDetail){
                                            self.userOrgDetail  = orgDetails
                                        }
                                    }
                                }
                                self.nameLabel.text = (self.otherUserDetail?.first_name ?? "") + (" ") + (self.otherUserDetail?.last_name ?? "")
                                self.TitleNameLabel.text = self.otherUserDetail?.title ?? ""
                                
                                if let image = self.otherUserDetail?.profile {
                                    if image.contains("https://"){
                                         self.upperImageView.sd_setImage(with: URL(string: image ), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                                         self.upperImageView.contentMode = .scaleToFill
                                    } else {
                                        let userImage = Constants.APIs.imageBaseURL + image
                                        let urlString = userImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                        self.upperImageView.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_dummy_profilepic"))
                                    }
                                }
                                self.userImageView.isHidden = true
                                if Int((self.otherUserDetail?.id) ?? "0") == Int(userInfoModel.id as! String){
                                    self.blockUserButtonOutlet.isHidden = true
                                    self.messageButtonOutlet.setImage(nil, for: .normal)
                                     self.messageButtonOutlet.isUserInteractionEnabled = false
                                    self.messageButtonOutlet.isHidden = true
                                    self.messageButtonOutlet.backgroundColor = UIColor.clear
                                    self.blockUserView.isHidden = true
                                }else{
                                    self.blockUserButtonOutlet.isHidden = false
                                    self.messageButtonOutlet.setImage(#imageLiteral(resourceName: "ic_chat"), for: .normal)
                                    self.messageButtonOutlet.isUserInteractionEnabled = true
                                    self.messageButtonOutlet.backgroundColor = UIColor.init(red: 47/255, green: 155/255, blue: 51/255, alpha: 1)
                                    self.messageButtonOutlet.isHidden = false
                                    self.blockUserView.isHidden = false
                                }
                                
                                if self.otherUserDetail?.blocked_status == "0"{
                                    self.blockUserButtonOutlet.setTitle("Block User",for: .normal)
                                    self.messageButtonOutlet.isHidden = false
                                }else{
                                    self.blockUserButtonOutlet.setTitle("Unblock User",for: .normal)
                                    self.messageButtonOutlet.isHidden  = true
                                }
                            } else {
                                print("something went wrong 1")
                            }
                        }
                    } else {
                        print("Something went wrong 2")
                    }
                } else {
                    print("Something went wrong 3")
                }
            } else {
                print("Something went wrong 4")
            }
            
            DispatchQueue.main.async {
                self.upperCollectionView.reloadData()
                self.trophyCollectionView.reloadData()
                //  self.blockUserButtonOutlet.setNeedsLayout()
                // self.blockUserButtonOutlet.layoutIfNeeded()
            }
        }
    }
        
    // Block Unblock User API
    
    func blockUnblockAPI(){
        //   WebServiceClass.showLoader(view: self.view)
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let to_id = otherUserDetail?.id
        let user_id = userInfoModel.id as? String
        let paramsStr = "from_id=\(user_id ?? "")&to_id=\(to_id!)"
        print(paramsStr)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + Constants.APIs.block_unBlock_User, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            //    WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    // do whatever with jsonResult
                    if let responeCode = jsonResult["error"] as? Bool {
                        if responeCode == false {
                            if (jsonResult["message"] as? String) != nil {
                                if let blockListData = jsonResult["result"] as? [Dictionary<String,AnyObject>] {
                                    if let blockInfo = Mapper<BlockedUserListModel>().mapArray(JSONObject: blockListData) {
                                        self.blockUnblockDataVlue = blockInfo
                                        print(self.blockUnblockDataVlue)
                                        
                                        if self.otherUserDetail?.blocked_status == "1"{
                                            self.blockUserButtonOutlet.setTitle("Block User",for: .normal)
                                            self.messageButtonOutlet.isHidden = false
                                            self.otherUserDetail?.blocked_status = "0"
                                        } else {
                                            self.blockUserButtonOutlet.setTitle("Unblock User",for: .normal)
                                            self.messageButtonOutlet.isHidden  = true
                                            self.otherUserDetail?.blocked_status = "1"
                                        }
                                    }
                                }
                            } else{
                                print("something went wrong 1")
                            }
                        }
                    }else{
                        print("Something went wrong 2")
                    }
                } else {
                    print("Something went wrong 3")
                }
                DispatchQueue.main.async {
                    self.upperCollectionView.reloadData()
                    self.trophyCollectionView.reloadData()
                    //                    self.blockUserButtonOutlet.setNeedsLayout()
                    //                    self.blockUserButtonOutlet.layoutIfNeeded()
                }
            } else {
                print("Somthing went wrong 4")
            }
        }
    }
}
