//
//  ChatMemberDetailCollectionCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 10/08/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ChatMemberDetailUpperCollectionCell: UICollectionViewCell {

    @IBOutlet weak var organizationImageView: ImageViewDesign!
    @IBOutlet weak var organizationNameLabel: UILabel!

}
