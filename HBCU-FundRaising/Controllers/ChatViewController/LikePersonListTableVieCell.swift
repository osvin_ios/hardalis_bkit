//
//  LikePersonListTableVieCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 24/10/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class LikePersonListTableVieCell: UITableViewCell {
    
    @IBOutlet weak var likePersonNameLabel: UILabel!
    @IBOutlet weak var likePersonImageView: UIImageView!
    @IBOutlet weak var upeprView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
