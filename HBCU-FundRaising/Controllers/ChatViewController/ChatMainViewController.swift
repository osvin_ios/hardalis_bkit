//
//  ChatMainViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 05/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ChatMainViewController: UIViewController {

    @IBOutlet var chatMessageTableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        chatMessageTableView.register(UINib(nibName: "TestTableViewCell", bundle: nil), forCellReuseIdentifier: "TestTableViewCell")
        
        chatMessageTableView.register(UINib(nibName: "SenderAudioChatTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderAudioChatTableViewCell")
        chatMessageTableView.register(UINib(nibName: "ReceiverAudioTableViewCell", bundle: nil), forCellReuseIdentifier: "ReceiverAudioTableViewCell")
        chatMessageTableView.register(UINib(nibName: "SenderAudioTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderAudioTableViewCell")
        chatMessageTableView.register(UINib(nibName: "SenderImageVideoTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderImageVideoTableViewCell")
        chatMessageTableView.register(UINib(nibName: "SenderTextMessageTableCell", bundle: nil), forCellReuseIdentifier: "SenderTextMessageTableCell")
        self.chatMessageTableView.delegate = self
        self.chatMessageTableView.dataSource = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ChatMainViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      let cell  = chatMessageTableView.dequeueReusableCell(withIdentifier: "ReceiverAudioTableViewCell", for: indexPath) as! ReceiverAudioTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 1500
    }
}


