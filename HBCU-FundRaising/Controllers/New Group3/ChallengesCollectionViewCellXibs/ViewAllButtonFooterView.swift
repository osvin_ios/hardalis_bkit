//
//  ViewAllButtonFooterView.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 22/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import Foundation

class ViewAllButtonFooterView: UICollectionReusableView {
    
    @IBOutlet weak var viewAllButton: UIButton!
    
    @IBAction func viewAllButtonPressed(_ sender: UIButton) {
    }
    
}

