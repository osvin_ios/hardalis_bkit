//
//  LeaderboardDetailCollectionViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 27/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class LeaderboardDetailCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var leaderboardImageView: UIImageView!
    @IBOutlet weak var throphyImageView: UIImageView!
    @IBOutlet weak var leaderboardTitleLabel: UILabel!
    @IBOutlet weak var leaderboardSubtitleLabel: UILabel!
    @IBOutlet weak var AmountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.viewDesign()
        
    }
    
    func viewDesign(){
        
        //containerView.layer.borderColor = UIColor.black.cgColor
        //containerView.layer.borderWidth = 0
        containerView.layer.cornerRadius = 2
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.shadowRadius = 1
        containerView.layer.shadowOffset = CGSize(width: 1, height: 1)
        //containerView.layer.masksToBounds = true
        //containerView.clipsToBounds = true
    }
    
}
