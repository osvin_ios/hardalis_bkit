//
//  AboutAndTermsAndConditionCollectionViewCell.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 27/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class AboutAndTermsAndConditionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backView: ViewDesign!
    @IBOutlet weak var MainTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//        setNeedsLayout()
//        layoutIfNeeded()
//        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
//        var frame = layoutAttributes.frame
//        frame.size.width = ceil(size.width)
//        layoutAttributes.frame = frame
//        return layoutAttributes
//    }
    
}
