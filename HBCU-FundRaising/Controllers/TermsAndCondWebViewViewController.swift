//
//  TermsAndCondWebViewViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 28/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class TermsAndCondWebViewViewController: UIViewController {

    @IBOutlet weak var myWebView: UIWebView!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.myWebView.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let url = URL (string: "http://iheartmyhbcu.org/pages/legal.php")
        let requestObj =  URLRequest(url: url!)
        myWebView.loadRequest(requestObj)
    }
}
