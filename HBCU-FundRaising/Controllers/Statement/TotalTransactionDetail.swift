//
//  TotalTransactionDetail.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 09/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class TotalTransactionDetail: UITableViewCell {

    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var UserNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
