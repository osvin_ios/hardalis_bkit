//
//  TransactionDetailsArrayView.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 09/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class TransactionDetailsArrayView: UITableViewCell {

    @IBOutlet weak var donatedAmount: UILabel!
    @IBOutlet weak var hbcuName: UILabel!
    @IBOutlet weak var dateLabelDonated: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
