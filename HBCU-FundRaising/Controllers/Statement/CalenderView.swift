//
//  calender.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 06/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import UIKit

class CalenderView : UIView {
    
    @IBAction func crossAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.removeFromSuperview()
        }
    }
    
    @IBOutlet weak var monthTableView: UITableView!
    @IBOutlet weak var monthHeight: NSLayoutConstraint!
    @IBOutlet weak var selectMonthButton: UIButton!
    @IBOutlet weak var selectYearButton: UIButton!
    
    @IBAction func okAction(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.removeFromSuperview()
        }
    }
    //MARK:- View Start
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let viewheader = UINib(nibName: "CalenderView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        viewheader.frame = bounds
        viewheader.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(viewheader)
    }
}
