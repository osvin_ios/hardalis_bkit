//
//  StatementsViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 03/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import FSCalendar
import MBProgressHUD
import Alamofire
import WebKit

class StatementsViewController: UIViewController{
    
    //MARK:- IBOutlet
    @IBOutlet weak var calender: FSCalendar!
    @IBOutlet weak var borderCalenderView: UIView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var transactionTableView: UITableView!
    @IBOutlet weak var endDateLabel: UITextField!
    @IBOutlet weak var selectedYearLabel: UILabel!
    @IBOutlet weak var selectedMonthLabel: UILabel!
    @IBOutlet weak var monthlyLabel: UILabel!
    @IBOutlet weak var fromDateLabel: UITextField!
    @IBOutlet weak var yearTableView: UITableView!
    @IBOutlet weak var dropDownTableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet var openChooseStartendView: UIView!
    @IBOutlet weak var yearTableHeight: NSLayoutConstraint!
    @IBOutlet weak var monthTableView: UITableView!
    @IBOutlet weak var monthHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewHoldingView: UIView!
    @IBOutlet var calenderView: UIView!
    @IBOutlet weak var yearView: UIView!
    @IBOutlet weak var monthView: UIView!
    @IBOutlet weak var transactionDetailViewHeight: NSLayoutConstraint!
    
    //MARK:- global variables
    var dropDoenarray = [String]()
    var monthArray    = [String]()
    var yearArray     = [String]()
    var fromDate : String!
    var nextMonth : String!
    var month : Int!
    var currentMonth : Int!
    var donationArray = [Dictionary<String, AnyObject>]()
    var name : String!
    var totalAmount : String!
    var from_date = String()
    var to_date = String()
    var toDate  : String!
    var didTapCal : Bool = false
    var stmtsPdfLink : String? = ""
    var webView = UIWebView()
    
    var selectedMonthIndex = Int()
    var selectedYearIndex = Int()
    var endDateYear = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        
        let buttonItemRight = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_download"), style: .done, target: self, action: #selector(downloadAction))
        self.navigationItem.rightBarButtonItem = buttonItemRight
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
        self.title = "Statements"
//        self.barButton()
        self.navigationController?.navigationBar.setBackgroundImage(image.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
        
        dropDoenarray = ["Monthly","Weekly"]
        monthArray = ["January","Feburary","March","April","May","June","July","August","Sepetember","October","November","December"]
        yearArray = ["2016","2017","2018","2019","2020","2021","2022","2023","2024","2025","2026","2027"]
        
        transactionTableView.register(UINib(nibName: "DonatedFirstCell", bundle: nil), forCellReuseIdentifier: "DonatedFirstCell")
        transactionTableView.register(UINib(nibName: "TotalTransactionDetail", bundle: nil), forCellReuseIdentifier: "TotalTransactionDetail")
        transactionTableView.register(UINib(nibName: "TransactionDetailsArrayView", bundle: nil), forCellReuseIdentifier: "TransactionDetailsArrayView")
        transactionTableView.register(UINib(nibName: "lastTransactionViewTableViewCell", bundle: nil), forCellReuseIdentifier: "lastTransactionViewTableViewCell")
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func downloadAction(){
        if monthlyLabel.text == "Monthly" {
            
            if self.fromDateLabel.text == "Select start Month" || self.endDateLabel.text == "Select end month" {
                self.showToast(message: "Please select start to end date")
            } else {
                self.hitGetTransactionAPI()
            }
        } else {
            
            if self.fromDateLabel.text == "Select start Date" || self.endDateLabel.text == "Select end Date" {
                self.showToast(message: "Please select start to end date")
            } else {
                self.hitGetTransactionAPI()
            }
        }
    }
    
    func savePdf(urlString:String, fileName:String){
        DispatchQueue.main.async {
            let url = URL(string: urlString)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "YourAppName-\(fileName).pdf"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            do {
                try pdfData?.write(to: actualPath, options: .atomic)
                self.showAlert("pdf successfully saved!")
                self.navigationController?.popViewController(animated: true)
                print("pdf successfully saved!")
            } catch {
                print("Pdf could not be saved")
            }
        }
    }
    
    func showSavedPdf(url:String, fileName:String){
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for url in contents {
                    if url.description.contains("\(fileName).pdf") {
                        // its your file! do what you want with it!
                        
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
    }
    
    // check to avoid saving a file multiple times
    func pdfFileAlreadySaved(url:String, fileName:String)-> Bool{
        var status = false
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for url in contents {
                    if url.description.contains("YourAppName-\(fileName).pdf") {
                        status = true
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
        return status
    }
    
    func downloadPDFFile(urlString:String){
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        
        hud.mode = MBProgressHUDMode.annularDeterminate
        
        hud.label.text = "Loading…"
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            
            let documentsURL:NSURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as    NSURL
            print("***documentURL: " ,documentsURL)
            let PDF_name : String = "Downloded_PDF_Name"
            
            let fileURL = documentsURL.appendingPathComponent(PDF_name)
            print("***fileURL: ",fileURL ?? "")
            return (fileURL!,[.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlString, to: destination).downloadProgress(closure: { (prog) in
            
            hud.progress = Float(prog.fractionCompleted)
            
        }).response { response in
            
            hud.hide(animated: true)
            
            if response.error == nil, let filePath = response.destinationURL?.path{
                
                print("File Path" ,filePath)
                
                //Open this filepath in Webview Object
                let fileURL = URL(fileURLWithPath: filePath)
                
                let request = URLRequest(url: fileURL)
                self.webView.loadRequest(request)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool){
        
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        
        let buttonItemRight = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_download"), style: .done, target: self, action: #selector(downloadAction))
        self.navigationItem.rightBarButtonItem = buttonItemRight
        
        let image = #imageLiteral(resourceName: "ic_navbar_bg")
        self.title = "Statements"
        
        self.monthView.layer.applySketchShadow()
        self.monthView.dropShadow()
        self.yearView.layer.applySketchShadow()
        self.yearView.dropShadow()
        self.borderView.layer.cornerRadius = 5
        self.borderCalenderView.layer.cornerRadius = 5
        self.tableViewHoldingView.dropShadow()
        self.tableViewHeight.constant = 0
        self.monthHeight.constant = 0
        self.yearTableHeight.constant = 0
        self.transactionDetailViewHeight.constant = 0
        
        self.calender.today = nil
        
        let currentDate = Date()
        self.calender.select(currentDate, scrollToDate: false)
        self.calender.setCurrentPage(currentDate, animated: true)
        
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        name = userInfoModel.first_name! + " " + userInfoModel.last_name!
    }

    //MARK:- IBAction
    @IBAction func crossAction(_ sender: UIButton){
        DispatchQueue.main.async {
            self.openChooseStartendView.removeFromSuperview()
        }
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.calenderView.removeFromSuperview()
            self.fromDateLabel.text = "Select start Date"
            self.endDateLabel.text = "Select end Date"
        }
    }
    
    @IBAction func okAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.openChooseStartendView.removeFromSuperview()
        }
    }
    
    @IBAction func okActionCal(_ sender: UIButton) {
        DispatchQueue.main.async {
            if self.didTapCal == true {
            } else {
                let currentDate = Date()
                let selectedDate = self.convertDateToString(instance: currentDate, instanceOf: "yyyy-MM-dd HH:mm:ss")
                let convertedDate = self.convertStringDateToLocal(dateofString: selectedDate)
                let finalDate = self.convertDateFormater(convertedDate)
                self.toDate = self.convertDate(convertedDate)
                print(self.toDate)
                self.fromDateLabel.text = finalDate
                let date = self.convertstringToDate(stringDate: convertedDate)
                self.dataPickerData(finalDate: date!)
            }
            self.calenderView.removeFromSuperview()
        }
    }
    
    @IBAction func SelectYearaction(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            //self.yearTableHeight.constant = 300
            self.yearTableHeight.constant = 150
        }else{
            self.yearTableHeight.constant = 0
        }
    }
    
    @IBAction func dropDownButtonPressed(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if dropDownButton.isSelected == true {
            self.tableViewHeight.constant = 80
            self.dropDownView.layer.applySketchShadow()
            self.dropDownView.dropShadow()
        } else {
            self.tableViewHeight.constant = 0
        }
    }
    
    @IBAction func openCalenderStart(_ sender: UIButton) {
        
        if monthlyLabel.text == "Monthly"{
            let view = self.openChooseStartendView
            self.monthHeight.constant = 0
            self.yearTableHeight.constant = 0
            view?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
            view?.layer.cornerRadius = 0.2
            self.selectedMonthLabel.text = "Month"
            self.selectedYearLabel.text = "Year"
            self.view.addSubview(view!)
        } else {
            let view = self.calenderView
            view?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
            view?.layer.cornerRadius = 0.2
            self.view.addSubview(view!)
        }
    }
    
    @IBAction func getTransactionButton(_ sender: UIButton){
        
        if monthlyLabel.text == "Monthly" {
            
            if self.fromDateLabel.text == "Select start Month" || self.endDateLabel.text == "Select end month" {
                self.showToast(message: "Please select start to end date")
            } else {
                self.hitGetTransactionAPI()
            }
        } else {
            
            if self.fromDateLabel.text == "Select start Date" || self.endDateLabel.text == "Select end Date" {
                self.showToast(message: "Please select start to end date")
            } else {
                self.hitGetTransactionAPI()
            }
        }
    }
    
    @IBAction func openMonthTableViewAction(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            //self.monthHeight.constant = 300
            self.monthHeight.constant = 150
        } else {
            self.monthHeight.constant = 0
        }
    }
    
    @IBAction func doneAction(_ sender: UIButton){
        if selectedMonthLabel.text == "Month" || selectedYearLabel.text  == "Year"{
            self.showToast(message: "Please select both month and year")
        } else {
            self.fromDateLabel.text = self.selectedMonthLabel.text! + " " +  "01" + "," + self.selectedYearLabel.text!
            self.fromDate  = self.fromDateLabel.text
            //self.endDateLabel.text = self.nextMonth + " " +  "01" + "," + self.selectedYearLabel.text!
            self.endDateLabel.text = self.nextMonth + " " +  "01" + "," + self.endDateYear
            
        }
    }
    
    //MARK:- functions
    
    func load(url: URL, to localUrl: URL, completion: @escaping () -> ()) {
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = try! URLRequest(url: url, method: .get)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Success: \(statusCode)")
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
                    completion()
                } catch (let writeError) {
                    print("error writing file \(localUrl) : \(writeError)")
                }
                
            } else {
                print("Failure: %@", error?.localizedDescription);
            }
        }
        task.resume()
    }
    
    func barButton(){
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
    }
    
    @objc func leftBar(sender: UIButton){
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
        }
    }
    
    func convertDateFormater(_ date: String) -> String{
        if date.isEmpty == false{
            let myDateString = date
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormatter.date(from: myDateString)!
            
            dateFormatter.dateFormat = "MMM dd, yyy"
            let somedateString = dateFormatter.string(from: myDate)
            return somedateString
            
        }
        return ""
    }
    
    func convertDate(_ date: String) -> String{
        if date.isEmpty == false {
            let myDateString = date
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormatter.date(from: myDateString)!
            
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let somedateString = dateFormatter.string(from: myDate)
            return somedateString
            
        }
        return ""
    }
    
    //MARK:- API
    func hitGetTransactionAPI(){
      
        WebServiceClass.showLoader(view: self.view)

        if monthlyLabel.text == "Monthly" {
            
            let months = self.month + 1
            
            if self.month == 0{
                from_date = self.selectedYearLabel.text! + "-" +  "\(12)" + "-" + "1"
                to_date  = self.endDateYear + "-" +  "\(months)" + "-" + "1"
            }else{
                from_date = self.selectedYearLabel.text! + "-" +  "\(self.month!)" + "-" + "1"
                //to_date  = self.selectedYearLabel.text! + "-" +  "\(months)" + "-" + "1"
                to_date  = self.endDateYear + "-" +  "\(months)" + "-" + "1"
            }

        } else {
//            from_date = self.fromDate
//            to_date  = self.toDate
            from_date = self.toDate
            to_date  = self.fromDate
            
        }
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id  = userInfoModel.id as? String 
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + "getDonationStatement?user_id=\(user_id ?? "")&from_date=\(from_date)&to_date=\(to_date)", method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject>{
                    if let result = jsonResult["result"] as? Dictionary<String, AnyObject>{
                        if let statements = result["statement"] as? Dictionary<String, AnyObject>{
                            if let donation = statements["donations"] as? [Dictionary<String, AnyObject>]{
                                if donation.count == 0 {
                                    self.showAlert("No record found")
                                } else {
                                    self.transactionDetailViewHeight.constant = 400
                                    self.donationArray = donation
                                    print(self.donationArray)
                                    if let pdfLink = result["pdflink"] as? String {
                                        self.stmtsPdfLink = pdfLink
                                        print(self.stmtsPdfLink)
                                        DispatchQueue.main.async {
                                            let pdfUrl = Constants.APIs.baseURL + self.stmtsPdfLink!
                                            print(pdfUrl)
                                            self.savePdf(urlString: pdfUrl , fileName: "abc")
                                        }
                                    }
                                }
                            }
                            if let totalDonatio = statements["total_donations"] as? String {
                                if totalDonatio.count > 0 {
                                    self.totalAmount = totalDonatio
                                }
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.transactionTableView.reloadData()
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
}

//MARK:- table view extension
extension StatementsViewController  :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case dropDownTableView:
            if tableView == dropDownTableView{
                return dropDoenarray.count
            }
        case monthTableView :
            if tableView == monthTableView{
                return monthArray.count
            }
        case transactionTableView :
            if tableView == transactionTableView {
                if section == 0 {
                    return 1
                } else if section == 1{
                    return 1
                } else if section == 2{
                    return donationArray.count
                } else if section == 3{
                    return 1
                }
            }
        default:
            if tableView == yearTableView {
                return yearArray.count
            }
        }
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == transactionTableView {
            return 4
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == transactionTableView {
            
            if indexPath.section == 0{
                let cell : DonatedFirstCell = (tableView.dequeueReusableCell(withIdentifier: "DonatedFirstCell", for: indexPath) as? DonatedFirstCell)!
                let fromDate = self.fromDateLabel.text!
                let toDate =  self.endDateLabel.text!
                cell.dateLabel.text = "From " +  fromDate + " " + "to" + " " + toDate
                return cell
            } else if indexPath.section == 1{
                let cell : TotalTransactionDetail = (tableView.dequeueReusableCell(withIdentifier: "TotalTransactionDetail", for: indexPath) as? TotalTransactionDetail)!
                cell.UserNameLabel.text = "Dear" + " " +  name
                cell.totalAmount.text = "$" +  totalAmount
                return cell
            } else if indexPath.section == 2{
                let value = donationArray[indexPath.row]
                let cell : TransactionDetailsArrayView = (tableView.dequeueReusableCell(withIdentifier: "TransactionDetailsArrayView", for: indexPath) as? TransactionDetailsArrayView)!
                let amount = value["amount"] as? String
                cell.donatedAmount.text = "$" + amount!
                cell.hbcuName.text = value["hbcutitle"] as? String
                let date = value["dateTime"] as? String
                let convertedDate = self.convertStringDateToLocal(dateofString: date!)
                let dateString = self.convertDateFormater(convertedDate)
                cell.dateLabelDonated.text =  dateString
                return cell
            }else{
                let cell : lastTransactionViewTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "lastTransactionViewTableViewCell", for: indexPath) as? lastTransactionViewTableViewCell)!
                return cell
            }
            
        } else {
            
            let cell : monthlyCellTableViewCell  = (tableView.dequeueReusableCell(withIdentifier: "dropDownCell", for: indexPath) as? monthlyCellTableViewCell)!
            switch  tableView {
            case dropDownTableView:
                let value = dropDoenarray[indexPath.row]
                cell.monthlyLabel.text = value
                cell.selectionStyle = .none
            case monthTableView :
                let value = monthArray[indexPath.row]
                cell.monthlyLabel.text = value
                cell.selectionStyle = .none
            default:
                let value = yearArray[indexPath.row]
                cell.monthlyLabel.text = value
                cell.selectionStyle = .none
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case dropDownTableView:
            let selectedRow = indexPath.row
            self.monthlyLabel.text = dropDoenarray[selectedRow]
            if monthlyLabel.text == "Monthly" {
                self.fromDateLabel.text = "Select start Month"
                self.endDateLabel.text = "Select end Month"
                self.transactionDetailViewHeight.constant = 0
            } else {
                self.fromDateLabel.text = "Select start Date"
                self.endDateLabel.text = "Select end Date"
                self.transactionDetailViewHeight.constant = 0
            }
            self.tableViewHeight.constant = 0
        case monthTableView:
//            let selectedRow = indexPath.row
//            self.selectedMonthLabel.text = monthArray[selectedRow]
//            self.nextMonth = monthArray[selectedRow + 1]
//            self.currentMonth = selectedRow
//            self.month = selectedRow + 1
//            self.monthHeight.constant = 0
            
            let selectedRow = indexPath.row
            self.selectedMonthLabel.text = monthArray[selectedRow]
            self.currentMonth = selectedRow
            self.monthHeight.constant = 0
            
            if selectedRow == 11{
                self.nextMonth = monthArray[0]
                self.month = 0
                
                self.selectedMonthIndex = 11
                
            }else{
                self.nextMonth = monthArray[selectedRow + 1]
                self.month = selectedRow + 1
                
                self.selectedMonthIndex = 0
                
            }
            
        default:
            let selectedRow = indexPath.row
            self.selectedYearLabel.text = yearArray[selectedRow]
            self.yearTableHeight.constant = 0
            
            if selectedRow == 11{
                self.selectedYearIndex = 11
            }else{
                self.selectedYearIndex = 0
            }
            
            if self.selectedMonthIndex == 11{
                if selectedRow == 11{
                    self.endDateYear = yearArray[0]
                }else{
                   self.endDateYear = yearArray[selectedRow+1]
                }
            }else{
                self.endDateYear = yearArray[selectedRow]
            }
          
        }
    }
    
    func dataPickerData(finalDate : Date) {
        let secondsInTen: TimeInterval = 7 * 24 * 60 * 60
        let dateSeven = Date.init(timeInterval: secondsInTen, since: finalDate)
        let selectedDate = self.convertDateToString(instance: dateSeven, instanceOf: "yyyy-MM-dd HH:mm:ss")
        let convertedDate = self.convertStringDateToLocal(dateofString: selectedDate)
        fromDate = self.convertDate(convertedDate)
        let final = self.convertDateFormater(convertedDate)
        self.endDateLabel.text = final
        print("\(dateSeven)")
    }
}

//MARK:- calender extension
extension StatementsViewController : FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.didTapCal = true
        print(date)
        let selectedDate = self.convertDateToString(instance: date, instanceOf: "yyyy-MM-dd HH:mm:ss")
        let convertedDate = self.convertStringDateToLocal(dateofString: selectedDate)
        let finalDate = self.convertDateFormater(convertedDate)
        toDate = self.convertDate(convertedDate)
        print(toDate)
        self.fromDateLabel.text = finalDate
        let date = self.convertstringToDate(stringDate: convertedDate)
        self.dataPickerData(finalDate: date!)
    }
}

extension CALayer {
    func applySketchShadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0)
    {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        }else{
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}
