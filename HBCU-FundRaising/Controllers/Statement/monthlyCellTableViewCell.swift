//
//  monthlyCellTableViewCell.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 06/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class monthlyCellTableViewCell: UITableViewCell {

    @IBOutlet weak var monthlyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
