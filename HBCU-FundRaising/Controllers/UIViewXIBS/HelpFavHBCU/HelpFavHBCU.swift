//
//  HelpFavHBCU.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 13/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class HelpFavHBCU: UIViewController {

    var gradientLayer: CAGradientLayer!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        createGradientLayer()
    }
    @IBAction func crossAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.view.bounds
        
        gradientLayer.colors = [UIColor(hexString: "#538795").cgColor, UIColor(hexString: "#09203f").cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
}
