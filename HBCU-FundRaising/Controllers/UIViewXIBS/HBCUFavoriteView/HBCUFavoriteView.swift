//
//  HBCUFavoriteView.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 18/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class HBCUFavoriteView: UIView {
    
    var parentInstance: UIViewController?
    @IBOutlet var nameLabel: UILabel!
    var parentVC : UIViewController?
    
    //MARK:- View Start
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let viewheader = UINib(nibName: "HBCUFavoriteView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        viewheader.frame = bounds
        viewheader.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(viewheader)
    }

    @IBAction func viewFavoriteHBCuAction(_ sender: Any) {
        
        DispatchQueue.main.async {
            self.removeFromSuperview()
            UserDefaults.standard.set(true, forKey: "loginsession")
            CallBackMethods.appDelegate?.changeRootViewControllerMakeFav()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FavHBCUViewController") as! FavHBCUViewController
//            vc.navigationController?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .plain, target: self, action: #selector(backbutonActn)
            vc.comingFromAccout = true
            self.parentVC?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func crossAction(_ sender: Any) {
        DispatchQueue.main.async {
            self.removeFromSuperview()
            UserDefaults.standard.set(true, forKey: "loginsession")
            CallBackMethods.appDelegate?.changeRootViewController()
        }
    }
}
