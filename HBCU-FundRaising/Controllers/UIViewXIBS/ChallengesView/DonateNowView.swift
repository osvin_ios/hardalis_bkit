//
//  DonateNowView.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 27/09/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

protocol ShowDonateViewDismissProtocol {
    func dismiss(hit : Bool)
    func donateButtonPressed(hit : Bool)
}
//protocol dismissKeyboardProtocol {
//    func dismissKeyboard(hit : Bool)
//}

import Foundation
import UIKit
// import IQKeyboardManagerSwift

class DonateNowView: UIView {
    
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var donateButton: UIButton!
    
    var showDonateViewDelegate : JoinNowViewController?
    var showDonateViewDelegate1 : BonusChallengeDetailViewController?
    var showDonateViewDelegate2 : ChatDetailViewController?
    
    
    //    MARK:- view life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib ()
        
        //self.amountTextField.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let view = UINib(nibName: "DonateNowView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
    }
    
    
    @IBAction func crossButtonPressed(_ sender: UIButton) {
        
        showDonateViewDelegate?.dismiss(hit: true)
        showDonateViewDelegate1?.dismiss(hit: true)
        
    }
    
    @IBAction func donateButtonPressed(_ sender: UIButton) {
        showDonateViewDelegate?.donateButtonPressed(hit: true)
        showDonateViewDelegate1?.donateButtonPressed(hit: true)
    }
    
}

extension DonateNowView : UITextFieldDelegate{
    
    
    func textFieldDidEndEditing(_ textField: UITextField){
        self.amountTextField.text = amountTextField.text
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        return false
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        return true
    }
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
//
//        //DonateNowView.endEditing(true)
//
//        showDonateViewDelegate?.dismissKeyboard()
//
//        //Disable iQkeyboarad
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
//        IQKeyboardManager.shared.enable = true
//        IQKeyboardManager.shared.enableAutoToolbar = true
//        return true
//
//    }
}
