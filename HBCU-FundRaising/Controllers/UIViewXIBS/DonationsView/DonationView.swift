//
//  DonationView.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 03/07/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import Foundation
import UIKit
//import IQKeyboardManagerSwift

class DonationView: UIView {
    
    @IBOutlet weak var donationBorderView: UIView!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var donateButton: UIButton!
    
    @IBAction func crossAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.removeFromSuperview()
        }
    }
    
    //MARK:- View Start
    override init(frame: CGRect){
        super.init(frame: frame)
        loadViewFromNib()
        self.amountTextField.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let viewheader = UINib(nibName: "DonationView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        viewheader.frame = bounds
        viewheader.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(viewheader)
    }
    
}
extension DonationView : UITextFieldDelegate {
   
    func textFieldDidEndEditing(_ textField: UITextField) {
       self.amountTextField.text = amountTextField.text
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return false
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        return true
    }
}
