//
//  ImagePreviewOfDetailViewController.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 06/12/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class ImagePreviewOfDetailViewController: UIViewController {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var profile = String()
    var labelName = String()
    var lastname = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if profile.contains("https://"){
            self.imageView.sd_setImage(with: URL(string: profile), placeholderImage: nil)
        }else{
            let image = Constants.APIs.imageBaseURL + profile
            self.imageView.sd_setImage(with: URL(string: image), placeholderImage: nil)
        }
        
        self.usernameLabel.text = labelName + " " + lastname
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func backButtonActn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
