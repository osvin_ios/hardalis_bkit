 //
//  DonationViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 20/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import Stripe
import PassKit
import Alamofire

class DonationViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet var recurringView: UIView!
    @IBOutlet weak var homeView: UIView!
    @IBOutlet var oneTimeDenationView: UIView!
    @IBOutlet weak var spareChangeUnderline: UIView!
    @IBOutlet weak var recurringUnderline: UIView!
    @IBOutlet weak var onetimeUnderline: UIView!
    @IBOutlet weak var spareChangeButton: UIButton!
    @IBOutlet weak var recurringButton: UIButton!
    @IBOutlet weak var oneTimeButton: UIButton!
    @IBOutlet var spareChangeView: UIView!
    @IBOutlet weak var bankLogoSpareChange: UIImageView!
    @IBOutlet weak var bankNameSpareChange: UILabel!
    @IBOutlet weak var weeklyToggleButton: ButtonDesign!
    @IBOutlet weak var monthluToggleButton: ButtonDesign!
    @IBOutlet weak var donationLabelRecuuring: UILabel!
    @IBOutlet weak var dollarAmountRecuuring: UILabel!
    @IBOutlet weak var donatingPeriodRecurring: UILabel!
    @IBOutlet weak var youAreDonatingRecuuringLabel: UILabel!
    @IBOutlet weak var reoccuringDonationButton: UIButton!
    @IBOutlet weak var selectHBCUBVutton: UIButton!
    @IBOutlet weak var spareChangeOn: UILabel!
    @IBOutlet weak var spareChangeTExt: UILabel!
    @IBOutlet weak var kinkedAccountLabel: UILabel!
    @IBOutlet weak var linkedAccountView: ViewDesign!
    @IBOutlet weak var spareChange: ViewDesign!
    
    @IBOutlet weak var donateWithApplePayButton: ButtonDesign!
    
    //MARK:- variables
    var activeStatus : String?
    var recurringActiveStatus : String?
    var recurringArray = [RecurringModel]()
    var dataRepetion : String?
    var amount : String?
    var reoccuringId : String? = ""
    var selectedHBCUArray : HBCUListModel?
    var otherAmount : String?
    var HBCUID : String? = "0"
    var mode = SelectHBCUEnum.oneTime
    var name : String? = ""
    var cycle : String?
    var titleHBCU : String?
    var isComingFromDivine9 = Bool()
    
    //Apple Pay
    var selectedPlanName = "Weekly"
    var customerId = ""
    var productId = ""
    var planId = ""
    var subscriptionId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isComingFromDivine9 == true {
 
            //amount = "5"
            recurringUnderline.backgroundColor = UIColor.clear
            onetimeUnderline.backgroundColor = UIColor.white
            spareChangeUnderline.backgroundColor = UIColor.clear
            recurringView.removeFromSuperview()
            oneTimeDenationView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            homeView.addSubview(oneTimeDenationView)
            
            self.title = "Donate"
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
            self.navigationItem.hidesBackButton = true
            
//            self.title = "Donate"
//            navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
//
//            if Reachability.isConnectedToNetwork() == true {
//                self.spareChangeAPI()
//            } else {
//                self.showAlert(Constants.commomInternetmsz.checkInternet)
//            }
//            self.navigationItem.hidesBackButton = true

        } else {
            
            if mode == .ComingFromHBCUDonation {
                homeView.addSubview(oneTimeDenationView)
                weeklyToggleButton.isSelected = true
                weeklyToggleButton.isUserInteractionEnabled = false
                spareChangeUnderline.backgroundColor = UIColor.clear
                recurringUnderline.backgroundColor   = UIColor.clear
                onetimeUnderline.backgroundColor     = UIColor.white
                oneTimeDenationView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                self.donationLabelRecuuring.text = "Start donating $5 per week today"
                
                self.title = "Donate"
                navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
                self.navigationItem.hidesBackButton = true
                
            } else {
                
                amount = "5"
                self.weeklyToggleButton.isSelected = true
                self.weeklyToggleButton.isUserInteractionEnabled = false
                spareChangeUnderline.backgroundColor = UIColor.white
                recurringUnderline.backgroundColor   = UIColor.clear
                onetimeUnderline.backgroundColor     = UIColor.clear
                spareChangeView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                homeView.addSubview(spareChangeView)
                
                self.title = "Donate"
                navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
                
                if Reachability.isConnectedToNetwork() == true {
                    self.spareChangeAPI()
                } else {
                    self.showAlert(Constants.commomInternetmsz.checkInternet)
                }
                self.navigationItem.hidesBackButton = true
            }
        }
    }
    
    @objc func leftBar(sender: UIButton) {
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            print("drawer opened")
        }
    }
    
    //MARK:- IBACtion
    @IBAction func dollar50Donation(_ sender: UIButton){
        self.goToSelectHBCU(amount: "50")
    }
    
    @IBAction func dollar40Donation(_ sender: UIButton) {
        self.goToSelectHBCU(amount: "40")
    }
    
    @IBAction func dollar20Donation(_ sender: UIButton) {
        self.goToSelectHBCU(amount: "20")
    }
    
    @IBAction func dollar5Donation(_ sender: UIButton) {
        self.goToSelectHBCU(amount: "5")
    }
    
    @IBAction func spareChangeView(_ sender: UIButton) {
        self.spareChangeAPI()
        oneTimeDenationView.removeFromSuperview()
        recurringUnderline.backgroundColor = UIColor.clear
        onetimeUnderline.backgroundColor = UIColor.clear
        spareChangeUnderline.backgroundColor = UIColor.white
        spareChangeView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        homeView.addSubview(spareChangeView)
    }
    
    
    @IBAction func recuuringViewButton(_ sender: UIButton) {
        self.getUserDetail()
        
        
        self.hitGetRecuuringDonationAPI()
        oneTimeDenationView.removeFromSuperview()
        recurringUnderline.backgroundColor = UIColor.white
        onetimeUnderline.backgroundColor = UIColor.clear
        spareChangeUnderline.backgroundColor = UIColor.clear
        recurringView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        homeView.addSubview(recurringView)
    }
    
    
    
    @IBAction func oneTimeView(_ sender: UIButton) {
        
        recurringUnderline.backgroundColor = UIColor.clear
        onetimeUnderline.backgroundColor = UIColor.white
        spareChangeUnderline.backgroundColor = UIColor.clear
        recurringView.removeFromSuperview()
        oneTimeDenationView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        homeView.addSubview(oneTimeDenationView)
    }
    
    @IBAction func recurringEditFunction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "otherAmount") as! OtherAmountDonate
        vc.mode = .SelectFromReocuuring
        vc.selectOtherAmount = otherAmount
        vc.completionBlock = { [weak self] (modelArrayObject) in
            
            guard let strongSelf = self else { return }
            
            if let otherPrice = modelArrayObject as? String {
                
                strongSelf.otherAmount =  otherPrice
                self?.dollarAmountRecuuring.text = "$" + otherPrice
                if self?.monthluToggleButton.isSelected == true {
                    self?.donationLabelRecuuring.text = "Start donating " + " " + "$" +  otherPrice + " " + "per month today"
                } else {
                    self?.donationLabelRecuuring.text = "Start donating " + " " + "$" + otherPrice + " " + "per week today"
                }
                self?.amount = otherPrice
            }
            
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func toggleWeeklyDonation(_ sender: ButtonDesign) {
        sender.isSelected = !sender.isSelected
        if weeklyToggleButton.isSelected == true{
            monthluToggleButton.isUserInteractionEnabled = true
            weeklyToggleButton.isUserInteractionEnabled = false
            donationLabelRecuuring.text = "Start donating $5 per week today"
            amount = "5"
            dollarAmountRecuuring.text = "$" + amount!
            donatingPeriodRecurring.text = "Weekly"
            monthluToggleButton.isSelected = false
            self.selectedPlanName = "Weekly"
        } else {
        }
    }
    
    @IBAction func toggleMonthlyDonation(_ sender: ButtonDesign) {
        sender.isSelected = !sender.isSelected
        if monthluToggleButton.isSelected == true {
            weeklyToggleButton.isUserInteractionEnabled = true
            monthluToggleButton.isUserInteractionEnabled = false
            donationLabelRecuuring.text = "Start donating $35 per month today"
            amount = "35"
            dollarAmountRecuuring.text = "$" + amount!
            donatingPeriodRecurring.text = "Monthly"
            weeklyToggleButton.isSelected = false
            self.selectedPlanName = "Monthly"
        }else{
        }
    }
    
    @IBAction func selectHBCUAction(_ sender: Any) {
        print("Testing")
        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SelectHBCUToDonateVC") as! SelectHBCUToDonateVC
            vc.donateValue = amount ?? ""
            vc.enumCases = .recuuring
            vc.selectedHBCUPost = selectedHBCUArray

            vc.completionBlock = { [weak self] (modelArrayObject) in
                guard let strongSelf = self else { return }

                if modelArrayObject is HBCUListModel {
                    strongSelf.selectedHBCUArray = modelArrayObject as? HBCUListModel
                    self?.name = self?.selectedHBCUArray?.title ?? ""
                    self?.HBCUID = self?.selectedHBCUArray?.id
                    self?.youAreDonatingRecuuringLabel.text = "You are donating to " + " " + (self?.name)!
                    self?.reoccuringDonationButton.isUserInteractionEnabled = true
                    self?.donateWithApplePayButton.isUserInteractionEnabled = true
                    self?.reoccuringDonationButton.backgroundColor = UIColor(red: 2/255, green: 182/255, blue: 91/255, alpha: 1)
                    self?.selectHBCUBVutton.setTitle("Change HBCU", for: .normal)
                    self?.recurringButton.isSelected = true
                }
            }
            currentNavVC.pushViewController(vc, animated: true)
        }
    }
    
    //    @IBAction func selectHBCUAction(_ sender: UIButton) {
//        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "SelectHBCUToDonateVC") as! SelectHBCUToDonateVC
//            vc.donateValue = amount ?? ""
//            vc.enumCases = .recuuring
//            vc.selectedHBCUPost = selectedHBCUArray
//
//            vc.completionBlock = { [weak self] (modelArrayObject) in
//                guard let strongSelf = self else { return }
//
//                if modelArrayObject is HBCUListModel {
//
//                    strongSelf.selectedHBCUArray = modelArrayObject as? HBCUListModel
//
//                    self?.name = self?.selectedHBCUArray?.title ?? ""
//                    self?.HBCUID = self?.selectedHBCUArray?.id
//                    self?.youAreDonatingRecuuringLabel.text = "You are donating to " + " " + (self?.name)!
//                    self?.reoccuringDonationButton.isUserInteractionEnabled = true
//                    self?.reoccuringDonationButton.backgroundColor = UIColor(red: 2/255, green: 182/255, blue: 91/255, alpha: 1)
//                    self?.selectHBCUBVutton.setTitle("Change HBCU", for: .normal)
//                    self?.recurringButton.isSelected = true
//                }
//            }
//            currentNavVC.pushViewController(vc, animated: true)
//        }
//    }
    
    @IBAction func donateWithApplePayButtonPressed(_ sender: ButtonDesign) {
        self.alertFunctionsWithCallBack(title: "Are you sure you want to Donate?") { (isObject) in
            if isObject.boolValue {
                if Reachability.isConnectedToNetwork() == true {
                    self.setupApplePay()
                } else {
                    self.showAlert(Constants.commomInternetmsz.checkInternet)
                }
            }
        }
    }
    
    @IBAction func donateButtonRecurring(_ sender: UIButton) {
        self.alertFunctionsWithCallBack(title: "Are you sure you want to Donate?") { (isObject) in
            if isObject.boolValue {
                if Reachability.isConnectedToNetwork() == true {
                    self.hitRecurringDonationAPI()
                } else {
                    self.showAlert(Constants.commomInternetmsz.checkInternet)
                }
            }
        }
    }
    
    //MARK:- function
    func goToSelectHBCU(amount : String){
        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SelectHBCUToDonateVC") as! SelectHBCUToDonateVC
            vc.donateValue = amount
            currentNavVC.pushViewController(vc, animated: true)
        }
    }
    
    func getUserDetail(){
        
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let user_id  = userInfoModel.id as? String
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + "getUserDetails?user_id=\(user_id ?? "0")", method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let result = jsonResult["result"] as? Dictionary<String, AnyObject>{
                      //  self.reoccurring = result["isActivereoccuring"] as? String
                        isActivereoccuring = (result["isActivereoccuring"] as? String)!
                        isActivesparechange = (result["isActivesparechange"] as? String)!
                        if isActivereoccuring == "0" {
                            self.showAlertWithoutCallBack(messageTitle: "Your Recurring status is inactive!")
                        }else{}
                    }
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
    //spare change view api
    func spareChangeAPI(){
        
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id        = userInfoModel.id as? String
        
        let paramsStr = "user_id=\(user_id ?? "")"
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + "getDonationstatusDetail", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        print("Error")
                    } else {
                        if let resultValue = jsonResult["result"] as? Dictionary<String, AnyObject> {
                            self.activeStatus = resultValue["activestatus"] as? String
                        
                            if self.activeStatus == "0" {
                                self.spareChangeOn.text = "There is an error"
                                self.spareChangeButton.setImage(#imageLiteral(resourceName: "ic_caution"), for: .normal)
                                self.spareChangeTExt.text = "Your Spare Change is Off. To donate your spare change you have to switch on your Spare Change Donation from Settings. "
                                self.kinkedAccountLabel.isHidden = true
                                self.linkedAccountView.isHidden = true
                                self.spareChange.dropShadow()
                            } else {
                                self.spareChangeOn.text = "Your Spare Change is On"
                                self.spareChangeButton.setImage(#imageLiteral(resourceName: "ic_round_tick_green_outline"), for: .normal)
                                self.spareChangeTExt.text = "When you make a purchase using your debit card, we will donate your spare change."
                                self.kinkedAccountLabel.isHidden = true
                                self.linkedAccountView.isHidden = true
                                self.spareChange.dropShadow()
                            }
                            if let bankDetail =  resultValue["bankdetail"] as? Dictionary<String, AnyObject> {
                                if let bankDetails = bankDetail["institution"] as? Dictionary<String, AnyObject> {
                                    self.bankNameSpareChange.text = bankDetails["brand_name"] as? String
                                    self.bankLogoSpareChange.image =  self.base64Convert(base64String: bankDetails["logo"] as? String)
                                    self.linkedAccountView.dropShadow()
                                    self.kinkedAccountLabel.text = "Linked Account"
                                    self.kinkedAccountLabel.isHidden = false
                                    self.linkedAccountView.isHidden = false
                                }
                            }
                        }
                    }
                } else {
                    print("Data is not in proper format")
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
    //convert
    func base64Convert(base64String: String?) -> UIImage{
        if (base64String?.isEmpty)! {
            return #imageLiteral(resourceName: "no_image_found")
        }else {
            // !!! Separation part is optional, depends on your Base64String !!!
            let dataDecoded : Data = Data(base64Encoded: base64String!, options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            return decodedimage ?? #imageLiteral(resourceName: "ic_dummy_card")
        }
    }
    
    //MARK:- hit api to donate recurring
    func hitGetRecuuringDonationAPI(){
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id        = userInfoModel.id as? String
        let paramsStr = "user_id=\(user_id ?? "")"
        let url = Constants.APIs.baseURL + "getReoccurringDonation?" + paramsStr
        
        WebServiceClass.dataTask(urlName:  url , method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        self.showAlert((jsonResult["message"] as? String)!)
                        self.reoccuringDonationButton.isUserInteractionEnabled = false
                        self.donateWithApplePayButton.isUserInteractionEnabled = false
                    } else {
                        if let resultValue = jsonResult["result"] as? Dictionary<String, AnyObject> {
//                            self.recurringActiveStatus = resultValue["isActive"] as? String
//                            if self.recurringActiveStatus == "0"{
//                                self.showAlertWithoutCallBack(messageTitle: "Your recurring status is inactive")
//                            }

                            if let recurringData = Mapper<RecurringModel>().map(JSONObject: resultValue) {
                                self.recurringArray.append(recurringData)
                                self.reoccuringId = recurringData.reoccuringid
                                self.HBCUID = recurringData.hbcu_id
                                self.amount = recurringData.amount
                                self.cycle = resultValue["cycle"] as? String
                                self.titleHBCU = recurringData.title
                                
                                self.selectHBCUBVutton.setTitle("Change HBCU", for: .normal)
                                DispatchQueue.main.async {
                                    self.reoccuringDonationButton.isUserInteractionEnabled = true
                                    self.donateWithApplePayButton.isUserInteractionEnabled = true
                                    self.reoccuringDonationButton.backgroundColor = UIColor(red: 2/255, green: 182/255, blue: 91/255, alpha: 1)
                                    if self.recurringArray.count > 0 {
                                        if self.cycle == "0"{
                                            self.dataRepetion = "Week"
                                            self.weeklyToggleButton.isSelected = true
                                            self.weeklyToggleButton.isUserInteractionEnabled = false
                                            self.monthluToggleButton.isUserInteractionEnabled = true
                                            self.monthluToggleButton.isSelected = false
                                            self.donatingPeriodRecurring.text = "Weekly"
                                            self.selectedPlanName = "Weekly"
                                        } else {
                                            self.dataRepetion = "Month"
                                            self.monthluToggleButton.isSelected = true
                                            self.monthluToggleButton.isUserInteractionEnabled = false
                                            self.weeklyToggleButton.isUserInteractionEnabled = true
                                            self.weeklyToggleButton.isSelected = false
                                            self.donatingPeriodRecurring.text = "Monthly"
                                            self.selectedPlanName = "Monthly"
                                        }
                                        self.donationLabelRecuuring.text = "Start donating " + "$" + "\(self.amount!)" + " per" + " " + "\(self.dataRepetion ?? "")" + " " + "today"
                                        self.youAreDonatingRecuuringLabel.text = "You are donating to " + "\(self.titleHBCU ?? "")"
                                        self.dollarAmountRecuuring.text = "$" + self.amount!
                                    }
                                }
                            }
                        }
                    }
                } else {
                    print("Data is not in proper format")
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    // MARK:- get recurring details api
    func hitRecurringDonationAPI(){
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let cycle : String?
        let user_id        = userInfoModel.id as? String
        if donatingPeriodRecurring.text == "Monthly"{
            cycle = "1"
        } else {
            cycle = "0"
        }
        let card_id = ""
        
        if reoccuringId == nil {
            reoccuringId = ""
        } else {
        }
        let hbcu_id = HBCUID ?? ""
        
        let paramsStr = "user_id=\(user_id ?? "")&cycle=\(cycle ?? "")&amount=\(amount ?? "")&card_id=\(card_id )&id=\(reoccuringId! )&hbcu_id=\(hbcu_id )"
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + "addReoccurringDonation", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    if let responeCode = jsonResult["error"] as? Bool, responeCode {
                        self.showAlert((jsonResult["message"] as? String)!)
                    }
                    else {
                        print(jsonResult)
                        if let result = jsonResult["result"] as? Dictionary<String, AnyObject> {
                            self.reoccuringId = result["id"] as? String
                        }
                        DispatchQueue.main.async {
                            let view = AlertView()
                            view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
                            view.alertView.layer.cornerRadius = 10
                            view.alertView.layer.borderColor = UIColor.black.cgColor
                            self.view.addSubview(view)
                        }}
                }} else {
                print("Data is not in proper format")
            }
        }
    }
}

extension UIView {
    func dropShadow() {
        layer.cornerRadius = 1
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.lightGray.cgColor
    }
 }

 extension DonationViewController : PKPaymentAuthorizationViewControllerDelegate{
    
    func setupApplePay(){
        
        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: Constants.ApplePayMerchantID)
        paymentRequest?.currencyCode = "USD"
        paymentRequest?.countryCode = "US"
       
        if let amount = Int(amount ?? ""){
            paymentRequest?.paymentSummaryItems = [
                PKPaymentSummaryItem(label: "", amount: NSDecimalNumber(value: amount))
            ]
            //Payment networks array
            let paymentNetworks = [PKPaymentNetwork.amex, .visa, .masterCard, .discover]
            paymentRequest?.supportedNetworks = paymentNetworks
            paymentRequest?.merchantCapabilities = .capability3DS
        }
        
        if Stripe.canSubmitPaymentRequest(paymentRequest) {
            var paymentController: PKPaymentAuthorizationViewController?
            paymentController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest!)
            paymentController?.delegate = self
            paymentController?.modalPresentationStyle = .formSheet
            
            if let paymentController = paymentController {
                present(paymentController, animated: true, completion: nil)
            }
        } else {
            //"Cannot submit payment request"
            print("Apple Pay is not available on this device")
            //self.showAlert("Apple Pay is not available on this device or May needs to set up the Apple Pay on the device")
            self.showAlert("You need to set up the Apple Pay on the device")
        }
        
    }
    
    //This function is called when Apple Pay payment will be authorized.
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void){
        // Payment token can be found like this
        print(payment.token)
        // Get response from the server and set the PKPaymentAuthorizationStatus
        let status = PKPaymentAuthorizationStatus(rawValue: 0)!
        
        switch status.rawValue {
        case 0:
            
            //perform Functionality on Apple Pay Successfull Payment
            
            //Stripe.setDefaultPublishableKey("pk_test_ho0D4zAZULYWZcvNHEHFasyE")
            Stripe.setDefaultPublishableKey("pk_live_egNBOvzPd1DewC3sOGQe4H0c") //live
            
            STPAPIClient.shared().createToken(with: payment) {
                (token, error) -> Void in
                
                if (error != nil) {
                    print("%@", error!)
                    completion(PKPaymentAuthorizationStatus.failure)
                    return
                }
                
                //ForRecurringDonation
                if let token = token{
                    self.hitAPIToCreateCustomerID(token: token, VC: controller, completion: completion)
                }
                
            }
            
        default:
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
    //This function is called when Apple Pay payment authorization is finished.
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController){
        self.dismiss(animated: true, completion: nil)
    }
    
 }

 extension DonationViewController{
    
    //"sk_test_0HEIX5gE8pRO5X5xuSPrPczU"
    //"sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
    
    //MARK:- hit api to create customer ID from token ID
    func hitAPIToCreateCustomerID(token: STPToken, VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)) {
        
        let url = "https://api.stripe.com/v1/customers"
        let email = ""   //"king182003@gmail.com"
        let token = token
        
        var dict = Dictionary<String, Any>()
        
        dict = ["email" :email ,
                "source" : token]
        let header = [
            "Content-Type"   : "application/x-www-form-urlencoded",
            "Authorization"  : "Bearer sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        ]
        WebServiceClass.showLoader(view: self.view)
        Alamofire.request(url , method: .post, parameters: dict, encoding:  URLEncoding.httpBody, headers: header ).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                if let json = response.result.value as? [String: Any] {
                    print("JSON: \(json)") // serialized json response
                    if let data = json["sources"] as? Dictionary<String,AnyObject> {
                        if let custData = data["data"] as? [Dictionary<String,AnyObject>]{
                            if let custId = custData[0]["customer"] as? String {
                                
                                self.customerId = custId
                                
                                DispatchQueue.main.async {
                                    self.hitAPIToCreateProduct(VC: VC, completion: completion)
                                }
                                
                            }else{
                                resultFailure()
                            }
                        }else{
                            resultFailure()
                        }
                    }else{
                        resultFailure()
                    }
                }else{
                    resultFailure()
                }
                
            case.failure(let error):
                print("Not Success",error)
                resultFailure()
            }
            
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
    //MARK:- API to create product
    func hitAPIToCreateProduct(VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)) {
        
        let url = "https://api.stripe.com/v1/products"
        let name = self.selectedPlanName
        let type = "service"
        
        var dict = Dictionary<String, Any>()
        
        dict = ["name"   : name ,
                "type"   : type]
        let header = [
            "Content-Type"   : "application/x-www-form-urlencoded",
            "Authorization"  : "Bearer sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        ]
        
        Alamofire.request(url , method: .post, parameters: dict, encoding:  URLEncoding.httpBody, headers: header ).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                if let json = response.result.value as? [String: Any] {
                    print("JSON: \(json)") // serialized json response
                    if let productId = json["id"] as? String {
                        
                        self.productId = productId
                        
                        DispatchQueue.main.async {
                            self.hitAPIToCreatePlane(VC: VC, completion: completion)
                        }
                        
                    }else{
                        resultFailure()
                    }
                }else{
                    resultFailure()
                }
                6
            case.failure(let error):
                print("Not Success",error)
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
    //MARK:- hit api to create plan
    func hitAPIToCreatePlane(VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)) {
        //let planCurrency = forwardDataDict["planPrice"] as? String
        let url     = "https://api.stripe.com/v1/plans"
        let product = self.productId
        let nickname = "plans"
        //let interval = "month"
        
        var interval = ""
        if self.selectedPlanName == "Weekly"{
            interval = "week"
        }else if self.selectedPlanName == "Monthly"{
            interval = "month"
        }else{}
        
        let currency = "usd"
        //let firstPrice = removeFormatAmount(valueStr: self.priceLabel.text!)
        //let price = Int(firstPrice * 100)
        var price = 0
        if let firstPrice = amount{
            let newFirstPrice = Int(firstPrice) ?? 0
            price = newFirstPrice * 100
        }
        
        var dict = Dictionary<String, Any>()
        
        dict = ["product"   : product ,
                "nickname"  : nickname,
                "interval"  : interval,
                "currency"  : currency,
                "amount"    : price]
        
        let header = [
            "Content-Type"   : "application/x-www-form-urlencoded",
            "Authorization"  : "Bearer sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        ]

        Alamofire.request(url , method: .post, parameters: dict, encoding:  URLEncoding.httpBody, headers: header ).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                if let json = response.result.value as? [String: Any] {
                    print("JSON: \(json)") // serialized json response
                    if let planId = json["id"] as? String {
                        
                        self.planId = planId
                        
                        DispatchQueue.main.async {
                            self.hitAPIToCreateSubscription(VC: VC, completion: completion)
                        }
                        
                    }else{
                        resultFailure()
                    }
                }else{
                    resultFailure()
                }
                
            case.failure(let error):
                print("Not Success",error)
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
    }
    
    //MARK:- hit api to create subscription(){
    func hitAPIToCreateSubscription(VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)) {
        
        let url            = "https://api.stripe.com/v1/subscriptions"
        let customer       = self.customerId
        let item           = self.planId
        
        var dict = Dictionary<String, Any>()
        
        dict = ["customer"        : customer ,
                "items[0][plan]"  : item ]
        
        let header = [
            "Content-Type"   : "application/x-www-form-urlencoded",
            "Authorization"  : "Bearer sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        ]
        
        Alamofire.request(url , method: .post, parameters: dict, encoding:  URLEncoding.httpBody, headers: header ).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                if let json = response.result.value as? [String: Any] {
                    print("JSON: \(json)") // serialized json response
                    if let subscriptionId = json["id"] as? String {
                        self.subscriptionId = subscriptionId
                
//                        if let txnId = json["balance_transaction"] as? String{
//                            if let source = json["source"] as? [String: Any]{
//                                if let cardNum = source["last4"] as? String{
//                                    if let cardId = source["id"] as? String{
//                                        self.saveReoccuringDonationWithApplePayDetail(card_num: cardNum, card_id: cardId, txnId: txnId, VC: VC, completion: completion)
//                                    }
//                                }
//                            }
//                        }
                        self.saveReoccuringDonationWithApplePayDetail(card_num: "", card_id: "", txnId: "", VC: VC, completion: completion)
                        
                    }else{
                        resultFailure()
                    }
                }else{
                    resultFailure()
                }
                
            case.failure(let error):
                print("Not Success",error)
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
    func saveReoccuringDonationWithApplePayDetail(card_num: String, card_id: String, txnId: String, VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)){
        
        guard let userInfoModel = Methods.sharedInstance.getloginData()else{return}
        
        let cycle : String?
        let user_id = userInfoModel.id as? String ?? ""
        if donatingPeriodRecurring.text == "Monthly"{
            cycle = "1"
        }else{
            cycle = "0"
        }
        if reoccuringId == nil {
            reoccuringId = ""
        }else{}
        let hbcu_id = HBCUID ?? ""
        
        let paramsStr = "user_id=\(user_id)&amount=\(amount ?? "")&donation_type=\(1)&hbcu=\(hbcu_id)&cycle=\(cycle ?? "")&card_id=\(card_id)&txnId=\(txnId)&id=\(reoccuringId ?? "")"
        
        print(paramsStr)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL +  Constants.APIs.paymentsDoneWithAppleI, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true{
                if let jsonResult = response as? Dictionary<String, AnyObject>{
                    print(jsonResult)
                    if let responseCode = jsonResult["error"] as? Bool ,responseCode{
                        //self.showAlert(jsonResult["message"] as! String)
                        resultFailure()
                    }else{
                        //                        let view = AlertView()
                        //                        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
                        //                        view.layer.cornerRadius = 0.2
                        //                        view.alertView.layer.cornerRadius = 10
                        //                        view.alertView.layer.borderColor = UIColor.black.cgColor
                        //                        self.view.addSubview(view)
                        
                        completion(PKPaymentAuthorizationStatus.success)
                        return
                    }
                }else{
                    print("Data is not in proper format")
                    resultFailure()
                }
                
            }else{
                //self.showAlert("Something is wrong. Please try again later.")
                //print("Rejected - 500")
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
 }
