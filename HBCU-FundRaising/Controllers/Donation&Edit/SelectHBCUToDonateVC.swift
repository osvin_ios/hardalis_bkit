 //
 //  SelectHBCUToDonateVC.swift
 //  HBCU-FundRaising
 //
 //  Created by Osvin Mac Jas on 21/06/18.
 //  Copyright © 2018 Osvin Mac Jas. All rights reserved.
 //
 
 import UIKit
 import ObjectMapper
 import Stripe
 import PassKit
 import Alamofire
 
 class SelectHBCUToDonateVC: UIViewController {
    
    @IBOutlet weak var selectHBCUHeight: NSLayoutConstraint!
    @IBOutlet weak var titleButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var titleButtonBottom: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var donateLabel: UILabel!
    @IBOutlet weak var DonateButton: UIButton!
    @IBOutlet weak var hbcuCollectionView: UICollectionView!
    @IBOutlet weak var donateWithApplePayButton: ButtonDesign!
    
    @IBOutlet weak var donateWithApplePayButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var donateWithApplePayButtonHeightConstraint: NSLayoutConstraint!
    
    var hcbuPostArray = [HBCUListModel]()
    var selectedHBCU : String?
    var selectedHBCUPostArray = [HBCUListModel]()
    var selectedHBCUPost : HBCUListModel?
    var donateValue    : String?
    var searchbarState : Bool = false
    var selectedId     : String?
    var newSelectedId  : String?
    var enumCases      = SelectHBCUEnum.oneTime
    
    var completionBlock: CallBackMethods.SourceCompletionHandler?
    
    //search
    var searchResultArray : [String]?
    var filterArray = [HBCUListModel]()
    
    var challenge_id : String?
    var challengeDonateAmount : String?
    
  //Apple Pay
    //var transactionId = ""
    //var status = ""
    var subscriptionId : String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.DonateButton.isUserInteractionEnabled = false
        self.donateWithApplePayButton.isUserInteractionEnabled = false
        
        self.title = "Select HBCU"
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        self.navigationItem.hidesBackButton = true
        
        if Reachability.isConnectedToNetwork() == true {
            self.getHBCUListAPI()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
        if donateValue != nil {
            donateLabel.text = " Select the HBCU, to whom you want to donate " + "$" + donateValue!
        }
        
        if challengeDonateAmount != nil {
            donateLabel.text = " Select the HBCU, to whom you want to donate " + "$" + challengeDonateAmount!
        }
        
        if enumCases == .recuuring {
            
            titleButtonBottom.setTitle("Save", for: .normal)
            //titleButtonBottom.setTitle("Submit", for: .normal)
            
            self.donateWithApplePayButton.isHidden = true
            self.donateWithApplePayButtonTopConstraint.constant = 0
            self.donateWithApplePayButtonHeightConstraint.constant = 0
            self.donateWithApplePayButton.layoutIfNeeded()
            
        } else if enumCases == .oneTime {
            
            titleButtonBottom.setTitle("Donate", for: .normal)
            
            self.donateWithApplePayButton.isHidden = false
            self.donateWithApplePayButtonTopConstraint.constant = 10
            self.donateWithApplePayButtonHeightConstraint.constant = 40
            self.donateWithApplePayButton.layoutIfNeeded()
            
        } else if enumCases == .ChallengeDonation{
            
            titleButtonBottom.setTitle("Donate", for: .normal)
            
            self.donateWithApplePayButton.isHidden = false
            self.donateWithApplePayButtonTopConstraint.constant = 10
            self.donateWithApplePayButtonHeightConstraint.constant = 40
            self.donateWithApplePayButton.layoutIfNeeded()
            
        } else {
            
            titleButtonHeight.constant = 0
            selectHBCUHeight.constant = 0
            let buttonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(SaveAction))
            self.navigationItem.rightBarButtonItem = buttonItem
            
            self.donateWithApplePayButton.isHidden = true
            self.donateWithApplePayButtonTopConstraint.constant = 0
            self.donateWithApplePayButtonHeightConstraint.constant = 0
            self.donateWithApplePayButton.layoutIfNeeded()
            
        }
  
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)  {
        searchBar.resignFirstResponder()
        
    }
    
//    func addDonateWithApplePayButton(){
//        let btn = PKPaymentButton.init(paymentButtonType: .donate, paymentButtonStyle: .black)
//        btn.frame = CGRect(x: self.donateWithApplePayButton.frame.minX, y: self.donateWithApplePayButton.frame.minY, width: self.donateWithApplePayButton.frame.width, height: self.donateWithApplePayButton.frame.height)
//        btn.center = self.donateWithApplePayButton.center
//        //self.donateWithApplePayButton.addSubview(btn)
//        view.addSubview(btn)
//    }
    
    @IBAction func donateWithApplePayButtonPressed(_ sender: ButtonDesign) {
        self.alertFunctionsWithCallBacks(title: "Are you sure you want to donate to this university") { (isTrue) in
            if isTrue.boolValue {
                self.setupApplePay()
            }else{}
        }
    }
    
    @objc func SaveAction(_ sender: UIButton) {
        self.hitUpdateHbcuAPI()
        //        if selectedUniv == selectedHBCU {
        //            self.showAlert("Already Added in Your List")
        //        } else {
        //            selectedUniv = selectedHBCU
        //            guard let cB = self.completionBlock else { return }
        //            cB(self.selectedHBCUPost as AnyObject)
        //            self.navigationController?.popViewController(animated: true)
        //        }
    }
    
    @objc func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- api function
    func getHBCUListAPI(){
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        if let userId = userInfoModel.id as? String{
            let paramsStr = "user_id=\(userId)"
            let urlName = Constants.APIs.baseURL + "getHbcu?" + paramsStr
            
            WebServiceClass.dataTask(urlName:  urlName, method: "GET", params: "") { (success, response, errorMsg) in
                WebServiceClass.hideLoader(view: self.view)
                if success == true {
                    if let jsonResult = response as? Dictionary<String, AnyObject> {
                        if let responeCode = jsonResult["error"] as? Bool, responeCode {
                            print("Error status - true, signUp unsuccessful")
                        } else {
                            if let result = jsonResult["result"] as? [Dictionary<String,AnyObject>]{
                                if let hcbuPost = Mapper<HBCUListModel>().mapArray(JSONObject: result){
                                    self.hcbuPostArray = hcbuPost
                                }
                            }
                        }
                    } else {
                        print("Data is not in proper format")
                    }
                    DispatchQueue.main.async {
                        self.hbcuCollectionView.reloadData()
                    }
                } else {
                    print("Data is not in proper format")
                }
            }
        }
        
    }
    
    @IBAction func DonateAction(_ sender: UIButton) {
        
        if enumCases == .recuuring {

            guard let cB = self.completionBlock else { return }
            cB(self.selectedHBCUPost as AnyObject)
            self.navigationController?.popViewController(animated: true)

        } else if enumCases == .ChallengeDonation{
            self.alertFunctionsWithCallBack(title: "Are you sure you want to donate to this university") { (isObject) in

                if isObject.boolValue {
                    self.hitChallengeDonationAPI()
                } else {
                    print("ii")
                }
            }
        }else{
            self.alertFunctionsWithCallBacks(title: "Are you sure you want to donate to this university") { (isTrue) in
                if isTrue.boolValue {
                     self.hitOneTimeDonationAPI()
                }else{
                    print("nil")
                }
            }

//            self.alertFunctionsWithCallBack(title: "Are you sure you want to donate to this university") { (isObject) in
//
//                if isObject.boolValue {
//                    self.hitOneTimeDonationAPI()
//                } else {print("ii")
//
//                }
//            }
        }
        
    }
    
    func hitOneTimeDonationAPI(){
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id    = userInfoModel.id as? String
        let amount     = self.donateValue ?? ""
        let card_num   = ""
        let comment    = "vc"
        let hbcu       = self.selectedHBCUPostArray[0].id ?? ""
        
        //let paramsStr = "user_id=\(user_id ?? "")&amount=\(amount)&card_num=\(card_num)&comment=\(comment)&hbcu=\(hbcu)"
        let paramsStr = "user_id=\(user_id ?? "")&amount=\(amount)&card_id=\(card_num)&hbcu=\(hbcu)"
        
        WebServiceClass.showLoader(view: self.view)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + "oneTimeDonation", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    if let responseCode = jsonResult["error"] as? Bool ,responseCode{
                        self.showAlert(jsonResult["message"] as! String)
                    } else {
                        let view = AlertView()
                        //view.frame = CGRect(x: 0, y: 0, width: 300 , height: 300)
                        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
                        view.center = CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2)
                        view.alertView.layer.cornerRadius = 10
                        view.alertView.layer.borderColor = UIColor.black.cgColor
                        self.view.addSubview(view)

//                        DispatchQueue.main.async {
//                            self.navigationController?.popViewController(animated: true)
//                        }
                    }
                } else {
                    print("Data is not in proper format")
                }
            } else {
                //self.showAlert("Something went wrong, try again after some time")
                self.showAlert("Something is wrong. Please try again later.")
                print("Rejected - 500")
            }
        }
    }
    
    func hitUpdateHbcuAPI() {
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id    = userInfoModel.id as? String
        let id         = selectedId ?? "0"
        let hbcu       = newSelectedId ?? "0"
        
        let paramsStr = "user_id=\(user_id ?? "")&id=\(id)&hbcu=\(hbcu)"
        
        WebServiceClass.showLoader(view: self.view)
        
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + "updateuserhbcu", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    self.showAlert(jsonResult["message"] as? String ?? "")
//                    self.alertFunctionsWithSingleCallBack(title: (jsonResult["message"] as? String)!, completionHandler: { (isObject) in
//
//                        if isObject.boolValue {
//                            DispatchQueue.main.async {
//                                self.navigationController?.popViewController(animated: true)
//                            }
//                        }
//                    })
                } else {
                    print("Data is not in proper format")
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
    
    func hitChallengeDonationAPI(){
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        if let user_id    = userInfoModel.id as? String {
            let card_num   = ""
            let hbcu       = self.selectedHBCUPostArray[0].id ?? ""
            let amount     = self.challengeDonateAmount ?? ""
            let comment    = "abc"
            let challenge_id = self.challenge_id ?? ""
            
            let paramsStr = "user_id=\(user_id)&card_num=\(card_num)&hbcu=\(hbcu)&amount=\(amount)&comment=\(comment)&challenge_id=\(challenge_id)"
            
            WebServiceClass.showLoader(view: self.view)
            
            WebServiceClass.dataTask(urlName: Constants.APIs.challengeBaseURL + Constants.APIs.challengeDonation, method: "POST", params: paramsStr) { (success, response, errorMsg) in
                WebServiceClass.hideLoader(view: self.view)
                if success == true {
                    if let jsonResult = response as? Dictionary<String, AnyObject> {
                        print(jsonResult)
                        if let responseCode = jsonResult["error"] as? Bool ,responseCode{
                            self.showAlert(jsonResult["message"] as! String)
                        } else {
                            
                            //                        self.showAlertWithActions("Congratulations your payment is suucessfully done.")
                            //                        let view = AlertView()
                            //                        view.frame = CGRect(x: 0, y: 0, width: 300 , height: 300)
                            //                        view.center = CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2)
                            //                        view.alertView.layer.cornerRadius = 10
                            //                        view.alertView.layer.borderColor = UIColor.black.cgColor
                            //                        self.view.addSubview(view)
                            
                            //self.showAlertWithActions(messageTitle: "Congratulations your payment is suucessfully done.", isPop: false)
                            self.showAlertWithActions(messageTitle: jsonResult["message"] as? String ?? "", isPop: false)
                        }
                    } else {
                        print("Data is not in proper format")
                    }
                } else {
                    //self.showAlert("Something went wrong, try again after some time")
                    self.showAlert("Something is wrong. Please try again later.")
                    print("Rejected - 500")
                }
            }
        }
    }
 }
 
 extension SelectHBCUToDonateVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if searchbarState == false {
            return hcbuPostArray.count
        } else {
            return filterArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hbcuCell", for: indexPath) as! hbcuCell
        
        if searchbarState == false {
            let value = hcbuPostArray[indexPath.item]
            let logoImage = Constants.APIs.imageBaseURL + value.logo!
            cell.hbcuLogoImage.sd_setImage(with: URL(string: logoImage ), placeholderImage: nil)
            
            if self.selectedHBCUPostArray.contains(where: { $0.id == self.hcbuPostArray[indexPath.row].id }) {
                cell.selectedCellImage.isHidden = false
            } else {
                cell.selectedCellImage.isHidden = true
            }
            
            cell.hbcuTitle.text = value.title ?? ""
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 1
            
            return cell
        } else {
            let value = filterArray[indexPath.item]
            let logoImage = Constants.APIs.imageBaseURL + value.logo!
            cell.hbcuLogoImage.sd_setImage(with: URL(string: logoImage ), placeholderImage: nil)
            
            if self.selectedHBCUPostArray.contains(where: { $0.id == self.filterArray[indexPath.row].id }) {
                cell.selectedCellImage.isHidden = false
            } else {
                cell.selectedCellImage.isHidden = true
            }
            
            cell.hbcuTitle.text = value.title ?? ""
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 1
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if searchbarState == true {
            if self.selectedHBCUPostArray.contains(where: { $0.id == self.filterArray[indexPath.row].id }) {
                if let _ = self.selectedHBCUPostArray.index(where: { $0.id ==  self.filterArray[indexPath.row].id}) {
                    //                self.selectedHBCUPostArray.remove(at: selectedIndex)
                }
            } else {
                self.DonateButton.isUserInteractionEnabled = true
                DonateButton.backgroundColor = UIColor(red: 2/255, green: 182/255, blue: 91/255, alpha: 1)
                self.selectedHBCUPostArray.removeAll()
                self.selectedHBCU     = self.filterArray[indexPath.row].title
                self.newSelectedId    = self.filterArray[indexPath.row].id
                self.selectedHBCUPostArray.append(self.filterArray[indexPath.row])
                selectedHBCUPost = self.filterArray[indexPath.row]
                
                self.donateWithApplePayButton.isUserInteractionEnabled = true
                
            }
            collectionView.reloadData()
        } else {
            if self.selectedHBCUPostArray.contains(where: { $0.id == self.hcbuPostArray[indexPath.row].id }) {
                if let _ = self.selectedHBCUPostArray.index(where: { $0.id ==  self.hcbuPostArray[indexPath.row].id}) {
                    //                self.selectedHBCUPostArray.remove(at: selectedIndex)
                }
            } else {
                self.DonateButton.isUserInteractionEnabled = true
                DonateButton.backgroundColor = UIColor(red: 2/255, green: 182/255, blue: 91/255, alpha: 1)
                self.selectedHBCUPostArray.removeAll()
                self.selectedHBCU     = self.hcbuPostArray[indexPath.row].title
                self.newSelectedId    = self.hcbuPostArray[indexPath.row].id
                self.selectedHBCUPostArray.append(self.hcbuPostArray[indexPath.row])
                selectedHBCUPost = self.hcbuPostArray[indexPath.row]
                
                self.donateWithApplePayButton.isUserInteractionEnabled = true
                
            }
            collectionView.reloadData()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0 - 2
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 1, 1, 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
 }
 extension SelectHBCUToDonateVC : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchbarState = true
        filterArray = hcbuPostArray.filter({ ($0.title?.contains(searchText))!})
        print(filterArray)
        self.hbcuCollectionView.reloadData()
        if searchText == "" {
            searchbarState = false
            self.dismissKeyboard()
            self.hbcuCollectionView.reloadData()
        }
    }
 }

 extension SelectHBCUToDonateVC : PKPaymentAuthorizationViewControllerDelegate{
    
    func setupApplePay(){
        
        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: Constants.ApplePayMerchantID)
        //paymentRequest?.requiredShippingAddressFields = [.email, .name]
        paymentRequest?.currencyCode = "USD"
        paymentRequest?.countryCode = "US"
        //paymentRequest?.merchantIdentifier = ApplePaySwagMerchantID
        
        var amount = ""
        if self.enumCases == .recuuring {
        }else if self.enumCases == .ChallengeDonation{
            amount = self.challengeDonateAmount ?? ""
        }else if self.enumCases == .oneTime{
            amount = self.donateValue  ?? ""
        }else{}
        
        if let amount = Int(amount){
            paymentRequest?.paymentSummaryItems = [
                PKPaymentSummaryItem(label: "", amount: NSDecimalNumber(value: amount))
                //PKPaymentSummaryItem(label: "100"),
                //PKPaymentSummaryItem(label: "Company Name", amount: amount)
            ]

            //Payment networks array
              let paymentNetworks = [PKPaymentNetwork.amex, .visa, .masterCard, .discover]
              paymentRequest?.supportedNetworks = paymentNetworks
              paymentRequest?.merchantCapabilities = .capability3DS
        }

        if Stripe.canSubmitPaymentRequest(paymentRequest) {
            var paymentController: PKPaymentAuthorizationViewController?
            paymentController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest!)
            paymentController?.delegate = self
            paymentController?.modalPresentationStyle = .formSheet

            if let paymentController = paymentController {
                present(paymentController, animated: true, completion: nil)
            }
        } else {
            //self.showAlert("Apple Pay is not available on this device or May needs to set up the Apple Pay on the device")
            self.showAlert("You need to set up the Apple Pay on the device")
        }
        
    }
    
    //This function is called when Apple Pay payment will be authorized.
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void){
        // Payment token can be found like this
        print(payment.token)
        // Get response from the server and set the PKPaymentAuthorizationStatus
        let status = PKPaymentAuthorizationStatus(rawValue: 0)!

        //self.transactionId = payment.token.transactionIdentifier

        switch status.rawValue {
        case 0:
            //self.status = "approved"
        
          //perform Functionality on Apple Pay Successfull Payment
            
            //Stripe.setDefaultPublishableKey("pk_test_ho0D4zAZULYWZcvNHEHFasyE")
            Stripe.setDefaultPublishableKey("pk_live_egNBOvzPd1DewC3sOGQe4H0c") //live
            
            STPAPIClient.shared().createToken(with: payment) {
                (token, error) -> Void in
    
                if (error != nil) {
                    print("%@", error!)
                    completion(PKPaymentAuthorizationStatus.failure)
                    return
                }
                
                if let token = token{
                    self.hitOneTimeAPI(token: token, VC: controller, completion: completion)
                }
                
            }
            
        default:
            //self.status = "failed"
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }

    }
    
    //This function is called when Apple Pay payment authorization is finished.
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController){
        self.dismiss(animated: true, completion: nil)
    }

 }
 
 extension SelectHBCUToDonateVC{
    
    //MARK:- one time charges
    func hitOneTimeAPI(token: STPToken, VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)) {
        
        let url            = "https://api.stripe.com/v1/charges"
        let description    = "One-off"
        let source         = token
        //let firstPrice = removeFormatAmount(valueStr: self.priceLabel.text!)
        //let price = Int(firstPrice * 100)
        var price = 0
        if self.enumCases == .recuuring {
        }else if self.enumCases == .ChallengeDonation{
            if let firstPrice = self.challengeDonateAmount{
                let newFirstPrice = Int(firstPrice) ?? 0
                price = newFirstPrice * 100
            }
        }else if self.enumCases == .oneTime{
            if let firstPrice = self.donateValue{
                let newFirstPrice = Int(firstPrice) ?? 0
                price = newFirstPrice * 100
            }
        }else{}
        
        var dict = Dictionary<String, Any>()
        
        dict = ["amount"        : price ,
                "currency"  : "usd" ,
                "description" : description  , "source" : source ]
        
        //"sk_test_0HEIX5gE8pRO5X5xuSPrPczU"
        //"sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        
        let header = [
            "Content-Type"   : "application/x-www-form-urlencoded",
            "Authorization"  : "Bearer sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        ]
        
        WebServiceClass.showLoader(view: self.view)
        Alamofire.request(url , method: .post, parameters: dict, encoding:  URLEncoding.httpBody, headers: header ).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                if let json = response.result.value as? [String: Any] {
                    print("JSON: \(json)") // serialized json response
                    if let id = json["id"] as? String{
                        self.subscriptionId = id

                        if let txnId = json["balance_transaction"] as? String{
                            if let source = json["source"] as? [String: Any]{
                                if let cardNum = source["last4"] as? String{
                                    if let cardId = source["id"] as? String{
                                        self.saveOneTimeDonationWithApplePayDetail(card_num: cardNum, card_id: cardId, txnId: txnId, VC: VC, completion: completion)
                                    }
                                }
                            }
                        }
                        
                    }else{
                        resultFailure()
                    }
                }else{
                    resultFailure()
                }
                
            case.failure(let error):
                print("Not Success",error)
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
  
    func saveOneTimeDonationWithApplePayDetail(card_num: String, card_id: String, txnId: String, VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)){
        
        guard let userInfoModel = Methods.sharedInstance.getloginData()else{return}
        let user_id    = userInfoModel.id as? String ?? ""
        let amount     = self.donateValue ?? ""
        let hbcu       = self.selectedHBCUPostArray[0].id ?? ""
        let challenge_id = self.challenge_id ?? ""
        let challengeAamount = self.challengeDonateAmount ?? ""
        

        var paramsStr = ""
        if self.enumCases == .recuuring {
        }else if self.enumCases == .ChallengeDonation{
            paramsStr = "user_id=\(user_id)&amount=\(challengeAamount)&donation_type=\(2)&hbcu=\(hbcu)&challenge_id=\(challenge_id)"
        }else if self.enumCases == .oneTime{
            paramsStr = "user_id=\(user_id)&amount=\(amount)&donation_type=\(2)&hbcu=\(hbcu)&card_num=\(card_num)&card_id=\(card_id)&txnId=\(txnId)"
        }else{}
        
        print(paramsStr)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL +  Constants.APIs.paymentsDoneWithAppleI, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true{
                if let jsonResult = response as? Dictionary<String, AnyObject>{
                    print(jsonResult)
                    if let responseCode = jsonResult["error"] as? Bool ,responseCode{
                        //self.showAlert(jsonResult["message"] as! String)
                        resultFailure()
                    }else{
                        //                        let view = AlertView()
                        //                        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
                        //                        view.layer.cornerRadius = 0.2
                        //                        view.alertView.layer.cornerRadius = 10
                        //                        view.alertView.layer.borderColor = UIColor.black.cgColor
                        //                        self.view.addSubview(view)
                        
                        completion(PKPaymentAuthorizationStatus.success)
                        return
                    }
                }else{
                    print("Data is not in proper format")
                    resultFailure()
                }
                
            }else{
                //self.showAlert("Something is wrong. Please try again later.")
                //print("Rejected - 500")
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
 }
