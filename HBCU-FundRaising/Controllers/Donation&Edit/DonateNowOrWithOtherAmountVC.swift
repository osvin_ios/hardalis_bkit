//
//  DonateNowOrWithOtherAmountVC.swift
//  HBCU-FundRaising
//
//  Created by osvinuser on 26/03/19.
//  Copyright © 2019 Osvin Mac Jas. All rights reserved.
//

import UIKit
import Stripe
import PassKit
import Alamofire

class DonateNowOrWithOtherAmountVC: UIViewController {
    
    @IBOutlet weak var dollor5ImageView: UIImageView!
    @IBOutlet weak var dollor20ImageView: UIImageView!
    @IBOutlet weak var dollor40ImageView: UIImageView!
    @IBOutlet weak var dollor50ImageView: UIImageView!
    @IBOutlet weak var otherAmountView: ViewDesign!
    @IBOutlet weak var otherAmountTextField: TextFieldDesign!
    @IBOutlet weak var donateWithApplePayButton: ButtonDesign!
    
    var amountStr = ""
    var selectedHBCUId = ""
    var isOtherAmount = ""
    
    //Apple Pay
    var subscriptionId : String? = ""
    var payAmount = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
  
    override func viewWillAppear(_ animated: Bool) {
        
        //self.otherAmountView.addDashedBorder()
//        let yourViewBorder = CAShapeLayer()
//        yourViewBorder.strokeColor = UIColor.black.cgColor
//        yourViewBorder.lineDashPattern = [2, 2]
//        yourViewBorder.frame = self.otherAmountView.bounds
//        yourViewBorder.fillColor = nil
//        yourViewBorder.path = UIBezierPath(rect: self.otherAmountView.bounds).cgPath
//        self.otherAmountView.layer.addSublayer(yourViewBorder)
       
    }
    
    @IBAction func crossButtonPressed(_ sender: ButtonDesign) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func dollor5ButtonPressed(_ sender: UIButton) {
        
        dollor5ImageView.image = UIImage(named: "ic_green_circle")
        dollor20ImageView.image = UIImage(named: "ic_white_circle")
        dollor40ImageView.image = UIImage(named: "ic_white_circle")
        dollor50ImageView.image = UIImage(named: "ic_white_circle")
        self.amountStr = "5"
        self.otherAmountTextField.text = ""
        self.isOtherAmount = ""
        self.view.endEditing(true)
        
    }
    @IBAction func dollor20ButtonPressed(_ sender: UIButton) {
        
        dollor5ImageView.image = UIImage(named: "ic_white_circle")
        dollor20ImageView.image = UIImage(named: "ic_green_circle")
        dollor40ImageView.image = UIImage(named: "ic_white_circle")
        dollor50ImageView.image = UIImage(named: "ic_white_circle")
        self.amountStr = "20"
        self.otherAmountTextField.text = ""
        self.isOtherAmount = ""
        self.view.endEditing(true)
        
    }
    @IBAction func dollor40ButtonPressed(_ sender: UIButton) {
        
        dollor5ImageView.image = UIImage(named: "ic_white_circle")
        dollor20ImageView.image = UIImage(named: "ic_white_circle")
        dollor40ImageView.image = UIImage(named: "ic_green_circle")
        dollor50ImageView.image = UIImage(named: "ic_white_circle")
        self.amountStr = "40"
        self.otherAmountTextField.text = ""
        self.isOtherAmount = ""
        self.view.endEditing(true)
        
    }
    @IBAction func dollor50ButtonPressed(_ sender: UIButton) {
        
        dollor5ImageView.image = UIImage(named: "ic_white_circle")
        dollor20ImageView.image = UIImage(named: "ic_white_circle")
        dollor40ImageView.image = UIImage(named: "ic_white_circle")
        dollor50ImageView.image = UIImage(named: "ic_green_circle")
        self.amountStr = "50"
        self.otherAmountTextField.text = ""
        self.isOtherAmount = ""
        self.view.endEditing(true)
        
    }
    
    @IBAction func donateWithApplePayButtonPressed(_ sender: ButtonDesign) {
        
        if self.isOtherAmount == "yes"{
            if !self.emptyString(string: otherAmountTextField.text!){
                if let textFieldValue = self.otherAmountTextField.text{
                    self.amountStr = textFieldValue
                }else{
                    self.amountStr = ""
                }
            }else{
                self.amountStr = ""
            }
        }
        
        if self.amountStr == ""{
            self.showAlert("Please enter some amount.")
        }else{
            self.alertFunctionsWithCallBacks(title: "Do you really want to donate this amount?") { (isTrue) in
                if isTrue.boolValue {
                    if Reachability.isConnectedToNetwork() == true {
                        self.payAmount = self.amountStr
                        self.setupApplePay()
                    }else{
                        self.showAlert(Constants.commomInternetmsz.checkInternet)
                    }
                }else{
                    print("No")
                }
            }
        }

        
    }
    
    @IBAction func donateNowButtonPressed(_ sender: ButtonDesign) {
        
        if self.isOtherAmount == "yes"{
            if !self.emptyString(string: otherAmountTextField.text!){
                if let textFieldValue = self.otherAmountTextField.text{
                    self.amountStr = textFieldValue
                }else{
                    self.amountStr = ""
                }
            }else{
                self.amountStr = ""
            }
        }
        
        if self.amountStr == ""{
            self.showAlert("Please enter some amount.")
        }else{
            self.alertFunctionsWithCallBacks(title: "Do you really want to donate this amount?") { (isTrue) in
                if isTrue.boolValue {
                    if Reachability.isConnectedToNetwork() == true {
                        self.hitAllSpareChangeAPI(amount:self.amountStr)
                    }else{
                        self.showAlert(Constants.commomInternetmsz.checkInternet)
                    }
                }else{
                    print("No")
                }
            }
        }
        
    }
    
    func emptyString(string : String)-> Bool{
        return string.trimmingCharacters(in: .whitespaces).count > 0 ? false : true
    }
   
}

//MARK:-  UITextFeild
extension DonateNowOrWithOtherAmountVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == otherAmountTextField {
            
            dollor5ImageView.image = UIImage(named: "ic_white_circle")
            dollor20ImageView.image = UIImage(named: "ic_white_circle")
            dollor40ImageView.image = UIImage(named: "ic_white_circle")
            dollor50ImageView.image = UIImage(named: "ic_white_circle")
            
            if (textField.text?.count)! + string.count > 5{
                return false
            }else{
                return true
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        dollor5ImageView.image = UIImage(named: "ic_white_circle")
        dollor20ImageView.image = UIImage(named: "ic_white_circle")
        dollor40ImageView.image = UIImage(named: "ic_white_circle")
        dollor50ImageView.image = UIImage(named: "ic_white_circle")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        dollor5ImageView.image = UIImage(named: "ic_white_circle")
        dollor20ImageView.image = UIImage(named: "ic_white_circle")
        dollor40ImageView.image = UIImage(named: "ic_white_circle")
        dollor50ImageView.image = UIImage(named: "ic_white_circle")
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldDidEndEditing1")
        self.isOtherAmount = "yes"
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("textFieldShouldEndEditing2")
        self.isOtherAmount = "yes"
        return true
    }
    
}

extension DonateNowOrWithOtherAmountVC{
    
    func hitAllSpareChangeAPI(amount:String){
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id    = userInfoModel.id as? String
        let amount     = amount
        let card_num   = ""
        let hbcu       = self.selectedHBCUId
        
        let paramsStr = "user_id=\(user_id ?? "")&amount=\(amount)&card_id=\(card_num)&hbcu=\(hbcu)"
        
        WebServiceClass.showLoader(view: self.view)
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + "oneTimeDonation", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    if let responseCode = jsonResult["error"] as? Bool ,responseCode{
                        self.showAlert(jsonResult["message"] as! String)
                    } else {
                        let view = AlertView()
                        //view.frame = CGRect(x: 0, y: 0, width: 300 , height: 300)
                        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
                        view.center = CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2)
                        view.alertView.layer.cornerRadius = 10
                        view.alertView.layer.borderColor = UIColor.black.cgColor
                        self.view.addSubview(view)
                    }
                } else {
                    print("Data is not in proper format")
                }
            } else {
                //self.showAlert("Something went wrong, try again after some time")
                self.showAlert("Something is wrong. Please try again later.")
                print("Rejected - 500")
            }
        }
    }
    
}

extension UIView {
    func addDashedBorder() {
        let color = UIColor.red.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
}

extension DonateNowOrWithOtherAmountVC : PKPaymentAuthorizationViewControllerDelegate{
    
    func setupApplePay(){
        
        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: Constants.ApplePayMerchantID)
        paymentRequest?.currencyCode = "USD"
        paymentRequest?.countryCode = "US"
        
        if let amount = Int(self.payAmount){
            paymentRequest?.paymentSummaryItems = [
                PKPaymentSummaryItem(label: "", amount: NSDecimalNumber(value: amount))
            ]
            
            //Payment networks array
            let paymentNetworks = [PKPaymentNetwork.amex, .visa, .masterCard, .discover]
            paymentRequest?.supportedNetworks = paymentNetworks
            paymentRequest?.merchantCapabilities = .capability3DS
        }
        
        if Stripe.canSubmitPaymentRequest(paymentRequest) {
            var paymentController: PKPaymentAuthorizationViewController?
            paymentController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest!)
            paymentController?.delegate = self
            paymentController?.modalPresentationStyle = .formSheet
            
            if let paymentController = paymentController {
                present(paymentController, animated: true, completion: nil)
            }
        } else {
            //self.showAlert("Apple Pay is not available on this device or May needs to set up the Apple Pay on the device")
            self.showAlert("You need to set up the Apple Pay on the device")
        }
        
    }
    
    //This function is called when Apple Pay payment will be authorized.
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void){
        // Payment token can be found like this
        print(payment.token)
        // Get response from the server and set the PKPaymentAuthorizationStatus
        let status = PKPaymentAuthorizationStatus(rawValue: 0)!
        switch status.rawValue {
        case 0:
            
            //Stripe.setDefaultPublishableKey("pk_test_ho0D4zAZULYWZcvNHEHFasyE")
            Stripe.setDefaultPublishableKey("pk_live_egNBOvzPd1DewC3sOGQe4H0c") //live
            
            STPAPIClient.shared().createToken(with: payment) {
                (token, error) -> Void in
                
                if (error != nil) {
                    print("%@", error!)
                    completion(PKPaymentAuthorizationStatus.failure)
                    return
                }
                
                if let token = token{
                    self.hitOneTimeAPI(token: token, VC: controller, completion: completion)
                }
                
            }
            
        default:
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
    //This function is called when Apple Pay payment authorization is finished.
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController){
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension DonateNowOrWithOtherAmountVC{
    
    //MARK:- one time charges
    func hitOneTimeAPI(token: STPToken, VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)) {
        
        let url            = "https://api.stripe.com/v1/charges"
        let description    = "One-off"
        let source         = token
        
        var price = 0
        let firstPrice = self.payAmount
        let newFirstPrice = Int(firstPrice) ?? 0
        price = newFirstPrice * 100
        
        var dict = Dictionary<String, Any>()
        
        dict = ["amount"        : price ,
                "currency"  : "usd" ,
                "description" : description  , "source" : source ]
        
        //"sk_test_0HEIX5gE8pRO5X5xuSPrPczU"
        //"sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        
        let header = [
            "Content-Type"   : "application/x-www-form-urlencoded",
            "Authorization"  : "Bearer sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        ]
        
        WebServiceClass.showLoader(view: self.view)
        Alamofire.request(url , method: .post, parameters: dict, encoding:  URLEncoding.httpBody, headers: header ).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                if let json = response.result.value as? [String: Any] {
                    print("JSON: \(json)") // serialized json response
                    if let id = json["id"] as? String{
                        self.subscriptionId = id
                        
                        if let txnId = json["balance_transaction"] as? String{
                            if let source = json["source"] as? [String: Any]{
                                if let cardNum = source["last4"] as? String{
                                    if let cardId = source["id"] as? String{
                                        self.saveOneTimeDonationWithApplePayDetail(card_num: cardNum, card_id: cardId, txnId: txnId, VC: VC, completion: completion)
                                    }
                                }
                            }
                        }
                        
                    }else{
                        resultFailure()
                    }
                }else{
                    resultFailure()
                }
                
            case.failure(let error):
                print("Not Success",error)
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
    func saveOneTimeDonationWithApplePayDetail(card_num: String, card_id: String, txnId: String, VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)){
        
        guard let userInfoModel = Methods.sharedInstance.getloginData()else{return}
        let user_id    = userInfoModel.id as? String ?? ""
        let amount     = self.payAmount
        let card_num   = ""
        let hbcu       = self.selectedHBCUId
        
        let paramsStr = "user_id=\(user_id)&amount=\(amount)&donation_type=\(2)&hbcu=\(hbcu)&card_num=\(card_num)&card_id=\(card_id)&txnId=\(txnId)"
        print(paramsStr)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL +  Constants.APIs.paymentsDoneWithAppleI, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true{
                if let jsonResult = response as? Dictionary<String, AnyObject>{
                    print(jsonResult)
                    if let responseCode = jsonResult["error"] as? Bool ,responseCode{
                        //self.showAlert(jsonResult["message"] as! String)
                        resultFailure()
                    }else{
                        //                        let view = AlertView()
                        //                        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
                        //                        view.layer.cornerRadius = 0.2
                        //                        view.alertView.layer.cornerRadius = 10
                        //                        view.alertView.layer.borderColor = UIColor.black.cgColor
                        //                        self.view.addSubview(view)
                        
                        completion(PKPaymentAuthorizationStatus.success)
                        return
                    }
                }else{
                    print("Data is not in proper format")
                    resultFailure()
                }
                
            }else{
                //self.showAlert("Something is wrong. Please try again later.")
                //print("Rejected - 500")
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
}
