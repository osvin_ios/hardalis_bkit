//
//  OtherAmountDonate.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 22/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit

class OtherAmountDonate: UIViewController {

    @IBOutlet weak var amountTextfield: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var buttonTitle: UIButton!
    
    var mode = SelectHBCUEnum.SelectFormDonation 
    var completionBlock: CallBackMethods.SourceCompletionHandler?
    var selectOtherAmount : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.amountTextfield.becomeFirstResponder()
        if mode == .SelectFromReocuuring{
            buttonTitle.setTitle("DONE", for: .normal)
        }
        
    }
    @IBAction func nextAction(_ sender: UIButton) {
        
        if mode == .SelectFromReocuuring {
            let amount  = Int(amountTextfield.text!)
            if amount == 0 {
                self.showAlert("Please enter valid amount. Minimum donation amount is $1")
            } else if amountTextfield.text == "" {
                self.showAlert("Please enter valid amount. Minimum donation amount is $1")
            }else {
                selectOtherAmount = amountTextfield.text
            }
            guard let cB = self.completionBlock else { return }
            cB(self.selectOtherAmount as AnyObject)
            
            self.navigationController?.popViewController(animated: true)
        }
        else{
            let amount  = Int(amountTextfield.text!)
            if amount == 0 {
                    self.showAlert("Please enter valid amount. Minimum donation amount is $1")
                } else if amountTextfield.text == "" {
                    self.showAlert("Please enter valid amount. Minimum donation amount is $1")
                } else {
                if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "SelectHBCUToDonateVC") as! SelectHBCUToDonateVC
                    vc.donateValue = amountTextfield.text ?? ""
                    currentNavVC.pushViewController(vc, animated: true)
                }
            }
        }
    }
}

