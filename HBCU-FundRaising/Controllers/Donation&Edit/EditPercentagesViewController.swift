//
//  EditPercentagesViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 19/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class EditPercentagesViewController: UIViewController {
    
    @IBOutlet weak var editPercentTableView: UITableView!
    @IBOutlet weak var updatePercentageButton: UIButton!
    
    var favVC = [HBCUListModel]()
    var hbcuIdArray = [String]()
    var hbcuType = [String]()
    var sum = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Edit Percentages"
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_white"), style: .done, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = buttonItem
        self.editPercentTableView.reloadData()
        editPercentTableView.register(UINib(nibName: "EditPercentageCell", bundle: nil), forCellReuseIdentifier: "EditPercentageCell")
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func updatePercenAction(_ sender: UIButton) {
        if sum.isEmpty == true && sum.count < 5 {
            self.showAlert("Oops! Your total percentage may not be less than 100%")
        } else {
             returnIntValue()
        }
    }
    
    func hitUpdatePercentageAPI(){
        
        WebServiceClass.showLoader(view: self.view)
        
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        if let userId = userInfoModel.id as? String{
            let hbcuId = hbcuIdArray.joined(separator: ",")
            let percentage = sum.map{String($0)}.joined(separator: ",")
            let hbcutype = hbcuType.joined(separator: ",")
            let paramsStr = "user_id=\(userId)&id=\(hbcuId)&percent=\(percentage)&hbcuType=\(hbcutype)"
            let urlName = Constants.APIs.baseURL +  "updateHBCUDonationPercent"
            
            WebServiceClass.dataTask(urlName:  urlName, method: "POST", params: paramsStr) { (success, response, errorMsg) in
                WebServiceClass.hideLoader(view: self.view)
                if success == true {
                    if let jsonResult = response as? Dictionary<String, AnyObject> {
                        if let result = jsonResult["result"] as? [Dictionary<String,AnyObject>]{
                            if let hcbuPost = Mapper<HBCUListModel>().mapArray(JSONObject: result){
                                self.favVC.removeAll()
                                self.favVC = hcbuPost
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        self.editPercentTableView.reloadData()
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    print("Data is not in proper format")
                }
            }
        }
    }
}

extension EditPercentagesViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favVC.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : EditPercentageCell = tableView.dequeueReusableCell(withIdentifier: "EditPercentageCell", for: indexPath) as! EditPercentageCell
        let data    = favVC[indexPath.row]
        cell.hbcuTitleLabel.text = data.title
        self.hbcuIdArray.append(data.id ?? "")
        self.hbcuType.append(data.hbcuType ?? "")
        cell.percentageText.text = (data.donation_percent ?? "")
        sum.append(Int(data.donation_percent ?? "") ?? 0)
        cell.selectionStyle = .none
        cell.percentageText.tag = indexPath.row
        cell.percentageText.delegate = self
        let whiteRoundedView : UIView = UIView(frame: CGRect(x: 10, y: 8, width: self.view.frame.size.width - 20, height: 60))
        
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 0.9])
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 2.0
        whiteRoundedView.layer.shadowOffset = CGSize(width: -1, height: 1)
        whiteRoundedView.layer.shadowOpacity = 0.2
        
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubview(toBack: whiteRoundedView)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        let titleLabel : UILabel = UILabel(frame: CGRect(x: self.view.frame.size.width/40, y: 0, width: 110, height: 30))
        titleLabel.text = "Favorite Five"
        titleLabel.font = UIFont(name: "Gotham-Book", size: 14)
        titleLabel.textColor = UIColor.lightGray
        view.addSubview(titleLabel)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}

extension EditPercentagesViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "0" {
            self.showAlert("Percentage cannot be zero")
            updatePercentageButton.isUserInteractionEnabled = false
        } else {
            updatePercentageButton.isUserInteractionEnabled = true
        }
        
        switch  textField.tag {
        case 0:
            if textField.text == "0" {
                self.showAlert("Percentage can not be zero")
            } else {
                if sum.indices.contains(0) {
                    sum.remove(at: 0)
                }
                sum.insert(Int(textField.text!) ?? 0, at: 0)
            }
            
        case 1:
            if textField.text == "0" {
                self.showAlert("Percentage can not be zero")
            } else {
                if sum.indices.contains(1) {
                    sum.remove(at: 1)
                }
                sum.insert(Int(textField.text!) ?? 0, at: 1)
            }
            
        case 2:
            if textField.text == "0" {
                self.showAlert("Percentage can not be zero")
            } else {
                if sum.indices.contains(2) {
                    sum.remove(at: 2)
                }
                sum.insert(Int(textField.text!) ?? 0, at: 2)
            }
           
        case 3:
            if textField.text == "0" {
                self.showAlert("Percentage can not be zero")
            } else {
                if sum.indices.contains(3) {
                    sum.remove(at: 3)
                } 
                sum.insert(Int(textField.text!) ?? 0, at: 3)
            }
            
        default:
            if textField.text == "0" {
                self.showAlert("Percentage can not be zero")
            } else {
                if sum.indices.contains(4) {
                    sum.remove(at: 4)
                }
                sum.insert(Int(textField.text!) ?? 0, at: 4)
            }
        }
    }
    
    func returnIntValue(){

        let total = sum.reduce(0, +)
        print(total)
        if total > 100 {
            self.showAlert("You cannot exceed 100 percent")
        } else if total < 100 {
            self.showAlert("You cannot go less than 100 percent")
        } else if sum.contains(0) {
            self.showAlert("Percentage can not be zero")
        } else {
            self.hitUpdatePercentageAPI()
        }
    }
}
