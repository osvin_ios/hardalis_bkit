//
//  MostLovedHBCU.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 22/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper
import Stripe
import PassKit
import Alamofire

class MostLovedHBCU: UIViewController {
    
    //IBOutlet
    @IBOutlet weak var donationPlusView: UIView!
    @IBOutlet weak var mostLovedTableView: UITableView!
    @IBOutlet weak var otherAmount: UITextField!
    @IBOutlet weak var fiftyDollar: ButtonDesign!
    @IBOutlet weak var fortyDollar: ButtonDesign!
    @IBOutlet weak var twntyDollar: ButtonDesign!
    @IBOutlet weak var fiveDollar: ButtonDesign!
    @IBOutlet weak var donateNowButton: UIButton!
    @IBOutlet weak var helpView: UIView!
    @IBOutlet weak var donationWithApplePayButton: ButtonDesign!
    
    //global variables
    var hbcuFavArray = [HBCUListModel]()
    var totalamount = Double()
    var totalAmountStr = ""
    var sort : String?
    var month : String?
    var year : String?
    var amount : String?
    var selectedHBCUId : String?
    var selectedHBCU : String?
    let viewHeaderView = MostLovedHBCUHeader()
    var selectedValues : Bool = false
    var newString = ""
    
    //Apple Pay
    var subscriptionId : String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mostLovedTableView.register(UINib(nibName: "univListTable", bundle: nil), forCellReuseIdentifier: "univListTable")
        self.title = "Most Loved HBCUs"
        self.barButton()
        
        if Reachability.isConnectedToNetwork() == true {
            self.hitMostLovedHBCUApi()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
        sort = "month"
        viewHeaderView.selectMonth.isSelected = true
        viewHeaderView.selectMonth.isUserInteractionEnabled = false
        self.amount = "0"
    }
    
    func barButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_question_mark"), style: .plain, target: self, action: #selector(rightBar(sender:)))
    }
    
    @objc func leftBar(sender: UIButton) {
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            
        }
    }
    
    @objc func rightBar(sender: UIButton) {
       self.performSegue(withIdentifier: "HelpscreenSegue", sender: self)
    }
    
    @IBAction func select5DollarAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if self.fiveDollar.isSelected == true {
            self.twntyDollar.isSelected = false
            self.fortyDollar.isSelected = false
            self.fiftyDollar.isSelected = false
            self.otherAmount.text = ""
            self.dismissKeyboard()
        }
        self.amount = "5"
        self.selectedValues = true
    }
    
    @IBAction func select20DollarAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if self.twntyDollar.isSelected == true {
            self.fiveDollar.isSelected = false
            self.fortyDollar.isSelected = false
            self.fiftyDollar.isSelected = false
            self.otherAmount.text = ""
            self.dismissKeyboard()
        }
        self.amount = "20"
        self.selectedValues = true
    }
    
    @IBAction func select40Dollar(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if self.fortyDollar.isSelected == true {
            self.twntyDollar.isSelected = false
            self.fiveDollar.isSelected = false
            self.fiftyDollar.isSelected = false
            self.otherAmount.text = ""
            self.dismissKeyboard()
        }
        self.amount = "40"
        self.selectedValues = true
    }
    
    @IBAction func select50Dollar(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if self.fiftyDollar.isSelected == true {
            self.twntyDollar.isSelected = false
            self.fortyDollar.isSelected = false
            self.fiveDollar.isSelected = false
            self.otherAmount.text = ""
            self.dismissKeyboard()
        }
        self.amount = "50"
        self.selectedValues = true
    }
    
    @IBAction func donationWithApplePayButtonPressed(_ sender: ButtonDesign) {
        self.view.endEditing(true)
        if self.selectedValues == false {
            self.showAlert("Minimum donation amount is $1")
        } else {
            self.alertFunctionsWithCallBack(title: "Do you really want to donate this amount?") { (isObject) in
                if isObject.boolValue {
                    self.setupApplePay()
                }else{}
            }
        }
    }
    
    @IBAction func DonateNowAction(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if self.selectedValues == false {
            self.showAlert("Minimum donation amount is $1")
        } else {
            self.alertFunctionsWithCallBack(title: "Do you really want to donate this amount?") { (isObject) in
                if isObject.boolValue {
                    self.hitOneTimeDonationAPI()
                } else {
                    print("rejected")
                }
            }
        }
    }
    
    func hitOneTimeDonationAPI(){
    
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id    = userInfoModel.id as? String
        let amount     = self.amount ?? ""
        let card_num   = "0"
        let comment    = "vc"
        let hbcu       = self.selectedHBCUId ?? ""
        
        //let paramsStr = "user_id=\(user_id ?? "")&amount=\(amount)&card_num=\(card_num)&comment=\(comment)&hbcu=\(hbcu)"
        let paramsStr = "user_id=\(user_id ?? "")&amount=\(amount)&card_id=\(card_num)&hbcu=\(hbcu)"
        print(paramsStr)
        
        WebServiceClass.showLoader(view: self.view)
        
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + "oneTimeDonation", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    if let responeCode = jsonResult["error"] as? Bool, responeCode{
                        self.showAlert(jsonResult["message"] as! String)
                    } else {
                        let view = AlertView()
                        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
                        view.layer.cornerRadius = 0.2
                        self.view.addSubview(view)
                    }
                } else {
                    print("Data is not in proper format")
                }
                DispatchQueue.main.async {
                    self.fiftyDollar.isSelected = false
                    self.fiveDollar.isSelected  = false
                    self.twntyDollar.isSelected = false
                    self.fortyDollar.isSelected = false
                    self.otherAmount.text = ""
                    self.hitMostLovedHBCUApi()
                    self.donationPlusView.removeFromSuperview()
                }}else {
                //self.showAlert("Something went wrong, try again after some time")
                self.showAlert("Something is wrong. Please try again later.")
                print("Error")
            }
        }
    }
    
    func hitMostLovedHBCUApi(){
        
        WebServiceClass.showLoader(view: self.view)
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let user_id        = userInfoModel.id as? String
        let paramsStr = "user_id=\(user_id ?? "")"
        let sort = self.sort ?? "month"
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + "getDonations?sort=\(sort)" + "&" + paramsStr, method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult        = response as? Dictionary<String, AnyObject> {
                    if let result        = jsonResult["result"] {
                        self.month       = result["month"] as? String
                        self.year        = result["year"] as? String
                        
                        if let donations = result["donations"] as? [Dictionary<String, AnyObject>]{
                            
                            if result["total"] as? String == ""{
                                self.totalamount = 0.00
                                self.totalAmountStr = "0.00"
                            }else{
                                let thousandNum = Double(result["total"] as? String ?? "0.00")
                                let result1 = thousandNum?.truncate(places: 2)
                                self.totalamount = result1 ?? 0.00
                                self.totalAmountStr = result["total"] as? String ?? "0.00"
                            }
                            if let favHBCU = Mapper<HBCUListModel>().mapArray(JSONObject: donations){
                                self.hbcuFavArray = favHBCU
                            }
                        }
                    }
                }
                DispatchQueue.main.async{
                    self.mostLovedTableView.reloadData()
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }

    @IBAction func crossActionButton(_ sender: UIButton){
        
        self.fiftyDollar.isSelected = false
        self.fiveDollar.isSelected  = false
        self.twntyDollar.isSelected = false
        self.fortyDollar.isSelected = false
        self.otherAmount.text = ""
        donationPlusView.removeFromSuperview()
    }
}

extension MostLovedHBCU : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hbcuFavArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : univListTable = tableView.dequeueReusableCell(withIdentifier: "univListTable", for: indexPath) as! univListTable
        let value = hbcuFavArray[indexPath.row]
        cell.goToCommentsAction.addTarget(self, action: #selector(GoToComment), for: .touchUpInside)
        cell.goToCommentsAction.tag = indexPath.row
        cell.selectionStyle = .none
        cell.univName.text = value.title
        cell.donateButton.tag = indexPath.row
        cell.donateButton.addTarget(self, action: #selector(donateToSelectedUnivFunction), for: .touchUpInside)
      
        if indexPath.row == 0{
            cell.univImages.image = #imageLiteral(resourceName: "ic_ranked_first-1")
            if let amount = value.donation_amount as? Int {
                cell.univDonationAmount.text = "$" + "\(amount)"
            }
            if let amount = value.donation_amount as? String {
                cell.univDonationAmount.text = "$" + amount
            }
        }else if indexPath.row == 1{
            cell.univImages.image = #imageLiteral(resourceName: "ic_ranked_second-1")
            if let amount = value.donation_amount as? Int{
                cell.univDonationAmount.text = "$" + "\(amount)"
            }
            if let amount = value.donation_amount as? String {
                cell.univDonationAmount.text = "$" + amount
            }
        }else if indexPath.row == 2{
            cell.univImages.image = #imageLiteral(resourceName: "ic_ranked_third-1")
            if let amount = value.donation_amount as? Int{
                cell.univDonationAmount.text = "$" + "\(amount)"
            }
            if let amount = value.donation_amount as? String {
                cell.univDonationAmount.text = "$" + amount
            }
        }else{
            let image = Constants.APIs.imageBaseURL + value.logo!
            cell.univImages.sd_setImage(with: URL(string: image ), placeholderImage: nil)
            if let amount = value.donation_amount as? Int{
                if amount == 0 {
                    cell.univDonationAmount.text = "$0.00"
                }else{
                    cell.univDonationAmount.text = "$" + "\(amount)"
                }
            }
            if let amount = value.donation_amount as? String{
                if amount == "0"{
                    cell.univDonationAmount.text = "$0.00"
                }else{
                    cell.univDonationAmount.text = "$" + "\(amount)"
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if sort == "year"{
            viewHeaderView.showMonthYearLabel.text =  year
            viewHeaderView.labelDef.text = "Total Donations for the Year"
        } else {
            viewHeaderView.showMonthYearLabel.text =  month
            viewHeaderView.labelDef.text = "Total Donations for the Month"
        }
        viewHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 300)
        
        
        
        //viewHeaderView.totalAmountDonation.text = "$" + "\(totalamount)"
        viewHeaderView.totalAmountDonation.text = "$" + "\(totalAmountStr)"
        viewHeaderView.selectMonth.addTarget(self, action: #selector(self.selectMonthAction(_:)), for: .touchUpInside)
        viewHeaderView.selectYear.addTarget(self, action: #selector(self.selectYearAction(_:)), for: .touchUpInside)
        if sort == nil || (sort?.isEmpty)! == true {
            viewHeaderView.showMonthYearLabel.text = sort
            viewHeaderView.labelDef.text = "Total Donations for the Month"
        }
        return viewHeaderView
    }
    
    @objc func GoToComment(_ sender : UIButton) {
        
        let tag = sender.tag
        let value = hbcuFavArray[tag]
        self.selectedHBCU = value.title
        self.selectedHBCUId = value.id
        if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CommentsDonationVC") as! CommentsDonationVC
            vc.hbcu = self.selectedHBCUId
            vc.hbcuTitle = self.selectedHBCU
            currentNavVC.pushViewController(vc, animated: true)
        }
    }
    
    @objc func selectMonthAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        sort = "month"
        if viewHeaderView.selectMonth.isSelected == true {
            viewHeaderView.selectYear.isSelected = false
            viewHeaderView.selectMonth.isUserInteractionEnabled = false
            viewHeaderView.selectYear.isUserInteractionEnabled = true
            viewHeaderView.showMonthYearLabel.text =  month
            viewHeaderView.labelDef.text = "Total Donations for the Month"
            self.hitMostLovedHBCUApi()
        } else {
        
        }
    }
    
    @objc func selectYearAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sort = "year"
        if viewHeaderView.selectYear.isSelected == true{
            viewHeaderView.selectMonth.isSelected = false
            viewHeaderView.selectYear.isUserInteractionEnabled = false
            viewHeaderView.selectMonth.isUserInteractionEnabled = true
            viewHeaderView.showMonthYearLabel.text =  year
            viewHeaderView.labelDef.text = "Total Donations for this Year"
            self.hitMostLovedHBCUApi()
        } else {
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 300
    }
    
    @objc func donateToSelectedUnivFunction(_ sender : UIButton){

        self.selectedValues = false
        let tag = sender.tag
        let value = hbcuFavArray[tag]

        self.selectedHBCU = value.title
        self.selectedHBCUId = value.id
//        let view = donationPlusView
//        view?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
//        view?.layer.cornerRadius = 0.2
//        self.view.addSubview(view!)
    
//        if value.donation_amount as? String != "" {
//            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "DonationViewController") as! DonationViewController
//                vc.isComingFromDivine9 = true
//                currentNavVC.pushViewController(vc, animated: true)
//            }
//        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DonateNowOrWithOtherAmountVC") as! DonateNowOrWithOtherAmountVC
        //vc.fileUrl = chatGetArray[indexPath.section].post_media ?? ""
        vc.selectedHBCUId = value.id ?? ""
        self.present(vc, animated: true, completion: nil)
        
    }
}

extension MostLovedHBCU : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        self.fiveDollar.isSelected = false
        self.twntyDollar.isSelected = false
        self.fortyDollar.isSelected = false
        self.fiftyDollar.isSelected = false
        self.selectedValues = false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        if newString.count > 0{
            if let amount = Int(newString){
                if amount != 0 {
                    self.selectedValues = true
                    self.amount = newString
                } else {
                    self.selectedValues = false
                }
            }
        }
        return true
    }
}

extension MostLovedHBCU : PKPaymentAuthorizationViewControllerDelegate{
    
    func setupApplePay(){
        
        let paymentRequest = Stripe.paymentRequest(withMerchantIdentifier: Constants.ApplePayMerchantID)
        paymentRequest?.currencyCode = "USD"
        paymentRequest?.countryCode = "US"
        
        if let amount = Int(self.amount ?? ""){
            paymentRequest?.paymentSummaryItems = [
                PKPaymentSummaryItem(label: "", amount: NSDecimalNumber(value: amount))
            ]
            
            //Payment networks array
            let paymentNetworks = [PKPaymentNetwork.amex, .visa, .masterCard, .discover]
            paymentRequest?.supportedNetworks = paymentNetworks
            paymentRequest?.merchantCapabilities = .capability3DS
        }
        
        if Stripe.canSubmitPaymentRequest(paymentRequest) {
            var paymentController: PKPaymentAuthorizationViewController?
            paymentController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest!)
            paymentController?.delegate = self
            paymentController?.modalPresentationStyle = .formSheet
            
            if let paymentController = paymentController {
                present(paymentController, animated: true, completion: nil)
            }
        } else {
            //self.showAlert("Apple Pay is not available on this device or May needs to set up the Apple Pay on the device")
            self.showAlert("You need to set up the Apple Pay on the device")
        }
        
    }
    
    //This function is called when Apple Pay payment will be authorized.
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void){
        // Payment token can be found like this
        print(payment.token)
        // Get response from the server and set the PKPaymentAuthorizationStatus
        let status = PKPaymentAuthorizationStatus(rawValue: 0)!
        switch status.rawValue {
        case 0:
            
            //Stripe.setDefaultPublishableKey("pk_test_ho0D4zAZULYWZcvNHEHFasyE")
            Stripe.setDefaultPublishableKey("pk_live_egNBOvzPd1DewC3sOGQe4H0c") //live
            
            STPAPIClient.shared().createToken(with: payment) {
                (token, error) -> Void in
                
                if (error != nil) {
                    print("%@", error!)
                    completion(PKPaymentAuthorizationStatus.failure)
                    return
                }
                
                if let token = token{
                    self.hitOneTimeAPI(token: token, VC: controller, completion: completion)
                }
                
            }
            
        default:
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
    //This function is called when Apple Pay payment authorization is finished.
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController){
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension MostLovedHBCU{
    
    //MARK:- one time charges
    func hitOneTimeAPI(token: STPToken, VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)) {
        
        let url            = "https://api.stripe.com/v1/charges"
        let description    = "One-off"
        let source         = token
        var price = 0
        if let firstPrice = self.amount{
            let newFirstPrice = Int(firstPrice) ?? 0
            price = newFirstPrice * 100
        }
        
        var dict = Dictionary<String, Any>()
        
        dict = ["amount"        : price ,
                "currency"  : "usd" ,
                "description" : description  , "source" : source ]
        
        //"sk_test_0HEIX5gE8pRO5X5xuSPrPczU"
        //"sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        
        let header = [
            "Content-Type"   : "application/x-www-form-urlencoded",
            "Authorization"  : "Bearer sk_live_QsIv63Ij0tCk2uBFMBYLDFUH"
        ]
        
        WebServiceClass.showLoader(view: self.view)
        Alamofire.request(url , method: .post, parameters: dict, encoding:  URLEncoding.httpBody, headers: header ).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                if let json = response.result.value as? [String: Any] {
                    print("JSON: \(json)") // serialized json response
                    if let id = json["id"] as? String{
                        self.subscriptionId = id
                        
                        if let txnId = json["balance_transaction"] as? String{
                            if let source = json["source"] as? [String: Any]{
                                if let cardNum = source["last4"] as? String{
                                    if let cardId = source["id"] as? String{
                                        self.saveOneTimeDonationWithApplePayDetail(card_num: cardNum, card_id: cardId, txnId: txnId, VC: VC, completion: completion)
                                    }
                                }
                            }
                        }
                        
                    }else{
                        resultFailure()
                    }
                }else{
                    resultFailure()
                }
                
            case.failure(let error):
                print("Not Success",error)
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
    func saveOneTimeDonationWithApplePayDetail(card_num: String, card_id: String, txnId: String, VC: PKPaymentAuthorizationViewController, completion: @escaping ((PKPaymentAuthorizationStatus) -> Void)){
        
        guard let userInfoModel = Methods.sharedInstance.getloginData()else{return}
        let user_id    = userInfoModel.id as? String
        let amount     = self.amount ?? ""
        let hbcu       = self.selectedHBCUId ?? ""
      
        let paramsStr = "user_id=\(user_id)&amount=\(amount)&donation_type=\(2)&hbcu=\(hbcu)&card_num=\(card_num)&card_id=\(card_id)&txnId=\(txnId)"
        print(paramsStr)
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL +  Constants.APIs.paymentsDoneWithAppleI, method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true{
                if let jsonResult = response as? Dictionary<String, AnyObject>{
                    print(jsonResult)
                    if let responseCode = jsonResult["error"] as? Bool ,responseCode{
                        //self.showAlert(jsonResult["message"] as! String)
                        resultFailure()
                    }else{
                        //                        let view = AlertView()
                        //                        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
                        //                        view.layer.cornerRadius = 0.2
                        //                        view.alertView.layer.cornerRadius = 10
                        //                        view.alertView.layer.borderColor = UIColor.black.cgColor
                        //                        self.view.addSubview(view)
                        
                        completion(PKPaymentAuthorizationStatus.success)
                        return
                    }
                }else{
                    print("Data is not in proper format")
                    resultFailure()
                }
                
            }else{
                //self.showAlert("Something is wrong. Please try again later.")
                //print("Rejected - 500")
                resultFailure()
            }
        }
        
        func resultFailure(){
            WebServiceClass.hideLoader(view: self.view)
            completion(PKPaymentAuthorizationStatus.failure)
            return
        }
        
    }
    
}
