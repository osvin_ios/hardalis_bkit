//
//  Divine9ViewController.swift
//  HBCU-FundRaising
//
//  Created by Osvin Mac Jas on 26/06/18.
//  Copyright © 2018 Osvin Mac Jas. All rights reserved.
//

import UIKit
import ObjectMapper

class Divine9ViewController: UIViewController {
    
    @IBOutlet var donationPlusView: UIView!
    @IBOutlet weak var divineTableView: UITableView!
    @IBOutlet weak var fiveDollarBtn: ButtonDesign!
    @IBOutlet weak var twentyDolarBtn: ButtonDesign!
    @IBOutlet weak var fortyDolarBtn: UILabel!
    @IBOutlet weak var fiftyDolarBtn: ButtonDesign!
    @IBOutlet weak var fortyDollarBtn: ButtonDesign!
    @IBOutlet weak var otherAmountTextField: UITextField!
    @IBOutlet weak var crossBtnOutlet: UIButton!
    @IBOutlet weak var donateNowBtnOutlet: UIButton!
    
    
    var hbcuFavArray = [HBCUOrganizationListModel]()
    var totalamount = String()
    var sort : String?
    var month : String?
    var year : String?
    var amount : String?
    var selectedHBCUId : String?
    var selectedHBCU : String?
    let viewHeaderView = MostLovedHBCUHeader()
      var selectedValues : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        divineTableView.register(UINib(nibName: "univListTable", bundle: nil), forCellReuseIdentifier: "univListTable")
        self.title = "Divine 9"
        self.barButton()
        
        if Reachability.isConnectedToNetwork() == true {
            self.hitDivineNineAPI()
        } else {
            self.showAlert(Constants.commomInternetmsz.checkInternet)
        }
        viewHeaderView.selectMonth.isSelected = true
        viewHeaderView.selectMonth.isUserInteractionEnabled = false
        sort = "month"
    }
    
    
    @IBAction func selectfivedolarActn(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if self.fiveDollarBtn.isSelected == true {
            self.twentyDolarBtn.isSelected = false
            self.fortyDollarBtn.isSelected = false
            self.fiftyDolarBtn.isSelected = false
            self.otherAmountTextField.text = ""
            self.dismissKeyboard()
        }
        self.amount = "5"
        self.selectedValues = true
    }
    
    @IBAction func selecttwentyDollarActn(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if self.twentyDolarBtn.isSelected == true {
            self.fiveDollarBtn.isSelected = false
            self.fortyDollarBtn.isSelected = false
            self.fiftyDolarBtn.isSelected = false
            self.otherAmountTextField.text = ""
            self.dismissKeyboard()
        }
        self.amount = "20"
        self.selectedValues = true
    }
    @IBAction func selectFortyDolarActn(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if self.fortyDollarBtn.isSelected == true {
            self.twentyDolarBtn.isSelected = false
            self.fiveDollarBtn.isSelected = false
            self.fiftyDolarBtn.isSelected = false
            self.otherAmountTextField.text = ""
            self.dismissKeyboard()
        }
        self.amount = "40"
        self.selectedValues = true
    }
    
    @IBAction func selecttfiftyDollarActn(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if self.fiftyDolarBtn.isSelected == true {
            self.twentyDolarBtn.isSelected = false
            self.fortyDollarBtn.isSelected = false
            self.fiveDollarBtn.isSelected = false
            self.otherAmountTextField.text = ""
            self.dismissKeyboard()
        }
        self.amount = "50"
        self.selectedValues = true
    }
    
    @IBAction func donateNowBtnActn(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if self.selectedValues == false {
            self.showAlert("Minimum donation amount is $1")
        } else {
            self.alertFunctionsWithCallBack(title: "Do you really want to donate this amount?") { (isObject) in
                
                if isObject.boolValue {
                    self.hitOneTimeDonationAPI()
                } else {print("rejected")
                    
                }
            }
        }
    }
    
    @IBAction func crossBtnActn(_ sender: UIButton) {
    }
    
    
    func barButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_drawer"), style: .plain, target: self, action: #selector(leftBar(sender:)))
    }
    @objc func leftBar(sender: UIButton){
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
            print("drawer opened")
        }
    }
    func hitDivineNineAPI(){
        
        WebServiceClass.showLoader(view: self.view)
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        let user_id        = userInfoModel.id as? String
        let paramsStr = "user_id=\(user_id ?? "")"
        let sort = self.sort ?? "month"
        
        WebServiceClass.dataTask(urlName: Constants.APIs.baseURL + "getTopDonors?sort=\(sort)" + "&" + paramsStr, method: "GET", params: "") { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult        = response as? Dictionary<String, AnyObject> {
                    if let result        = jsonResult["result"]{
                        self.month       = result["month"] as? String
                        self.year        = result["year"] as? String
                        
                        if let donations = result["donations"] as? [Dictionary<String, AnyObject>]{
                            if result["total"] as? String == nil{
                                self.totalamount = "$0"
                            }else{
                                self.totalamount = result["total"] as? String ?? ""
                            }
                            if let favHBCU = Mapper<HBCUOrganizationListModel>().mapArray(JSONObject: donations){
                                self.hbcuFavArray = favHBCU
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.divineTableView.reloadData()
                }
            } else {
                print("Data is not in proper format")
            }
        }
    }
}

extension Divine9ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hbcuFavArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : univListTable = tableView.dequeueReusableCell(withIdentifier: "univListTable", for: indexPath) as! univListTable
        let value = hbcuFavArray[indexPath.row]
        let id = value.id
        
        if id == nil || id?.isEmpty == true {
            cell.cellPlusButtonHeight.constant = 0
        } else {
            //cell.cellPlusButtonHeight.constant = 20
            cell.cellPlusButtonHeight.constant = 30
        }
        cell.selectionStyle = .none
        cell.univName.text = value.title
        if indexPath.row == 0  {
            cell.univImages.image = #imageLiteral(resourceName: "ic_ranked_first")
            if let amount = value.donation_amount as? String{
                cell.univDonationAmount.text = "$" + amount
            } else {
            }
        } else if indexPath.row == 1 {
            cell.univImages.image = #imageLiteral(resourceName: "ic_ranked_second")
            if let amount = value.donation_amount as? String{
                cell.univDonationAmount.text = "$" + amount
            } else {
            }
        } else if indexPath.row == 2 {
            cell.univImages.image = #imageLiteral(resourceName: "ic_ranked_third")
            if let amount = value.donation_amount as? String {
                cell.univDonationAmount.text = "$" + amount
            } else {
            }
        } else {
            let image = Constants.APIs.imageBaseURL + value.logo!
            cell.univImages.sd_setImage(with: URL(string: image ), placeholderImage: nil)
            if let amount = value.donation_amount as? String {
                cell.univDonationAmount.text = "$" + amount
            } else {
            }
        }
        cell.donateButton.addTarget(self, action: #selector(donateButtonaction), for: .touchUpInside)
        cell.donateButton.tag = indexPath.row
        return cell
    }
    
    @objc func donateButtonaction(_ sender: UIButton) {
        let tag = sender.tag
        let value = hbcuFavArray[tag]

//        let view = donationPlusView
//        view?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
//        view?.layer.cornerRadius = 0.2
//        self.view.addSubview(view!)
        if value.donation_amount as? String != "" {
            if let currentNavVC = CallBackMethods.appDelegate?.getCurrentTopVC(topVCObject: 1) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "DonationViewController") as! DonationViewController
                vc.isComingFromDivine9 = true
                currentNavVC.pushViewController(vc, animated: true)
            }
        }
    }
    
    func hitOneTimeDonationAPI() {
        guard let userInfoModel = Methods.sharedInstance.getloginData() else {
            return
        }
        
        let user_id    = userInfoModel.id as? String
        let amount     = self.amount ?? ""
        let card_num   = "0"
        let comment    = "vc"
        let hbcu       = self.selectedHBCUId ?? ""
        
        //let paramsStr = "user_id=\(user_id ?? "")&amount=\(amount)&card_num=\(card_num)&comment=\(comment)&hbcu=\(hbcu)"
        let paramsStr = "user_id=\(user_id ?? "")&amount=\(amount)&card_id=\(card_num)&hbcu=\(hbcu)"
        print(paramsStr)
        
        WebServiceClass.showLoader(view: self.view)
        
        WebServiceClass.dataTask(urlName:  Constants.APIs.baseURL + "oneTimeDonation", method: "POST", params: paramsStr) { (success, response, errorMsg) in
            WebServiceClass.hideLoader(view: self.view)
            if success == true {
                if let jsonResult = response as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    if let responeCode = jsonResult["error"] as? Bool, responeCode{
                        self.showAlert(jsonResult["message"] as! String)
                    } else {
                        let view = AlertView()
                        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: self.view.frame.size.height)
                        view.layer.cornerRadius = 0.2
                        self.view.addSubview(view)
                    }
                } else {
                    print("Data is not in proper format")
                }
                DispatchQueue.main.async {
                    self.fiftyDolarBtn.isSelected = false
                    self.fiveDollarBtn.isSelected  = false
                    self.twentyDolarBtn.isSelected = false
                    self.fortyDollarBtn.isSelected = false
                    self.otherAmountTextField.text = ""
                    self.hitDivineNineAPI()
                    self.donationPlusView.removeFromSuperview()
                }}else {
                //self.showAlert("Something went wrong, try again after some time")
                self.showAlert("Something is wrong. Please try again later.")
                print("Error")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if sort == "year"{
                viewHeaderView.showMonthYearLabel.text =  year
            viewHeaderView.labelDef.text = "Total Donations for the Year"
            } else {
                viewHeaderView.showMonthYearLabel.text =  month
            viewHeaderView.labelDef.text = "Total Donations for the Month"
            }
            viewHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 300)
            viewHeaderView.totalAmountDonation.text = "$" + totalamount
            viewHeaderView.selectMonth.addTarget(self, action: #selector(self.selectMonthAction(_:)), for: .touchUpInside)
            viewHeaderView.selectYear.addTarget(self, action: #selector(self.selectYearAction(_:)), for: .touchUpInside)
            if sort == nil || (sort?.isEmpty)! == true {
                viewHeaderView.showMonthYearLabel.text = sort
                viewHeaderView.labelDef.text = "Total Donations for the Month"
            }
        return viewHeaderView
    }
    
    @objc func selectMonthAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sort = "month"
        if viewHeaderView.selectMonth.isSelected == true {
            viewHeaderView.selectYear.isSelected = false
            viewHeaderView.selectMonth.isUserInteractionEnabled = false
            viewHeaderView.selectYear.isUserInteractionEnabled = true
            viewHeaderView.showMonthYearLabel.text =  month
            viewHeaderView.labelDef.text = "Total Donations for the Month"
            self.hitDivineNineAPI()
        } else {
            
        }
    }
    
    @objc func selectYearAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sort = "year"
        if viewHeaderView.selectYear.isSelected == true{
            viewHeaderView.selectMonth.isSelected = false
            viewHeaderView.selectMonth.isUserInteractionEnabled = true
            viewHeaderView.selectYear.isUserInteractionEnabled = false
            viewHeaderView.showMonthYearLabel.text =  year
            viewHeaderView.labelDef.text = "Total Donations for this Year"
            self.hitDivineNineAPI()
        } else {
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 300
    }
}
