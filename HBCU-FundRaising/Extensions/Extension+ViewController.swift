//
//  Extension+ViewController.swift
//  PLUME
//
//  Created by osvinuser on 11/21/17.
//  Copyright © 2017 osvinuser. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    func getDateFormatterFromServer(stringDate: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: stringDate)
        return date
    }
    
    internal func alertFunctionsWithCallBack(title : String, isThirdButton: Bool = false, completionHandler:@escaping CallBackMethods.SourceCompletionHandler) {
        
        let alertController = UIAlertController(title: title, message: "", preferredStyle:UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default)
        { action -> Void in
            // Put your code here
            completionHandler(true as AnyObject)
        })
        alertController.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default)
        { action -> Void in
            // Put your code here
            completionHandler(false as AnyObject)
        })
        DispatchQueue.main.async {
               self.present(alertController, animated: true, completion: nil) // present the alert
        }
    }
    
    internal func alertFunctionsWithCallBacks(title : String, isThirdButton: Bool = false, completionHandler:@escaping CallBackMethods.SourceCompletionHandler) {
        
        let alertController = UIAlertController(title: title, message: "", preferredStyle:UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default)
        { action -> Void in
            // Put your code here
            completionHandler(true as AnyObject)
        })
        alertController.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default)
        { action -> Void in
            // Put your code here
            completionHandler(false as AnyObject)
        })
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil) // present the alert
        }
    }

    internal func alertFunctionsWithSingleCallBack(title : String, completionHandler:@escaping CallBackMethods.SourceCompletionHandler) {
        let alertController = UIAlertController(title: title, message: title, preferredStyle:UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default)
        { action -> Void in
            // Put your code here
            completionHandler(true as AnyObject)
        })
        DispatchQueue.main.async {
               self.present(alertController, animated: true, completion: nil) // present the alert
        }
    }
    
    internal func alertFunctionsWithSingleCallBack2(title : String, completionHandler:@escaping CallBackMethods.SourceCompletionHandler) {
        let alertController = UIAlertController(title: title, message: title, preferredStyle:UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        { action -> Void in
            // Put your code here
            completionHandler(true as AnyObject)
        })
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil) // present the alert
        }
    }
    
    //MARK: - Alert Methods
    internal func showAlertView(title OfAlert:String, message OfBody:String) {
        let alertController = UIAlertController(title: OfAlert, message: OfBody, preferredStyle: .alert)
        let someAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(someAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //toast
    internal func showToast(message : String){
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: self.view.frame.size.width - 100, height: 35))
        toastLabel.center = self.view.center
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        //toastLabel.font = constantsNaming.fontType.kOpenSans_RegularMedium
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    internal func setGradientColorToView(view : AnyObject, hexString1:String, hexString2:String) {
        
        /* Step 1 set the colors which you want to show in the view */
        //        let colorTop = UIColor().hexStringToUIColor(hex: hexString1)
        //        let colorBottom = UIColor().hexStringToUIColor(hex: hexString2)
        
        /* Step 2 create the gradient layer, add the colors and set the frame */
        let gradient: CAGradientLayer = CAGradientLayer()
        //      gradient.colors = [colorTop.cgColor, colorBottom.cgColor]
        gradient.locations = [0.0, 0.7]
        
        gradient.frame = view.bounds
        view.layer.insertSublayer(gradient, at: 0)
    }
    
    //MARK: - Convert Date to String
    internal func convertDateToString(instance OfDate:Date, instanceOf DateFormat:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")!
        return dateFormatter.string(from:OfDate)
    }
    
    //MARK: - Convert String to Date
    internal func convertStringToDate(dateofString:String,DateFormat:String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let date = dateFormatter.date(from: dateofString)// create   date from string
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let timeStamp =  dateFormatter.date(from: dateofString)
        return  timeStamp ?? Date()
    }
    
    internal func convertStringDateToLocal(dateofString:String/*,DateFormat:String*/) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")!
        let date = dateFormatter.date(from: dateofString)
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date ?? Date())
        
        return timeStamp
    }
    
    
    // for new datecheck
    
    internal func convertStringDateToLocalDateType(dateofString:String/*,DateFormat:String*/) -> Date? {
        
        let dateFormatter = DateFormatter()
        let dateFormatter2 = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")!
        let date = dateFormatter.date(from: dateofString ?? "")
        
        // change to a readable time format and change to local time zone
        dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter2.timeZone = NSTimeZone.local
        //        let timeStamp = dateFormatter2.string(from: date ?? Date())
        let dat = dateFormatter2.date(from: dateofString)
        return dat 
    }
    
    internal func convertDateToUTC(datestring:String) -> String? {
        
        let dateForm = DateFormatter()
        dateForm.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateForm.timeZone = TimeZone(abbreviation: "GMT")
        dateForm.amSymbol = "AM"
        dateForm.pmSymbol = "PM"
        
        let date = dateForm.date(from: datestring)
        dateForm.timeZone = TimeZone(abbreviation: "IST")
        dateForm.dateFormat = "h:mm a"
        let date1 = dateForm.string(from: date!)
        return date1
    }
    
    internal func convertStingToTime(timeString : String) -> String?{
        
        let dateForm = DateFormatter()
        dateForm.dateFormat = "mm, ss"

        print(TimeZone.current.abbreviation()!)
        
        let date = dateForm.date(from: timeString)
        dateForm.dateFormat = "mm:ss"
        let date1 = dateForm.string(from: date!)
        return date1
    }
    
    
    internal func convertTimeFormater(_ date: String) -> String?{
        
        if date.isEmpty == false {
            let myDateString = date
            let dateFormatter = DateFormatter()
         
            dateFormatter.amSymbol = "AM"
            dateFormatter.pmSymbol = "PM"
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
           
            let myDate = dateFormatter.date(from: myDateString)!
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "h:mm a"
            dateFormatter.timeZone = TimeZone.current
            let somedateString = dateFormatter.string(from: myDate as Date)
           
            return somedateString
        }
        return ""
    }
    
    
    internal func serverToLocal(_ date:String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "h:mm a"
        
        return dateFormatter.string(from: dt!)
    }
    
    
    internal func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) yrs ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 yr ago"
            } else {
                return "Last yr"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1) {
            
            if (numericDates) {
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1) {
            
            if (numericDates) {
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hrs ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hr ago"
            } else {
                return "An hr ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) mins ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) secs ago"
        } else {
            return "Just now"
        }
    }
    
    
    func convertstringToDate(stringDate: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")!
        let date = dateFormatter.date(from: stringDate)
        
        if let dateObject = date {
            return dateObject // return the date if date is not equal to null
        }
        
        return date
    }
    
    //MARK: - Keyboard Hide Method
    internal func hideKeyboardWhenTappedAround() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc internal func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showAlert(_ message: String){
        
        let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion:nil)
    }
    
    func roundedof(toPlaces places:Double) -> Double {
        let numberOfPlaces = 2.0
        let multiplier = pow(10.0, numberOfPlaces)
        let rounded1 = round(places * multiplier) / multiplier
        return rounded1
    }
    
    func showAlertWithoutCallBack(messageTitle : String ) {
        
        let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: messageTitle, preferredStyle:UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        { action -> Void in
            // Put your code here
        })
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil) // present the alert
        }
    }
    
    func showAlertWithActions(messageTitle : String, isPop: Bool = false) {
        
        let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: messageTitle, preferredStyle:UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        { action -> Void in
            // Put your code here
            
            if isPop {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        })
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil) // present the alert
        }
    }
  
    func showAlertWithActions(_ message:String) {
        
        let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        { action -> Void in
            _ = self.navigationController?.popViewController(animated: true)
        })
        present(alertController, animated: true, completion:nil)
    }
    
    func showAlertWithTwoActions(_message:String) {
        
        let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: _message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
        { action -> Void in
            _ = self.navigationController?.popViewController(animated: true)
        })
        present(alertController, animated: true, completion:nil)
    }
    
    func showAlertForDeleteRecording(_message:String) {
        
        let alertController = UIAlertController(title: Constants.appTitle.alertTitle, message: _message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
        { action -> Void in
            
            //PrepareToWinViewController.sharedInstance
        })
        present(alertController, animated: true, completion:nil)
    }
    
    internal func UTCToLocal(date:String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "h:mm a"
        
        return dateFormatter.string(from: dt ?? Date())
    }
    
    func UTCToLocalStringValue(date:String, incomingDateFormat: String, outgoingDateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = outgoingDateFormat
        
        return dateFormatter.string(from: dt ?? Date())
    }
    
    func getDateFormatterDate(date:String, incomingDateFormat: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        return dt
    }
}
