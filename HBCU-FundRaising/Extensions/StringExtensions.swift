//
//  File.swift
//  ConstruPlaza
//
//  Created by osvinuser on 10/20/16.
//  Copyright © 2016 osvinuser. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    /* Random String */
    func randomString(length: Int) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd-HHmmss"
        var _: String = formatter.string(from: Date())
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = self
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        randomString += randomString
        
        return randomString
    }
    
    func dateformatter() -> String {
        let todaysDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let todayString = dateFormatter.string(from: todaysDate)
        let calendar = Calendar.current
        let yesterday = calendar.date(byAdding: .day, value: -1, to: Date())
        let yesterdayString = dateFormatter.string(from: yesterday ?? Date())
//        let isoDate = self
//        let dateFormatter1 = DateFormatter()
//        dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter1.timeZone = TimeZone(abbreviation: "UTC")
//       // dateFormatter1.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
//        dateFormatter1.dateFormat = "yyyy-MM-dd"
//        let date = dateFormatter1.date(from: isoDate)!
//        let myStringafd = dateFormatter1.string(from: date)
//
//        if myStringafd == todayString {
//            return "Today"
//        } else if myStringafd == yesterdayString {
//            return "Yesterday"
//        } else {
//            return self
//        }
        if self == todayString {
            return "Today"
        } else if self == yesterdayString {
            return "Yesterday"
        } else {
            return self
        }
    }
 
    func dateFromStringForChat() -> String {
      
        // change to your date format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let date = dateFormatter.date(from: self)
        let myCalender = Calendar(identifier: .gregorian)

        let date1 = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        let dayInWeek = formatter.string(from: date1)
        
        let weekDay = myCalender.component(.weekday, from: date! )
       // return weekDay
        dateFormatter.dateFormat =  " EEEE dd MMM"
        dateFormatter.timeZone = NSTimeZone.local
        let str = dateFormatter.string(from: date ?? Date())
        return str
    }
}


